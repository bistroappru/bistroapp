<h3>Получение запросов на выполнение в R-Keeper</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/r_keeper_api/v1/getRequests<br/>
</p>

<hr/>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF6}" ServerTimeStamp="UTC_DATETIME"/>
</BA_PROXY_REQUEST>');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK">
        <REQUESTS>
            <REQUEST id="{374D1624-864A-5585-B83B-759C9C19FDBC}">
                <![CDATA[
                <RK7Query>
                    <RK7CMD CMD="GetRefData" RefName="MenuItems"/>
                </RK7Query>
                ]]>
            </REQUEST>
            <REQUEST id="{4ED8E977-C2D3-16CD-1E9A-DDF6FEAE372F}">
                <![CDATA[
                <RK7Query>
                    <RK7CMD CMD="GetRefData" RefName="Employees"/>
                </RK7Query>
                ]]>
            </REQUEST>
            <REQUEST id="{06FBD489-688D-B440-AF08-94BC5360869E}">
                <![CDATA[
                <RK7Query>
                    <RK7CMD CMD="GetRefData" RefName="CASHES"/>
                </RK7Query>
                ]]>
            </REQUEST>
        </REQUESTS>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="ERROR">
        <ERROR code="17" description="Proxy с UUID {96896BD3-1D9F-45D5-B87C-9B2E6A924AF4} не зарегистрирован"/>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>