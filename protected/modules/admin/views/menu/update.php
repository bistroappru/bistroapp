<?php
/* @var $this MenuController */
/* @var $model Menu */
$this->pageTitle = Yii::app()->name . ' - Редактирование меню';
?>

<div class="column_center">
    <?php $this->widget('DishList', array('menu_id'=>$model->id)); ?>
</div>

<div class="column_right">
    <div class="product_section">
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>