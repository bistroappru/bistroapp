<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:25
 */
?>

<?php

?>

<div class="dishes_head">
    <div class="dish_box">
        <span class="dish_entry">
            <span class="dish_title"><?=$company->name;?></span>
            <span class="dish_desc"><?=AbcHelper::trimString($company->description, 80);?></span>
        </span>
        <span class="dish_icon"><img width="80" height="80" src="<?=$company->logo ? $company->logo : Yii::app()->params['emptyImage'];?>"></span>
    </div>
</div>

<div class="rest_search" id="restaurant_search">
    <form>
        <input class="inp_rest_srh" autocomplete="off" type="text" name="restaurant_search" value="Поиск" onfocus="this.value=(this.value=='Поиск')? '' : this.value ;" onblur="this.value=(this.value=='')? 'Поиск' : this.value ;">
        <input class="submit_rest_srh" type="submit" value=" ">
    </form>
</div>

<ul id="restaurant_list" class="rests_menu"></ul>

<ul class="leftmenu_buttons">
    <li><a id="restaurant_create" href="<? echo CHtml::normalizeUrl('/admin/restaurant/create/company_id/'.$company->id);?>"><img src="/images/leftmenu-5.png" width="50" height="50" alt=""><span>Добавить ресторан</span></a></li>
</ul>

<script>
    var restaurant_json = <?=CJSON::encode($restaurant_list)?>;
    var restaurant_id = <?=$restaurant_id?>;
    function searchRestaurants(){
        var search = $('#restaurant_search input[name=restaurant_search]').val();
        if (!search || search == 'Поиск') search = '';
        var ul_html = '';
        if(restaurant_json['ids']){
            $.each(restaurant_json['ids'], function(k, v){
                pos = restaurant_json['list'][v].name.toLowerCase().indexOf(search.toLowerCase());
                if (pos != -1){
                    ul_html += '<li class="'+(restaurant_id == restaurant_json['list'][v].id ? 'active' : '')+' '
                        +(restaurant_json['list'][v].deleted == 1 ? 'style_del' : '')+' '
                        +(restaurant_json['list'][v].hidden == 1 ? 'style_hid' : '')+'"> \
                    <a href="/admin/restaurant/update/id/'+restaurant_json['list'][v].id+'"> \
                        <span class="rest_title">'+restaurant_json['list'][v].name+'</span> \
                        <span>'+restaurant_json['list'][v].description+'</span> \
                    </a> \
                </li>';
                }
            });
        }
        $('#restaurant_list').html(ul_html);
    }
    searchRestaurants();

    $('#restaurant_search input[name=restaurant_search]').on('keyup', function(){
        searchRestaurants();
    });

    $('#restaurant_search form').on('submit', function(e){
        e.preventDefault();
        searchRestaurants();
    });
</script>