<div class="food_detail <?=$model->id ? 'opened' : ''?>" style="display: <?=$model->id ? 'block' : 'none'?>;">
    <div class="detail_meta">
        <div class="food_buttons_left">
            <!--
            <a href="#" class="food_plus"></a>
            <a href="#" class="food_minus"></a>
            -->
            <a href="javascript:void(0);" class="food_del"></a>
        </div>
        <a href="#" class="detail_arrow">Детально</a>
    </div>
    <div class="food_table_wrap" style="display:<?=$model->id ? 'block' : 'none'?>;">
        <div data-id="<?php echo CHtml::encode($model->id); ?>">
            <div class="resizable_block_detail">
                <div class="resizable_inner_detail">
                    <table class="food_table">
                        <?php
                        foreach ($model->items as $one) {
                            echo $this->renderPartial('_orderitem', array('one' => $one));
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="detail_meta_link">
                <a href="/admin/orders/view/id/<?= $model->id; ?>">Расширенный просмотр заказа &raquo;</a>
            </div>
        </div>
    </div>
</div>