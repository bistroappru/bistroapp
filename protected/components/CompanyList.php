<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class CompanyList extends CWidget {
    public $company_id = 0;
    public $fields = array('id','name','logo','hidden','deleted');
    public function run(){
        $criteria = new CDbCriteria();
        $criteria->select = $this->fields;
        $criteria->order = 'name ASC';
        if ($cid = Yii::app()->user->company_id) $criteria->compare('id', $cid);

        $list = array();
        $tmp = Company::model()->findAll($criteria);
        foreach($tmp as $k=>$v){
            $image_url = $_SERVER['DOCUMENT_ROOT'].$v->logo;
            if(is_file($image_url)) {
                $image_info = pathinfo($image_url);
                if(!file_exists($_SERVER['DOCUMENT_ROOT'].'data/images/logos/thumbnail/'.$image_info['filename'] . '_small.'.$image_info['extension'])) {
                    $image = Yii::app()->image->load($_SERVER['DOCUMENT_ROOT'].$v->logo);
                    $image->resize(40, 41)->quality(100);
                    $small_image = 'data/images/logos/thumbnail/'.$image_info['filename'] . '_small.'.$image_info['extension'];
                    if($image->save($small_image)) {
                        $v->logo = $small_image;
                    }
                }
            }
            $list['ids'][] = $v->id;
            $list['list'][$v->id] = $v->getAttributes($this->fields);
            $list['list'][$v->id]['restaurants'] = count($v->restaurants);
        }

        $this->render('_company_list',array(
            'company_list' => $list,
            'company_id' => $this->company_id
        ));
    }
}