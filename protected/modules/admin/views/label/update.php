<?php
/* @var $this LabelController */
/* @var $model Label */
$this->pageTitle = Yii::app()->name . ' - Редактирование спецвывода';
$this->menu=array(
	array('label'=>'Список спецвыводов', 'url'=>array('index')),
	array('label'=>'Создать спецвывод', 'url'=>array('create')),
);
?>

<h1>Редактирование спецвывода "<?php echo $model->text;?>"</h1>
<br/>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>