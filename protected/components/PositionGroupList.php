<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class PositionGroupList extends CWidget{
    public $dish_id = 0;
    public $menu;
    public $group;
    public function run(){
        if (!$this->group) return;
        if ($this->menu instanceof Menu){
            $company = $this->menu->company;
            $list = PositionGroup::model()->findAllByAttributes(array('company_id' => $company->id));

            $this->render('_position_group_list', array(
                'company' => $company,
                'list' => $list,
                'dish_id' => $this->dish_id,
            ));
        }else{
            throw new CHttpException(500, 'Произошла ошибка получения групп блюд');

        }
    }
}