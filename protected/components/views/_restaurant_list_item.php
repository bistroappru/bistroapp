<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 18.07.14
 * Time: 13:08
 */
?>
<li <?= $restaurant_id == $data->id ? 'class="active"' : ''?>>
    <a href="<?=CHtml::normalizeUrl('/admin/restaurant/update/id/'.$data->id);?>">
        <span class="rest_title"><?=$data->name?></span>
        <span><?=$data->description?></span>
    </a>
</li>