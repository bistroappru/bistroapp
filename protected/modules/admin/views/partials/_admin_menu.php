<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 8:42
 */
?>
<?php $this->widget('zii.widgets.CMenu', array(
    'activeCssClass' => 'active',
    'encodeLabel' => false,
    'htmlOptions'=>array(
        'class' => 'table_menu_top'
    ),
    'items' => array(
        array('label' => '<span class="menu_icon"></span>Панель управления', 'itemOptions' => array('class'=>'menu_panel'), 'url' => array('/admin'), 'active' => Yii::app()->controller->id == 'default' ? true : false),
        array('label' => '<span class="menu_icon"></span>Компании', 'itemOptions' => array('class'=>'menu_restaurants'), 'url' => array('/admin/company'), 'visible' => Yii::app()->user->checkAccess('companyView', array('company_id' => Yii::app()->user->company_id)), 'active' => in_array(Yii::app()->controller->id, array('company','restaurant')) ? true : false),
        array('label' => '<span class="menu_icon"></span>Пользователи', 'itemOptions' => array('class'=>'menu_users'), 'url' => array('/admin/user'), 'visible' => Yii::app()->user->checkAccess('userView', array('user' => Yii::app()->user)), 'active' => Yii::app()->controller->id == 'user' ? true : false),
        array('label' => '<span class="menu_icon"></span>Меню', 'itemOptions' => array('class'=>'menu_menus'), 'url' => array('/admin/menu'), 'visible' => Yii::app()->user->checkAccess('menuCategoryView', array('company_id' => Yii::app()->user->company_id)), 'active' => in_array(Yii::app()->controller->id, array('menu','position')) ? true : false),
        array('label' => '<span class="menu_icon"></span>Заказы', 'itemOptions' => array('class'=>'menu_orders'), 'url' => array('/admin/orders'), 'visible' => Yii::app()->user->checkAccess('inAdmin'), 'active' => Yii::app()->controller->id == 'orders' ? true : false),
        array('label' => '<span class="menu_icon"></span>Перейти на сайт', 'url' => array('/site'), 'itemOptions' => array('class'=>'menu_site'), 'linkOptions' => array('target'=>'_blank')),
    ),
));?>
