<?php
/**
 * Created by PhpStorm.
 * User: birlaver
 * Date: 07.09.14
 * Time: 11:18
 */
?>
<div class="dish_list_wrap">
    <div class="dishes_head">
        <div class="dish_box">
        <span class="dish_entry">
            <span class="dish_title"><?= $category->name; ?></span>
            <span class="dish_desc"><?= AbcHelper::trimString($category->description, 80); ?></span>
        </span>
            <span class="dish_icon"><img
                    src="<?= $category->image ? $category->image : Yii::app()->params['emptyImage']; ?>" width="80"
                    height="80"></span>
        </div>
    </div>

    <div class="rest_search" id="dish_choose">
        <form>
            <div class="input_row">
                <select name="dish_choose">
                    <?php
                    foreach ($categories as $v) {
                        ?>
                        <option <?= ($v->id == $category->id ? 'selected="selected"' : '') ?>
                            value="<?= $v->id ?>"><?= $v->name ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </form>
    </div>

    <div class="dishes_section">
        <div class="scroll-pane">
            <ul class="dishes_list2" id="dishes_group_list">2</ul>
        </div>
    </div>
    <br/>
    <ul class="leftmenu_buttons">
        <li><?php echo CHtml::link('<img src="/images/leftmenu-3.png" width="50" height="50" alt=""><span>Добавить бизнес-ланч</span>', array('/admin/position/create/menu_id/' . Menu::getBusinessLunchRoot($category->company_id)), array('id' => 'position_create_group')); ?></li>
    </ul>

    <script>
        var dishes_json = <?= CJSON::encode($dishes);?>;
        var dishes_id = <?=$dish_id;?>;

        function generateDishesList() {
            var search = $('#dish_search input[name=dish_search]').val();
            if (!search || search == 'Поиск') search = '';
            var ul_html = '';
            if (dishes_json['ids'] && dishes_json['ids'].length) {
                $.each(dishes_json['ids'], function (k, v) {
                    pos = dishes_json['list'][v].name.toLowerCase().indexOf(search.toLowerCase());
                    if (pos != -1) {
                        ul_html += '<li>\
                        <input type="checkbox" id="dishes_' + v + '" data-id="' + v + '">\
                        <label for="dishes_' + v + '">\
                        <div class="dish_box">\
                        <span class="dish_entry">\
                        <span class="dish_title">' + dishes_json['list'][v].name + '</span>';

                        if (dishes_json['list'][v]['conditions']) {
                            $.each(dishes_json['list'][v]['conditions'], function (k1, v1) {
                                ul_html += '<a href="#" cond-id="' + v1.id + '" class="dish_desc">' + v1.measure + ' <span class="black">(' + v1.price + ' р.)</span></a>';
                            });
                        }

                        ul_html += '</span>\
                        <span class="dish_icon"><img src="' + (dishes_json['list'][v].image ? dishes_json['list'][v].image : '<?=Yii::app()->params['emptyImage'];?>') + '" width="50" height="50"></span>\
                    </div>\
                    </label>\
                    </li>';
                    }
                });
            }
            $('#dishes_group_list').replaceWith('<ul class="dishes_list2" id="dishes_group_list">' + ul_html + '</ul>');
        }
        generateDishesList();
    </script>
</div>
