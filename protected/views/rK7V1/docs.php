<?php
$this->beginClip('sidebar');
?>
<li><a href="#registrationProxy">registrationProxy</a></li>
<li><a href="#postNeedUpdate">postNeedUpdate</a></li>
<li><a href="#getRequests">getRequests</a></li>
<li><a href="#postChanges">postChanges</a></li>
<?php
$this->endClip();
?>
<!-- registrationProxy -->
<div class="section">
    <h2 id="registrationProxy">registrationProxy</h2>
    <div class="method">
        <div class="title">Регистрация прокси сервера в системе:</div>
        <div class="title_1">Метод HTTP-запроса: <b>POST</b></div>
        <div class="title_1">Ссылка HTTP-запроса:</div>
        <blockquote>
            <pre>
http://bistroapp.ru/r_keeper_api/v1/registrationProxy
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML запроса:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF}" type="REF" name="REF-сервер Тануки"/>
    <BISTRO_APP companyID="1"/>
</BA_PROXY_REQUEST>
                ');?>
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML ответа:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="ERROR">
            <ERROR code="14" description="Прокси сервер со значением {96896BD3-1D9F-45D5-B87C-9B2E6A924AF} уже существует"/>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>
                ');?>
            </pre>
        </blockquote>
        <br/>
    </div>
</div>


<!-- postNeedUpdate -->
<div class="section">
    <h2 id="postNeedUpdate">postNeedUpdate</h2>
    <div class="method">
        <div class="title">Регистрация прокси сервера в системе:</div>
        <div class="title_1">Метод HTTP-запроса: <b>POST</b></div>
        <div class="title_1">Ссылка HTTP-запроса:</div>
        <blockquote>
            <pre>
http://bistroapp.ru/r_keeper_api/v1/postNeedUpdate
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML запроса:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF9}"/>
    <R_KEEPER RefName="MenuItems"/>
</BA_PROXY_REQUEST>
                ');?>
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML ответа:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK"></BISTRO_APP>
</BISTRO_APP_RESPONSE>
                ');?>
            </pre>
        </blockquote>
        <br/>
    </div>
</div>




<!-- getRequests -->
<div class="section">
    <h2 id="getRequests">getRequests</h2>
    <div class="method">
        <div class="title">Регистрация прокси сервера в системе:</div>
        <div class="title_1">Метод HTTP-запроса: <b>POST</b></div>
        <div class="title_1">Ссылка HTTP-запроса:</div>
        <blockquote>
            <pre>
http://bistroapp.ru/r_keeper_api/v1/getRequests
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML запроса:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF9}"/>
</BA_PROXY_REQUEST>
                ');?>
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML ответа:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK">
        <REQUESTS>
            <REQUEST id="{6E728677-4D89-D82F-3026-141174750EE0}">
                <![CDATA[
                    <RK7Query>
                        <RK7CMD CMD="GetRefData" RefName="MenuItems"/>
                    </RK7Query>
                ]]>
            </REQUEST>
        </REQUESTS>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>
                ');?>
            </pre>
        </blockquote>
        <br/>
    </div>
</div>



<!-- postChanges -->
<div class="section">
    <h2 id="postChanges">postChanges</h2>
    <div class="method">
        <div class="title">Регистрация прокси сервера в системе:</div>
        <div class="title_1">Метод HTTP-запроса: <b>POST</b></div>
        <div class="title_1">Ссылка HTTP-запроса:</div>
        <blockquote>
            <pre>
http://bistroapp.ru/r_keeper_api/v1/postChanges
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML запроса:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF9}"/>
    <BISTRO_APP requestID="{134C2757-E79F-23B6-BF99-88B935C94A30}"/>
    <R_KEEPER>
        <RESPONSE>
            <![CDATA[
                <RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="GetRefList" ErrorText="" DateTime="2014-11-13T16:21:15" WorkTime="0" Processed="1">
                    <RK7RefList Count="119">
                    <RK7Reference RefName="BONUSTYPES" Count="1" DataVersion="2"/>
                    <RK7Reference RefName="BRIGADES" Count="0" DataVersion="1"/>
                    </RK7RefList>
                </RK7QueryResult>
            ]]>
        </RESPONSE>
    </R_KEEPER>
</BA_PROXY_REQUEST>
                ');?>
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">XML ответа:</div>
        <blockquote>
            <pre>
                <?=CHtml::encode('
<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK"></BISTRO_APP>
</BISTRO_APP_RESPONSE>
                ');?>
            </pre>
        </blockquote>
        <br/>
    </div>
</div>