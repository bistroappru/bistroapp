<?php

return array(
    // ROLES LIST
    User::ROLE_GUEST => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_GUEST),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_MOBILE_GUEST => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_MOBILE_GUEST),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_MOBILE => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_MOBILE),
        'children' => array(
            User::ROLE_MOBILE_GUEST,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_COOK => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_COOK),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_BARMAN => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_BARMAN),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_GARCON => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_GARCON),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_MANAGER => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_MANAGER),
        'children' => array(
            User::ROLE_COOK,
            User::ROLE_BARMAN,
            User::ROLE_GARCON,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_OWNER => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_OWNER),
        'children' => array(
            User::ROLE_MANAGER,
        ),
        'bizRule' => null,
        'data' => null
    ),
    User::ROLE_ADMIN => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => User::getRoles(User::ROLE_ADMIN),
        'children' => array(
            User::ROLE_OWNER,
        ),
        'bizRule' => null,
        'data' => null
    )
);