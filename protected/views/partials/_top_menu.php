<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:00
 */
?>
<nav id="nav_block">
<?php $this->widget('zii.widgets.CMenu', array(
    'htmlOptions'=>array(
        'class'=>'main_menu'
    ),
    'items' => array(
        array('label' => 'Главная', 'url' => array('/site/index')),
        array('label' => 'О проекте', 'url' => array('/site/page', 'view' => 'about')),
        array('label' => 'Контакты', 'url' => array('/site/contact')),
        array('label' => 'Вход в систему', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest,'itemOptions'=>array('class'=>'login-link')),
        array('label' => 'Личный Кабинет ('.Yii::app()->user->name.')', 'url' => array('/admin'), 'visible' => !Yii::app()->user->isGuest,'itemOptions'=>array('class'=>'login-link'))
    ),
));?>
</nav>