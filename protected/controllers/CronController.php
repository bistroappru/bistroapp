<?php

class CronController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionResetOrders(){
		$criteria = new CDbCriteria();
		$criteria->order = 'id ASC';
		$criteria->limit = 20;
		$criteria->addCondition('status_id = ' . Order::STATUS_CREATE);
		$criteria->addCondition('date_create < ' . (time()-60*2));

		$orders = Order::model()->findAll($criteria);
		foreach ($orders as $order){
			$order->status_id = Order::STATUS_CANCELED;
			if ($order->save()){
				echo 'cancel order id=' . $order->id . '<br>';
				$order->sendPush('cancel');
			}
		}
	}

	public function actionResetCAOrders(){
		$rest_id = Yii::app()->request->getParam('rest_id', 0);
		$criteria = new CDbCriteria();
		$criteria->order = 'id ASC';
		$criteria->limit = 20;
		$criteria->addCondition('status_id = ' . Order::STATUS_CREATE);
		$criteria->addCondition('status_id = ' . Order::STATUS_ADOPTED, 'OR');
		$criteria->addCondition('restaurant_id = '.$rest_id);
		$orders = Order::model()->findAll($criteria);
		foreach ($orders as $order){
			$order->status_id = Order::STATUS_CANCELED;
			if ($order->save()){
				echo 'cancel order id=' . $order->id . '<br>';
				$order->sendPush('cancel');
			}
		}
	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}