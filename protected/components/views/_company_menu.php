<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:25
 */
?>
<div id="menu_categories">
    <ul class="left_menu">
        <li>
            <a style="cursor: default;" href="javascript:void(0);">
            <?php
            Yii::app()->controller->renderPartial('/partials/_lc_filter', array('key' => $filter_key, 'company_id' => $company_id, 'menu_id' => $menu_id));
            ?>
            </a>
        </li>
        <li class="opened">
            <ul class="submenu" id="menu_sortable">
                <?php
                foreach ($menu as $one) {
                    ?>
                    <li data-id="<?php echo $one->id; ?>"
                        class="category_item <?php echo $one->deleted ? 'style_del' : ''; ?> <?php echo $one->hidden ? 'style_hid' : ''; ?> <?php echo $menu_id == $one->id ? 'active' : ''; ?>">
                        <a href="/admin/menu/update/id/<?php echo $one->id; ?>">
                            <span class="menu_ico"><img width="40" height="41"
                                                        src="<?php echo $one->image ? $one->image : Yii::app()->params['emptyImage']; ?>"></span>
                            <?php echo CHtml::encode($one->name); ?>
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li><a style="cursor: default;" href="javascript:void(0);">&nbsp;</a></li>
    </ul>
    <ul class="leftmenu_buttons">
        <li><?php echo CHtml::link('<img src="/images/leftmenu-3.png" width="50" height="50" alt=""><span>Добавить категорию</span>', array('/admin/menu/create/parent_id/0'), array('id' => 'menu_create')); ?></li>
    </ul>
</div>