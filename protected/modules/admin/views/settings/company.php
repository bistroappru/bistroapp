<?php
/* @var $this SettingsController */


?>
<div class="opacity_block">
    <h2>НАСТРОЙКИ КОМПАНИИ &laquo;<?= $model->company->name?>&raquo;</h2>
    <br/>
    <?php
        if (!Yii::app()->user->company_id){
            $this->renderPartial('_company_select', array('company_id' => $model->company_id));
        }
    ?>
    <br/>
    <?php
        $this->renderPartial('_company_form', array('model' => $model));
    ?>
</div>
