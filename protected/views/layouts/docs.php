<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/docs.css"/>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.0.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/google-code-prettify/run_prettify.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/google-code-prettify/prettify.css"/>
        <title><?=Yii::app()->name;?> - Документация</title>
    </head>
    <body>
        <div id="layout" class="pure-g-r content">
            <div id="nav" class="pure-u">
                <div class="nav-inner">
                    <button class="pure-button primary-button"><?= Yii::app()->name ?> Docs</button>
                    <div class="pure-menu pure-menu-open">
                        <ul>
                            <li class="pure-menu-heading">API-документация</li>
                            <li <?= $this->action->id == 'api_mobile' ? 'class="active"' : ''; ?>><a href="<?= $this->createUrl('/docs/api_mobile') ?>">Mobile Apps</a></li>
                            <li <?= $this->action->id == 'api_mobile_v2' ? 'class="active"' : ''; ?>><a href="<?= $this->createUrl('/docs/api_mobile_v2') ?>">Mobile Apps v2</a></li>
                            <li <?= $this->action->id == 'api_rkeeper6' ? 'class="active"' : ''; ?>><a href="<?= $this->createUrl('/docs/api_rkeeper6') ?>">R-Keeper 6</a></li>
                            <li <?= $this->action->id == 'api_rkeeper7' ? 'class="active"' : ''; ?>><a href="<?= $this->createUrl('/docs/api_rkeeper7') ?>">R-Keeper 7</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="list" class="pure-u-1">
                <?= $this->renderClip('action_menu') ?>
            </div>
            <div class="pure-u-1" id="main">
                <div class="email-content">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </body>
</html>