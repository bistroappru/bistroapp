<?php
/* @var $this CompanyController */
/* @var $model Company */
$this->pageTitle = Yii::app()->name . ' - Обновление компании';
?>

<div class="column_center2">
    <?php $this->widget('application.components.RestaurantList', array('company_id' => $model->id));?>
</div>

<div class="column_right">
    <?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>