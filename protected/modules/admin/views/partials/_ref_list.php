<?php
$list = array(
    'positionGroups' => 'Блюда (группы)',
    'label' => 'Блюда (метки)',
    'restaurantTable' => 'Ресторан (столы)',
    'rKeeperProxy' => 'R-Keeper (прокси-сервера)',
    'rKeeperQueue' => 'R-Keeper (задачи)',
);
asort($list);
?>

<ul class="left_menu">
    <li class="opened">
        <ul id="reference_list" class="submenu">
            <?php
            foreach($list as $k=>$v){
            ?>
                <li class="category_item <?= $this->id == $k ? 'active' : ''?>">
                    <a href="<?=CHtml::normalizeUrl('/admin/'.$k.'/');?>">
                        <span class="menu_ico"><img width="40" height="41" src="<?= Yii::app()->params['emptyImage']?>"></span>
                        <?=$v?>
                    </a>
                </li>
            <?php
            }
            ?>
        </ul>
    </li>
    <li><a href="javascript:void(0);" style="cursor: default;">&nbsp;</a></li>
</ul>