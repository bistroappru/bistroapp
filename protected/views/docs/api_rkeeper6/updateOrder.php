<h4>Пример запроса с сайта:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>

<RK6Query>
    <RK6CMD CMD="UpdateOrder">
        <Order Guid="{55260C19-F771-68D1-0D3E-F2B7B96854D8}" Comment="без лука">
            <Guests Count="1"/>
            <Table ID="12"/>
            <MenuItems>
                <MenuItem Sifr="3056" Quantity="1" Price = "250" Comment="Средней прожарки"/>
                <MenuItem Sifr="3057" Quantity="1" Price = "250" Comment=""/>
            </MenuItems>
        </Order>
    </RK6CMD>
</RK6Query>
');?>
    </code></pre>
<hr/>



<h4>Пример ответа BA_PROXY:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<RK6QueryResult ServerVersion="6" Status="Ok" CMD="UpdateOrder" Guid="{55260C19-F771-68D1-0D3E-F2B7B96854D8}" ErrorText="" DateTime="2016-02-20T14:49:07"/>
');?>
    </code></pre>
<hr/>