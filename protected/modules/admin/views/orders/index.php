<?php
/* @var $this OrdersController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle = Yii::app()->name . ' - Заказы';
?>

<script>
    params = <?= $params_json?>;
</script>
<?php
$dataProvider->getData();
$orders_count = count($dataProvider->data);
?>
<div class="order_meta">
    <?php if ($orders_count): ?>
        <div class="pages_filter">
            <span class="pf_text">Перейти к странице</span>
            <input class="pf_input" type="text" name="page_num">
            <?php
            $this->widget('CLinkPager', array(
                'header' => '',
                'firstPageLabel' => '',
                'prevPageLabel' => '',
                'nextPageLabel' => '',
                'lastPageLabel' => '',
                'pages' => $dataProvider->pagination,
                'htmlOptions' => array('class' => 'pages_list')
            ));
            ?>
        </div>

        <div class="pages_visible">
            Элементы
            <?= $dataProvider->pagination->currentPage * $dataProvider->pagination->pageSize + 1 ?>
            -
            <?= ($dataProvider->pagination->pageCount == $dataProvider->pagination->currentPage + 1) ? $dataProvider->pagination->itemCount : ($dataProvider->pagination->currentPage + 1) * $dataProvider->pagination->pageSize; ?>
            из
            <?= $dataProvider->pagination->itemCount; ?>
        </div>
    <?php endif ?>
</div>

<div class="orb_wrapper">
    <div class="orb_container">
        <div class="orb_scroller">
            <div class="table_thead">
                <table class="order_table" id="table_orders_head">
                    <thead>
                    <tr class="header_row">
                        <th data-alias="id" data-width="90">
                            <div class="th_inner"><span>ID</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="ID"></span>

                                <div class="deploy_tooltip">ID<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="table" data-width="90">
                            <div class="th_inner"><span>Стол</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Стол"></span>

                                <div class="deploy_tooltip">Стол<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="status" data-width="100">
                            <div class="th_inner"><span>Статус</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Статус"></span>

                                <div class="deploy_tooltip">Статус<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="start_date" data-width="90">
                            <div class="th_inner"><span>Время<br/>заказа</span><span
                                    class="deploy_minus double"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Время заказа"></span>
                                <div class="deploy_tooltip">Время заказа<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="payment" data-width="100">
                            <div class="th_inner"><span>Оплата</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Оплата"></span>
                                <div class="deploy_tooltip">Оплата<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="attention" data-width="50">
                            <div class="th_inner" style="font-size:0; line-height:0;">
                                <span><img src="/images/exclam_gray.png" width="22" height="22" alt=""></span>
                                <span class="deploy_minus dimage"></span>
                            </div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Внимание оффицианта"></span>
                                <div class="deploy_tooltip">
                                    <img src="/images/exclam_white.png" width="22" height="22" alt="">
                                    <span class="tooltip_corner"></span>
                                </div>
                            </div>
                        </th>
                        <th data-alias="positions" data-width="150">
                            <div class="th_inner"><span>Позиций</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Позиций"></span>

                                <div class="deploy_tooltip">Позиций<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="cs" data-width="80">
                            <div class="th_inner"><span>С/S</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="С/S"></span>

                                <div class="deploy_tooltip">С/S<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="send_date" data-width="100">
                            <div class="th_inner"><span>Отправлен</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Отправлен"></span>

                                <div class="deploy_tooltip">Отправлен<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="close_date" data-width="100">
                            <div class="th_inner"><span>Закрыт</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Закрыт"></span>

                                <div class="deploy_tooltip">Закрыт<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="garcon" data-width="120">
                            <div class="th_inner"><span>Оффициант</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Оффициант"></span>

                                <div class="deploy_tooltip">Оффициант<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                        <th data-alias="total_price" data-width="80">
                            <div class="th_inner"><span>Сумма</span><span class="deploy_minus"></span></div>
                            <div class="deploy_hidden"><span class="deploy_plus" title="Сумма"></span>

                                <div class="deploy_tooltip">Сумма<span class="tooltip_corner"></span></div>
                            </div>
                        </th>
                    </tr>
                    <?php $this->renderPartial('_manage_row', array('data_params' => $data_params, 'dataProvider' => $dataProvider)); ?>
                    </thead>
                </table>
            </div>
            <div class="resizable_block bol_wrapper">
                    <div class="bol_scroller">
                        <div class="bol_container">
                            <table class="order_table <?= !$orders_count ? 'empty' : '' ?>" id="table_orders">
                                <tbody>
                                <?php
                                if ($orders_count) {
                                    foreach ($dataProvider->data as $v) {
                                        echo $this->renderPartial('_view', array('data' => $v, 'orderid' => $orderid));
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td style="height:100px;">Ничего не найдено</td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="bol_scroller__bar-wrapper">
                        <div class="bol_scroller__bar"></div>
                    </div>
                </div>
        </div>
    </div>
    <div class="orb_scroller__bar-wrapper">
        <div class="orb_scroller__bar"></div>
    </div>
</div>

<div class="food_detail_wrap">
    <?php
    if ($orderid) {
        $this->renderPartial('orderdetail', array(
            'model' => $this->loadModel($orderid),
        ));
    }
    ?>
</div>