-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 25 2015 г., 18:54
-- Версия сервера: 5.5.42
-- Версия PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `bistroappru`
--

-- --------------------------------------------------------

--
-- Структура таблицы `app_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `app_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_auth_assignment`
--

INSERT INTO `app_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;'),
('owner', '2', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `app_auth_item`
--

CREATE TABLE IF NOT EXISTS `app_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_auth_item`
--

INSERT INTO `app_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'СуперАдмин', NULL, 'N;'),
('barman', 2, 'Бармен', NULL, 'N;'),
('companyCreate', 0, 'Создание компании', NULL, 'N;'),
('companyDelete', 0, 'Удаление компании', NULL, 'N;'),
('companyOwnTools', 1, 'Настройки своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('companyOwnUpdate', 1, 'Обновление своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('companyOwnView', 1, 'Просмотр своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('companyTools', 0, 'Настройки компании', NULL, 'N;'),
('companyUpdate', 0, 'Обновление компании', NULL, 'N;'),
('companyView', 0, 'Просмотр компании', NULL, 'N;'),
('cook', 2, 'Повар', NULL, 'N;'),
('garcon', 2, 'Оффициант', NULL, 'N;'),
('guest', 2, 'Гость', NULL, 'N;'),
('inAdmin', 0, 'Доступ к админке', NULL, 'N;'),
('manager', 2, 'Администратор ресторана', NULL, 'N;'),
('menuCategoryCreate', 0, 'Создание категории меню', NULL, 'N;'),
('menuCategoryDelete', 0, 'Удаление категории меню', NULL, 'N;'),
('menuCategoryUpdate', 0, 'Обновление категории меню', NULL, 'N;'),
('menuCategoryView', 0, 'Просмотр категории меню', NULL, 'N;'),
('menuDishCreate', 0, 'Создание блюда меню', NULL, 'N;'),
('menuDishDelete', 0, 'Удаление блюда меню', NULL, 'N;'),
('menuDishUpdate', 0, 'Обновление блюда меню', NULL, 'N;'),
('menuDishView', 0, 'Просмотр блюда меню', NULL, 'N;'),
('menuOwnCategoryCreate', 1, 'Создание категории меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnCategoryDelete', 1, 'Удаление категории меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnCategoryUpdate', 1, 'Обновление категории меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnCategoryView', 1, 'Просмотр категории меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnDishCreate', 1, 'Создание блюда меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnDishDelete', 1, 'Удаление блюда меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnDishUpdate', 1, 'Обновление блюда меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('menuOwnDishView', 1, 'Просмотр блюда меню своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('mobile', 2, 'Девайс', NULL, 'N;'),
('mobile_guest', 2, 'Девайс (гость)', NULL, 'N;'),
('owner', 2, 'Владелец сети', NULL, 'N;'),
('restaurantCompanyCreate', 1, 'Создание ресторанов в своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('restaurantCompanyDelete', 1, 'Удаление ресторанов в своей компании', 'return Yii::app()->user->company_id==$params["restaurant"]->company_id;', 'N;'),
('restaurantCompanyUpdate', 1, 'Обновление ресторанов своей компании', 'return Yii::app()->user->company_id==$params["restaurant"]->company_id;', 'N;'),
('restaurantCompanyView', 1, 'Просмотр ресторанов своей компании', 'return Yii::app()->user->company_id==$params["company_id"];', 'N;'),
('restaurantCreate', 0, 'Создание ресторана', NULL, 'N;'),
('restaurantDelete', 0, 'Удаление ресторана', NULL, 'N;'),
('restaurantOwnUpdate', 1, 'Обновление своего ресторана', 'return Yii::app()->user->restaurant_id==$params["restaurant"]->id;', 'N;'),
('restaurantUpdate', 0, 'Обновление ресторана', NULL, 'N;'),
('restaurantView', 0, 'Просмотр ресторана', NULL, 'N;'),
('userCreate', 0, 'Создание пользователя', NULL, 'N;'),
('userDelete', 0, 'Удаление пользователя', NULL, 'N;'),
('userOwnCompanyCreate', 1, 'Создание пользователя своей компании', 'return Yii::app()->user->company_id==$params["user"]->company_id;', 'N;'),
('userOwnCompanyDelete', 1, 'Удаление пользователя своей компании', 'return Yii::app()->user->company_id==$params["user"]->company_id;', 'N;'),
('userOwnCompanyUpdate', 1, 'Редактирование пользователя своей компании', 'return Yii::app()->user->company_id==$params["user"]->company_id;', 'N;'),
('userOwnCompanyView', 1, 'Просмотр пользователя своей компании', 'return Yii::app()->user->company_id==$params["user"]->company_id;', 'N;'),
('userOwnRestaurantCreate', 1, 'Создание пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;', 'N;'),
('userOwnRestaurantDelete', 1, 'Удаление пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;', 'N;'),
('userOwnRestaurantUpdate', 1, 'Редактирование пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;', 'N;'),
('userOwnRestaurantView', 1, 'Просмотр пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;', 'N;'),
('userUpdate', 0, 'Обновление пользователя', NULL, 'N;'),
('userView', 0, 'Просмотр пользователя', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `app_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `app_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_auth_item_child`
--

INSERT INTO `app_auth_item_child` (`parent`, `child`) VALUES
('manager', 'barman'),
('admin', 'companyCreate'),
('admin', 'companyDelete'),
('owner', 'companyOwnTools'),
('owner', 'companyOwnUpdate'),
('manager', 'companyOwnView'),
('admin', 'companyTools'),
('companyOwnTools', 'companyTools'),
('admin', 'companyUpdate'),
('companyOwnUpdate', 'companyUpdate'),
('admin', 'companyView'),
('companyOwnView', 'companyView'),
('manager', 'cook'),
('manager', 'garcon'),
('barman', 'inAdmin'),
('cook', 'inAdmin'),
('garcon', 'inAdmin'),
('owner', 'manager'),
('admin', 'menuCategoryCreate'),
('menuOwnCategoryCreate', 'menuCategoryCreate'),
('admin', 'menuCategoryDelete'),
('menuOwnCategoryDelete', 'menuCategoryDelete'),
('admin', 'menuCategoryUpdate'),
('menuOwnCategoryUpdate', 'menuCategoryUpdate'),
('admin', 'menuCategoryView'),
('menuOwnCategoryView', 'menuCategoryView'),
('admin', 'menuDishCreate'),
('menuOwnDishCreate', 'menuDishCreate'),
('admin', 'menuDishDelete'),
('menuOwnDishDelete', 'menuDishDelete'),
('admin', 'menuDishUpdate'),
('menuOwnDishUpdate', 'menuDishUpdate'),
('admin', 'menuDishView'),
('menuOwnDishView', 'menuDishView'),
('owner', 'menuOwnCategoryCreate'),
('owner', 'menuOwnCategoryDelete'),
('owner', 'menuOwnCategoryUpdate'),
('owner', 'menuOwnCategoryView'),
('owner', 'menuOwnDishCreate'),
('owner', 'menuOwnDishDelete'),
('owner', 'menuOwnDishUpdate'),
('owner', 'menuOwnDishView'),
('mobile', 'mobile_guest'),
('admin', 'owner'),
('owner', 'restaurantCompanyCreate'),
('owner', 'restaurantCompanyDelete'),
('owner', 'restaurantCompanyUpdate'),
('manager', 'restaurantCompanyView'),
('admin', 'restaurantCreate'),
('restaurantCompanyCreate', 'restaurantCreate'),
('admin', 'restaurantDelete'),
('restaurantCompanyDelete', 'restaurantDelete'),
('manager', 'restaurantOwnUpdate'),
('admin', 'restaurantUpdate'),
('restaurantCompanyUpdate', 'restaurantUpdate'),
('restaurantOwnUpdate', 'restaurantUpdate'),
('admin', 'restaurantView'),
('restaurantCompanyView', 'restaurantView'),
('admin', 'userCreate'),
('userOwnCompanyCreate', 'userCreate'),
('userOwnRestaurantCreate', 'userCreate'),
('admin', 'userDelete'),
('userOwnCompanyDelete', 'userDelete'),
('userOwnRestaurantDelete', 'userDelete'),
('owner', 'userOwnCompanyCreate'),
('owner', 'userOwnCompanyDelete'),
('owner', 'userOwnCompanyUpdate'),
('owner', 'userOwnCompanyView'),
('manager', 'userOwnRestaurantCreate'),
('manager', 'userOwnRestaurantDelete'),
('manager', 'userOwnRestaurantUpdate'),
('manager', 'userOwnRestaurantView'),
('admin', 'userUpdate'),
('userOwnCompanyUpdate', 'userUpdate'),
('userOwnRestaurantUpdate', 'userUpdate'),
('admin', 'userView'),
('userOwnCompanyView', 'userView'),
('userOwnRestaurantView', 'userView');

-- --------------------------------------------------------

--
-- Структура таблицы `app_cash`
--

CREATE TABLE IF NOT EXISTS `app_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `cashgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- Дамп данных таблицы `app_cash`
--

INSERT INTO `app_cash` (`id`, `out_id`, `out_code`, `name`, `status`, `cashgroup_id`) VALUES
(53, '15002', '1', 'F_ST', 1, 47),
(54, '15003', '3', 'rasp_rasp_STO1', 3, 47),
(55, '15004', '1', 'rasp_rasp_ST01', 3, 47),
(56, '15006', '4', 'ST02', 3, 48),
(57, '15008', '3', 'ST01', 3, 49),
(58, '15010', '5', 'ST04', 3, 50),
(59, '15012', '6', 'ST05', 3, 51),
(60, '15014', '7', 'ST06', 3, 52),
(61, '15016', '8', 'ST07', 3, 53),
(62, '15018', '9', 'ST08', 3, 54),
(63, '15020', '10', 'ST09', 3, 55),
(64, '15022', '11', 'ST10', 3, 56),
(65, '15025', '2', 'F_ST', 3, 57),
(66, '15028', '1', 'F_ST', 3, 58),
(67, '15030', '1', 'F_ST', 3, 59),
(68, '15033', '1', 'F_ST', 3, 60),
(69, '15036', '1', 'Новая станция', 1, 61);

-- --------------------------------------------------------

--
-- Структура таблицы `app_cash_groups`
--

CREATE TABLE IF NOT EXISTS `app_cash_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Дамп данных таблицы `app_cash_groups`
--

INSERT INTO `app_cash_groups` (`id`, `out_id`, `restaurant_id`, `name`, `status`) VALUES
(47, '15001', 45, 'F_MIDSERV', 1),
(48, '15005', 45, 'MIDSERV2', 3),
(49, '15007', 45, 'MIDSERV3', 3),
(50, '15009', 45, 'MIDSERV4', 3),
(51, '15011', 45, 'MIDSERV5', 3),
(52, '15013', 45, 'MIDSERV6', 3),
(53, '15015', 45, 'MIDSERV7', 3),
(54, '15017', 45, 'MIDSERV8', 3),
(55, '15019', 45, 'MIDSERV9', 3),
(56, '15021', 45, 'MIDSERV10', 3),
(57, '15024', 43, 'F_MIDSERV', 3),
(58, '15027', 43, 'F_MIDSERV', 3),
(59, '15029', 43, 'F_MIDSERV', 3),
(60, '15031', 43, 'F_MIDSERV', 3),
(61, '15034', 47, 'NEW_CASH_SERVER', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_company`
--

CREATE TABLE IF NOT EXISTS `app_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(500) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `kitchen` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `workplaces` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `imageVersion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `app_company`
--

INSERT INTO `app_company` (`id`, `out_id`, `name`, `logo`, `hidden`, `deleted`, `description`, `kitchen`, `website`, `email`, `prefix`, `workplaces`, `version`, `imageVersion`) VALUES
(1, '', 'Тануки', '/data/images/logos/1401926957_c8b85841d570f75773a922451541676d.jpeg', 0, 1, 'Рестораны Тануки заметно отличаются своим аутентичным интерьером. Частокол из бамбука, стены с иероглифами и статуэтками, светильники из рисовой бумаги, большие столы, сделанные не для экономии места, а для удобства Гостей, впрочем, как и всё в ресторанах Тануки', '', 'http://www.tanuki.ru/', 'hotline@tanuki.ru', 'tanuki', '1,2', 0, 0),
(2, '', 'Япоша', '/data/images/logos/yaposha.jpg', 0, 1, '«Япоша» — сеть популярных демократичных японских ресторанов с яркой, жизнеутверждающей концепцией, с оригинальным двойным меню – суши и антисуши. Мы подарим Вам счастливые минуты беззаботного отдыха в кругу семьи и друзей.', '', '', '', 'yapo', '1', 0, 0),
(3, '', 'Якитория', '/data/images/logos/yakitoriya.jpg', 0, 1, 'Сеть культовых кафе авторской японской кухни «Якитория» – самый масштабный проект ассоциации ресторанов "Веста-центр интернешнл". Городские кафе, удобно расположенные вблизи станций метро в Москве, предлагают лучшую японскую кухню в городе.', '', '', '', 'yaki', '1', 0, 0),
(4, '', 'IL PATIO', '/data/images/logos/il_patio.jpg', 0, 1, '', '', '', '', 'patio', '1', 0, 0),
(6, '', 'Кофе Хауз', '/data/images/logos/1403777097_b54435daaf447e4d08027640ee117ff2.jpeg', 0, 1, '', '', '', '', 'house', '1', 0, 0),
(7, '', 'шоколадница', '/data/images/logos/1403786730_a08d079201683af1763fae7e6f6e6270.jpeg', 1, 1, '', '', '', '', 'choko', '', 0, 0),
(8, '', 'тестовая', '/data/images/logos/1404304731_6c88a64d5f598c51d6a8e9e9aa69fc3b.png', 0, 1, '', '', '', '', 'test', '', 0, 0),
(9, '', 'Тануки (RKeeper7)', '/data/images/logos/1422972523_60318ae052edef881beb015dfad26228.jpeg', 0, 0, 'Сеть «Тануки» включает 63 ресторанов, расположенных по всей Москве, Украине и регионам России. ', '', 'http://www.tanuki.ru/', 'hotline@tanuki.ru', 'rkeeper', '1,2', 0, 0),
(10, '', 'IL Patio (RKeeper 6)', '/data/images/logos/1443678213_b88f6b2585680baf258f30fc94569c3b.jpeg', 0, 0, '', '', 'http://ilpatio.ru/', 'hotline@ilpatio.ru', 'ilpatio', '1,2', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_company_tools`
--

CREATE TABLE IF NOT EXISTS `app_company_tools` (
  `company_id` int(11) NOT NULL,
  `business_lunch_ids` int(11) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_company_tools`
--

INSERT INTO `app_company_tools` (`company_id`, `business_lunch_ids`) VALUES
(1, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `app_condition`
--

CREATE TABLE IF NOT EXISTS `app_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `position_id` int(11) NOT NULL,
  `measure` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `action_on` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

--
-- Дамп данных таблицы `app_condition`
--

INSERT INTO `app_condition` (`id`, `out_id`, `position_id`, `measure`, `sort`, `price`, `action_on`) VALUES
(16, '', 9, '1000 мл.', 0, 150, 0),
(25, '', 8, '200 гр.', 0, 250, 1),
(30, '', 17, '8 шт.', 0, 95, 0),
(31, '', 18, '8 шт.', 0, 145, 0),
(32, '', 19, '1 шт.', 0, 355, 0),
(33, '', 20, '1 шт.', 0, 325, 0),
(34, '', 21, '6 шт.', 0, 95, 0),
(35, '', 22, '1 шт.', 0, 235, 0),
(38, '', 25, '1 шт.', 0, 290, 0),
(39, '', 26, '1000 мл.', 0, 110, 0),
(40, '', 27, '330 мл.', 0, 145, 0),
(41, '', 28, '250 мл.', 0, 90, 0),
(42, '', 29, '1000 мл.', 0, 120, 0),
(43, '', 30, '1 шт.', 0, 310, 0),
(44, '', 31, '1 шт.', 0, 245, 0),
(45, '', 32, '1 шт.', 0, 360, 0),
(46, '', 33, '1 шт.', 0, 235, 0),
(47, '', 34, '1 шт.', 0, 290, 0),
(48, '', 35, '1 шт.', 0, 1230, 0),
(49, '', 36, '1 шт.', 0, 2790, 0),
(50, '', 37, '1 шт.', 0, 1110, 0),
(51, '', 38, '1 шт.', 0, 310, 0),
(52, '', 39, '1 шт.', 0, 440, 0),
(53, '', 40, '1 шт.', 0, 35, 0),
(54, '', 41, '1 шт.', 0, 75, 0),
(55, '', 42, '1 шт.', 0, 70, 0),
(56, '', 43, '1 шт.', 0, 90, 0),
(57, '', 44, '1 шт.', 0, 80, 0),
(69, '', 48, '500 мл.', 0, 150, 0),
(70, '', 49, '150 мл.', 0, 50, 0),
(71, '', 49, '250 мл.', 0, 100, 0),
(72, '', 49, '500 мл.', 0, 200, 0),
(74, '', 51, 'Основная', 0, 115, 0),
(75, '', 52, 'Основная', 0, 150, 0),
(76, '', 53, 'Основная', 0, 220, 0),
(77, '', 54, 'Основная', 0, 245, 0),
(78, '', 55, 'Основная', 0, 247, 0),
(79, '', 56, 'Основная', 0, 299, 0),
(80, '', 57, 'Основная', 0, 287, 0),
(81, '', 58, 'Основная', 0, 237, 0),
(82, '', 59, 'Основная', 0, 100, 0),
(83, '', 60, 'комбо', 0, 250, 0),
(88, '', 24, '1 шт.', 0, 260, 0),
(90, '', 63, '1 шт.', 1, 100, 0),
(91, '', 64, 'Общая стоимость', 1, 250, 0),
(92, '', 65, '1 шт.', 1, 20, 0),
(93, '', 66, '1 шт.', 1, 345, 0),
(101, '', 23, '1 шт.', 1410877406, 1, 0),
(102, '', 67, 'Общая стоимость', 1415782139, 123, 0),
(103, '', 142, '1 шт.', 1419233612, 150, 0),
(104, '', 143, '1 шт.', 1419233944, 98, 0),
(105, '', 213, '1 шт.', 1421217907, 55, 0),
(106, '', 212, '1 шт.', 1421218166, 100, 0),
(107, '', 220, '1 шт.', 1422967719, 299, 0),
(108, '', 221, '1 шт.', 1422969146, 285, 0),
(109, '', 222, '1 шт.', 1422969307, 240, 0),
(110, '', 223, '1 шт.', 1422969629, 190, 0),
(111, '', 224, '1 шт.', 1422970052, 520, 0),
(112, '', 214, '1 шт.', 1422970469, 1, 0),
(113, '', 215, '1 шт.', 1422970489, 1, 0),
(114, '', 217, '1 шт.', 1422970504, 1, 0),
(115, '', 216, '1 шт.', 1422970510, 1, 0),
(116, '', 225, '1 шт.', 1422970747, 240, 0),
(117, '', 226, '1 шт.', 1422974556, 440, 0),
(118, '', 227, '1 шт.', 1422975189, 145, 0),
(119, '', 228, '1 шт.', 1422975225, 90, 0),
(120, '', 229, '1 шт.', 1422975235, 150, 0),
(121, '', 230, '1 шт.', 1422975248, 430, 0),
(122, '', 231, '1 шт.', 1422976655, 190, 0),
(123, '', 232, '1 шт.', 1422976935, 75, 0),
(124, '', 233, '1 шт.', 1422977064, 470, 0),
(125, '', 234, '1 шт.', 1422977175, 510, 0),
(126, '', 235, '1 шт.', 1422977307, 510, 0),
(127, '', 236, '1 шт.', 1422978503, 245, 0),
(128, '', 237, '1 шт.', 1422978635, 360, 0),
(129, '', 238, '1 шт.', 1422979066, 930, 0),
(130, '', 239, '1 шт.', 1422979304, 1290, 0),
(131, '', 240, '1 шт.', 1422979434, 920, 0),
(132, '', 241, '1 шт.', 1422979618, 3500, 0),
(133, '', 245, '1 шт.', 1422979968, 290, 0),
(134, '', 242, '1 шт.', 1422980009, 95, 0),
(135, '', 243, '1 шт.', 1422980047, 210, 0),
(136, '', 244, '1 шт.', 1422980083, 125, 0),
(137, '', 246, '1 шт.', 1422980411, 70, 0),
(138, '', 247, '1 шт.', 1422980451, 85, 0),
(139, '', 248, '1 шт.', 1422980480, 80, 0),
(140, '', 249, '1 шт.', 1422980510, 100, 0),
(141, '', 250, '1 шт.', 1422980567, 145, 0),
(142, '', 255, '1 шт.', 1422981798, 290, 0),
(143, '', 251, '1 шт.', 1422981967, 360, 0),
(144, '', 252, '1 шт.', 1422982000, 385, 0),
(145, '', 253, '1 шт.', 1422982076, 310, 0),
(146, '', 254, '1 шт.', 1422982098, 295, 0),
(147, '', 256, '1 шт.', 1422982450, 920, 0),
(148, '', 257, '1 шт.', 1422982486, 230, 0),
(149, '', 258, '1 шт.', 1422982526, 210, 0),
(150, '', 259, '1 шт.', 1422982558, 185, 0),
(151, '', 260, '1 шт.', 1422982589, 295, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_device`
--

CREATE TABLE IF NOT EXISTS `app_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `blocked` int(11) NOT NULL,
  `push_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `app_device`
--

INSERT INTO `app_device` (`id`, `uuid`, `platform`, `name`, `version`, `blocked`, `push_token`) VALUES
(2, '550e8400-e29b-41d4-a716-446655440000', 'iOS', 'iPhone 5', '7.1', 0, 'f5950b10 eb5e8888 ce549b49 0d24cbb1 a0202f49 929ca991 bd37a6c2 7070dbea'),
(3, 'E13AE406-83C2-4DC6-86AD-F2936DF84D47', 'iPhone OS', 'iPhone 5(GSM+CDMA)', '8.2', 0, NULL),
(4, '21C4B362-B878-4D06-9B59-C9186300F036', 'iPhone OS', 'iPhone 4', '7.1.2', 0, NULL),
(5, 'BEA82D01-8AC6-4E53-9602-E3D40F3E5BD6', 'iPhone OS', 'iPad5,4', '8.2', 0, NULL),
(6, 'A84905DB-27E0-48F0-B675-E35014302720', 'iPhone OS', 'iPhone 5s(GSM+CDMA)', '8.2', 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `app_device_log`
--

CREATE TABLE IF NOT EXISTS `app_device_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `command` varchar(255) DEFAULT NULL,
  `body` text,
  `request` text,
  `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `device` (`device_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=369 ;

--
-- Дамп данных таблицы `app_device_log`
--

INSERT INTO `app_device_log` (`id`, `device_id`, `command`, `body`, `request`, `request_time`) VALUES
(5, 2, 'getCompanies', '', '{"version":-1}', '2015-02-25 12:14:06'),
(6, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-02-25 12:28:01'),
(7, 2, 'getRestaurants', '', '{"company_id":"1","version":-1}', '2015-02-25 12:28:26'),
(8, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:30:31'),
(9, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:30:33'),
(10, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:33:04'),
(11, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:33:21'),
(12, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:33:48'),
(13, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:47:17'),
(14, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:48:16'),
(15, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:48:18'),
(16, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 12:58:26'),
(17, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:06:21'),
(18, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:06:23'),
(19, 2, 'getDishes', '', '{"company_id":"9","version":"9"}', '2015-02-25 14:06:31'),
(20, 2, 'getDishes', '', '{"company_id":"9","version":"9"}', '2015-02-25 14:06:33'),
(21, 2, 'getDishes', '', '{"company_id":"9","version":"89"}', '2015-02-25 14:06:39'),
(22, 2, 'getDishes', '', '{"company_id":"9","version":"89"}', '2015-02-25 14:06:41'),
(23, 2, 'getDishes', '', '{"company_id":"9","version":"189"}', '2015-02-25 14:06:50'),
(24, 2, 'getDishes', '', '{"company_id":"9","version":"189"}', '2015-02-25 14:06:51'),
(25, 2, 'getDishes', '', '{"company_id":"9","version":"8888"}', '2015-02-25 14:07:08'),
(26, 2, 'getDishes', '', '{"company_id":"9","version":"8888"}', '2015-02-25 14:12:00'),
(27, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:12:04'),
(28, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:12:38'),
(29, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:12:39'),
(30, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:12:57'),
(31, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:16:52'),
(32, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:16:53'),
(33, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:17:12'),
(34, 2, 'getDishes', '', '{"company_id":"9","version":-1}', '2015-02-25 14:21:08'),
(35, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:21:43'),
(36, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:16'),
(37, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:18'),
(38, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:20'),
(39, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:20'),
(40, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:22'),
(41, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:23'),
(42, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:23'),
(43, 2, 'getDishesImages', '', '{"company_id":"9","version":-1}', '2015-02-25 14:22:23'),
(44, 2, 'getDishesImages', '', '{"company_id":"1","version":-1}', '2015-02-25 14:22:27'),
(45, 2, 'getCompanies', '', '{"version":-1}', '2015-02-25 14:47:27'),
(46, 2, 'getCompanies', '', '{"version":-1}', '2015-02-27 15:59:21'),
(47, 2, 'postOrder', '', '', '2015-03-10 14:02:29'),
(48, 2, 'postOrder', '', '', '2015-03-10 14:02:48'),
(49, 2, 'postOrder', '', '', '2015-03-10 14:23:45'),
(50, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":250}],"separated":0,"table":36}', '', '2015-03-10 15:46:34'),
(51, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":250}],"separated":0,"table":36}', '', '2015-03-10 15:57:16'),
(52, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":250}],"separated":0,"table":36}', '', '2015-03-10 15:57:28'),
(53, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":250}],"separated":0,"table":36}', '', '2015-03-11 10:41:29'),
(54, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table":36}', '', '2015-03-11 11:27:05'),
(55, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table":36}', '', '2015-03-11 12:44:46'),
(56, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":36}', '', '2015-03-11 12:45:29'),
(57, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:45:42'),
(58, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:45:56'),
(59, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:46:11'),
(60, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:46:11'),
(61, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:46:12'),
(62, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:46:13'),
(63, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:46:13'),
(64, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 12:46:33'),
(65, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 13:28:16'),
(66, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 13:28:37'),
(67, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 13:28:43'),
(68, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-03-11 13:29:16'),
(69, 2, 'getOrderStatuses', '', '', '2015-03-30 11:06:42'),
(70, 2, 'getOrder', '', '', '2015-03-30 11:42:01'),
(71, 2, 'getOrder', '', '', '2015-03-30 11:42:01'),
(72, 2, 'getOrder', '', '', '2015-03-30 11:44:00'),
(73, 2, 'getOrder', '', '', '2015-03-30 11:44:00'),
(74, 2, 'getOrder', '', '', '2015-03-30 12:06:06'),
(75, 2, 'getOrder', '', '', '2015-03-30 12:06:06'),
(76, 2, 'getOrder', '', '', '2015-03-30 12:21:39'),
(77, 2, 'getOrder', '', '', '2015-03-30 12:21:40'),
(78, 2, 'getOrder', '', '', '2015-03-30 12:40:34'),
(79, 2, 'getOrder', '', '', '2015-03-30 12:40:34'),
(80, 2, 'getOrder', '', '', '2015-03-30 12:42:25'),
(81, 2, 'getOrder', '', '', '2015-03-30 12:42:25'),
(82, 2, 'getOrder', '', '', '2015-03-30 12:42:46'),
(83, 2, 'getOrder', '', '', '2015-03-30 12:42:47'),
(84, 2, 'getOrder', '', '', '2015-03-30 12:43:19'),
(85, 2, 'getOrder', '', '', '2015-03-30 12:43:20'),
(86, 2, 'getOrder', '', '', '2015-03-30 12:43:34'),
(87, 2, 'getOrders', '', '', '2015-03-30 13:14:15'),
(88, 2, 'getOrders', '', '', '2015-03-30 13:16:29'),
(89, 2, 'getOrderStatuses', '', '', '2015-03-30 13:17:06'),
(90, 2, 'getOrders', '', '', '2015-03-30 13:17:58'),
(91, 2, 'getOrders', '', '', '2015-03-30 13:18:27'),
(92, 2, 'getOrders', '', '', '2015-03-30 13:18:33'),
(93, 2, 'getOrders', '', '', '2015-03-30 13:18:50'),
(94, 2, 'getOrders', '', '', '2015-03-30 13:18:59'),
(95, 2, 'getOrders', '', '', '2015-03-30 13:19:04'),
(96, 2, 'getCompanies', '', '{"version":-1}', '2015-03-30 13:52:19'),
(97, 2, 'setPushToken', '', '', '2015-03-30 14:58:20'),
(98, 2, 'setPushToken', '', '', '2015-03-30 15:44:04'),
(99, 3, 'setPushToken', '', '', '2015-03-30 15:44:43'),
(100, 4, 'setPushToken', '', '', '2015-03-30 15:45:00'),
(101, 2, 'setPushToken', '', '', '2015-03-30 15:49:54'),
(102, 2, 'setPushToken', '', '', '2015-03-30 16:18:08'),
(103, 2, 'setPushToken', '', '', '2015-03-30 16:38:19'),
(104, 2, 'getCompanies', '', '{"version":-1}', '2015-03-30 18:23:48'),
(105, 2, 'getCompanies', '', '{"version":"1"}', '2015-03-30 18:24:42'),
(106, 2, 'getCompanies', '', '{"version":"0"}', '2015-03-30 18:24:45'),
(107, 2, 'getCompanies', '', '{"version":"1"}', '2015-03-30 18:24:48'),
(108, 2, 'getCompanies', '', '{"version":"-1"}', '2015-03-30 18:24:53'),
(109, 2, 'getCompaniesImages', '', '{"version":"-1"}', '2015-03-30 18:32:50'),
(110, 2, 'getCompaniesImages', '', '{"version":"1"}', '2015-03-30 18:32:56'),
(111, 2, 'getCompaniesImages', '', '{"version":"0"}', '2015-03-30 18:33:00'),
(112, 2, 'getCompaniesImages', '', '{"version":"-1"}', '2015-03-30 18:33:04'),
(113, 2, 'getRestaurants', '', '{"company_id":"9","version":"-1"}', '2015-03-30 18:36:46'),
(114, 2, 'getRestaurantPhotos', '', '{"company_id":"9","version":"-1"}', '2015-03-30 18:50:33'),
(115, 2, 'getRestaurantPhotos', '', '{"company_id":"1","version":"-1"}', '2015-03-30 18:50:39'),
(116, 2, 'getRestaurantPhotos', '', '{"company_id":"1","version":"1"}', '2015-03-30 18:50:43'),
(117, 2, 'getRestaurantPhotos', '', '{"company_id":"1","version":"-1"}', '2015-03-30 18:50:46'),
(118, 2, 'getRestaurantTables', '', '{"restaurant_id":"45","version":"-1"}', '2015-03-30 18:56:42'),
(119, 2, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-03-30 19:48:21'),
(120, 2, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-03-30 19:50:16'),
(121, 2, 'getDishesCategoriesImages', '', '{"company_id":"1","version":"-1"}', '2015-03-30 19:50:21'),
(122, 2, 'getDishesCategoriesImages', '', '{"company_id":"1","version":"-1"}', '2015-03-30 19:53:07'),
(123, 2, 'getDishesCategoriesImages', '', '{"company_id":"1","version":"-1"}', '2015-03-30 19:53:31'),
(124, 2, 'getDishes', '', '{"company_id":"1","version":"-1"}', '2015-03-30 19:53:36'),
(125, 2, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-03-30 19:53:49'),
(126, 2, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-03-30 19:57:40'),
(127, 2, 'getDishesImages', '', '{"company_id":"1","version":"-1"}', '2015-03-30 19:57:45'),
(128, 2, 'getDishesImages', '', '{"company_id":"1","version":"4600"}', '2015-03-30 19:57:55'),
(129, 2, 'getDishesImages', '', '{"company_id":"1","version":"3500"}', '2015-03-30 19:57:58'),
(130, 2, 'getDishesImages', '', '{"company_id":"1","version":"200"}', '2015-03-30 19:58:00'),
(131, 2, 'getDishesImages', '', '{"company_id":"1","version":"2"}', '2015-03-30 19:58:02'),
(132, 2, 'getDishesImages', '', '{"company_id":"1","version":"200"}', '2015-03-30 19:58:06'),
(133, 2, 'getDishesImages', '', '{"company_id":"1","version":"20"}', '2015-03-30 19:58:12'),
(134, 2, 'getDishesImages', '', '{"company_id":"1","version":"30"}', '2015-03-30 19:58:16'),
(135, 2, 'getOrder', '', '', '2015-03-30 20:07:43'),
(136, 2, 'getLabels', '', '', '2015-03-30 20:08:18'),
(137, 2, 'getOrder', '', '', '2015-03-30 20:09:17'),
(138, 2, 'getOrder', '', '', '2015-03-30 20:10:54'),
(139, 2, 'getOrders', '', '', '2015-03-30 22:45:36'),
(140, 2, 'getOrderPayments', '', '', '2015-03-30 22:54:10'),
(141, 2, 'getOrderStatuses', '', '', '2015-03-30 23:12:56'),
(142, 2, 'getLabels', '', '', '2015-03-30 23:18:26'),
(143, 2, 'setPushToken', '', '', '2015-03-30 23:20:57'),
(144, 2, 'getOrders', '', '', '2015-03-31 09:41:20'),
(145, 2, 'getCompanies', '', '{"version":-1}', '2015-03-31 10:14:41'),
(146, 2, 'getCompanies', '', '{"version":-1}', '2015-03-31 10:16:26'),
(147, 2, 'getCompanies', '', '{"version":-1}', '2015-03-31 10:19:01'),
(148, 2, 'setPushToken', '', '', '2015-03-31 10:21:06'),
(149, 3, 'getCompanies', '', '{"version":-1}', '2015-04-06 19:19:59'),
(150, 3, 'getCompanies', '', '{"version":-1}', '2015-04-06 21:21:11'),
(151, 3, 'getCompaniesImages', '', '{"version":"0"}', '2015-04-06 21:21:19'),
(152, 3, 'getCompanies', '', '{"version":-1}', '2015-04-06 21:22:28'),
(153, 3, 'getCompanies', '', '{"version":-1}', '2015-04-06 21:23:22'),
(154, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-06 21:23:24'),
(155, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:34:26'),
(156, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:34:27'),
(157, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:43:14'),
(158, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:43:14'),
(159, 3, 'getOrders', '', '', '2015-04-07 10:43:16'),
(160, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:43:59'),
(161, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:43:59'),
(162, 3, 'getOrders', '', '', '2015-04-07 10:44:00'),
(163, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:45:05'),
(164, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:45:05'),
(165, 3, 'getOrders', '', '', '2015-04-07 10:45:06'),
(166, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:46:39'),
(167, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:46:39'),
(168, 3, 'getOrders', '', '', '2015-04-07 10:46:40'),
(169, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:48:14'),
(170, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:48:14'),
(171, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:49:22'),
(172, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:49:22'),
(173, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 10:50:33'),
(174, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 10:50:33'),
(175, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:06:38'),
(176, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:06:39'),
(177, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:10:30'),
(178, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:10:30'),
(179, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:10:59'),
(180, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:10:59'),
(181, 2, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:11:00'),
(182, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:12:21'),
(183, 2, 'getOrders', '', '', '2015-04-07 12:12:21'),
(184, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:12:21'),
(185, 2, 'getOrders', '', '', '2015-04-07 12:12:36'),
(186, 2, 'getRestaurants', '', '{"company_id":0,"version":"0"}', '2015-04-07 12:12:54'),
(187, 2, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:13:02'),
(188, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:14:55'),
(189, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:14:55'),
(190, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:15:04'),
(191, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:15:19'),
(192, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:15:20'),
(193, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:16:09'),
(194, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:20:18'),
(195, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:21:03'),
(196, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:22:02'),
(197, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:23:04'),
(198, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:23:04'),
(199, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:23:13'),
(200, 3, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:23:24'),
(201, 3, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:23:25'),
(202, 3, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:23:25'),
(203, 3, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:23:26'),
(204, 3, 'getOrders', '', '', '2015-04-07 12:23:35'),
(205, 3, 'getOrders', '', '', '2015-04-07 12:24:00'),
(206, 3, 'getOrders', '', '', '2015-04-07 12:24:04'),
(207, 3, 'getOrders', '', '', '2015-04-07 12:24:07'),
(208, 3, 'getOrders', '', '', '2015-04-07 12:24:09'),
(209, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:25:13'),
(210, 2, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:25:13'),
(211, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:25:16'),
(212, 3, 'getOrders', '', '', '2015-04-07 12:25:16'),
(213, 2, 'getRestaurantPhotos', '', '{"company_id":0,"version":-1}', '2015-04-07 12:28:53'),
(214, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:28:58'),
(215, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:28:58'),
(216, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:28:59'),
(217, 3, 'getOrders', '', '', '2015-04-07 12:29:00'),
(218, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:30:39'),
(219, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:30:39'),
(220, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:30:40'),
(221, 3, 'getOrders', '', '', '2015-04-07 12:30:40'),
(222, 3, 'getOrders', '', '', '2015-04-07 12:32:51'),
(223, 3, 'getOrders', '', '', '2015-04-07 12:32:53'),
(224, 3, 'getOrders', '', '', '2015-04-07 12:32:57'),
(225, 3, 'getOrders', '', '', '2015-04-07 12:34:32'),
(226, 3, 'getOrders', '', '', '2015-04-07 12:34:47'),
(227, 3, 'getOrders', '', '', '2015-04-07 12:34:53'),
(228, 2, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:37:43'),
(229, 2, 'getRestaurants', '', '{"company_id":"9","version":-1}', '2015-04-07 12:37:59'),
(230, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:42:10'),
(231, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:42:10'),
(232, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:42:11'),
(233, 3, 'getOrders', '', '', '2015-04-07 12:42:12'),
(234, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:46:19'),
(235, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:46:19'),
(236, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:46:20'),
(237, 3, 'getOrders', '', '', '2015-04-07 12:46:20'),
(238, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":239,"count":1,"price":1290},{"position_id":240,"count":1,"price":920},{"position_id":241,"count":1,"price":3500}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 12:47:03'),
(239, 3, 'getOrders', '', '', '2015-04-07 12:47:04'),
(240, 3, 'getOrders', '', '', '2015-04-07 12:47:18'),
(241, 3, 'getOrders', '', '', '2015-04-07 12:47:32'),
(242, 3, 'getOrders', '', '', '2015-04-07 12:47:40'),
(243, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:53:31'),
(244, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:53:32'),
(245, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:53:32'),
(246, 3, 'getOrders', '', '', '2015-04-07 12:53:33'),
(247, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:55:51'),
(248, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:55:51'),
(249, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:55:52'),
(250, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:56:15'),
(251, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:56:16'),
(252, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:56:17'),
(253, 3, 'getOrders', '', '', '2015-04-07 12:56:17'),
(254, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:56:39'),
(255, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:56:40'),
(256, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:56:41'),
(257, 3, 'getOrders', '', '', '2015-04-07 12:56:41'),
(258, 3, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:56:50'),
(259, 3, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:56:51'),
(260, 3, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:56:51'),
(261, 3, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 12:56:52'),
(262, 3, 'getOrders', '', '', '2015-04-07 12:56:56'),
(263, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":221,"count":1,"price":285}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 12:57:03'),
(264, 3, 'getOrders', '', '', '2015-04-07 12:57:03'),
(265, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:57:28'),
(266, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:57:28'),
(267, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:57:29'),
(268, 3, 'getOrders', '', '', '2015-04-07 12:57:29'),
(269, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 12:58:06'),
(270, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 12:58:06'),
(271, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 12:58:07'),
(272, 3, 'getOrders', '', '', '2015-04-07 12:58:07'),
(273, 3, 'getOrders', '', '', '2015-04-07 12:58:29'),
(274, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":226,"count":1,"price":440},{"position_id":227,"count":2,"price":145},{"position_id":228,"count":3,"price":90}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 12:58:54'),
(275, 3, 'getOrders', '', '', '2015-04-07 12:58:54'),
(276, 3, 'getOrders', '', '', '2015-04-07 12:59:51'),
(277, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-04-07 13:10:54'),
(278, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":225,"count":1,"price":240}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:31:14'),
(279, 3, 'getOrders', '', '', '2015-04-07 13:31:14'),
(280, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":225,"count":1,"price":240}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"45","separated_info":""}', '', '2015-04-07 13:32:21'),
(281, 3, 'getOrders', '', '', '2015-04-07 13:32:21'),
(282, 3, 'getOrders', '', '', '2015-04-07 13:43:22'),
(283, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":242,"count":1,"price":95}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"45","separated_info":""}', '', '2015-04-07 13:43:29'),
(284, 3, 'getOrders', '', '', '2015-04-07 13:43:30'),
(285, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":243,"count":1,"price":210},{"position_id":242,"count":1,"price":95},{"position_id":244,"count":1,"price":125}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:45:39'),
(286, 3, 'getOrders', '', '', '2015-04-07 13:45:40'),
(287, 3, 'getOrders', '', '', '2015-04-07 13:46:43'),
(288, 3, 'getOrders', '', '', '2015-04-07 13:46:58'),
(289, 3, 'getOrders', '', '', '2015-04-07 13:47:05'),
(290, 3, 'getOrders', '', '', '2015-04-07 13:48:34'),
(291, 3, 'getDishesCategories', '', '{"company_id":"1","version":"-1"}', '2015-04-07 13:49:08'),
(292, 3, 'getDishesCategoriesImages', '', '{"company_id":"1","version":"-1"}', '2015-04-07 13:49:08'),
(293, 3, 'getDishes', '', '{"company_id":"1","version":"-1"}', '2015-04-07 13:49:09'),
(294, 3, 'getDishesImages', '', '{"company_id":"1","version":"-1"}', '2015-04-07 13:49:09'),
(295, 3, 'getOrders', '', '', '2015-04-07 13:49:12'),
(296, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":236,"count":1,"price":245},{"position_id":237,"count":1,"price":360}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:52:13'),
(297, 3, 'getOrders', '', '', '2015-04-07 13:52:13'),
(298, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":231,"count":1,"price":190},{"position_id":232,"count":1,"price":75}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:53:17'),
(299, 3, 'getOrders', '', '', '2015-04-07 13:53:18'),
(300, 3, 'getOrders', '', '', '2015-04-07 13:53:34'),
(301, 3, 'getOrders', '', '', '2015-04-07 13:53:58'),
(302, 3, 'getOrders', '', '', '2015-04-07 13:54:07'),
(303, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":231,"count":1,"price":190}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:55:00'),
(304, 3, 'getOrders', '', '', '2015-04-07 13:55:00'),
(305, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 13:55:16'),
(306, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 13:55:16'),
(307, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 13:55:17'),
(308, 3, 'getOrders', '', '', '2015-04-07 13:55:17'),
(309, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":225,"count":1,"price":240}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:59:37'),
(310, 3, 'getOrders', '', '', '2015-04-07 13:59:38'),
(311, 3, 'getOrders', '', '', '2015-04-07 13:59:48'),
(312, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":243,"count":1,"price":210}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 13:59:59'),
(313, 3, 'getOrders', '', '', '2015-04-07 13:59:59'),
(314, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 14:01:44'),
(315, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 14:01:44'),
(316, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 14:01:46'),
(317, 3, 'getOrders', '', '', '2015-04-07 14:01:46'),
(318, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 14:04:16'),
(319, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 14:04:16'),
(320, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 14:04:17'),
(321, 3, 'getOrders', '', '', '2015-04-07 14:04:18'),
(322, 3, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:04:40'),
(323, 3, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:04:41'),
(324, 3, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:04:41'),
(325, 3, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:04:42'),
(326, 3, 'getOrders', '', '', '2015-04-07 14:04:59'),
(327, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-04-07 14:06:08'),
(328, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-04-07 14:13:38'),
(329, 3, 'getCompanies', '', '{"version":-1}', '2015-04-07 14:17:46'),
(330, 3, 'getCompaniesImages', '', '{"version":-1}', '2015-04-07 14:17:46'),
(331, 3, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-07 14:17:47'),
(332, 3, 'getOrders', '', '', '2015-04-07 14:17:47'),
(333, 3, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:18:04'),
(334, 3, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:18:05'),
(335, 3, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:18:05'),
(336, 3, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-07 14:18:06'),
(337, 3, 'getOrders', '', '', '2015-04-07 14:18:10'),
(338, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":229,"count":1,"price":150},{"position_id":230,"count":1,"price":430}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 14:18:41'),
(339, 3, 'getOrders', '', '', '2015-04-07 14:18:42'),
(340, 3, 'postOrder', '{"attention":0,"positions":[{"position_id":229,"count":1,"price":150},{"position_id":230,"count":1,"price":430}],"separated":0,"payment_id":2,"table":34,"restaurant_id":"47","separated_info":""}', '', '2015-04-07 14:29:00'),
(341, 3, 'getOrders', '', '', '2015-04-07 14:29:00'),
(342, 2, 'postOrder', '{"attention":1,"payment_id":1,"restaurant_id":47,"positions":[{"position_id":224,"count":1,"price":520},{"position_id":225,"count":1,"price":240}],"separated":0,"table_id":34}', '', '2015-04-07 14:49:28'),
(343, 4, 'getCompanies', '', '{"version":-1}', '2015-04-08 14:41:07'),
(344, 4, 'getCompaniesImages', '', '{"version":-1}', '2015-04-08 14:41:08'),
(345, 4, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-08 14:41:10'),
(346, 4, 'getOrders', '', '', '2015-04-08 14:41:10'),
(347, 4, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-04-08 14:41:18'),
(348, 4, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-08 14:41:18'),
(349, 4, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-04-08 14:41:19'),
(350, 4, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-08 14:41:20'),
(351, 4, 'getOrders', '', '', '2015-04-08 14:41:29'),
(352, 4, 'getCompanies', '', '{"version":-1}', '2015-04-08 15:53:34'),
(353, 5, 'getCompanies', '', '{"version":-1}', '2015-04-09 03:38:47'),
(354, 5, 'getCompaniesImages', '', '{"version":-1}', '2015-04-09 03:38:48'),
(355, 5, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-09 03:38:49'),
(356, 5, 'getOrders', '', '', '2015-04-09 03:38:50'),
(357, 6, 'getCompanies', '', '{"version":-1}', '2015-04-09 16:03:39'),
(358, 6, 'getCompaniesImages', '', '{"version":-1}', '2015-04-09 16:03:40'),
(359, 6, 'getRestaurants', '', '{"company_id":0,"version":-1}', '2015-04-09 16:03:41'),
(360, 6, 'getOrders', '', '', '2015-04-09 16:03:42'),
(361, 6, 'getDishesCategories', '', '{"company_id":"9","version":"-1"}', '2015-04-09 17:55:44'),
(362, 6, 'getDishesCategoriesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-09 17:55:45'),
(363, 6, 'getDishes', '', '{"company_id":"9","version":"-1"}', '2015-04-09 17:55:46'),
(364, 6, 'getDishesImages', '', '{"company_id":"9","version":"-1"}', '2015-04-09 17:55:46'),
(365, 6, 'getOrders', '', '', '2015-04-09 17:55:54'),
(366, 4, 'getCompanies', '', '{"version":-1}', '2015-06-18 18:03:25'),
(367, 4, 'getCompaniesImages', '', '{"version":-1}', '2015-06-18 18:03:28'),
(368, 2, 'getCompanies', '', '{"version":-1}', '2015-09-23 14:24:36');

-- --------------------------------------------------------

--
-- Структура таблицы `app_group_details`
--

CREATE TABLE IF NOT EXISTS `app_group_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `condition_id` int(11) NOT NULL,
  `pg_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=68 ;

--
-- Дамп данных таблицы `app_group_details`
--

INSERT INTO `app_group_details` (`id`, `group_id`, `sort`, `condition_id`, `pg_item_id`) VALUES
(26, 67, 0, 25, 2),
(27, 67, 1, 35, 2),
(63, 64, 0, 88, 1),
(64, 64, 1, 38, 1),
(65, 64, 0, 35, 2),
(66, 64, 0, 25, 3),
(67, 64, 1, 101, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `app_label`
--

CREATE TABLE IF NOT EXISTS `app_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `style_id` varchar(125) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `app_label`
--

INSERT INTO `app_label` (`id`, `text`, `style_id`) VALUES
(1, 'ХИТ', 'hit'),
(2, 'НОВИНКА', 'new');

-- --------------------------------------------------------

--
-- Структура таблицы `app_menu`
--

CREATE TABLE IF NOT EXISTS `app_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97 ;

--
-- Дамп данных таблицы `app_menu`
--

INSERT INTO `app_menu` (`id`, `out_id`, `name`, `company_id`, `parent_id`, `sort`, `description`, `image`, `hidden`, `deleted`, `version`, `imageVersion`) VALUES
(1, '', 'Закуски', 1, 0, 0, 'Описание "Закуски"\r\nЗдесь могла бы быть  ваша реклама.', '/data/images/menu/1404301480_40b5de0c3abc86700b15cd62a6aa0793.png', 0, 0, 15, 5),
(2, '', 'Напитки', 1, 0, 1, 'Описание "Напитки"', '/data/images/menu/74f4f3fba4992673c6568afaa6e8748f.png', 0, 0, 21, 0),
(3, '', 'Роллы', 1, 0, 2, 'Описание "Роллы"', '/data/images/menu/ee927ac9dd447449d100a297c55a7030.png', 0, 0, 17, 0),
(4, '', 'Салаты', 1, 0, 5, 'Описание "Салаты"', '/data/images/menu/a0bb357344944233001a7aa70df48faf.png', 0, 0, 0, 0),
(5, '', 'Сашими', 1, 0, 4, 'Описание "Сашими"', '/data/images/menu/5acfabbff1963fe83ec0a5b671625f44.png', 0, 1, 0, 0),
(6, '', 'Сеты', 1, 0, 6, 'Описание "Сети"', '/data/images/menu/c094dfc66865c2bca0e109286ee7ca1c.png', 0, 0, 0, 0),
(7, '', 'Суши', 1, 0, 7, 'Описание "Суши"', '/data/images/menu/a00e1192e3e7cd2c07a28c9974952063.png', 0, 0, 0, 0),
(9, '', 'Антисуши', 2, 0, 0, '', '/data/images/menu/1403777336_952582388a081cda41d8fb3ab6a7f448.png', 0, 0, 16, 0),
(10, '', 'Супы', 3, 0, 0, 'Супы японской кухни', '/data/images/menu/1403778236_f89f3b28186a4fc7b2a495315f14535d.png', 0, 0, 0, 0),
(11, '', 'Шоколад', 7, 0, 0, 'вкусно и питательно', '/data/images/menu/1403786888_9502a5ed48137020cfe8ff8f50b52730.jpeg', 0, 0, 0, 0),
(12, '', 'Супы', 7, 0, 0, '', '', 0, 0, 0, 0),
(13, '', 'Бизнес-ланчи', 1, 0, 3, 'djkfhkjd', '', 0, 0, 0, 0),
(14, '', 'Супы', 6, 0, 0, '', '', 0, 0, 0, 0),
(15, '', 'закуски', 6, 0, 0, '', '', 0, 0, 0, 0),
(18, '', 'первая', 8, 0, 0, '', '', 0, 0, 1, 1),
(19, '', 'вторые блюда', 3, 0, 1, '', '', 0, 0, 1, 1),
(44, '1000018', 'Напитки', 9, 65, 126, '', '', 0, 1, 1941, 0),
(45, '1000063', 'Групповой выбор', 9, 0, 128, '', '', 0, 1, 1943, 0),
(46, '1000073', 'Бизнес ланч', 9, 65, 129, '', '', 0, 1, 1944, 0),
(47, '1000078', 'Салаты', 9, 46, 130, '', '', 0, 1, 1945, 0),
(48, '1000082', 'Супы', 9, 46, 131, '', '', 0, 1, 1946, 0),
(49, '1000086', 'Напитки', 9, 46, 132, '', '', 0, 1, 1947, 0),
(50, '1000104', 'Десерты2', 9, 65, 133, '', '', 0, 1, 1948, 0),
(51, '1000112', 'Салаты', 9, 65, 134, '', '', 0, 1, 1949, 0),
(52, '1000191', 'Вторые блюда', 9, 46, 135, '', '', 0, 1, 1950, 0),
(53, '1000207', 'Вторые блюда', 9, 65, 136, '', '', 0, 1, 1951, 0),
(54, '1000221', 'Супы', 9, 65, 137, '', '', 0, 1, 1952, 0),
(55, '1000227', 'Горячие закуски', 9, 65, 138, '', '', 0, 1, 1953, 0),
(56, '1000233', 'Коньяк', 9, 66, 139, '', '', 0, 1, 1954, 0),
(57, '1000474', 'фисв5', 9, 73, 153, '', '', 0, 1, 1968, 0),
(58, '1000239', 'Пиво бутылочное', 9, 65, 140, '', '', 0, 1, 1955, 0),
(59, '1000346', 'Новое комбо', 9, 65, 141, '', '', 0, 1, 1956, 0),
(60, '1000468', 'тест 300500', 9, 69, 148, '', '', 0, 1, 1963, 0),
(61, '1000490', 'комбо', 9, 0, 155, '', '', 0, 1, 1970, 0),
(62, '1000504', 'тестовая группа', 9, 54, 156, '', '', 0, 1, 1971, 0),
(63, '1000062', 'Групповой выбор', 9, 44, 127, '', '', 0, 1, 1942, 0),
(64, '1000459', 'НЕ БУДЕТ ТАКОГО 2', 9, 65, 142, '', '', 0, 1, 1957, 0),
(65, '1000463', 'Корень', 9, 0, 143, '', '', 0, 1, 1958, 0),
(66, '1000464', 'Еще 1 корень', 9, 65, 144, '', '', 0, 1, 1959, 0),
(67, '1000465', 'Не будет такого 3', 9, 64, 145, '', '', 0, 1, 1960, 0),
(68, '1000466', 'тест 100500', 9, 65, 146, '', '', 0, 1, 1961, 0),
(69, '1000467', 'тест 200500', 9, 68, 147, '', '', 0, 1, 1962, 0),
(70, '1000470', 'фисв', 9, 68, 149, '', '', 0, 1, 1964, 0),
(71, '1000471', 'фисв2', 9, 70, 150, '', '', 0, 1, 1965, 0),
(72, '1000472', 'фисв3', 9, 71, 151, '', '', 0, 1, 1966, 0),
(73, '1000473', 'фисв4', 9, 65, 152, '', '', 0, 1, 1967, 0),
(74, '1000489', 'тест', 9, 0, 154, '', '', 0, 1, 1969, 0),
(75, '1000505', 'хаха папка', 9, 73, 157, '', '', 0, 1, 1972, 0),
(76, '1000549', 'Напитки', 9, 0, 158, '', '', 0, 1, 1973, 0),
(77, '1000550', 'Безалкогольные', 9, 76, 159, '', '', 0, 1, 1974, 0),
(78, '1000551', 'Алкогольные', 9, 76, 160, '', '', 0, 1, 1975, 0),
(79, '', 'Кофе', 9, 0, 0, 'Лучшее кофе', '/data/images/menu/1422962043_44c5c7cb9d709555d90425bfa5274021.png', 0, 1, 29, 1),
(80, '1000563', 'Напитки (безалкгольные)', 9, 0, 161, 'Газированные напитки, минеральная вода, соки', '/data/images/menu/1422970296_fc3a6645fa4ff1fc014cdd9558f403f3.png', 0, 0, 1986, 4),
(81, '1000568', 'Блины, сырники, выпечка и каши', 9, 0, 162, '', '', 0, 1, 1977, 0),
(82, '1000571', 'Лапша', 9, 0, 163, '', '', 0, 1, 1978, 0),
(83, '1000573', 'Салаты', 9, 0, 164, '', '/data/images/menu/1422969408_f61f277547fb9049d7dee24f7f66e584.png', 0, 0, 1979, 2),
(84, '1000577', 'Напитки (алкогольные)', 9, 0, 165, 'Пиво, вино, игристые вина, крепкий алкоголь', '/data/images/menu/1422969943_a6d05f2857cf8a8e49876a93d2bdc0da.png', 0, 0, 1980, 3),
(85, '1000579', 'Супы', 9, 0, 166, '', '/data/images/menu/1422970938_b477fa31f2e89dd391ef98dacac47803.png', 0, 0, 1981, 5),
(86, '1000586', 'Горячее', 9, 0, 167, 'Из мяса, из рыбы, из птицы, из лапши, из риса', '/data/images/menu/1422976743_4b4ebba90d183bfeefa21a3c9e4f638e.png', 0, 0, 1982, 6),
(87, '1000592', 'Десерты', 9, 0, 168, '', '/data/images/menu/1422978373_51c04de95c303f63e7be4d84a8c83181.png', 0, 0, 1983, 9),
(88, '1000593', 'Суши', 9, 0, 169, 'Нигири, гункан', '/data/images/menu/1422978280_8f4364d399467195595ae660d91ba6ad.png', 0, 0, 1984, 7),
(89, '1000594', 'Роллы', 9, 0, 170, 'Традиционные, теплые, спринг', '/data/images/menu/1422978329_d4346c2422c7c920872298c254f10537.png', 0, 0, 1985, 8),
(94, '1000621', '_Режим цены', 9, 0, 171, '', '', 0, 0, 0, 0),
(95, '1000624', '_ Бизнес-ланчи', 9, 0, 172, '', '', 0, 0, 0, 0),
(96, '1000626', '_Модификаторы', 9, 0, 173, '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_order`
--

CREATE TABLE IF NOT EXISTS `app_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_visit` varchar(255) NOT NULL,
  `device_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `date_create` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `attention` tinyint(1) NOT NULL DEFAULT '0',
  `separated` tinyint(1) NOT NULL DEFAULT '0',
  `separated_info` text NOT NULL,
  `date_execute` int(11) NOT NULL,
  `date_close` int(11) NOT NULL,
  `comment` text NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `garcon_id` int(11) NOT NULL,
  `count_items` int(11) NOT NULL,
  `total_price` float NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=135 ;

--
-- Дамп данных таблицы `app_order`
--

INSERT INTO `app_order` (`id`, `out_id`, `out_visit`, `device_id`, `table_id`, `status_id`, `date_create`, `payment_id`, `attention`, `separated`, `separated_info`, `date_execute`, `date_close`, `comment`, `restaurant_id`, `garcon_id`, `count_items`, `total_price`, `version`) VALUES
(110, '256', '655361045', 2, 34, 2, 1424194500, 2, 0, 0, '', 0, 0, '', 47, 36, 2, 760, 0),
(117, '', '', 3, 0, 1, 1428400023, 2, 0, 0, '', 0, 0, '', 47, 0, 3, 5710, 0),
(118, '', '', 3, 0, 1, 1428400623, 2, 0, 0, '', 0, 0, '', 47, 0, 1, 285, 0),
(119, '', '', 3, 0, 1, 1428400733, 2, 0, 0, '', 0, 0, '', 47, 0, 6, 1000, 0),
(120, '256', '2', 2, 34, 1, 1428401454, 1, 1, 0, '', 0, 0, '', 47, 36, 2, 760, 0),
(121, '', '', 3, 0, 1, 1428402674, 2, 0, 0, '', 0, 0, '', 47, 0, 1, 240, 0),
(122, '', '', 3, 0, 1, 1428402741, 2, 0, 0, '', 0, 0, '', 45, 0, 1, 240, 0),
(123, '', '', 3, 0, 1, 1428403409, 2, 0, 0, '', 0, 0, '', 45, 0, 1, 95, 0),
(124, '', '', 3, 0, 1, 1428403539, 2, 0, 0, '', 0, 0, '', 47, 0, 3, 430, 0),
(125, '256', '2', 3, 0, 1, 1428403932, 2, 0, 0, '', 0, 0, '', 47, 0, 2, 605, 0),
(126, '', '', 3, 0, 1, 1428403997, 2, 0, 0, '', 0, 0, '', 47, 0, 2, 265, 0),
(127, '', '', 3, 0, 1, 1428404100, 2, 0, 0, '', 0, 0, '', 47, 0, 1, 190, 0),
(128, '256', '655361054', 3, 0, 1, 1428404377, 2, 0, 0, '', 0, 0, '', 47, 0, 1, 240, 0),
(129, '256', '655361055', 3, 0, 1, 1428404399, 2, 0, 0, '', 0, 0, '', 47, 0, 1, 210, 0),
(130, '256', '655361056', 2, 34, 1, 1428404768, 1, 1, 0, '', 0, 0, '', 47, 36, 2, 760, 0),
(131, '256', '655361057', 2, 34, 2, 1428405218, 1, 1, 0, '', 0, 0, '', 47, 36, 2, 760, 0),
(132, '256', '655361058', 3, 0, 2, 1428405521, 2, 0, 0, '', 0, 0, '', 47, 0, 2, 580, 0),
(133, '256', '655361059', 3, 0, 2, 1428406139, 2, 0, 0, '', 0, 0, '', 47, 0, 2, 580, 0),
(134, '256', '655361060', 2, 34, 2, 1428407368, 1, 1, 0, '', 0, 0, '', 47, 36, 2, 760, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_order_item`
--

CREATE TABLE IF NOT EXISTS `app_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=194 ;

--
-- Дамп данных таблицы `app_order_item`
--

INSERT INTO `app_order_item` (`id`, `order_id`, `position_id`, `status_id`, `price`, `count`) VALUES
(160, 110, 225, 9, 240, 1),
(161, 110, 224, 9, 520, 1),
(162, 117, 239, 1, 1290, 1),
(163, 117, 240, 1, 920, 1),
(164, 117, 241, 1, 3500, 1),
(165, 118, 221, 1, 285, 1),
(166, 119, 226, 1, 440, 1),
(167, 119, 227, 1, 145, 2),
(168, 119, 228, 1, 90, 3),
(169, 120, 224, 1, 520, 1),
(170, 120, 225, 1, 240, 1),
(171, 121, 225, 1, 240, 1),
(172, 122, 225, 1, 240, 1),
(173, 123, 242, 1, 95, 1),
(174, 124, 243, 1, 210, 1),
(175, 124, 242, 1, 95, 1),
(176, 124, 244, 1, 125, 1),
(177, 125, 236, 1, 245, 1),
(178, 125, 237, 1, 360, 1),
(179, 126, 231, 1, 190, 1),
(180, 126, 232, 1, 75, 1),
(181, 127, 231, 1, 190, 1),
(182, 128, 225, 1, 240, 1),
(183, 129, 243, 1, 210, 1),
(184, 130, 224, 1, 520, 1),
(185, 130, 225, 1, 240, 1),
(186, 131, 224, 1, 520, 1),
(187, 131, 225, 1, 240, 1),
(188, 132, 229, 1, 150, 1),
(189, 132, 230, 1, 430, 1),
(190, 133, 229, 1, 150, 1),
(191, 133, 230, 1, 430, 1),
(192, 134, 224, 1, 520, 1),
(193, 134, 225, 1, 240, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_order_payment`
--

CREATE TABLE IF NOT EXISTS `app_order_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `app_order_payment`
--

INSERT INTO `app_order_payment` (`id`, `name`) VALUES
(1, 'Наличными'),
(2, 'Картой');

-- --------------------------------------------------------

--
-- Структура таблицы `app_order_status`
--

CREATE TABLE IF NOT EXISTS `app_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'order',
  `prev_id` tinyint(1) NOT NULL,
  `next_id` tinyint(1) NOT NULL,
  `roles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `app_order_status`
--

INSERT INTO `app_order_status` (`id`, `name`, `type`, `prev_id`, `next_id`, `roles`) VALUES
(1, 'В обработке', 'order', 0, 0, ''),
(2, 'Новый', 'order', 0, 0, 'admin,owner,manager'),
(3, 'Принят', 'order', 0, 0, 'admin,owner,manager'),
(4, 'Оплачен', 'order', 0, 0, 'admin,owner,manager,garcon'),
(5, 'Отменен', 'order', 0, 0, 'admin,owner,manager'),
(6, 'Закрыт', 'order', 0, 0, 'admin,owner,manager,garcon'),
(8, 'Новый', 'item', 0, 9, 'admin,owner,manager,garcon'),
(9, 'Принято', 'item', 8, 10, 'admin,owner,manager,garcon'),
(10, 'Готовится', 'item', 9, 11, 'admin,owner,manager,barman,cook'),
(11, 'Готово', 'item', 10, 12, 'admin,owner,manager,barman,cook'),
(12, 'Доставлено', 'item', 11, 0, 'admin,owner,manager,garcon');

-- --------------------------------------------------------

--
-- Структура таблицы `app_position`
--

CREATE TABLE IF NOT EXISTS `app_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `is_group` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `depressed` tinyint(1) NOT NULL DEFAULT '0',
  `label_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `calories` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `in_stock` varchar(500) NOT NULL,
  `food_type` tinyint(1) NOT NULL DEFAULT '1',
  `workplace` tinyint(1) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  `price_mode` int(11) NOT NULL,
  `measure` varchar(255) NOT NULL,
  `portion_weight` float NOT NULL,
  `count_accuracy` int(11) NOT NULL,
  `one_change_weight` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=272 ;

--
-- Дамп данных таблицы `app_position`
--

INSERT INTO `app_position` (`id`, `out_id`, `is_group`, `name`, `description`, `menu_id`, `sort`, `depressed`, `label_id`, `image`, `hidden`, `calories`, `deleted`, `in_stock`, `food_type`, `workplace`, `version`, `imageVersion`, `price`, `price_mode`, `measure`, `portion_weight`, `count_accuracy`, `one_change_weight`) VALUES
(8, '', 0, 'Калифорния рору', 'Мясо краба, авокадо, руккола', 1, 1, 0, 2, '/data/images/positions/1401927643_297cc5cc806b05ef6c055377170e81f8.jpeg', 0, '', 0, '3,4', 1, 1, 77, 3, 250, 0, '200 гр.', 0, 0, 0),
(9, '', 0, 'Сок апельсиновый', 'Пакетированный апельсиновый сок\r\n\r\n21313234', 2, 1, 0, 0, '/data/images/positions/e5c0c07963cc3d3d0b75bb704091eda2.jpeg', 0, '', 0, '', 2, 1, 0, 0, 150, 0, '1000 мл.', 0, 0, 0),
(17, '', 0, 'Абокадо ролл', 'Классический ролл с авокадо', 3, 1, 0, 0, '/data/images/positions/f0837c2e091cd50ce59b12b03a585bcd.jpeg', 0, '', 0, '', 1, 1, 0, 0, 95, 0, '8 шт.', 0, 0, 0),
(18, '', 0, 'Сякэ ролл', 'Классический ролл с лососем\r\n\r\nСостав:(рис, водоросли)', 3, 0, 0, 0, '/data/images/positions/1b63e7273a45181773e764dd46468ae7.jpeg', 0, '', 0, '3,4,6', 1, 1, 124, 0, 145, 0, '8 шт.', 0, 0, 0),
(19, '', 0, 'Калифорния', 'Ролл с мясом краба, авокадо, огурцом, тобико и майонезом', 3, 2, 0, 0, '/data/images/positions/89a1127cd2d987cc4918949480db00dc.jpeg', 0, '', 0, '', 1, 1, 0, 0, 355, 0, '1 шт.', 0, 0, 0),
(20, '', 0, 'Филадельфия', 'Ролл с лососем, мягким сыром, зеленым луком, кунжутом, огурцом и авокадо', 3, 3, 0, 0, '/data/images/positions/d3b5ac1126fd045fb69c591d789cd343.jpeg', 0, '', 0, '', 1, 1, 0, 0, 325, 0, '1 шт.', 0, 0, 0),
(21, '', 0, 'Каппа ролл', 'Классический ролл с огурцом', 3, 4, 0, 0, '/data/images/positions/83d648b0a2638c769651c334e5c5fad9.jpeg', 0, '', 0, '', 1, 1, 0, 0, 95, 0, '6 шт.', 0, 0, 0),
(22, '', 0, 'Гюнику татаки', 'Маринованная говядина с перцем и чесноком, дайконом, салатом, лимоном и луком, украшается петрушкой \r\n\r\n+ соус Каниши', 1, 2, 0, 2, '/data/images/positions/709d251f56858dff4da693b1a46337a2.jpeg', 0, '', 0, '', 1, 1, 0, 0, 235, 0, '1 шт.', 0, 0, 0),
(23, '', 0, 'Кимучи', 'Острый салат из капусты, моркови, яблока, лука-порей и дайкона', 1, 0, 0, 1, '/data/images/positions/1404307274_2fb3eec1550ba464fbd732c607568722.jpeg', 0, '', 0, '', 1, 1, 136, 4, 1, 0, '1 шт.', 0, 0, 0),
(24, '', 0, 'Сякэ усугири', 'Ломтики лосося с перцем чили и соусом "Юзу"', 1, 3, 0, 0, '/data/images/positions/cb43416d18773ec2bc624ae03e839a0c.jpeg', 0, '', 0, '', 1, 1, 98, 0, 260, 0, '1 шт.', 0, 0, 0),
(25, '', 0, 'Торинику соба', 'Гречневая лапша с курицей, морковью, кунжутом и соусом "Юзу-понзу"', 1, 4, 0, 0, '/data/images/positions/dacc7b13857bc54a87ce851b3734ede7.jpeg', 0, '', 0, '', 1, 1, 0, 0, 290, 0, '1 шт.', 0, 0, 0),
(26, '', 0, 'Спрайт', 'Газированный напиток Спрайт', 2, 0, 0, 0, '/data/images/positions/bfd78bb7f13c5cae729e72ea1ab008f9.jpeg', 0, '', 0, '', 2, 1, 120, 0, 110, 0, '1000 мл.', 0, 0, 0),
(27, '', 0, ' Перье', 'Минеральная вода Перье', 2, 2, 0, 0, '/data/images/positions/2bc6f91d3cef70ab4805a42e43829ebc.jpeg', 0, '', 0, '', 2, 1, 104, 0, 145, 0, '330 мл.', 0, 0, 0),
(28, '', 0, 'Кока-кола', 'Газированный напиток Кока-кола', 2, 3, 0, 0, '/data/images/positions/31ab3caf52db4a6f0cae3b9cebbb7407.jpeg', 0, '', 0, '', 2, 1, 105, 0, 90, 0, '250 мл.', 0, 0, 0),
(29, '', 0, 'Нести Лимон', 'Холодный чай Нести Лимон', 2, 4, 0, 0, '/data/images/positions/2461b677fe0d18d2f665937a687e23b2.jpeg', 0, '', 0, '', 2, 1, 106, 0, 120, 0, '1000 мл.', 0, 0, 0),
(30, '', 0, ' Рифу сарада', 'Кальмар, мидии, креветки с листьями салата корн, базиликом, кинзой и острым соусом', 4, 0, 0, 2, '/data/images/positions/c7d2f2cc2c8558421a038ca84e7e4e92.jpeg', 0, '', 0, '', 1, 1, 7, 0, 310, 0, '1 шт.', 0, 0, 0),
(31, '', 0, '«Цезарь-сан» с курицей', 'Салат с курицей, салатом айсберг, чесночными гренками, отварным яйцом, черри и сыром грана падано', 4, 0, 0, 0, '/data/images/positions/db6fda642133122a0a9390263e465106.jpeg', 0, '', 0, '', 1, 1, 0, 0, 245, 0, '1 шт.', 0, 0, 0),
(32, '', 0, 'Дайкани сарада', 'Салат с мясом краба, апельсином и тобико', 4, 0, 0, 0, '/data/images/positions/3033a1c3b159c0d0e7a3369353187302.jpeg', 0, '', 0, '', 1, 1, 0, 0, 360, 0, '1 шт.', 0, 0, 0),
(33, '', 0, 'Осэй сарада', 'Салат из пикантной курицы с салатом айсберг, сельдереем, авокадо, огурцами и сладким перцем', 4, 0, 0, 0, '/data/images/positions/7f2c9de3808410e00f15c0040849c4f1.jpeg', 0, '', 0, '', 1, 1, 0, 0, 235, 0, '1 шт.', 0, 0, 0),
(34, '', 0, 'Гюнику сарада', 'Говядина с томатами, огурцом и зелёным луком', 4, 0, 0, 0, '/data/images/positions/dc407fad2b77d7b9ca81fcdca7b576d8.jpeg', 0, '', 0, '', 1, 1, 0, 0, 290, 0, '1 шт.', 0, 0, 0),
(35, '', 0, 'Тануки сет', 'Сяке темпура - 1\r\nФиладельфия -1\r\nЭби сирогома -1\r\nСяке кадо - 1\r\nХотате караи - 1\r\nПорция васаби XL (30г) - 1\r\nПорция имбиря XL (60г) - 1', 6, 0, 0, 0, '/data/images/positions/75322a8622bee9eaba6e554ded571ec7.jpeg', 0, '', 0, '', 1, 1, 0, 0, 1230, 0, '1 шт.', 0, 0, 0),
(36, '', 0, 'Харакири сет', 'Дракон ролл - 1\r\nОвара ролл - 1\r\nЙонака ролл - 1\r\nОкинава ролл - 1\r\nКалифорния - 2\r\nФиладельфия - 1\r\nХигата ролл - 1\r\nМексиканский ролл - 1\r\nХияши унаги - 1\r\nУнаги урамаки - 1\r\nПорция васаби XL (30 г) - 2\r\nПорция имбиря XL (60 г) - 2\r\n', 6, 0, 0, 0, '/data/images/positions/04279f8326e750e85a75a50571878969.jpeg', 0, '300', 0, '', 1, 1, 0, 0, 2790, 0, '1 шт.', 0, 0, 0),
(37, '', 0, 'Суши сет', 'Суши с угрем - 5\r\nСуши с лососем - 5\r\nСуши с тунцом - 5\r\nСуши с лакедрой - 2\r\nПорция васаби (10г) - 2\r\nПорция имбиря (20г) - 2\r\n', 6, 0, 0, 0, '/data/images/positions/251fdbc662899a16ed50ca94cc548864.jpeg', 0, '', 0, '', 1, 1, 0, 0, 1110, 0, '1 шт.', 0, 0, 0),
(38, '', 0, 'Якинику мориавасэ', 'Ассорти мясное: шашлычки из куриного фарша, говядины, свинины и кукурузы', 6, 0, 0, 0, '/data/images/positions/7981d6b7a38d03d12b639fc0d9fa7c1e.jpeg', 0, '', 0, '', 1, 1, 0, 0, 310, 0, '1 шт.', 0, 0, 0),
(39, '', 0, 'Сифудо мориавасэ', 'Ассорти из морепродуктов: шашлычки из креветок, морского гребешка, лосося и осьминожек', 6, 0, 0, 0, '/data/images/positions/a04ea60470ca58ddfc36804ab98ce963.jpeg', 0, '', 0, '', 1, 1, 0, 0, 440, 0, '1 шт.', 0, 0, 0),
(40, '', 0, 'Сякэ', 'Суши нигири с лососем', 7, 1, 0, 0, '/data/images/positions/76a954a2f113df9109946ce5c9d10250.jpeg', 0, '', 0, '', 1, 1, 0, 0, 35, 0, '1 шт.', 0, 0, 0),
(41, '', 0, 'Хотатэ', 'Суши нигири с морским гребешком', 7, 2, 0, 0, '/data/images/positions/cdf8a124550b9cf2c472c5c5dcbd6b2d.jpeg', 0, '', 0, '', 1, 1, 0, 0, 75, 0, '1 шт.', 0, 0, 0),
(42, '', 0, 'Эби', 'Суши нигири с вареной сладкой креветкой, маринованной в имбирном соусе', 7, 0, 0, 0, '/data/images/positions/b4e6e929f2c157942f4c90bede5e707b.jpeg', 0, '', 0, '', 1, 1, 0, 0, 70, 0, '1 шт.', 0, 0, 0),
(43, '', 0, 'Икура', 'Суши гункан с икрой лосося', 7, 3, 0, 0, '/data/images/positions/d4b9f8240ae9d09674b50e5276aeece5.jpeg', 0, '', 0, '', 1, 1, 0, 0, 90, 0, '1 шт.', 0, 0, 0),
(44, '', 0, 'Спайси сякэ', 'Острый суши гункан с лососем', 7, 4, 0, 0, '/data/images/positions/4780ead67ef309276efd0327436c4cef.jpeg', 0, '', 0, '', 1, 1, 0, 0, 80, 0, '1 шт.', 0, 0, 0),
(48, '', 0, 'Напиток для ланча 1', '', 2, 5, 1, 0, '', 0, '', 0, '4', 2, 1, 107, 0, 150, 0, '500 мл.', 0, 0, 0),
(49, '', 0, 'Напиток для ланча 2', '', 2, 6, 1, 0, '', 0, '', 0, '3,4', 2, 1, 108, 0, 50, 0, '150 мл.', 0, 0, 0),
(51, '', 0, 'Вареники с картошкой', 'Домашние вареники ручной лепки с картофелем. Подаются со сметаной.', 9, 1, 0, 0, '/data/images/positions/1403777530_9f2df9e3815e570d992c3d3f9066ecd3.jpeg', 0, '', 0, '7', 1, 1, 1, 0, 115, 0, 'Основная', 0, 0, 0),
(52, '', 0, 'Сибирские пельмени', 'Домашние пельмени, ручной лепки, с нежным мясом. Подаются со сметаной.', 9, 0, 0, 0, '/data/images/positions/1403777673_7f785013ba9c808fd9d1569cea05a442.jpeg', 0, '', 0, '7', 1, 1, 0, 0, 150, 0, 'Основная', 0, 0, 0),
(53, '', 0, 'Жареная картошечка с грибами', 'Вкус знакомый с детства, настоящая картошка с грибами по домашнему.', 9, 2, 0, 0, '/data/images/positions/1403777736_14e2e8157345fb67f3c3abbca2563b00.jpeg', 0, '', 0, '7', 1, 1, 0, 0, 220, 0, 'Основная', 0, 0, 0),
(54, '', 0, 'Домашние котлеты с курицей', 'Домашние котлеты подаются с картофелем и соусом.', 9, 3, 0, 0, '/data/images/positions/1403777806_fbe1fa3d940d51939a8037f9a9ee4641.jpeg', 0, '', 0, '7', 1, 1, 0, 0, 245, 0, 'Основная', 0, 0, 0),
(55, '', 0, 'Ао чиз', 'крем-суп из голубого сыра, с креветками', 10, 0, 0, 0, '/data/images/positions/1403778356_4eb753196945f76f362eb00e77b04036.jpeg', 0, '', 0, '8', 1, 1, 0, 0, 247, 0, 'Основная', 0, 0, 0),
(56, '', 0, 'Янагава набэ', 'набэ из копченого угря', 10, 0, 0, 0, '/data/images/positions/1403778489_21cb40c0718c77d529d29825a946a7fb.jpeg', 0, '', 0, '8', 1, 1, 0, 0, 299, 0, 'Основная', 0, 0, 0),
(57, '', 0, 'Том Ям набэ', 'традиционный кокосовый суп с креветками и грибами. Сервируется рисом "Гохан"', 10, 0, 0, 0, '/data/images/positions/1403778545_dfa04c8886ee9729a5378a8063be92d8.jpeg', 0, '', 0, '', 1, 1, 0, 0, 287, 0, 'Основная', 0, 0, 0),
(58, '', 0, 'Нику рамэн', 'суп-лапша со свининой и яйцом', 10, 0, 0, 0, '/data/images/positions/1403778650_c0fdcdd3a8f8f4ff71504a7e34659070.jpeg', 0, '', 0, '8', 1, 1, 0, 0, 237, 0, 'Основная', 0, 0, 0),
(59, '', 0, 'Борщ', '', 12, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0, 100, 0, 'Основная', 0, 0, 0),
(60, '', 1, 'Бизнес ланч 1', '', 12, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0, 250, 0, 'комбо', 0, 0, 0),
(63, '', 0, 'Борщ1', '', 14, 0, 0, 0, '', 0, '', 0, '', 1, 1, 1, 0, 100, 0, '1 шт.', 0, 0, 0),
(64, '', 1, 'Лайт', '', 13, 0, 0, 0, '', 0, '', 0, '', 1, 1, 134, 0, 250, 0, 'Общая стоимость', 0, 0, 0),
(65, '', 0, 'рпг', '', 13, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0, 20, 0, '1 шт.', 0, 0, 0),
(66, '', 0, '345345', '', 18, 0, 0, 0, '', 0, '', 0, '', 1, 1, 1, 1, 345, 0, '1 шт.', 0, 0, 0),
(67, '', 1, '123', '', 13, 0, 0, 0, '', 0, '', 0, '', 1, 0, 123, 5, 123, 0, 'Общая стоимость', 0, 0, 0),
(142, '1000019', 0, 'Кола', '', 44, 0, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 1, 4561, 0, 150, 1, '1 шт.', 0, 0, 0),
(143, '1000020', 0, 'Фанта', '', 44, 1, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 1, 4562, 0, 98, 1, '1 шт.', 0, 0, 0),
(144, '1000064', 0, 'Бизнес-ланч', '', 45, 2, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4563, 0, 0, 1, '', 0, 0, 0),
(145, '1000074', 0, 'Ланч 3 позиции', '', 46, 3, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4564, 0, 0, 1, '', 0, 0, 0),
(146, '1000079', 0, 'Овощной', '', 47, 4, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4565, 0, 0, 1, '', 0, 0, 0),
(147, '1000080', 0, 'Мясной', '', 47, 5, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4566, 0, 0, 1, '', 0, 0, 0),
(148, '1000081', 0, 'Рыбный', '', 47, 6, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4567, 0, 0, 1, '', 0, 0, 0),
(149, '1000083', 0, 'Борщ', '', 48, 7, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4568, 0, 0, 1, '', 0, 0, 0),
(150, '1000084', 0, 'Уха', '', 48, 8, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4569, 0, 0, 1, '', 0, 0, 0),
(151, '1000085', 0, 'Харчо', '', 48, 9, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4570, 0, 0, 1, '', 0, 0, 0),
(152, '1000087', 0, 'Молочный коктель', '', 49, 10, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4571, 0, 0, 1, '', 0, 0, 0),
(153, '1000088', 0, 'Морс', '', 49, 11, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4572, 0, 0, 1, '', 0, 0, 0),
(154, '1000089', 0, 'Чай', '', 49, 12, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4573, 0, 0, 1, '', 0, 0, 0),
(155, '1000105', 0, 'Мороженое', '', 50, 13, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4574, 0, 0, 1, '', 0, 0, 0),
(156, '1000110', 0, 'Тирамису', '', 50, 14, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4575, 0, 0, 1, '', 0, 0, 0),
(157, '1000111', 0, 'Медовик', '', 50, 15, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4576, 0, 0, 1, '', 0, 0, 0),
(158, '1000113', 0, 'Греческий', '', 51, 16, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4577, 0, 0, 1, '', 0, 0, 0),
(159, '1000114', 0, 'Цезарь', '', 51, 17, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4578, 0, 0, 1, '', 0, 0, 0),
(160, '1000175', 0, 'Пиво', '', 44, 18, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4579, 0, 0, 1, '', 0, 0, 0),
(161, '1000190', 0, 'Ланч 4 позиции', '', 46, 19, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4580, 0, 0, 1, '', 0, 0, 0),
(162, '1000192', 0, 'Котлеты по киевски', '', 52, 20, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4581, 0, 0, 2, 'кг', 1000, 3, 0),
(163, '1000193', 0, 'Рагу из овощей', '', 52, 21, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4582, 0, 0, 1, '', 0, 0, 0),
(164, '1000208', 0, '"Медальон"', '', 53, 22, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4583, 0, 0, 1, '', 0, 0, 0),
(165, '1000209', 0, 'Мясо "По- мексикански"', '', 53, 23, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4584, 0, 0, 4, 'л', 5000, 3, 0),
(166, '1000210', 0, 'Шашлык из свинины', '', 53, 24, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4585, 0, 0, 1, '', 0, 0, 0),
(167, '1000211', 0, 'Говядина «Фламбэ» с белыми грибами', '', 53, 25, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4586, 0, 0, 1, '', 0, 0, 0),
(168, '1000212', 0, 'Стейк говяжий на решетке', '', 53, 26, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4587, 0, 0, 3, '', 4000, 3, 0),
(169, '1000213', 0, '"Капрезе"', '', 51, 27, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4588, 0, 0, 1, '', 0, 0, 0),
(170, '1000214', 0, '"Экзотика"', '', 51, 28, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4589, 0, 0, 1, '', 0, 0, 0),
(171, '1000215', 0, '"Парма ди Руккола"', '', 51, 29, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4590, 0, 0, 1, '', 0, 0, 0),
(172, '1000216', 0, 'Штрудель яблочный', '', 50, 30, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4591, 0, 0, 1, '', 0, 0, 0),
(173, '1000217', 0, 'Салат "Фруктовый"', '', 50, 31, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4592, 0, 0, 1, '', 0, 0, 0),
(174, '1000218', 0, 'Соки "RICH" в ассортименте	200 мл.', '', 44, 32, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4593, 0, 0, 1, '', 0, 0, 0),
(175, '1000219', 0, 'Брусничный морс', '', 44, 33, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4594, 0, 0, 1, '', 0, 0, 0),
(176, '1000220', 0, 'Акваминерале', '', 44, 34, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4595, 0, 0, 1, '', 0, 0, 0),
(177, '1000222', 0, 'Борщ "Московский"', '', 54, 35, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4596, 0, 0, 1, '', 0, 0, 0),
(178, '1000223', 0, 'Солянка по-домашнему', '', 54, 36, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4597, 0, 0, 1, '', 0, 0, 0),
(179, '1000224', 0, 'Уха ростовская', '', 54, 37, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4598, 0, 0, 1, '', 0, 0, 0),
(180, '1000225', 0, 'Крем-суп с белыми грибами', '', 54, 38, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4599, 0, 0, 1, '', 0, 0, 0),
(181, '1000226', 0, '"Сицилия"', '', 54, 39, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4600, 0, 0, 1, '', 0, 0, 0),
(182, '1000228', 0, 'Сыр в кляре', '', 55, 40, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4601, 0, 0, 1, '', 0, 0, 0),
(183, '1000229', 0, 'Крылышки куриные на углях', '', 55, 41, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4602, 0, 0, 1, '', 0, 0, 0),
(184, '1000230', 0, 'Креветки тигровые в кляре', '', 55, 42, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4603, 0, 0, 1, '', 0, 0, 0),
(185, '1000231', 0, 'Копченый сыр к пиву', '', 55, 43, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4604, 0, 0, 2, 'кг', 2000, 3, 0),
(186, '1000232', 0, 'Гренки острые с сыром', '', 55, 44, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4605, 0, 0, 2, 'кг', 123000, 3, 0),
(187, '1000234', 0, '"Ахтамар" 0,050', '', 56, 45, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4606, 0, 0, 1, '', 0, 0, 0),
(188, '1000235', 0, '"Арарат" 0,050', '', 56, 46, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4607, 0, 0, 1, '', 0, 0, 0),
(189, '1000236', 0, '"Remy Martin" V.S. 0,050', '', 56, 47, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4608, 0, 0, 1, '', 0, 0, 0),
(190, '1000237', 0, '"Hennessy" V.S. 0,050', '', 57, 48, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4609, 0, 0, 1, '', 0, 0, 0),
(191, '1000238', 0, '"Праздничный" 0,050', '', 56, 49, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4610, 0, 0, 1, '', 0, 0, 0),
(192, '1000240', 0, '"Miller" 0,33', '', 58, 50, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4611, 0, 0, 1, '', 0, 0, 0),
(193, '1000241', 0, '"Corona Extra" 0,33', '', 58, 51, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4612, 0, 0, 1, '', 0, 0, 0),
(194, '1000242', 0, '"Beck''s" безалкогольное 0,33', '', 58, 52, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4613, 0, 0, 1, '', 0, 0, 0),
(195, '1000243', 0, '"Hoegaarden" 0,5', '', 58, 53, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4614, 0, 0, 1, '', 0, 0, 0),
(196, '1000244', 0, 'Сибирская Корона 0,5', '', 58, 54, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4615, 0, 0, 1, '', 0, 0, 0),
(197, '1000305', 0, 'Новый напиток', '', 44, 55, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4616, 0, 0, 1, '', 0, 0, 0),
(198, '1000306', 0, 'Новый напиток', '', 44, 56, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4617, 0, 0, 1, '', 0, 0, 0),
(199, '1000347', 0, 'модикомбо', '', 59, 57, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4618, 0, 0, 1, '', 0, 0, 0),
(200, '1000391', 0, '111', '', 59, 58, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4619, 0, 0, 1, '', 0, 0, 0),
(201, '1000404', 0, 'вкусняшка', '', 54, 59, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4620, 0, 0, 1, '', 0, 0, 0),
(202, '1000469', 0, '111', '', 60, 60, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4621, 0, 0, 1, '', 0, 0, 0),
(203, '1000475', 0, 'приветик', '', 56, 61, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4622, 0, 0, 1, '', 0, 0, 0),
(204, '1000482', 0, 'хахаха', '', 56, 62, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4623, 0, 0, 1, '', 0, 0, 0),
(205, '1000483', 0, 'xxx', '', 56, 63, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4624, 0, 0, 1, '', 0, 0, 0),
(206, '1000484', 0, 'куку черновик', '', 56, 64, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4625, 0, 0, 1, '', 0, 0, 0),
(207, '1000493', 0, 'комбо', '', 61, 65, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4626, 0, 0, 1, '', 0, 0, 0),
(208, '1000503', 0, 'Фокус тест', '', 62, 66, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4627, 0, 0, 1, '', 0, 0, 0),
(209, '1000520', 0, 'Новый десерт', '', 50, 67, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4628, 0, 0, 1, '', 0, 0, 0),
(210, '1000521', 0, 'Блюдо1', '', 61, 68, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4629, 0, 0, 1, '', 0, 0, 0),
(211, '1000529', 0, 'Блюдо2', '', 61, 69, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4630, 0, 0, 1, '', 0, 0, 0),
(212, '1000552', 0, 'Водка', '', 78, 70, 0, 0, '', 0, '', 1, '43,44,45,46,47', 2, 0, 4631, 0, 100, 1, '1 шт.', 0, 0, 0),
(213, '1000553', 0, 'Пепси', '', 77, 71, 0, 0, '', 0, '', 1, '43,44,45,46,47', 2, 0, 4632, 0, 55, 1, '1 шт.', 0, 0, 0),
(214, '1000564', 0, 'Эспрессо классический', '', 80, 72, 0, 0, '', 0, '', 1, '43,44,45,46,47', 2, 0, 4633, 0, 1, 1, '1 шт.', 0, 0, 0),
(215, '1000565', 0, 'Ристретто', '', 80, 73, 0, 0, '', 0, '', 1, '43,44,45,46,47', 2, 0, 4634, 0, 1, 1, '1 шт.', 0, 0, 0),
(216, '1000566', 0, 'Эспрессо Романо', '', 80, 74, 0, 0, '', 0, '', 1, '43,44,45,46,47', 2, 0, 4635, 0, 1, 1, '1 шт.', 0, 0, 0),
(217, '1000567', 0, 'Дабл Капучино сливочный «Амереттини-кара', '', 80, 75, 0, 0, '', 0, '', 1, '43,44,45,46,47', 2, 0, 4636, 0, 1, 1, '1 шт.', 0, 0, 0),
(218, '1000569', 0, 'Блинчики 2 штуки, без начинки', '', 81, 76, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4637, 0, 0, 1, '', 0, 0, 0),
(219, '1000570', 0, 'Сырники со сметаной и ягодами', '', 81, 77, 0, 0, '', 0, '', 1, '43,44,45,46,47', 1, 0, 4638, 0, 0, 1, '', 0, 0, 0),
(220, '1000572', 0, 'Стеклянная лапша в соусе «Тай-вок»', 'Калории: 660 kcal\r\nВес: 360 г', 82, 78, 0, 0, '/data/images/positions/1422968051_d22e24b2e499d3349d2ca524c6df58d6.jpeg', 0, '', 1, '43,44,45,46,47', 1, 1, 4639, 1, 299, 1, '1 шт.', 0, 0, 0),
(221, '1000574', 0, 'Тако беби сарада', 'Осьминоги с салатом «Чука»', 83, 79, 0, 0, '/data/images/positions/1422969177_ada6cf5e96f758bf1eb4a326ea34188b.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4640, 2, 285, 1, '1 шт.', 0, 0, 0),
(222, '1000575', 0, 'Кагаяки сарада', 'Краб сурими с кукурузой, рисом и тобико с пикантным соусом', 83, 80, 0, 0, '/data/images/positions/1422969325_46f8a1ae3c1a8a9a164e61528577e3da.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4641, 3, 240, 1, '1 шт.', 0, 0, 0),
(223, '1000576', 0, 'Тори но маринадо', 'Курица, обжаренная в сливках и соевом соусе, с фасолью, сладким перцем и зелёным луком', 83, 81, 0, 0, '/data/images/positions/1422969694_49b35646513e413edb52c9ad1755a7d3.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4642, 4, 190, 1, '1 шт.', 0, 0, 0),
(224, '1000578', 0, 'Кроненбург Блан', 'Пиво нефильтрованное 1000мл', 84, 0, 0, 0, '/data/images/positions/1422970150_47c3028f01611f60e8152fcf314aa24b.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4643, 5, 520, 1, '1 шт.', 0, 0, 0),
(225, '1000580', 0, 'Канраку сиру', 'Сырный суп с копчёной курицей, беконом, томатами черри и шампиньонами', 85, 83, 0, 0, '/data/images/positions/1422970772_5259e08ce22df10f341c87e9da1b4219.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4644, 6, 240, 1, '1 шт.', 0, 0, 0),
(226, '1000581', 0, 'Фреш (Яблоко-сельдерей-морковь)', 'Свежевыжатый сок яблоко-сельдерей-морковь', 80, 84, 0, 0, '/data/images/positions/1422974935_ccb6e12ea6389dde52783bd16b8aa02c.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4645, 7, 440, 1, '1 шт.', 0, 0, 0),
(227, '1000582', 0, 'Перье (с газом)', 'Производство Франция. Минеральная вода с газом Перье', 80, 85, 0, 0, '/data/images/positions/1422975540_beebe5914eb4ef644e66a97cc814a6bd.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4646, 9, 145, 1, '1 шт.', 0, 0, 0),
(228, '1000583', 0, 'Кока-кола Лайт', 'Производство Россия. Газированный напиток Кока-кола Лайт', 80, 86, 0, 0, '/data/images/positions/1422975404_dc0614c8be3692180f4beb198f55b147.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4647, 8, 90, 1, '1 шт.', 0, 0, 0),
(229, '1000584', 0, 'Сок апельсиновый', 'Производство Россия. Пакетированный апельсиновый сок', 80, 87, 0, 0, '/data/images/positions/1422975594_566d29c93685f6b47cc2a9e2b50eeab0.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4648, 10, 150, 1, '1 шт.', 0, 0, 0),
(230, '1000585', 0, 'Морс клюквенный', 'Домашний клюквенный морс', 80, 88, 0, 0, '/data/images/positions/1422975645_f7930524591cd98786545c8de3a2ace4.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4649, 11, 430, 1, '1 шт.', 0, 0, 0),
(231, '1000587', 0, 'Тамагояки тяхан', 'Жареный рис с беконом, яйцом и чесноком', 86, 89, 0, 0, '/data/images/positions/1422976824_367d262ad584481734fa4e4ad799b8b6.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4650, 12, 190, 1, '1 шт.', 0, 0, 0),
(232, '1000588', 0, 'Гохан', 'Отварной рис по-японски', 86, 90, 0, 0, '/data/images/positions/1422976996_01f43d5ea8673c535e5a27b8bbb0e1af.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4651, 13, 75, 1, '1 шт.', 0, 0, 0),
(233, '1000589', 0, 'Унайдзю', 'Копчёный угорь на рисе', 86, 91, 0, 0, '/data/images/positions/1422977117_5b443ea6c56884c65b8642e80ac8099c.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4652, 14, 470, 1, '1 шт.', 0, 0, 0),
(234, '1000590', 0, 'Дорадо', 'Дорадо с соусом «Тар-тар»', 86, 92, 0, 0, '/data/images/positions/1422977227_f4b1b887bd5169c8dd9a7b274707ee76.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4653, 15, 510, 1, '1 шт.', 0, 0, 0),
(235, '1000591', 0, 'Киниро сякэ', 'Стейк из сёмги под остро-сладким соусом, с овощами и мини-картофелем', 86, 93, 0, 0, '/data/images/positions/1422977315_4d6fcf5191245ea8f2e5305bc56b06a9.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4654, 16, 510, 1, '1 шт.', 0, 0, 0),
(236, '1000595', 0, 'Хияши вакамэ', 'Салат «Чука» с ореховым соусом', 83, 94, 0, 0, '/data/images/positions/1422978539_f429f1dd59a0f1d3c5d31d1ce6ce4f3e.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4655, 17, 245, 1, '1 шт.', 0, 0, 0),
(237, '1000596', 0, 'Дайкани сарада', 'Мясо краба, сурими с апельсином и тобико', 83, 95, 0, 0, '/data/images/positions/1422978641_48ffabf63fdaaffb4394a841eb2ad825.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4656, 18, 360, 1, '1 шт.', 0, 0, 0),
(238, '1000597', 0, 'Саке нефильтрованное', 'Саке производится на основе местной родниковой воды. Ферментация протекает при низких температурах. После окончания ферментации саке проходит грубый отжим, в результате которого часть осадка попадает в саке. Выдерживается в стальных эмалированных чанах в течение 6 месяцев.\r\nНефильтрованное саке с фруктовыми нотами.\r\nПрекрасно сочетается с японской кухней. Также с азиатской и средиземноморской. Саке прекрасно сочетается с любыми рыбными блюдами.\r\n12°С-14°С\r\n14.90%', 84, 1, 0, 0, '/data/images/positions/1422979087_86d0f04a233a0cdb8682c7190b6cb397.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4657, 19, 930, 1, '1 шт.', 0, 0, 0),
(239, '1000598', 0, 'Сушивайн белое', 'Производство Франция. Вино Сушивайн белое, обладает лёгким цветочным ароматом с нотами яблока и грейпфрута, специально созданное для того, чтобы подчеркнуть тонкий, нежный и свежий вкус традиционных японских блюд', 84, 2, 0, 0, '/data/images/positions/1422979310_4ee1613f44f427cf4441b314c605201b.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4658, 20, 1290, 1, '1 шт.', 0, 0, 0),
(240, '1000599', 0, 'Фрескелло Бьянко', 'Производство Италия. Тонкий фруктовый аромат, легкий сбалансированный вкус с освежающим послевкусием.', 84, 3, 0, 0, '/data/images/positions/1422979453_e45d8cb491b4ec40c1fc7e2f2dda2812.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4659, 21, 920, 1, '1 шт.', 0, 0, 0),
(241, '1000600', 0, 'Сантори Олд', 'Производство Япония. Виски Сантори Олд', 84, 4, 0, 0, '/data/images/positions/1422979625_9747b7461931c7a765b8b79a885e09e5.jpeg', 0, '', 0, '43,44,45,46,47', 2, 0, 4660, 22, 3500, 1, '1 шт.', 0, 0, 0),
(242, '1000601', 0, 'Мисо сиру', 'Суп из соевой пасты мисо с тофу и водорослями', 85, 100, 0, 0, '/data/images/positions/1422980027_328127bd011d45947767cb6198e4cc49.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4661, 24, 95, 1, '1 шт.', 0, 0, 0),
(243, '1000602', 0, 'Сякэ тядзукэ', 'Рисовый суп с лососем, водорослями нори и кунжутом', 85, 101, 0, 0, '/data/images/positions/1422980073_a598b2878b04dee6f1a363542fe6b580.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4662, 25, 210, 1, '1 шт.', 0, 0, 0),
(244, '1000603', 0, 'Эби чили мисо', 'Острый суп из соевой пасты мисо с креветкой', 85, 102, 0, 0, '/data/images/positions/1422980102_6b3c5be348587f0e3868ff7ff5c0cc76.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4663, 26, 125, 1, '1 шт.', 0, 0, 0),
(245, '1000604', 0, 'Унаги сиру', 'Суп с угрём, стеклянной лапшой, шампиньонами и яйцом', 85, 103, 0, 0, '/data/images/positions/1422979994_ad8351a0f296052663abeb64bf925680.jpeg', 0, '', 0, '43,44,45,46,47', 1, 1, 4664, 23, 290, 1, '1 шт.', 0, 0, 0),
(246, '1000605', 0, 'Сякэ', 'Лосось', 88, 104, 0, 0, '/data/images/positions/1422980432_ec0282b20a01cda17fa01bccc10e280a.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4665, 27, 70, 1, '1 шт.', 0, 0, 0),
(247, '1000606', 0, 'Хотатэ', 'Морской гребешок', 88, 105, 0, 0, '/data/images/positions/1422980472_1472bc356f5e6b1f708f57189c1273d2.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4666, 28, 85, 1, '1 шт.', 0, 0, 0),
(248, '1000607', 0, 'Эби', 'Креветка', 88, 106, 0, 0, '/data/images/positions/1422980500_33c4b825558eed45504ef46859d3e30d.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4667, 29, 80, 1, '1 шт.', 0, 0, 0),
(249, '1000608', 0, 'Икура', 'Красная икра', 88, 107, 0, 0, '/data/images/positions/1422980546_c63b5a6c82e6bb63a6ef0a702fe09fd1.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4668, 30, 100, 1, '1 шт.', 0, 0, 0),
(250, '1000609', 0, 'Инари Унаги', 'Копчёный угорь, тофу, острый соус', 88, 108, 0, 0, '/data/images/positions/1422980586_066216e22c2d01b84cc648cda6b93d36.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4669, 31, 145, 1, '1 шт.', 0, 0, 0),
(251, '1000610', 0, 'Филадельфия', 'С лососем, огурцом, авокадо и мягким сыром', 89, 109, 0, 0, '/data/images/positions/1422981985_c19815b118b4db855490cc663ca61751.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4670, 33, 360, 1, '1 шт.', 0, 0, 0),
(252, '1000611', 0, 'Калифорния', 'С мясом краба, авокадо, огурцом и тобико', 89, 110, 0, 0, '/data/images/positions/1422982023_0481ebfdd5ef1262ebbdb66a704170da.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4671, 34, 385, 1, '1 шт.', 0, 0, 0),
(253, '1000612', 0, 'Сякэ кадо', 'С лососем, угрём, красной икрой, мягким сыром и огурцом', 89, 111, 0, 0, '/data/images/positions/1422982083_ade200dc0c2fab614dd5b18b2aced842.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4672, 35, 310, 1, '1 шт.', 0, 0, 0),
(254, '1000613', 0, 'Дракон ролл', 'С угрём, авокадо, мягким сыром и тобико', 89, 112, 0, 0, '/data/images/positions/1422982117_04c75f1905a8645358cf82a97c02ba4e.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4673, 36, 295, 1, '1 шт.', 0, 0, 0),
(255, '1000614', 0, 'Изуми спринг ролл', 'Морской окунь, шпинат, сыр, зеленый лук и шампиньоны в хрустящем тесте, с соусом «Каниши»', 89, 113, 0, 0, '/data/images/positions/1422981941_a6ba8b9ccf294dd19a99addb63c4c12f.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4674, 32, 290, 1, '1 шт.', 0, 0, 0),
(256, '1000615', 0, 'Моджи "Манго"', 'Традиционное японское мороженое со вкусом манго в тонком рисовом тесте.\r\n8 шт', 87, 0, 0, 0, '/data/images/positions/1422982472_6eddaa6a8b579bb4759a1cedf0ecad77.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4675, 37, 920, 1, '1 шт.', 0, 0, 0),
(257, '1000616', 0, 'Тирамису', 'Десерт из сыра маскарпоне и бисквитного печеньем савоярди, пропитанного кофе и коньяком', 87, 1, 0, 0, '/data/images/positions/1422982504_bf7dab300173beb83589baa4706097ce.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4676, 38, 230, 1, '1 шт.', 0, 0, 0),
(258, '1000617', 0, 'Куро ёру', 'Ролл с киви, ананасом, клубникой, бананом, миндалём и сырным кремом', 87, 2, 0, 0, '/data/images/positions/1422982547_f67f6a5b69851015c6483b458e2a6c80.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4677, 39, 210, 1, '1 шт.', 0, 0, 0),
(259, '1000618', 0, 'Итиго панкэки', 'Блинный торт с заварным кремом и клубничным соусом с ликёром', 87, 3, 0, 0, '/data/images/positions/1422982576_f474d79f8168896b9f19292ecd6067cd.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4678, 40, 185, 1, '1 шт.', 0, 0, 0),
(260, '1000619', 0, 'Пайнаппуру', 'Кусочки свежего ананаса', 87, 4, 0, 0, '/data/images/positions/1422982608_f7e42536eac45cbc114a5c5d3cb1ecfc.jpeg', 0, '', 0, '43,44,45,46,47', 1, 2, 4679, 41, 295, 1, '1 шт.', 0, 0, 0),
(261, '1000620', 0, 'Цезарь', '', 83, 119, 0, 0, '', 0, '', 0, '43,44,45,46,47', 1, 0, 4680, 0, 0, 1, '', 0, 0, 0),
(267, '1000623', 0, 'Блюдо штучное (целое)', '', 94, 120, 0, 0, '', 0, '', 0, '43,44,45,46,47', 1, 0, 0, 0, 0, 1, 'штук', 500, 0, 0),
(268, '1000625', 0, 'Блюдо штучное (дробное)', '', 94, 121, 0, 0, '', 0, '', 0, '43,44,45,46,47', 1, 0, 0, 0, 0, 1, '', 0, 1, 0),
(269, '1000627', 0, 'Блюдо порционное за вес', '', 94, 122, 0, 0, '', 0, '', 0, '43,44,45,46,47', 1, 0, 0, 0, 0, 2, 'кг', 100, 3, 1),
(270, '1000628', 0, 'Блюдо дозируемое за ст. дозу', '', 94, 123, 0, 0, '', 0, '', 0, '43,44,45,46,47', 1, 0, 0, 0, 0, 4, 'л', 5000, 3, 0),
(271, '1000629', 0, 'Блюдо порционное за ст. порцию', '', 94, 124, 0, 0, '', 0, '', 0, '43,44,45,46,47', 1, 0, 0, 0, 0, 3, 'кг.', 100, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_position_groups`
--

CREATE TABLE IF NOT EXISTS `app_position_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `app_position_groups`
--

INSERT INTO `app_position_groups` (`id`, `out_id`, `company_id`, `name`) VALUES
(1, '', 1, 'Первое'),
(2, '', 1, 'Второе'),
(3, '', 1, 'Десерт');

-- --------------------------------------------------------

--
-- Структура таблицы `app_restaurant`
--

CREATE TABLE IF NOT EXISTS `app_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `working_hours` varchar(255) NOT NULL,
  `map` varchar(500) NOT NULL,
  `photos` varchar(500) NOT NULL,
  `version` int(11) NOT NULL,
  `imageVersion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Дамп данных таблицы `app_restaurant`
--

INSERT INTO `app_restaurant` (`id`, `out_id`, `deleted`, `name`, `location`, `hidden`, `company_id`, `description`, `phone`, `working_hours`, `map`, `photos`, `version`, `imageVersion`) VALUES
(3, '', 0, 'Борисовские пруды', 'г. Москва, ул Борисовские Пруды, д 10', 0, 1, 'Лучший ресторан', '+7 (555) 100-02-30', 'Пн-Пт 10:00-23:00', '{"zoom":11,"coords":{"x":55.635869,"y":37.740974},"place":{"city":"7700000000000","street":{"id":"77000000000047100","name":"Борисовские Пруды","zip":null,"type":"Улица","typeShort":"ул","okato":null,"contentType":"street"},"building":{"id":"7700000000004710001","name":"10","zip":115211,"type":"дом","typeShort":"д","okato":"45296557000","contentType":"building"}}}', '["\\/data\\/images\\/restaurants\\/1406195747_0cf8570408c51a95107b73e5d6844e8b.jpeg"]', 0, 0),
(4, '', 0, 'Боровское шоссе, д.31', 'г. Москва, ш. Боровское, д. 31', 0, 1, '', '+7 (111) 222-33-44', '', '{"zoom":13,"coords":{"x":55.642453,"y":37.361517},"place":{"city":"7700000000000","street":{"id":"77000000000089000","name":"Боровское","zip":null,"type":"Шоссе","typeShort":"ш","okato":null,"contentType":"street"},"building":{"id":"7700000000008900030","name":"31","zip":119633,"type":"дом","typeShort":"д","okato":"45268577000","contentType":"building"}}}', '', 0, 0),
(6, '', 0, 'ул. Большая Академическая, д.65', 'г. Москва, ул. Академическая Б., д. 65', 0, 1, 'Посадочных мест: 198, Парковка: Есть, Wi-fi: Free, Кальянная карта: Есть', '+7 (499) 153-81-44', 'Пн-Чт (11:30 – 00:00), Пт-Сб (11:30 – 06:00), Вс.11:30 – 00:00', '{"zoom":11,"coords":{"x":55.840371,"y":37.547477},"place":{"city":"7700000000000","street":{"id":"77000000000072300","name":"Академическая Б.","zip":null,"type":"Улица","typeShort":"ул","okato":null,"contentType":"street"},"building":{"id":"7700000000007230016","name":"65","zip":125183,"type":"дом","typeShort":"д","okato":"45277580000","contentType":"building"}}}', '', 0, 0),
(7, '', 0, 'шоссе Энтузиастов, 20', 'шоссе Энтузиастов, 20', 0, 2, 'Wi-Fi', '+7(495) 362-27-07', '11:00-00:00 ежедневно (до 6:00 только по праздникам), 11:00-16:00 ланч', '', '', 0, 0),
(8, '', 0, 'Авиамоторная ул., 41', 'Авиамоторная ул., 41', 0, 3, 'Одно из лучших кафе нашей сети,расположенное в 1-ой минуте ходьбы от метро, с курящим и отдельным некурящим залами. На летний период — веранда на крыше.', '+7 (495) 514-02-70', 'с 10:00 до 06:00 .бронирование: с 10:00 до 12:00  (только на текущий день, в Пт, Сб, Вс, а так же праздничные дни резервы не принимаются )', '', '', 0, 0),
(9, '', 0, 'Лениградка', 'Лениградка', 0, 6, '', '', '', '', '', 0, 0),
(10, '', 0, 'какой-то адрес', 'какой-то адрес', 0, 7, '', '', '', '', '', 0, 0),
(11, '', 0, 'маяквока', 'маяквока', 0, 6, '', '', '', '', '', 0, 0),
(12, '', 0, '6666', 'г. Москва, ул. 8 Марта 4-я, д. 5', 0, 4, '', '+7 (666) 666-66-66', '', '{"zoom":11,"coords":{"x":55.804218,"y":37.548887},"place":{"city":"7700000000000","street":{"id":"77000000000103000","name":"8 Марта 4-я","zip":null,"type":"Улица","typeShort":"ул","okato":"45277553000","contentType":"street"},"building":{"id":"7700000000010300002","name":"5","zip":125319,"type":"дом","typeShort":"д","okato":"45277553000","contentType":"building"}}}', '', 0, 0),
(13, '', 0, '44', 'г. Москва, ул. 8 Марта 4-я, д. 4к1', 0, 4, '', '+7 (444) 444-44-44', '', '{"zoom":11,"coords":{"x":55.803398,"y":37.548896},"place":{"city":"7700000000000","street":{"id":"77000000000103000","name":"8 Марта 4-я","zip":null,"type":"Улица","typeShort":"ул","okato":"45277553000","contentType":"street"},"building":{"id":"7700000000010300001","name":"4к1","zip":125167,"type":"дом","typeShort":"д","okato":"45277553000","contentType":"building"}}}', '', 0, 0),
(43, '1', 1, 'Наименование Ресторана', 'г. Москва, ул. 26-ти Бакинских Комиссаров, д. 10', 0, 9, '', '+7 (586) 854-52-65', '', '{"zoom":11,"coords":{"x":55.660054,"y":37.490326},"place":{"city":"7700000000000","street":{"id":"77000000000116600","name":"26-ти Бакинских Комиссаров","zip":null,"type":"Улица","typeShort":"ул","okato":"45268592000","contentType":"street"},"building":{"id":"7700000000011660003","name":"10","zip":119526,"type":"дом","typeShort":"д","okato":"45268592000","contentType":"building"}}}', '', 0, 0),
(44, '2', 1, 'Центральный Офис', 'г. Москва, ул. Тверская, д. 22', 0, 9, '', '+7 (777) 888-99-11', '', '{"zoom":11,"coords":{"x":55.767418,"y":37.601169},"place":{"city":"7700000000000","street":{"id":"77000000000287700","name":"Тверская","zip":null,"type":"Улица","typeShort":"ул","okato":"45286585000","contentType":"street"},"building":{"id":"7700000000028770019","name":"22","zip":125009,"type":"дом","typeShort":"д","okato":"45286585000","contentType":"building"}}}', '', 0, 0),
(45, '1000010', 0, 'Кантемировская, Каширская', 'г. Москва, ш. Каширское, д. 46к1', 0, 9, '198 посадочных мест, парковка, Wi-Fi, кальянная карта', '+7 (499) 324-71-91', 'Пн-Чт,Вс(11:30-00:00), Пт-Сб(11:30-02:00)', '{"zoom":11,"coords":{"x":55.647894,"y":37.664788},"place":{"city":"7700000000000","street":{"id":"77000000000049600","name":"Каширское","zip":null,"type":"Шоссе","typeShort":"ш","okato":null,"contentType":"street"},"building":{"id":"7700000000004960082","name":"46к1","zip":115409,"type":"дом","typeShort":"д","okato":"45296569000","contentType":"building"}}}', '', 0, 0),
(46, '1000506', 1, 'Центральный офис', '', 0, 9, '', '', '', '', '', 0, 0),
(47, '1000546', 0, 'Волгоградский пр-т, Пролетарская', 'г. Москва, пр-кт. Волгоградский, д. 17', 0, 9, '150 посадочных мест, парковка, Wi-Fi, кальянная карта', '+7 (495) 676-99-22', 'Пн-Чт,Вс(11:30-00:00), Пт-Сб(11:30-02:00)', '{"zoom":11,"coords":{"x":55.72856,"y":37.679116},"place":{"city":"7700000000000","street":{"id":"77000000000001300","name":"Волгоградский","zip":null,"type":"Проспект","typeShort":"пр-кт","okato":null,"contentType":"street"},"building":{"id":"7700000000000130026","name":"17","zip":109316,"type":"дом","typeShort":"д","okato":"45286580000","contentType":"building"}}}', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_restaurant_plan`
--

CREATE TABLE IF NOT EXISTS `app_restaurant_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `app_restaurant_plan`
--

INSERT INTO `app_restaurant_plan` (`id`, `out_id`, `name`, `restaurant_id`, `hidden`, `deleted`) VALUES
(5, '1000015', '1', 45, 0, 0),
(6, '1000561', 'Новый план', 47, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_restaurant_table`
--

CREATE TABLE IF NOT EXISTS `app_restaurant_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `app_restaurant_table`
--

INSERT INTO `app_restaurant_table` (`id`, `out_id`, `out_code`, `name`, `plan_id`, `restaurant_id`, `hidden`, `deleted`, `version`) VALUES
(8, '', '', '44', 0, 13, 0, 0, 0),
(9, '', '', '44', 0, 13, 0, 0, 0),
(10, '', '', '66', 0, 13, 0, 0, 0),
(11, '', '', '1', 0, 3, 0, 0, 0),
(12, '', '', '2', 0, 3, 0, 0, 0),
(13, '', '', '3', 0, 3, 0, 0, 0),
(14, '', '', '4', 0, 3, 0, 0, 0),
(15, '', '', '5', 0, 3, 0, 0, 0),
(16, '', '', '6', 0, 3, 0, 0, 0),
(17, '', '', '7', 0, 3, 0, 0, 0),
(18, '', '', '8', 0, 3, 0, 0, 0),
(19, '', '', '9', 0, 3, 0, 0, 0),
(20, '', '', '10', 0, 3, 0, 0, 0),
(21, '', '', '11', 0, 3, 0, 0, 0),
(22, '', '', '12', 0, 3, 0, 0, 0),
(23, '', '', '13', 0, 3, 0, 0, 0),
(24, '', '', '14', 0, 3, 0, 0, 1),
(25, '', '', '15', 0, 3, 0, 0, 0),
(26, '', '', '1', 0, 4, 0, 0, 0),
(27, '', '', '2', 0, 4, 0, 0, 0),
(28, '1000017', '2', '2', 3, 45, 0, 0, 1),
(29, '1000027', '3', '3', 3, 45, 0, 0, 0),
(30, '1000028', '1', '1', 3, 45, 0, 0, 0),
(31, '1000132', '6', 'Pool1', 3, 45, 0, 0, 0),
(32, '1000206', '4', 'Игра', 3, 45, 0, 0, 0),
(33, '1000328', '111', 'Д-ка', 3, 45, 0, 0, 0),
(34, '1000562', '5', 'Стол', 4, 47, 0, 0, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `app_restaurant_tools`
--

CREATE TABLE IF NOT EXISTS `app_restaurant_tools` (
  `restaurant_id` int(11) NOT NULL,
  `cash_id` int(11) NOT NULL,
  PRIMARY KEY (`restaurant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_restaurant_tools`
--

INSERT INTO `app_restaurant_tools` (`restaurant_id`, `cash_id`) VALUES
(45, 1),
(47, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `app_rkeeper_proxy`
--

CREATE TABLE IF NOT EXISTS `app_rkeeper_proxy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `proxy_uuid` varchar(255) NOT NULL,
  `proxy_type` varchar(255) NOT NULL,
  `proxy_name` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `alive_interval` int(11) NOT NULL,
  `last_request_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `app_rkeeper_proxy`
--

INSERT INTO `app_rkeeper_proxy` (`id`, `company_id`, `proxy_uuid`, `proxy_type`, `proxy_name`, `create_time`, `alive_interval`, `last_request_time`) VALUES
(30, 9, '{2B039492-0768-4315-B3B5-A7A7FB58B811}', 'REF', 'BA_PROXY_REF', 1422951998, 0, 1423208772),
(31, 9, '{96896BD3-1D9F-45D5-B87C-9B2E6A924AF6}', 'MID', 'BA_PROXY_MID', 1422952157, 0, 1428507714),
(32, 9, '{FC949F7D-9FF3-4F2F-BD06-1A63314BD9D3}', 'MID', 'BA_PROXY_MID2', 1422973242, 0, 1423208772),
(34, 10, '{695F0BEA-1813-472D-82C2-B09F5F2EB7B7}', 'RK6', 'BA_PROXY', 1443768636, 0, 1443768636),
(35, 10, '{12310BEA-1813-472D-82C2-B09F5F2EB7B7}', 'RK6', 'BA_PROXY', 1443768675, 0, 1443768675),
(36, 10, '{66C1CF6F-7DA9-4623-9752-FD37B8BC18B1}', 'RK6', 'BA_PROXY', 1443768736, 0, 1444491821);

-- --------------------------------------------------------

--
-- Структура таблицы `app_rkeeper_queue`
--

CREATE TABLE IF NOT EXISTS `app_rkeeper_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `proxy_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `body` longtext NOT NULL,
  `response` longtext NOT NULL,
  `create_time` int(11) NOT NULL,
  `request_time` int(11) NOT NULL,
  `response_time` int(11) NOT NULL,
  `error_description` varchar(255) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=196 ;

--
-- Дамп данных таблицы `app_rkeeper_queue`
--

INSERT INTO `app_rkeeper_queue` (`id`, `uuid`, `proxy_id`, `status`, `type`, `body`, `response`, `create_time`, `request_time`, `response_time`, `error_description`, `data`) VALUES
(26, '{EB3F2071-609B-4181-6638-7A94B3352DF7}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361054" OrderID="256" ErrorText="" DateTime="2015-04-07T14:59:42" WorkTime="0" Processed="1"/>\n		', 1428404377, 1428404383, 1428404383, '', 'a:1:{s:8:"order_id";s:3:"128";}'),
(27, '{B75BC841-08DA-7CC2-E093-94BCAAC4A8E7}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361055" OrderID="256" ErrorText="" DateTime="2015-04-07T14:59:59" WorkTime="0" Processed="1"/>\n		', 1428404399, 1428404400, 1428404400, '', 'a:1:{s:8:"order_id";s:3:"129";}'),
(28, '{B7438655-6B82-DC4C-7CFC-563E308535CC}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361054" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000580" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:00:15" WorkTime="70" Processed="1">\n	<Order visit="655361054" orderIdent="256" basicSum="24000" nationalSum="24000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428404414, 1428404416, 1428404417, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"128";}'),
(29, '{B6B6B2C3-F269-0332-4E7E-95ABD6631BA7}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361055" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000602" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:00:32" WorkTime="10" Processed="1">\n	<Order visit="655361055" orderIdent="256" basicSum="21000" nationalSum="21000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428404432, 1428404433, 1428404433, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"129";}'),
(30, '{C563644A-A725-6C0E-CF30-AAE9B54F30CD}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n<Table Ident="1000562"/>\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361056" OrderID="256" ErrorText="" DateTime="2015-04-07T15:06:09" WorkTime="0" Processed="1"/>\n		', 1428404768, 1428404770, 1428404771, '', 'a:1:{s:8:"order_id";s:3:"130";}'),
(31, '{1BA62CFC-91C8-C07F-CEF5-F310646AE0EC}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361056" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000578" quantity="1000"/>\n                            <Dish id="1000580" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:06:32" WorkTime="40" Processed="1">\n	<Order visit="655361056" orderIdent="256" basicSum="76000" nationalSum="76000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428404789, 1428404793, 1428404793, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"130";}'),
(32, '{F5F33E73-0239-BF3E-C677-CD520A91DF5B}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n<Table Ident="1000562"/>\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361057" OrderID="256" ErrorText="" DateTime="2015-04-07T15:13:40" WorkTime="10" Processed="1"/>\n		', 1428405218, 1428405220, 1428405221, '', 'a:1:{s:8:"order_id";s:3:"131";}'),
(33, '{8CBB92BF-5CA6-64C5-69BC-8037513434C3}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361057" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000578" quantity="1000"/>\n                            <Dish id="1000580" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:14:56" WorkTime="20" Processed="1">\n	<Order visit="655361057" orderIdent="256" basicSum="76000" nationalSum="76000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428405294, 1428405296, 1428405297, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"131";}'),
(34, '{44578D56-7ABA-E675-23AA-4FBA8A8D2C3C}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361058" OrderID="256" ErrorText="" DateTime="2015-04-07T15:32:45" WorkTime="10" Processed="1"/>\n		', 1428405521, 1428406365, 1428406365, '', 'a:1:{s:8:"order_id";s:3:"132";}'),
(35, '{6DAE4841-733D-5AB0-BBC5-687E630439F7}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361059" OrderID="256" ErrorText="" DateTime="2015-04-07T15:32:51" WorkTime="0" Processed="1"/>\n		', 1428406140, 1428406371, 1428406371, '', 'a:1:{s:8:"order_id";s:3:"133";}'),
(36, '{D4EDF605-F7B0-2B36-628B-2360FB86357E}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361058" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000584" quantity="1000"/>\n                            <Dish id="1000585" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:33:02" WorkTime="20" Processed="1">\n	<Order visit="655361058" orderIdent="256" basicSum="58000" nationalSum="58000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428406376, 1428406381, 1428406382, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"132";}'),
(37, '{7844D952-07C5-F635-863E-BBDF644905B5}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361059" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000584" quantity="1000"/>\n                            <Dish id="1000585" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:33:07" WorkTime="10" Processed="1">\n	<Order visit="655361059" orderIdent="256" basicSum="58000" nationalSum="58000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428406379, 1428406387, 1428406387, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"133";}'),
(38, '{4732F977-2F21-D223-9493-B7D97A00E714}', 31, 5, 3, '<RK7Query>\n<RK7CMD CMD="CreateOrder">\n<Order persistentComment="">\n<Table Ident="1000562"/>\n</Order>\n</RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="CreateOrder" VisitID="655361060" OrderID="256" ErrorText="" DateTime="2015-04-07T15:49:29" WorkTime="0" Processed="1"/>\n		', 1428407368, 1428407368, 1428407369, '', 'a:1:{s:8:"order_id";s:3:"134";}'),
(39, '{A31B5366-8E1F-8FC6-286B-D0CD14C8EE4C}', 31, 2, 4, '<RK7Query>\n    <RK7CMD CMD="SaveOrder">\n        <Order visit="655361060" orderIdent="256"/>\n        <Session>\n                            <Dish id="1000578" quantity="1000"/>\n                            <Dish id="1000580" quantity="1000"/>\n                    </Session>\n    </RK7CMD>\n</RK7Query>', '<?xml version="1.0" encoding="utf-8"?>\n<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="SaveOrder" ErrorText="" DateTime="2015-04-07T15:49:36" WorkTime="10" Processed="1">\n	<Order visit="655361060" orderIdent="256" basicSum="76000" nationalSum="76000"/>\n	<Session sessionID="8388609"/>\n</RK7QueryResult>\n		', 1428407370, 1428407376, 1428407376, 'Не найден обработчик', 'a:1:{s:8:"order_id";s:3:"134";}'),
(40, '{57D6F9C4-5FD1-8C1B-2158-6FD46A5E7E32}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443771945, 1443771945, 0, '', NULL),
(41, '{D4AFFBCC-A078-D58E-8529-D819712E010E}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443771945, 1443771951, 0, '', NULL),
(42, '{C14D7748-C108-38C5-7A5D-E677DA3C6B97}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443771945, 1443771957, 0, '', NULL),
(43, '{AC28B17C-9DBB-D5DC-C349-64E78582F3ED}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443771945, 1443987396, 0, '', NULL),
(44, '{4DC12573-62F6-9BDC-D79D-C92C068739B1}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443771951, 1443987412, 0, '', NULL),
(45, '{AFAEDE00-E0CF-B7B3-CA07-B6EC13D7D75C}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443771951, 1443987418, 0, '', NULL),
(46, '{C75D2BAC-59E6-E905-5CAF-DC82EFE258B2}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443771951, 1443987423, 0, '', NULL),
(47, '{7DA8B95E-264B-6D0D-4069-3B3A6C36ADB3}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443771951, 1443987430, 0, '', NULL),
(48, '{29A6FD43-F194-518D-D4A9-14979429F39A}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443771957, 1443987435, 0, '', NULL),
(49, '{EA820C8F-E77D-2DA5-558B-30C2EDF67AAA}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443771957, 1443987441, 0, '', NULL),
(50, '{21541D0B-FAA1-0FEF-9A47-69684ABE03AF}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443771957, 1443987474, 0, '', NULL),
(51, '{11EBC67D-9BB0-681F-5B22-990615847659}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443771957, 1443987480, 0, '', NULL),
(52, '{D1FB503C-E5B8-9D62-5B8A-F8AE187BE06F}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', '', 1443987396, 1443987583, 0, '', NULL),
(53, '{91A92268-0CE7-E00C-34FB-B57D56456B14}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK7Query>', '', 1443987396, 1443987651, 0, '', NULL),
(54, '{67C35182-C6BF-E1B5-1E8A-47BB8A5F1662}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', '', 1443987396, 1444028008, 0, '', NULL),
(55, '{4B2F805E-C90D-F602-4BC8-1A642DD3DE64}', 36, 2, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK7Query>', 'EAccessViolation:Access violation at address 004C0916 in module ''BA_Proxy.exe''. Read of address C35DE58B', 1443987396, 1444032274, 1444032276, 'Не удалось проебразовать response в xml', NULL),
(56, '{AEE2D357-D1B0-7976-30BE-12B19D588C29}', 36, 1, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK7Query>', '', 1443987396, 1444032281, 0, '', NULL),
(57, '{629AB739-D60C-50E5-466C-69446B82FCA0}', 36, 2, 2, '<RK7Query>\n    <RK7CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK7Query>', 'EAccessViolation:Access violation at address 004C0916 in module ''BA_Proxy.exe''. Read of address C35DE58B', 1443987396, 1444032391, 1444032433, 'Не удалось проебразовать response в xml', NULL),
(58, '{FA1A44BD-D337-B4F8-4835-6566969DC326}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444028981, 1444028981, 0, '', NULL),
(59, '{DF58FF75-43D2-D9AB-BF60-946C4623AEE2}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', 'EAccessViolation:Access violation at address 004C0916 in module ''BA_Proxy.exe''. Read of address C35DE58B', 1444028981, 1444032259, 1444032261, 'Не найден обработчик', NULL),
(60, '{6F9A4906-2477-2019-C21E-88CC8DF22DDC}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', 'EAccessViolation:Access violation at address 004C0916 in module ''BA_Proxy.exe''. Read of address C35DE58B', 1444028981, 1444032267, 1444032269, 'Не найден обработчик', NULL),
(61, '{F8C22336-E714-C233-1719-A198B2ADA6BE}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444032669, 1444032669, 1444032697, 'Не найден обработчик', NULL),
(62, '{E533756B-27B9-D25F-97FE-20CE95DD9056}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444033019, 1444033019, 0, '', NULL),
(63, '{70D96E37-1BEB-146C-D896-3749BAE20A09}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444033797, 1444033797, 0, '', NULL),
(64, '{19493AA7-F474-DA84-4ED2-CA0C68D9F2E3}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', 'EDatabaseError:Cannot perform this operation on a closed dataset', 1444035722, 1444035722, 1444035783, 'Не найден обработчик', NULL),
(65, '{BFCDA6D7-ACA1-0EF0-2C9F-DC7E0C8AF70C}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444035846, 1444035846, 0, '', NULL),
(66, '{5769E7E7-38CD-3D09-1CFD-253607F7A05F}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444036124, 1444036124, 0, '', NULL),
(67, '{788141FB-B288-20F4-A677-1949B975FCF8}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444036210, 1444036210, 1444036225, 'Не найден обработчик', NULL),
(68, '{78082C6B-4FE4-73B2-2DC9-FD99992D3ECF}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444038328, 1444038328, 1444038336, 'Не найден обработчик', NULL),
(69, '{2AB9BFB1-3DA6-0F20-8E12-8FA058DD7B32}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:46:51">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038328, 1444038402, 1444038411, 'Не найден обработчик', NULL),
(70, '{B5B60BCB-A484-854F-84F4-7CAC97FCA66A}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:47:00">\n	<RK7Reference DataVersion="1493882824" Name="EMPLOYEES">\n		<Items>\n			<Item SIFR="1" NAME="Test Barmen" CODE="5438" TYPE="B" DEL="False"/>\n			<Item SIFR="3" NAME="Test Manager" CODE="7908" TYPE="M" DEL="False"/>\n			<Item SIFR="4" NAME="Официк" CODE="1297" TYPE="W" DEL="False"/>\n			<Item SIFR="6" NAME="manager2" CODE="1230" TYPE="M" DEL="False"/>\n			<Item SIFR="7" NAME="kassir" CODE="7906" TYPE="K" DEL="False"/>\n			<Item SIFR="2" NAME="Test Cashier" CODE="7906" TYPE="M" DEL="True"/>\n			<Item SIFR="5" NAME="manager" CODE="7906" TYPE="K" DEL="True"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038328, 1444038416, 1444038422, 'Не найден обработчик', NULL),
(71, '{4639AFCD-7E96-3CE4-A228-BDD831977571}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:50:21">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038619, 1444038619, 1444038625, 'Не найден обработчик', NULL),
(72, '{03A11A4C-10C2-9337-16B3-A91A7075699C}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:50:32">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038619, 1444038631, 1444038632, 'Не найден обработчик', NULL),
(73, '{C8D49481-25AA-40E0-E71D-F54D5514AF6F}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:50:40">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038630, 1444038638, 1444038639, 'Не найден обработчик', NULL),
(74, '{6A791577-23B2-A7C0-3678-545A4FDBD8F6}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:50:47">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038630, 1444038645, 1444038646, 'Не найден обработчик', NULL),
(75, '{E84BE303-C576-D2F9-6B26-D5C46D1A451A}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:50:54">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038638, 1444038652, 1444038653, 'Не найден обработчик', NULL),
(76, '{0390AFA6-B241-9B72-BEDB-E0A8D2E05E09}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:51:01">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038638, 1444038659, 1444038660, 'Не найден обработчик', NULL),
(77, '{7E6DFB7F-7D4F-FA26-5C2A-D0350C747D94}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:51:08">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038645, 1444038666, 1444038667, 'Не найден обработчик', NULL),
(78, '{BD95E454-746E-DB60-82D6-E9477D2F3F2A}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:51:15">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038645, 1444038673, 1444038674, 'Не найден обработчик', NULL),
(79, '{08C3E0B8-4556-1BDD-8C5D-9716FD663190}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:51:22">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038652, 1444038680, 1444038681, 'Не найден обработчик', NULL),
(80, '{DAFC6D67-7433-BEC3-BB51-F44078E27D23}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:51:40">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038652, 1444038687, 1444038706, 'Не найден обработчик', NULL),
(81, '{59F7B6CB-5345-1378-C50A-7A1CAAD878E9}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:51:55">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038659, 1444038712, 1444038715, 'Не найден обработчик', NULL),
(82, '{B7E909F4-608F-2629-1E3E-15A646C3F2DB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:52:04">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038659, 1444038721, 1444038723, 'Не найден обработчик', NULL),
(83, '{AFA29990-F58B-6E0E-6C76-D9714C8C58CD}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:52:12">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038666, 1444038730, 1444038731, 'Не найден обработчик', NULL),
(84, '{6AF348AA-8948-92C2-19C8-2F829E9CE387}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-05T12:52:21">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK6QueryResult>\n		', 1444038666, 1444038738, 1444038741, 'Не найден обработчик', NULL),
(85, '{15A598F4-2661-0356-3B3C-51573749F2EE}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T15:59:47">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038673, 1444481989, 1444482033, 'Не найден обработчик', NULL),
(86, '{F1DB4F20-8E7F-5200-08A6-30C9107DA2E5}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:00:37">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038673, 1444482039, 1444482040, 'Не найден обработчик', NULL),
(87, '{8DCF9370-B586-CB73-E88F-DA650432DD34}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:00:44">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038680, 1444482046, 1444482046, 'Не найден обработчик', NULL),
(88, '{26BB5BD9-057A-E2BF-3221-E7024E6C45C4}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:00:51">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038680, 1444482052, 1444482053, 'Не найден обработчик', NULL),
(89, '{00139C79-B125-9B30-1E9A-D6E501597F04}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:00:58">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038687, 1444482059, 1444482060, 'Не найден обработчик', NULL),
(90, '{2185E2F2-1B50-7040-2924-BEF377080B26}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:01:42">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038687, 1444482087, 1444482120, 'Не найден обработчик', NULL),
(91, '{A644D0CB-E732-47DB-9567-3562E606539C}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:03:11">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038712, 1444482187, 1444482208, 'Не найден обработчик', NULL);
INSERT INTO `app_rkeeper_queue` (`id`, `uuid`, `proxy_id`, `status`, `type`, `body`, `response`, `create_time`, `request_time`, `response_time`, `error_description`, `data`) VALUES
(92, '{DBEB010E-A55D-1476-FDD0-92886E0107BC}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:04:56">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038712, 1444482274, 1444482300, 'Не найден обработчик', NULL),
(93, '{C3B5633C-D268-FE09-99B8-5842F39E31DB}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444038721, 1444482355, 0, '', NULL),
(94, '{F6B2DB77-69CB-38CF-0903-A1E16EF2E042}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:07:34">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038721, 1444482430, 1444482481, 'Не найден обработчик', NULL),
(95, '{079223F2-02CA-2DD0-BD4C-692404B2CCC8}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444038730, 1444482492, 0, '', NULL),
(96, '{B66CA7CA-FF5A-ED45-9E3A-4B7DC219DB66}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:11:53">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444038730, 1444482711, 1444482717, 'Не найден обработчик', NULL),
(97, '{C3005B39-E5BC-8657-360A-3C3FF1C3831E}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444038738, 1444483311, 0, '', NULL),
(98, '{C4438043-3C7D-0CEC-3652-A06DBC3932E4}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '', 1444038738, 1444484785, 0, '', NULL),
(99, '{6085918B-05FF-43E3-9B87-041A7539F741}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK7QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:47:07">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK7QueryResult>\n		', 1444481989, 1444484827, 1444484829, 'Не найден обработчик', NULL),
(100, '{8B297E4D-1958-E266-DA79-22E896ECECD7}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK7QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:47:16">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK7QueryResult>\n		', 1444481989, 1444484836, 1444484838, 'Не найден обработчик', NULL),
(101, '{C79099D4-A23D-2AD1-9991-3E18FF2E9AF6}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK7QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:47:26">\n	<RK7Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK7Reference>\n</RK7QueryResult>\n		', 1444482039, 1444484845, 1444484848, 'Не найден обработчик', NULL),
(102, '{37D1EF21-B87C-831E-DE1D-D7CF47486675}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK7QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:47:35">\n	<RK7Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK7Reference>\n</RK7QueryResult>\n		', 1444482039, 1444484855, 1444484858, 'Не найден обработчик', NULL),
(103, '{D83F6C2B-AF3F-1E03-866B-837C9A595D87}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444482046, 1444484865, 0, '', NULL),
(104, '{1FD2B667-BECD-3797-8C2A-5B31BB2564B8}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:48:33">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482046, 1444484914, 1444484915, 'Не найден обработчик', NULL),
(105, '{8FC843EA-05EF-8B8E-DE25-4E09B5E842C7}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:48:50">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482052, 1444484923, 1444484932, 'Не найден обработчик', NULL),
(106, '{49E08125-5A79-75B1-5A75-9C4BE260D952}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:49:18">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482052, 1444484939, 1444484963, 'Не найден обработчик', NULL),
(107, '{080CD853-0761-0BF9-143A-C0DDBD222FBE}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:49:34">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482059, 1444484973, 1444484977, 'Не найден обработчик', NULL),
(108, '{1FAA0F09-B19F-73AC-DB51-0E487516D059}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:49:46">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482059, 1444484984, 1444484989, 'Не найден обработчик', NULL),
(109, '{61776AB1-21FB-90B1-F178-C3DE9F3D1083}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T16:49:56">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482087, 1444484996, 1444484999, 'Не найден обработчик', NULL),
(110, '{A2A787EA-A5AE-C8A6-3837-BC21183FC235}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', 'EDBEngineError:Insufficient memory for this operation.\nAlias\n		', 1444482087, 1444485214, 1444485222, 'Не найден обработчик', NULL),
(111, '{DE7CA38B-4303-1010-DBA6-C21CD3D107C3}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', 'EDBEngineError:Insufficient memory for this operation.\nAlias\n		', 1444482187, 1444485234, 1444485244, 'Не найден обработчик', NULL),
(112, '{B78D31CD-9583-5419-B80B-955C002C86FB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', 'EDBEngineError:Insufficient memory for this operation.\nAlias\n		', 1444482187, 1444485252, 1444485286, 'Не найден обработчик', NULL),
(113, '{7FCDBDB1-0851-5E41-7820-99C60AD5EE09}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '', 1444482187, 1444485300, 0, '', NULL),
(114, '{D5D1BE34-D8AF-A449-9BBB-568E8A49A6E8}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444482274, 1444486283, 0, '', NULL),
(115, '{91CC8009-996F-0CF1-3B75-E61E5DAEC0C6}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '', 1444482274, 1444486480, 0, '', NULL),
(116, '{44F9CF55-EAE1-3FE7-8D3C-8E34F40368F3}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444482274, 1444486848, 0, '', NULL),
(117, '{4CB726C1-38DE-54B6-6D95-E571D1DA64A3}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:22:12">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="   0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482274, 1444486930, 1444486946, 'Не найден обработчик', NULL),
(118, '{30E0ED1E-59AE-6254-53F4-878E25C99428}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:23:32">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482355, 1444487012, 1444487031, 'Не найден обработчик', NULL),
(119, '{60551640-6B7B-12D7-D4E2-85ECF2FDEBEF}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '', 1444482355, 1444487037, 0, '', NULL),
(120, '{2915B340-B357-53AD-99EF-2FB2C8EA12BF}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:24:44">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item/>\n			<Item/>\n			<Item/>\n			<Item SIFR="4" NAME="Шашлык" CODE="   1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="   2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="   3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="   4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="   5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="   6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482430, 1444487081, 1444487086, 'Не найден обработчик', NULL),
(121, '{B17F2E69-C4D4-D716-42AC-DBDB32CCA7AA}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '', 1444482430, 1444487091, 0, '', NULL),
(122, '{0169A5F0-6C9D-6B20-1A53-10B4EEA9CE80}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:29:35">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482492, 1444487375, 1444487386, 'Не найден обработчик', NULL),
(123, '{CF0DBAFE-9CB8-806A-725B-264B41DD52F2}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:29:49">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482492, 1444487391, 1444487394, 'Не найден обработчик', NULL),
(124, '{EC39B9B0-FE55-B491-A370-59DF849E0026}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:29:58">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482711, 1444487399, 1444487404, 'Не найден обработчик', NULL),
(125, '{E79AA735-7405-E3DD-A282-7126FB2566DB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:30:08">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482711, 1444487409, 1444487416, 'Не найден обработчик', NULL),
(126, '{929BFFAA-192E-B8FB-A9FA-AD6A56BB7EB6}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:30:20">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482711, 1444487421, 1444487423, 'Не найден обработчик', NULL),
(127, '{B4B2668A-CD01-83F0-10D3-BD032F92BF52}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:30:27">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482711, 1444487429, 1444487431, 'Не найден обработчик', NULL),
(128, '{B1EDE74F-A155-E452-9C0D-445C7A1D44E4}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:30:35">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" TREETYPE="F" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" TREETYPE="F" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" TREETYPE="F" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" TREETYPE="F" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" TREETYPE="F" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" TREETYPE="F" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482711, 1444487436, 1444487438, 'Не найден обработчик', NULL),
(129, '{34307697-7060-9E30-0E5A-D424750590A4}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:30:42">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" TREETYPE="T" CATEG="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444482711, 1444487444, 1444487451, 'Не найден обработчик', NULL),
(130, '{072B0F2E-1019-5B91-6B62-B47D7860730C}', 36, 1, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '', 1444483311, 1444487457, 0, '', NULL),
(131, '{51182CAD-6A91-534C-2E15-7314EECA52CF}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:40:00">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1"/>\n			<Item SIFR="2"/>\n			<Item SIFR="3"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444483311, 1444488002, 1444488003, 'Не найден обработчик', NULL),
(132, '{361D2A29-6ACC-0869-A07F-E4EA8E8631DA}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:40:06">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4"/>\n			<Item SIFR="5"/>\n			<Item SIFR="6"/>\n			<Item SIFR="7"/>\n			<Item SIFR="8"/>\n			<Item SIFR="9"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444483311, 1444488008, 1444488008, 'Не найден обработчик', NULL),
(133, '{05FCADC2-9242-9D75-7D6A-AEC6B5EEC966}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:40:12">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1"/>\n			<Item SIFR="2"/>\n			<Item SIFR="3"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444483311, 1444488014, 1444488014, 'Не найден обработчик', NULL),
(134, '{7ED129F2-9690-8860-AA6F-063E782275D5}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:02">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484785, 1444488123, 1444488124, 'Не найден обработчик', NULL),
(135, '{E46AA0FF-E5C0-A236-435A-9F0ECD70C27A}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:07">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484785, 1444488129, 1444488129, 'Не найден обработчик', NULL),
(136, '{B03A0DDF-9F93-FD60-77B3-5F40BA078901}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:13">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484827, 1444488135, 1444488135, 'Не найден обработчик', NULL),
(137, '{C0E6DDC4-713C-5101-E7D1-85862717ADFE}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:19">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484827, 1444488141, 1444488141, 'Не найден обработчик', NULL),
(138, '{ABD4E7AE-DCE9-EA67-2695-B5912E0787EB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:25">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484836, 1444488147, 1444488147, 'Не найден обработчик', NULL),
(139, '{EEAEA32D-DB10-C9FC-28EB-12E604DF2662}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:31">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PRICE="0" PARENT="0" DEL="False" BARCODE="" COMPLEX="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484836, 1444488153, 1444488153, 'Не найден обработчик', NULL),
(140, '{40D8C4AF-BB47-B3B0-0B20-04DAD5440BC1}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:42:38">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484845, 1444488159, 1444488160, 'Не найден обработчик', NULL),
(141, '{66B40A1A-EF4D-3AF4-CEC2-FDB228F42C00}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:43:54">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484845, 1444488236, 1444488236, 'Не найден обработчик', NULL),
(142, '{786D4F92-557D-D318-0765-D008216D6D9D}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:00">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484855, 1444488242, 1444488242, 'Не найден обработчик', NULL),
(143, '{3777CCE7-46C6-9B04-16B7-CF3FB1A9920E}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:06">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484855, 1444488247, 1444488248, 'Не найден обработчик', NULL),
(144, '{652AD574-4B63-3B07-DA5E-AA7CCD0A9773}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:12">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484865, 1444488253, 1444488254, 'Не найден обработчик', NULL),
(145, '{E3B35081-D351-8264-18D3-0AE39A254FB5}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:18">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484865, 1444488259, 1444488260, 'Не найден обработчик', NULL);
INSERT INTO `app_rkeeper_queue` (`id`, `uuid`, `proxy_id`, `status`, `type`, `body`, `response`, `create_time`, `request_time`, `response_time`, `error_description`, `data`) VALUES
(146, '{AE1921E0-3774-376C-7670-380DA39F65BB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:24">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484914, 1444488265, 1444488266, 'Не найден обработчик', NULL),
(147, '{BC456C5D-25B4-FC48-3BB9-2BD38FCB9A90}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:30">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484914, 1444488272, 1444488272, 'Не найден обработчик', NULL),
(148, '{7CA7C981-0DFD-3857-981D-885165390280}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:37">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484923, 1444488278, 1444488279, 'Не найден обработчик', NULL),
(149, '{5A82FFE0-B924-0430-7A63-540A81784C7F}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:43">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484923, 1444488284, 1444488285, 'Не найден обработчик', NULL),
(150, '{40A0FC68-DA73-2781-BF96-C84FB0C1B2B7}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T17:44:49">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484939, 1444488291, 1444488292, 'Не найден обработчик', NULL),
(151, '{5F025C22-106F-D4BB-F066-FB9B0BDBEC72}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:36:01">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484939, 1444491362, 1444491363, 'Не найден обработчик', NULL),
(152, '{16B3A587-AFED-5ABE-7BAC-0ED878473CC2}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:36:07">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484973, 1444491368, 1444491369, 'Не найден обработчик', NULL),
(153, '{988B054A-FD82-9D2B-6BCD-8A763A8C015B}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:36:13">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484973, 1444491374, 1444491375, 'Не найден обработчик', NULL),
(154, '{BEE9C67B-0BF5-37E1-6A07-8AABD2DB8EDA}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:36:19">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484984, 1444491380, 1444491381, 'Не найден обработчик', NULL),
(155, '{A371135E-AAD2-C9BE-452C-9FCC29252DDB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:36:25">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484984, 1444491386, 1444491387, 'Не найден обработчик', NULL),
(156, '{B194BE92-1F43-6FB7-B8AC-756F1953E4F6}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:36:31">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484996, 1444491392, 1444491393, 'Не найден обработчик', NULL),
(157, '{28B6F10B-5A35-8534-83E7-1DA851951FAB}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:10">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444484996, 1444491431, 1444491432, 'Не найден обработчик', NULL),
(158, '{08F548F2-8317-FF03-8C0A-DF3220D86885}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:11">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485214, 1444491432, 1444491432, 'Не найден обработчик', NULL),
(159, '{9B25B382-AD37-398F-FC08-1C6CBED08B5F}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:11">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485214, 1444491433, 1444491433, 'Не найден обработчик', NULL),
(160, '{2E47E14A-C90C-A9B0-70A5-296563C45080}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:12">\n	<RK6Reference DataVersion="1493882824" Name="EMPLOYEES">\n		<Items>\n			<Item SIFR="1" NAME="Test Barmen" CODE="5438" TYPE="B" DEL="False"/>\n			<Item SIFR="3" NAME="Test Manager" CODE="7908" TYPE="M" DEL="False"/>\n			<Item SIFR="4" NAME="Официк" CODE="1297" TYPE="W" DEL="False"/>\n			<Item SIFR="6" NAME="manager2" CODE="1230" TYPE="M" DEL="False"/>\n			<Item SIFR="7" NAME="kassir" CODE="7906" TYPE="K" DEL="False"/>\n			<Item SIFR="2" NAME="Test Cashier" CODE="7906" TYPE="M" DEL="True"/>\n			<Item SIFR="5" NAME="manager" CODE="7906" TYPE="K" DEL="True"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485214, 1444491434, 1444491434, 'Не найден обработчик', NULL),
(161, '{8031D94A-D922-4146-EDB5-1631344EEA14}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:13">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485234, 1444491435, 1444491435, 'Не найден обработчик', NULL),
(162, '{BF7CFCAB-09EA-94F7-804E-FAC4BD71D15E}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:14">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485234, 1444491436, 1444491436, 'Не найден обработчик', NULL),
(163, '{F3418A2A-7001-5DEB-B3A2-379A0DF3B23F}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:16">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485252, 1444491437, 1444491438, 'Не найден обработчик', NULL),
(164, '{473159F5-A69A-C872-6F72-1A29EA4F81E6}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:17">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485252, 1444491438, 1444491439, 'Не найден обработчик', NULL),
(165, '{3D2F1095-4552-EEE4-35F6-C8BAFBE6570B}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:18">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485300, 1444491440, 1444491440, 'Не найден обработчик', NULL),
(166, '{1DC2C1AC-1F1F-5882-ACC4-8C28F9FB8E04}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:20">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444485300, 1444491441, 1444491442, 'Не найден обработчик', NULL),
(167, '{49CCD56B-4EB8-D380-83A9-FB6B4F507B0E}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:22">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444486283, 1444491443, 1444491443, 'Не найден обработчик', NULL),
(168, '{3E36385A-5C8F-4043-DF20-1BD25106996A}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:23">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444486283, 1444491444, 1444491445, 'Не найден обработчик', NULL),
(169, '{F1C96C57-BCA8-4573-6A9C-DDC392AEAB36}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:24">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444486283, 1444491446, 1444491446, 'Не найден обработчик', NULL),
(170, '{622A12DE-2586-0568-6FDC-246913F0B9F0}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:26">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444486283, 1444491447, 1444491448, 'Не найден обработчик', NULL),
(171, '{883A9260-0B55-FD4F-4633-D02D1806B20E}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:27">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444486283, 1444491448, 1444491449, 'Не найден обработчик', NULL),
(172, '{CC932F8C-65DF-E467-D8AD-2FD2882D9200}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:37:29">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444486283, 1444491450, 1444491451, 'Не найден обработчик', NULL),
(173, '{0A6C3F00-B02E-B9CA-1F24-08257709DD4A}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:39:31">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491569, 1444491569, 1444491573, 'Не найден обработчик', NULL),
(174, '{297A3FEE-FDC5-24B6-E9A6-507CFC33FEA1}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:19">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491569, 1444491597, 1444491620, 'Не найден обработчик', NULL),
(175, '{A636F74E-2554-7E32-5A35-602FAF58B5FD}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="EMPLOYEES"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:40">\n	<RK6Reference DataVersion="1493882824" Name="EMPLOYEES">\n		<Items>\n			<Item SIFR="1" NAME="Test Barmen" CODE="5438" TYPE="B" DEL="False"/>\n			<Item SIFR="3" NAME="Test Manager" CODE="7908" TYPE="M" DEL="False"/>\n			<Item SIFR="4" NAME="Официк" CODE="1297" TYPE="W" DEL="False"/>\n			<Item SIFR="6" NAME="manager2" CODE="1230" TYPE="M" DEL="False"/>\n			<Item SIFR="7" NAME="kassir" CODE="7906" TYPE="K" DEL="False"/>\n			<Item SIFR="2" NAME="Test Cashier" CODE="7906" TYPE="M" DEL="True"/>\n			<Item SIFR="5" NAME="manager" CODE="7906" TYPE="K" DEL="True"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491569, 1444491642, 1444491642, 'Не найден обработчик', NULL),
(176, '{41F30A75-2B50-8964-F2C9-863ED4202AA8}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:44">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491597, 1444491645, 1444491645, 'Не найден обработчик', NULL),
(177, '{8942BF09-940C-F80D-809C-D0E538C80E27}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:45">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491597, 1444491647, 1444491647, 'Не найден обработчик', NULL),
(178, '{911BD24C-11F1-9229-A3B5-C78180A040E0}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:48">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491642, 1444491649, 1444491649, 'Не найден обработчик', NULL),
(179, '{075173E9-56C5-6A83-D0F4-DFD07BCE1087}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:49">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491642, 1444491651, 1444491651, 'Не найден обработчик', NULL),
(180, '{AEE9F368-D988-07C0-D40D-A7D589308702}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:52">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491645, 1444491653, 1444491654, 'Не найден обработчик', NULL),
(181, '{394757E4-670D-FD43-6737-6EE32AD50F43}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:40:54">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491645, 1444491655, 1444491656, 'Не найден обработчик', NULL),
(182, '{FF5A8E30-7975-48BD-778D-954456280EC8}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:42:29">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491647, 1444491751, 1444491751, 'Не найден обработчик', NULL),
(183, '{94C89262-25A8-041B-7203-D741E643B5BA}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:42:30">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491647, 1444491751, 1444491752, 'Не найден обработчик', NULL),
(184, '{D5115593-F4CB-4C1A-65D2-4A742DAD9DCD}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:42:31">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491649, 1444491752, 1444491752, 'Не найден обработчик', NULL),
(185, '{BE534534-FF65-1B20-F061-CD815DC3127C}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:42:31">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491649, 1444491753, 1444491753, 'Не найден обработчик', NULL),
(186, '{A3D499AC-2EEB-605C-5EF4-921BDC18D4A5}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:42:33">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491651, 1444491754, 1444491755, 'Не найден обработчик', NULL),
(187, '{569B4656-3D6F-17F3-D5EC-DAA630022B0B}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:21">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491651, 1444491803, 1444491803, 'Не найден обработчик', NULL),
(188, '{CB6EDF19-B081-FADF-AAE9-57C1923ED233}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:23">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491653, 1444491804, 1444491805, 'Не найден обработчик', NULL),
(189, '{98756BDD-7E61-EB0C-FC78-7817C96EA665}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:24">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491653, 1444491806, 1444491806, 'Не найден обработчик', NULL),
(190, '{D9D2A672-AF90-DF77-76CC-DC6E8AB166D0}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:26">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491655, 1444491807, 1444491808, 'Не найден обработчик', NULL),
(191, '{1A85C98E-D910-BE40-CCA1-B6FE03EE31FE}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:27">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491655, 1444491809, 1444491809, 'Не найден обработчик', NULL),
(192, '{1C276392-5E09-DFAF-D317-0AA835152E24}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:29">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491750, 1444491810, 1444491811, 'Не найден обработчик', NULL);
INSERT INTO `app_rkeeper_queue` (`id`, `uuid`, `proxy_id`, `status`, `type`, `body`, `response`, `create_time`, `request_time`, `response_time`, `error_description`, `data`) VALUES
(193, '{8E5BA2B1-F164-252F-7590-21385A867240}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:30">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491751, 1444491812, 1444491812, 'Не найден обработчик', NULL),
(194, '{65855E28-7EE7-E48E-35F7-00BB12B6CA92}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="MENUITEMS"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:32">\n	<RK6Reference DataVersion="2765280887" Name="MENUITEMS">\n		<Items>\n			<Item SIFR="4" NAME="Шашлык" CODE="1" CATEG="1" PRICE="190" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="5" NAME="Борщ" CODE="2" CATEG="1" PRICE="95" PARENT="2" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="6" NAME="Пиво" CODE="3" CATEG="2" PRICE="80" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="7" NAME="Лимонад" CODE="4" CATEG="3" PRICE="45" PARENT="3" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="8" NAME="Салат весенний" CODE="5" CATEG="1" PRICE="55" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n			<Item SIFR="9" NAME="Салат цезарь" CODE="6" CATEG="1" PRICE="65" PARENT="1" DEL="False" BARCODE="" ABSENT="False" COMPLEX="False" RECCOM="0"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491751, 1444491813, 1444491814, 'Не найден обработчик', NULL),
(195, '{BC99D313-9DC3-D16D-865E-4CF1FFA7B99D}', 36, 2, 5, '<RK6Query>\n    <RK6CMD CMD="GetRefData" RefName="CATEGLIST"/>\n</RK6Query>', '<?xml version="1.0" encoding="UTF-8"?>\n<RK6QueryResult ServerVersion="6" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2015-10-10T18:43:34">\n	<RK6Reference DataVersion="2765280887" Name="CATEGLIST">\n		<Items>\n			<Item SIFR="1" NAME="ХЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="2" NAME="ГЦ" CODE="0" PARENT="0" DEL="False"/>\n			<Item SIFR="3" NAME="Напитки" CODE="0" PARENT="0" DEL="False"/>\n		</Items>\n	</RK6Reference>\n</RK6QueryResult>\n		', 1444491751, 1444491815, 1444491815, 'Не найден обработчик', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `app_rkeeper_turn`
--

CREATE TABLE IF NOT EXISTS `app_rkeeper_turn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proxy_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `app_rkeeper_turn`
--


-- --------------------------------------------------------

--
-- Структура таблицы `app_sale_price`
--

CREATE TABLE IF NOT EXISTS `app_sale_price` (
  `condition_id` int(11) NOT NULL,
  `date_on` int(11) NOT NULL,
  `date_off` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_sale_price`
--


-- --------------------------------------------------------

--
-- Структура таблицы `app_user`
--

CREATE TABLE IF NOT EXISTS `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `restaurant_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Дамп данных таблицы `app_user`
--

INSERT INTO `app_user` (`id`, `out_id`, `out_code`, `username`, `password`, `email`, `fullname`, `role`, `blocked`, `restaurant_id`, `company_id`) VALUES
(1, '', '', 'ssa', 'e10adc3949ba59abbe56e057f20f883e', 'birlaver@gmail.com', 'Eugene Lepeshko', 'admin', 0, 0, 0),
(2, '', '', 'tanuki_owner', 'e10adc3949ba59abbe56e057f20f883e', 'ssa@bistroapp.ru', 'Владелец Тануки', 'owner', 0, 0, 1),
(3, '', '', 'tanuki_manager', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_manager@bistroapp.ru', 'Администратор Тануки', 'manager', 0, 3, 1),
(4, '', '', 'tanuki_cook', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_cook@bistroapp.ru', 'Повар Тануки', 'cook', 0, 3, 1),
(5, '', '', 'tanuki_garcon', 'e10adc3949ba59abbe56e057f20f883e', 'kalin@abcwww.ru', 'Оффициант Тануки', 'garcon', 0, 3, 1),
(6, '', '', 'tanuki_barman', 'e10adc3949ba59abbe56e057f20f883e', 'lunatik_37@mail.ru', 'Бармен', 'barman', 0, 3, 1),
(7, '', '', 'tanuki_wer', 'e10adc3949ba59abbe56e057f20f883e', 'wer@bistroapp.ru', 'wer', 'barman', 0, 3, 1),
(8, '', '', 'tanuki_ert', 'e10adc3949ba59abbe56e057f20f883e', 'ert@bistroapp.ru', 'ert', 'manager', 0, 7, 2),
(9, '', '', 'tanuki_1234323', 'e10adc3949ba59abbe56e057f20f883e', 'abc-coder@mail.ru', '6324234', 'garcon', 0, 3, 1),
(10, '', '', 'tanuki_234243', 'e10adc3949ba59abbe56e057f20f883e', 'ssa23423@ewrwer.ui', '234234234', 'garcon', 0, 3, 1),
(11, '', '', 'rkeeper_owner', '202cb962ac59075b964b07152d234b70', 'rkeeper_owner@bistroapp.ru', 'Владелец компании', 'owner', 0, 0, 9),
(38, '1', '7', 'rkeeper_user1', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1@bistroapp.ru', 'Администратор', 'garcon', 1, 0, 9),
(39, '9001', '9001', 'rkeeper_user9001', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9001@bistroapp.ru', 'Система', 'garcon', 1, 0, 9),
(40, '9002', '9002', 'rkeeper_user9002', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9002@bistroapp.ru', '777', 'garcon', 1, 0, 9),
(41, '9003', '9003', 'rkeeper_user9003', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9003@bistroapp.ru', 'Контроль Кодов', 'garcon', 1, 0, 9),
(42, '9004', '9004', 'rkeeper_user9004', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9004@bistroapp.ru', 'RK6 Импорт', 'garcon', 1, 0, 9),
(43, '9005', '9005', 'rkeeper_user9005', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9005@bistroapp.ru', 'Веб-Резервирование', 'garcon', 1, 0, 9),
(44, '401862', '3', 'rkeeper_user401862', '202cb962ac59075b964b07152d234b70', 'rkeeper_user401862@bistroapp.ru', 'Ахметзянова Алсу', 'garcon', 1, 0, 9),
(45, '401923', '1', 'rkeeper_user401923', '202cb962ac59075b964b07152d234b70', 'rkeeper_user401923@bistroapp.ru', 'Распопов Андрей', 'garcon', 1, 0, 9),
(46, '402314', '10', 'rkeeper_user402314', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402314@bistroapp.ru', 'Кравцов Евгений', 'garcon', 1, 0, 9),
(47, '402341', '13', 'rkeeper_user402341', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402341@bistroapp.ru', 'Беляев Владимир', 'garcon', 1, 0, 9),
(48, '402356', '9', 'rkeeper_user402356', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402356@bistroapp.ru', 'Щербакова Ирина', 'garcon', 1, 0, 9),
(49, '402444', '11', 'rkeeper_user402444', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402444@bistroapp.ru', 'Виталий Тепляков', 'garcon', 1, 0, 9),
(50, '1000002', '2', 'rkeeper_user1000002', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000002@bistroapp.ru', 'Tester', 'garcon', 1, 0, 9),
(51, '1000003', '3', 'rkeeper_user1000003', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000003@bistroapp.ru', 'Крылов', 'garcon', 1, 0, 9),
(52, '1000004', '15', 'rkeeper_user1000004', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000004@bistroapp.ru', 'Алсу', 'garcon', 1, 0, 9),
(53, '1000005', '4', 'rkeeper_user1000005', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000005@bistroapp.ru', 'Официант №2', 'garcon', 1, 0, 9),
(54, '1000006', '5', 'rkeeper_user1000006', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000006@bistroapp.ru', 'Официант №1', 'garcon', 1, 0, 9),
(55, '1000007', '6', 'rkeeper_user1000007', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000007@bistroapp.ru', 'Кассир №1', 'garcon', 1, 0, 9),
(56, '1000008', '8', 'rkeeper_user1000008', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000008@bistroapp.ru', 'Кассир №2', 'garcon', 1, 0, 9),
(57, '1000009', '12', 'rkeeper_user1000009', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000009@bistroapp.ru', 'Спец работник', 'garcon', 1, 0, 9),
(58, '1000011', '1', 'rkeeper_user1000011', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000011@bistroapp.ru', 'Кассир1', 'garcon', 1, 0, 9),
(59, '1000012', '3', 'rkeeper_user1000012', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000012@bistroapp.ru', 'Админ', 'garcon', 1, 0, 9),
(60, '1000013', '9', 'rkeeper_user1000013', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000013@bistroapp.ru', '45345', 'garcon', 1, 0, 9),
(61, '1000015', '13', 'rkeeper_user1000015', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000015@bistroapp.ru', 'Кассир', 'garcon', 1, 0, 9),
(62, '1000016', '14', 'rkeeper_user1000016', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000016@bistroapp.ru', 'Tester', 'garcon', 1, 0, 9),
(63, '1000017', '1', 'rkeeper_user1000017', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000017@bistroapp.ru', 'test1', 'garcon', 1, 0, 9),
(64, '', '', 'ilpatio_owner', '202cb962ac59075b964b07152d234b70', 'ilpatio_owner@bistroapp.ru', 'Владелец компании', 'owner', 0, 0, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `app_user_table`
--

CREATE TABLE IF NOT EXISTS `app_user_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `app_user_table`
--

INSERT INTO `app_user_table` (`id`, `table_id`, `user_id`) VALUES
(6, 12, 9),
(7, 14, 9),
(8, 20, 9),
(9, 24, 9),
(10, 34, 36);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `app_auth_assignment`
--
ALTER TABLE `app_auth_assignment`
  ADD CONSTRAINT `app_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `app_auth_item_child`
--
ALTER TABLE `app_auth_item_child`
  ADD CONSTRAINT `app_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `app_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
