<?php

/**
 * This is the model class for table "{{company}}".
 *
 * The followings are the available columns in table '{{company}}':
 * @property integer $id
 * @property string $out_id
 * @property string $name
 * @property string $logo
 * @property string $description
 * @property integer $hidden
 * @property integer $deleted
 * @property integer $website
 * @property integer $email
 * @property string $prefix
 * @property string $workplaces
 * @property integer $version
 * @property integer $imageVersion
 */
class Company extends CActiveRecord
{
    const WORKPLACE_HOT = 1;
    const WORKPLACE_COLD = 2;
    const WORKPLACE_TASTY = 3;

    public static function getWorkplace($key = NULL){
        $items = array(
            self::WORKPLACE_HOT => 'Горячий цех',
            self::WORKPLACE_COLD => 'Холодный цех',
            self::WORKPLACE_TASTY => 'Кондитерский цех',
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }

    public function getActiveWorkplaces(){
        if ($this->workplaces)
            return explode(',', $this->workplaces);
        return array();
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,prefix,workplaces', 'required'),
			array('hidden, deleted', 'numerical', 'integerOnly'=>true),
			array('out_id, name, website, email, workplaces', 'length', 'max'=>255),
            array('prefix', 'length', 'max' => 10),
            array('prefix', 'unique'),
            array('prefix', 'match', 'pattern'=>'/^([a-z])+$/', 'message'=>'Префикс должен состоять из латинских букв в нижнем регистре.'),
            array('email', 'email'),
            array('website', 'url'),
			array('logo, description', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, logo, hidden, deleted, description, workplaces', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'restaurants' => array(self::HAS_MANY, 'Restaurant', 'company_id'),
            'users' => array(self::HAS_MANY, 'User', 'company_id'),
            'menu' => array(self::HAS_MANY, 'Menu', 'company_id'),
            'proxies' => array(self::HAS_MANY, 'RKeeperProxy', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'logo' => 'Логотип',
			'hidden' => 'Скрытый',
			'deleted' => 'Удален',
            'description' => 'Описание',
            'website' => 'Сайт',
            'email' => 'E-Mail',
            'prefix' => 'Префикс',
            'workplaces' => 'Отделы кухни'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
        $criteria->compare('website',$this->website,true);
        $criteria->compare('email',$this->email,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('hidden',$this->hidden);
		$criteria->compare('deleted',$this->deleted);
        $criteria->compare('description',$this->description);
        $criteria->compare('workplaces',$this->workplaces);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize' => 15
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function createUser($params) {
        $user = new User();
        $user->setAttributes($params);
        if($user->save()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return array
     */
    public function getMenuCategories(){
        $list = array();
        foreach($this->menu as $row){
            $list[$row->id] = $row->name;
        }
        return $list;
    }

    /**
     * получение индекса последней версии данных
     */
    public static function getLastVersion($field = 'version'){
        $row = self::model()->find(array(
            'select' => $field,
            'order' => "{$field} DESC",
            'limit' => 1
        ));
        return is_object($row) ? $row->version : 0;
    }

    public static function getAllCompanies(){
        $list = Company::model()->findAll();
        return $list;
    }

}
