<h3>Получение данных заказа</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getOrder<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>order_id</em></strong>: ID заказа <b>(обязательный)</b><br/>
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
    {
        "status": "ok",
        "data": {
            "id": "110",
            "device_id": "2",
            "table_id": "34",
            "status_id": "2",
            "date_create": "1424194500",
            "payment_id": "2",
            "attention": "0",
            "separated": "0",
            "separated_info": "",
            "date_execute": "0",
            "date_close": "0",
            "comment": "",
            "restaurant_id": "47",
            "company_id": "9",
            "garcon_id": "36",
            "count_items": "2",
            "total_price": "760",
            "version": "0",
            "items": [
                {
                    "id": "160",
                    "order_id": "110",
                    "position_id": "225",
                    "status_id": "9",
                    "price": "240",
                    "count": "1"
                },
                {
                    "id": "161",
                    "order_id": "110",
                    "position_id": "224",
                    "status_id": "9",
                    "price": "520",
                    "count": "1"
                }
            ]
        }
    }
    </code></pre>
<hr/>