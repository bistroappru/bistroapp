<tr data-id="<?=$one->id?>">
    <td style="width:60px;"><div class="food_thumb"><img src="<?php echo $one->position->image;?>" width="60" height="60"></div></td>
    <td class="lefttext">
        <h4><?php echo $one->position->name;?></h4>
        <p class="small"><?php echo $one->position->measure;?></p>
    </td>
    <td><?php echo $one->status->name;?></td>
    <td><div class="nowrap"><?php echo $one->count;?>x &nbsp;&nbsp; <?php echo $one->price;?> <span class="small">руб.</span></div></td>
    <td>
        <div class="food_buttons">
            <?php if($one->status->prev_id){ ?>
                <a item-id="<?=$one->id?>" data-id="<?=$one->status->prev_id?>" class="set_order_item_status food_cancel" href="javascript:void(0);">
                    <span class="icon"></span>
                    Отменить
                </a>
            <?php } ?>

            <?php if($one->status->next_id){ ?>
                <a item-id="<?=$one->id?>" data-id="<?=$one->status->next_id?>" class="set_order_item_status status_<?=$one->status->next_id?>" href="javascript:void(0);">
                    <span class="icon"></span>
                    <?=OrderStatus::getParamById($one->status->next_id, 'name')?>
                </a>
            <?php } ?>

            <a item-id="<?=$one->id?>" class="delete_order_item food_delete" href="javascript:void(0);"><span class="icon"></span>Удалить</a>
        </div>
    </td>
</tr>