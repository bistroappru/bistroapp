<div style="overflow: hidden; background: url(http://bistroapp.ru/images/bg.jpg) no-repeat fixed center center / cover  rgba(0, 0, 0, 0);">
    <div style="width: 800px; margin: 50px auto;">
        <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #fff;">
            <tr style="height: 40px; background: #fff;">
                <th style="width:38%; padding: 20px; font-size: 20px;">Информация по заказу <b>№<?php echo $model->id;?></b></th>
                <th style="width:38%; padding: 20px; font-size: 20px;">Состав заказа</th>
            </tr>
            <tr>
                <td style="vertical-align:top; width:38%;border: 1px solid #fff">
                    <div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">ID</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->id);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>СТОЛИК</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->table_text);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>ГОСТИ</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->guests);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>СТАТУС</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->status->name);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>ОПЛАТА</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->payment->name);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Дата создания</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo date('H:i:s', $model->date_create);?></b> &nbsp;&nbsp;<span class="small"><?php echo date('d.m.Y', $model->date_create);?></span></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Дата обработки</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;">
                                <?php
                                if($model->date_execute){
                                    ?>
                                    <b><?php echo date('H:i:s', $model->date_execute);?></b> &nbsp;&nbsp;<span class="small"><?php echo date('d.m.Y', $model->date_execute);?></span>
                                    <?php
                                }else{
                                    ?>
                                    <b>&mdash;</b>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Дата закрытия</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;">
                                <?php
                                if($model->date_close){
                                    ?>
                                    <b><?php echo date('H:i:s', $model->date_close);?></b> &nbsp;&nbsp;<span class="small"><?php echo date('d.m.Y', $model->date_close);?></span>
                                    <?php
                                }else{
                                    ?>
                                    <b>&mdash;</b>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Внимание официанта</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><?php echo CHtml::encode($model->attention ? 'Да' : 'Нет');?></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Раздельный заказ</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><?php echo CHtml::encode($model->separated ? 'Да' : 'Нет');?></div>
                        </div>
                        <div style="min-height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="min-height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Информация о раздельном заказе</div>
                            <div style="min-height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><?php echo CHtml::encode($model->separated_info);?></div>
                        </div>
                    </div>
                </td>
                <td style="vertical-align:top; border: 1px solid #fff">
                    <?php
                    foreach($model->items as $one){
                        ?>
                        <ul style="min-height: 1px; padding: 10px; overflow: hidden;">
                            <li style="color: #fff;float: left;font-size: 18px;position: relative; min-height: 1px;overflow: hidden;">
                                <div class="pad">
                                    <img style="float: left; border-radius: 50%;" src="http://bistroapp.ru/<?php echo $one->position->image ? $one->position->image : '/data/images/no-image.png';?>" width="50" height="50">
                                    <div style="float: left; padding-left: 20px; text-shadow: 0 0 4px #555;">
                                        <?php echo $one->position->name;?>, <?php echo $one->position->measure;?>
                                        <br/>
                                        <?php echo $one->price;?> руб.
                                        х <?php echo $one->count;?>
                                    </div>
                                </div>
                            </li>
                            <li style="color: #fff; list-style: none; float: left;font-size: 18px;position: relative;"></li>
                            <li style="color: #fff; list-style: none; float: left;font-size: 18px;position: relative;"></li>
                        </ul>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <tr class="total_row">
                <td style="border: 1px solid #fff">
                    <div class="order_def">
                        <div style="float: left; padding: 10px; width: 150px; height: 38px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">Комментарий к заказу</div>
                        <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><?php echo CHtml::encode($model->comment);?></div>
                    </div>
                </td>
                <td style="border: 1px solid #fff">
                    <ul style="padding: 0px; margin: 0px;">
                        <li style="margin: 0px; float: left; height: 42px; line-height: 42px; padding: 8px 0px 8px 20px;list-style: none;background: none repeat scroll 0 0 #fff;color: #3d3a3e;float: left;font-size: 28px;width: 150px;">ИТОГО</li>
                        <li style="margin: 0px; color: #fff;float: left;font-size: 28px; width: 170px; list-style: none; float: left; height: 42px; line-height: 42px; padding: 8px 20px;"><?php echo $model->getTotalPrice(); ?> <span class="small">руб.</span></li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</div>