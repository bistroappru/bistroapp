<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_main');?>
    <div id="page">
        <div id="content_wrapper">
            <?php echo $content; ?>
        </div>
        <div class="clear"></div>
    </div>
<?php $this->endContent(); ?>