<?php
/* @var $this UserController */
/* @var $model User */
$this->pageTitle = Yii::app()->name . ' - Создание пользователя';
?>

<div class="column_center2">
    <?php $this->widget('application.components.UserList', array('restaurant_id' => $this->user_restaurant_id, 'filter_key' => 'lc_user_list'));?>
</div>

<div class="column_right">
    <div class="product_section">
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>