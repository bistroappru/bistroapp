<div style="overflow: hidden; background: url(http://bistroapp.ru/images/bg.jpg) no-repeat fixed center center / cover  rgba(0, 0, 0, 0);">
    <div style="margin: 50px auto;">
        <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #fff;">
            <tr style="height: 40px; background: #fff;">
                <th style="width:38%; padding: 20px; font-size: 20px;">Информация о фидбеке</th>
                <th style="width:38%; padding: 20px; font-size: 20px;">Содержание</th>
            </tr>
            <tr>
                <td style="vertical-align:top; width:38%;border: 1px solid #fff">
                    <div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)">ID</div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->id);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>Тема</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->title);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>ID устройства</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->device_id);?></b></div>
                        </div>
                        <div style="height: 40px; background: url(http://bistroapp.ru/images/order_data_line.png) repeat-x scroll 0 0 / 100% 3px rgba(0, 0, 0, 0);">
                            <div style="height: 20px; float: left; padding: 10px; width: 150px; background: url(http://bistroapp.ru/images/white60.png) repeat scroll 0 0 rgba(0, 0, 0, 0)"><b>Автор</b></div>
                            <div style="height: 20px; color: #fff; float: left; padding: 10px; width: 150px;"><b><?php echo CHtml::encode($model->author);?></b></div>
                        </div>
                    </div>
                </td>
                <td style="vertical-align:top; border: 1px solid #fff">
                    <div style="color: #fff; padding: 20px;"><?php echo CHtml::encode($model->description);?></div>
                </td>
            </tr>
        </table>
    </div>
</div>