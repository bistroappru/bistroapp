/**
 * Created by TWT on 02.09.14.
 */

function updateRestaurantTables(){
    var tables = [];
    $('.table_list .table_inline').each(function(){
        tables.push({
            id: $(this).attr('data-id'),
            name: $(this).attr('data-name'),
            action: $(this).attr('data-action')
        });
        $(this).find('.name').html($(this).attr('data-name')+' (id='+$(this).attr('data-id')+')');
    });
    $('#Restaurant_tables').val(JSON.stringify(tables));
}

$(document).ready(function(){
    $('#root').on('keyup', '#Restaurant_name', function(){
        $('#restaurant_title').html($(this).val());
    });

    $('#root').on('click', '#table_list_add .add', function(){
        var name = $('#table_list_add input[type=text]');
        if (name.val()){
            $('.table_list').append('<div class="table_inline" data-id="" data-action="new" data-name="'+name.val()+'">\
                <span class="name">'+name.val()+'</span>\
                <span class="edit"><i class="fa fa-pencil"></i></span>\
                <span class="del"><i class="fa fa-close"></i></span>\
                <span class="recovery"><i class="fa fa-undo"></i></span>\
            </div>');
            name.val('');
            updateRestaurantTables();
        }
    });

    $('#root').on('click', '#table_list_edit .cancel', function(){
        var name = $('#table_list_edit input[type=text]');
        name.val('');
        $('#table_list .table_inline[data-action=edit]').attr('data-action', 'update');
        $('#table_list_edit').hide();
        $('#table_list_add').show();
    });

    $('#root').on('click', '#table_list_edit .save', function(){
        var name = $('#table_list_edit input[type=text]');
        if (!name.val().length) return;
        $('#table_list .table_inline[data-action=edit]').attr({
            'data-action': 'update',
            'data-name': name.val()
        });
        name.val('');
        updateRestaurantTables();
        $('#table_list_edit').hide();
        $('#table_list_add').show();
    });

    $('#root').on('click', '#table_list .table_inline .edit', function(){
        var name = $('#table_list_edit input[type=text]');
        var row = $(this).closest('.table_inline');

        $('#table_list .table_inline[data-action=edit]').attr('data-action', 'update');
        row.attr('data-action', 'edit');

        name.val(row.attr('data-name'));
        $('#table_list_edit').show();
        $('#table_list_add').hide();
    });

    $('#root').on('click', '#table_list .table_inline .del', function(){
        var row = $(this).closest('.table_inline');
        if (row.attr('data-action') == 'new'){
            row.remove();
            updateRestaurantTables();
        }else{
            row.attr('data-action', 'remove');
            updateRestaurantTables();
            // ajax запрос
        }
    });

    $('#root').on('click', '#table_list .table_inline .recovery', function(){
        var row = $(this).closest('.table_inline');
        row.attr('data-action', 'update');
        updateRestaurantTables();
    });

    $('#root').on('click', '#restaurant_form .photo_list li', function(){
        $('.photo_list li').removeClass('selected');
        $(this).addClass('selected');
        $('.photo_buttons .ph_refresh').show();
        $('.photo_buttons .ph_del').show();
        return false;
    });

    $('#root').on('click', '#restaurant_form .photo_buttons .ph_del', function(){
        $('.photo_list li.selected').remove();
        $('.photo_buttons .ph_refresh').hide();
        $('.photo_buttons .ph_del').hide();
        return false;
    });

    $('#root').on('click', '#restaurant_form .photo_buttons .ph_add', function(){
        var index = time();
        $('.photo_list').append('<li><a><img id="Restaurant_photos_'+index+'_img" src="/images/photo.jpg" width="88" height="88"></a><input type="hidden" id="Restaurant_photos_'+index+'" name="Restaurant[photos]['+index+']" value="/images/photo.jpg"/></li>');
        return false;
    });

    $('#root').on('click', '#restaurant_form .phone_list .phone_del', function(){
        $(this).parent('.inp_wrap').remove();
        return false;
    });

    $('#root').on('click', '#restaurant_list a', function () {
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function (jqXHR, textStatus) {
                hideLoading();
                jAlert(jqXHR.responseText);
            },
            success: function (data) {
                $('#content_wrapper').html(data);
                history.pushState({}, '', link.attr('href'));
                inputChecked($('input[type=checkbox]'));
                hideLoading();
            },
            type: 'post'
        });
        return false;
    });

    $('#root').on('click', '#restaurant_create', function () {
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function (jqXHR, textStatus) {
                hideLoading();
                jAlert(jqXHR.responseText);
            },
            success: function (data) {
                $('#content_wrapper').html(data);
                history.pushState({}, '', link.attr('href'));
                hideLoading();
            },
            type: 'post'
        });
        return false;
    });

    $('#root').on('click', '#restaurant_form .phone_add', function(){
        $('.phone_list').append('<div class="inp_wrap"><input class="phone_valid" type="text" value="" name="Restaurant[phone]['+time()+']" placeholder="+7 (___) ___-__-__"/><a class="phone_del" href="#"></a></div>');
        $('.phone_list input').inputmask("+7 (999) 999-99-99");
        return false;
    });

    $('#root').on('click', '#restaurant_form .right_box .box_link', function(){
        $(this).closest('.right_box').find('.box_addit').slideToggle();
        $(this).toggleClass('opened');
        return false;
    });

    $('#root').on('submit', '#restaurant-form', function(e){
        e.preventDefault();
        var form = $(this);
        var type_before = form.attr('data-type');
        var type_after = '';
        $.ajax({
            url: form.attr('action'),
            beforeSend: showLoading,
            data: form.serialize(),
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                var newform = $('#restaurant-form');
                type_after = newform.attr('data-type');
                if (type_before != type_after) history.pushState({},'',newform.attr('action'));
                if (newform.find('.errorMessage').length == 0){
                    updateCompanyList($('#Restaurant_company_id').val());
                }
                inputChecked($('input[type=checkbox]'));
                hideLoading();
            },
            type: 'POST'
        });
    });

    $('#root').on('click','#restaurant_save',function(){
        $('#restaurant-form').submit();
        return false;
    });

    $('#root').on('click','#restaurant_set_deleted',function(){
        if ($('#Restaurant_deleted').val() == 1){
            $('#Restaurant_deleted').val(0);
        }else{
            $('#Restaurant_deleted').val(1);
        }
        $('#restaurant_save').trigger('click');
        return false;
    });

    $('#root').on('click','#restaurant_set_hidden',function(){
        if ($('#Restaurant_hidden').val() == 1){
            $('#Restaurant_hidden').val(0);
        }else{
            $('#Restaurant_hidden').val(1);
        }
        $('#restaurant_save').trigger('click');
        return false;
    });

    $('#root').on('change', '#lock_order_transfer', function(){
        if ($(this).is(':checked')){
            $('#Restaurant_lock_order_transfer').val(1);
        }else{
            $('#Restaurant_lock_order_transfer').val(0);
        }
    });

});