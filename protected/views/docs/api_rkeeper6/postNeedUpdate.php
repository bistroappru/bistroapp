<h3>Уведомление о справочниках которые необходимо обновить</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/r_keeper_api/v1/postNeedUpdate<br/>
</p>

<hr/>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF9}" ServerTimeStamp="UTC_DATETIME"/>
    <R_KEEPER>
        <REFERENCES>
            <REFERENCE RefName="MenuItems"/>
            <REFERENCE RefName="Employees"/>
            <REFERENCE RefName="CASHES"/>
        </REFERENCES>
    </R_KEEPER>
</BA_PROXY_REQUEST>');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK"></BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="ERROR">
        <ERROR code="18" description="Не задан ни один справочник для обновления"/>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>

