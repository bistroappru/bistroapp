<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 10.12.14
 * Time: 17:29
 */

class RKeeper7Parse {


    public static function updateOrders($list = array()){
        foreach($list as $queue){
            Order::model()->parseKeeperData($queue);
        }
    }

    public static function updateReferences($list = array()){
        foreach($list as $queue){
            var_dump($queue->getRefName());
            switch (strtoupper($queue->getRefName())){
                case 'RESTAURANTS':
                    Restaurant::model()->parseKeeperData($queue);
                break;
                case 'MENUITEMS':
                    Position::model()->parseKeeperData($queue);
                break;
                case 'CATEGLIST':
                    Menu::model()->parseKeeperData($queue);
                break;
                case 'CASHES':
                    Cash::model()->parseKeeperData($queue);
                break;
                case 'CASHGROUPS':
                    CashGroups::model()->parseKeeperData($queue);
                break;
                case 'EMPLOYEES':
                    User::model()->parseKeeperData($queue);
                break;
                case 'HALLPLANS':
                    RestaurantPlan::model()->parseKeeperData($queue);
                break;
                case 'TABLES':
                    RestaurantTable::model()->parseKeeperData($queue);
                break;
                default:
                    $queue->setFail("Не найден обработчик для справочника {$queue->getRefName()}");
                break;
            }
        }
    }
}