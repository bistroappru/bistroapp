<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 23.05.14
 * Time: 10:15
 */

class AbcHelper
{
    public static function test()
    {
        return 1;
    }

    public static function trimString($text, $length=50, $suffix='...')
    {
        $string = strip_tags($text);
        $result = iconv("utf-8", "windows-1251", $string);
        $result = implode(array_slice(explode('<br>',wordwrap($result,((intval($length) > 0) ? $length : 150),'<br>',false)),0,1));
        $result = iconv("windows-1251","utf-8", $result);
        return $result.($result!=$string ? $suffix : '');
    }

    public static function createUUID(){
        mt_srand((double)microtime()*10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return $uuid;
    }

    public static function clearUserPrefix($username){
        if (!$username) return;
        $tmp = explode('_', $username);
        unset($tmp[0]);
        return implode('_', $tmp);
    }

    public static function getYesNo($val){
        $array = array(
            0=>'Нет',
            1=>'Да'
        );
        return $array[$val];
    }

    public static function prepareModelsToArray($list){
        $out = array();
        if (is_array($list)){
            foreach($list as $v){
                $out[$v->id] = $v->getAttributes();
            }
        }
        return $out;
    }

    public static function allCompanies($id=''){
        if ($id){
            $condition = 'id = '.$id;
        }
        $tmp = Company::model()->findAll($condition);
        $companies = array();
        if(is_array($tmp)){
            foreach($tmp as $k=>$v){
                $companies[$v->id] = $v->name;
            }
        }

        return $companies;
    }

    public static function allCompanyRestaurants($id=''){
        $restaurants = array();
        if ($id){
            $tmp = Restaurant::model()->findAll('company_id = '.$id);
            if(is_array($tmp)){
                foreach($tmp as $k=>$v){
                    $restaurants[$v->id] = $v->getTitle();
                }
            }
        }
        return $restaurants;
    }

    public static function getRoleList(){
        return array(
            'admin' => 'Администратор портала',
            'owner'=>'Владелец сети',
            'manager'=>'Администратор ресторана',
            'cook'=>'Повар',
            'barman'=>'Бармен',
            'garcon'=>'Оффициант'
        );
    }

    public static function getRoleName($role){
        switch ($role){
            case 'admin':
                return 'Администратор портала';
            case 'owner':
                return 'Владелец сети';
            case 'manager':
                return 'Администратор ресторана';
            case 'cook':
                return 'Повар';
            case 'barman':
                return 'Бармен';
            case 'garcon':
                return 'Оффициант';
        }
        return 'Не определено';
    }

    public static function getAccessCreateRoles($role){
        // TODO сделать массив, и делать slice до заданной роли
        $roles = array();
        switch ($role){
            case 'admin':
                $roles = array('owner'=>self::getRoleName('owner'),'manager'=>self::getRoleName('manager'),'cook'=>self::getRoleName('cook'),'barman'=>self::getRoleName('barman'),'garcon'=>self::getRoleName('garcon'));
            break;
            case 'owner':
                $roles = array('manager'=>self::getRoleName('manager'),'cook'=>self::getRoleName('cook'),'barman'=>self::getRoleName('barman'),'garcon'=>self::getRoleName('garcon'));
            break;
            case 'manager':
                $roles = array('cook'=>self::getRoleName('cook'),'barman'=>self::getRoleName('barman'),'garcon'=>self::getRoleName('garcon'));
            break;
        }
        return $roles;
    }

    public static function simple_xml_attr_to_array($item){
        $out = array();
        if (is_object($item) && $item->attributes()->count()){
            foreach($item->attributes() as $k=>$v){
                $out[$k] = (string) $v;
            }
        }
        return $out;
    }
}