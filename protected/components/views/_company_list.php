<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:25
 */
?>

<div class="left_search" id="company_search">
    <form>
        <input class="inp_left_srh" autocomplete="off" type="text" name="company_search" value="Поиск" onfocus="this.value=(this.value=='Поиск')? '' : this.value ;" onblur="this.value=(this.value=='')? 'Поиск' : this.value ;">
        <input class="submit_left_srh" type="submit" value=" ">
    </form>
</div>
<ul id="company_list" class="company_menu test"></ul>
<ul class="leftmenu_buttons">
    <li><a id="company_create" href="<? echo CHtml::normalizeUrl('/admin/company/create');?>"><img src="/images/leftmenu-5.png" width="50" height="50" alt=""><span>Создать компанию</span></a></li>
</ul>

<script>
    var company_json = <?=CJSON::encode($company_list)?>;
    var company_id = <?=$company_id?>;

    function searchCompanies(){
        var search = $('#company_search input[name=company_search]').val();
        if (!search || search == 'Поиск') search = '';
        var ul_html = '';
        if (company_json['ids']){
            $.each(company_json['ids'], function(k, v){
                pos = company_json['list'][v].name.toLowerCase().indexOf(search.toLowerCase());
                if (pos != -1){
                    ul_html += '<li data-id="'+company_json['list'][v].id+'" class="'+(company_id == company_json['list'][v].id ? 'active':'')+' '+(company_json['list'][v].deleted == 1 ? 'style_del':'')+' '+(company_json['list'][v].hidden == 1 ? 'style_hid':'')+'"> \
                    <a href="/admin/company/update/id/'+ company_json['list'][v].id+'"> \
                        <span class="company_ico"><img src="'+(company_json['list'][v].logo ? company_json['list'][v].logo : '<?=Yii::app()->params['emptyImage']?>')+'" width="40" height="41"></span> \
                        <span class="company_name">'+company_json['list'][v].name+'</span> \
                        <span class="company_rest">ресторанов ('+ company_json['list'][v].restaurants+')</span> \
                    </a> \
                </li>';
                }
            });
        }
        $('#company_list').html(ul_html);
    }
    searchCompanies();

    $('#company_search input[name=company_search]').on('keyup', function(){
        searchCompanies();
    });

    $('#company_search form').on('submit', function(e){
        e.preventDefault();
        searchCompanies();
    });
</script>