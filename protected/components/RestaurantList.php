<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class RestaurantList extends CWidget{
    public $company_id = 0;
    public $restaurant_id = 0;
    public $fields = array('id','name','description','hidden','deleted');
    public function run(){
        if (!$this->company_id) return;

        $company = Company::model()->findByPk($this->company_id);


        $image_url = $_SERVER['DOCUMENT_ROOT'].$company->logo;
        if(is_file($image_url)) {
            $image_info = pathinfo($image_url);
            if(!file_exists($_SERVER['DOCUMENT_ROOT'].'data/images/logos/thumbnail/'.$image_info['filename'] . '_average.'.$image_info['extension'])) {
                $image = Yii::app()->image->load($_SERVER['DOCUMENT_ROOT'].$company->logo);
                $image->resize(80, 80)->quality(100);
                $average_image = 'data/images/logos/thumbnail/'.$image_info['filename'] . '_average.'.$image_info['extension'];
                if($image->save($average_image)) {
                    $company->logo = $average_image;
                }
            }
        }


        $criteria = new CDbCriteria();
        $criteria->select = $this->fields;
        $criteria->order = 'name ASC';
        $criteria->compare('company_id', $this->company_id);
        //if ($rid = Yii::app()->user->restaurant_id) $criteria->compare('id', $rid);


        $restaurant_list = Restaurant::model()->findAll($criteria);

        $list = array();
        $tmp = Restaurant::model()->findAll($criteria);
        foreach($tmp as $k=>$v){




            $list['ids'][] = $v->id;
            $list['list'][$v->id] = $v->getAttributes($this->fields);
        }

        $this->render('_restaurant_list',array(
            'restaurant_list' => $list,
            'restaurant_id' => $this->restaurant_id,
            'company' => $company
        ));
    }
}