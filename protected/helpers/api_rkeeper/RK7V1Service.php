<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 22.01.15
 * Time: 16:31
 */

class RK7V1Service {

    private static $_api_errors = array(
        // XML
        100 => 'Ошибка синтаксиса XML. Не удалось обработать тело xml-запроса',
        101 => 'Ошибка структуры XML. Корневой элемент xml-запроса должен быть BA_PROXY_REQUEST',
        102 => 'Ошибка структуры XML. Не удалось найти узел {path}',
        103 => 'Ошибка структуры XML. Команда должна быть PayOrder',

        // Model
        200 => 'Не удалось создать прокси-сервер. ',
        201 => 'Прокси-сервер с UUID {uuid} не зарегистрирован',
        202 => 'Ошибка сохранения очереди. ',
        203 => 'Ошибка сохранения запроса. ',
        204 => 'Ошибка удаления очереди после подготовки запроса. ',

        // Other
        300 => 'Не удалось найти MID сервер для компании ID = {id}',
        301 => 'Запроса с UUID {uuid} не существует',

        // Payment
        400 => 'Не удалось найти заказ с заданным GUID',
        401 => 'Суммы заказа не совпадают',
        402 => 'Ошибка сохранения заказа',


        /*
        10 => 'Не удалось распарсить тело запроса. Проверьте тело запроса на наличие синтакисческих ошибок',
        11 => '{attribute} не может быть пустым',
        12 => '{attribute} должен быть целым числом',
        13 => '{attribute} превышает допустимую длинну в 255 символов',
        14 => '{attribute} со значением {value} уже существует',
        15 => 'Значение {value} параметра {attribute} должно быть одним из следующих: REF, MID',
        16 => '{attribute} с идентификатором {value} не существует',
        17 => 'Прокси-сервер с UUID {value} не зарегистрирован',
        18 => 'Не задан ни один справочник для обновления',
        19 => 'Не задано имя справочника для обновления',
        20 => '{attribute} {value} должен иметь валидный UUID вида /^\{?[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}\}?$/'
        */
    );

    /**
     * @param $code
     * @param bool $parse
     * @param array $params
     * @return mixed
     */
    public static function getError($code, $parse = false, $params = array()){
        if (isset($code) && array_key_exists($code, self::$_api_errors)){
            $msg = self::$_api_errors[$code];

            if ($parse && count($params)){
                $msg = str_replace(array_keys($params), array_values($params), self::$_api_errors[$code]);
            }

            return $msg;
        }
        return $code . ' (Неизвестная ошибка)';
    }

    private static function getFistValidationError(CActiveRecord $model){
        $errors = $model->getErrors();
        $error = reset($errors);
        return $error[0];
    }

    /**
     * @param $input
     * @return SimpleXMLElement
     * @throws Exception
     */
    public static function XMLParseRequest($input){
        $xml_object = simplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOERROR);

        if (!is_object($xml_object) || !($xml_object instanceof SimpleXMLElement)){
            throw new Exception(self::getError(100), 100);
        }

        if ($xml_object->getName() !== 'BA_PROXY_REQUEST'){
            throw new Exception(self::getError(101), 101);
        }

        return $xml_object;
    }

    /**
     * @param SimpleXMLElement $xml_object
     * @param $path
     * @return SimpleXMLElement[]
     * @throws Exception
     */
    public static function XMLGetPath(SimpleXMLElement $xml_object, $path){
        if (! $xml_element = $xml_object->xpath($path)){
            throw new Exception(self::getError(102, true, array('{path}' => $path)), 102);
        }
        return $xml_element;
    }

    /**
     * @param SimpleXMLElement $xml_object
     * @param $path
     * @return SimpleXMLElement[]
     * @throws Exception
     */
    public static function XMLParsePayData(SimpleXMLElement $input){
        $xml_object = simplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOERROR);

        if (!is_object($xml_object) || !($xml_object instanceof SimpleXMLElement)){
            throw new Exception(self::getError(100), 100);
        }

        if ((string) $xml_object->RK6CMD['CMD'] != 'PayOrder'){
            throw new Exception(self::getError(103), 103);
        }

        $data = array();
        $data['uuid'] = (string) $xml_object->Order['guid'];
        $data['amount'] = (int) $xml_object->Payments->Payment['amount']/100;

        if (count($xml_object->Dishes->Dish)) {
            foreach ($xml_object->Dishes->Dish as $dish) {
                $data['dishes'][(string) $dish['id']] = array(
                    'id' => trim((string) $dish['id']),
                    'quantity' => trim((string) $dish['quantity'])/1000,
                    'sum' => trim((string) $dish['sum'])/100,
                );
            }
        }

        return $data;
    }

    /**
     * @param array $attributes
     * @return RKeeperProxy
     * @throws CDbException
     */
    public static function createProxyServer($attributes = array()){
        $model = new RKeeperProxy;
        $model->attributes = $attributes;

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new CDbException(self::getError(200) . self::getFistValidationError($model), 200);
            }else{
                throw new CDbException(self::getError(200) . 'Неизвестная ошибка сохранения в базу данных', 200);
            }
        }

        return $model;
    }

    /**
     * @param $uuid
     * @return RKeeperProxy
     * @throws Exception
     */
    public static function loadProxyServer($uuid){
        $model = RKeeperProxy::model()->findByAttributes(array('proxy_uuid' => $uuid));
        if($model === null)
            throw new Exception(self::getError(201, true, array('{uuid}' => $uuid)), 201);
        return $model;
    }

    /**
     * @param array $attributes
     * @return RKeeperTurn
     * @throws CDbException
     */
    public static function createTurn($attributes = array()){
        $model = new RKeeperTurn();
        $model->attributes = $attributes;

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new CDbException(self::getError(202) . self::getFistValidationError($model), 202);
            }else{
                throw new CDbException(self::getError(202) . 'Неизвестная ошибка сохранения в базу данных', 202);
            }
        }

        return $model;
    }

    public static function getCompanyMID($company_id){
        $model = RKeeperProxy::loadCompanyProxy($company_id, RKeeperProxy::TYPE_MID);
        if ($model === null)
            throw new Exception(self::getError(300, true, array('{id}' => $company_id)), 300);
        else
            return $model->id;
    }

    public static function prepareRequests(RKeeperProxy $proxy){
        $turns = RKeeperTurn::model()->findAllByAttributes(array('proxy_id' => $proxy->id));
        if(count($turns)){
            foreach($turns as $turn){
                switch ($turn->reference){
                    case 'PRICES':
                        $request_body = '';
                        break;
                    case 'VISITS':
                        $request_body = '';
                        break;
                    default:
                        $suffix = ($proxy->proxy_type == RKeeperProxy::TYPE_RK6) ? '_'.RKeeperProxy::TYPE_RK6 : '';
                        $request_body = Yii::app()->controller->renderPartial('_request_GetRefData'.$suffix, array('ref_name' => $turn->reference), true);
                        break;
                }

                try {
                    $transaction = Yii::app()->db->beginTransaction();

                    switch($proxy->proxy_type){
                        case RKeeperProxy::TYPE_REF :
                            $request_type = RKeeperQueue::TYPE_REF;
                        break;
                        case RKeeperProxy::TYPE_RK6 :
                            $request_type = RKeeperQueue::TYPE_RK6;
                        break;
                        case RKeeperProxy::TYPE_MID :
                        default:
                            $request_type = RKeeperQueue::TYPE_MID;
                        break;
                    }

                    self::createRequest(array(
                        'uuid' => AbcHelper::createUUID(),
                        'proxy_id' => $proxy->id,
                        'status' => RKeeperQueue::STATUS_WAIT,
                        'body' => $request_body,
                        'type' => $request_type,
                        'create_time' => time()
                    ));

                    if (!$turn->delete()){
                        throw new CDbException(self::getError(204), 204);
                    }

                    $transaction->commit();
                } catch (Exception $e){
                    // LOG
                    if ($transaction)
                        $transaction->rollback();

                    continue;
                }
            }
        }
    }

    public static function createRequest($attributes){
        $model = new RKeeperQueue();
        $model->attributes = $attributes;

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new CDbException(self::getError(203) . self::getFistValidationError($model), 203);
            }else{
                throw new CDbException(self::getError(203) . 'Неизвестная ошибка сохранения в базу данных', 203);
            }
        }

        return $model;
    }

    public static function getRequests(RKeeperProxy $proxy){
        $list = RKeeperQueue::model()->getProxyRequests($proxy, 1);
        $requests = array();
        foreach($list as $v){
            $v->status = RKeeperQueue::STATUS_ACTIVE;
            $v->request_time = time();
            if ($v->save()){
                $requests[] = $v;
            }
        }
        $proxy->updateLastRequestTime();

        return $requests;
    }

    public static function setResponse($request_id, $response){
        $model = RKeeperQueue::model()->loadModelByRequest($request_id);

        if ($model === null){
            throw new Exception(self::getError(301, true, array('{uuid}' => $request_id)), 301);
        }

        $model->status = RKeeperQueue::STATUS_RESPONSE;
        $model->response = $response;
        $model->response_time = time();
        if (!$model->save()){
            if ($model->hasErrors()){
                throw new CDbException(self::getError(203) . self::getFistValidationError($model), 203);
            }else{
                throw new CDbException(self::getError(203) . 'Неизвестная ошибка сохранения в базу данных', 203);
            }
        }

        return $model;
    }

    public static function getRequestsForParse($limit = 10){
        $queues = RKeeperQueue::model()->findAll(array(
            'condition' => 'status = :status',
            'params' => array(
                ':status' => RKeeperQueue::STATUS_RESPONSE
            ),
            'order' => 'id ASC',
            'limit' => $limit
        ));

        return $queues;
    }

    public static function getWaitRequests($limit = 50){
        $criteria = new CDbCriteria();
        $criteria->order = 'id ASC';
        $criteria->limit = $limit;
        $criteria->addCondition('status = ' . RKeeperQueue::STATUS_WAIT);
        $criteria->addCondition('create_time < ' . (time()-60*10));
        $criteria->addInCondition('type', array(RKeeperQueue::TYPE_ORDER_RK6, RKeeperQueue::TYPE_ORDER));

        $queues = RKeeperQueue::model()->findAll($criteria);

        return $queues;
    }

    public static function paymentOrder($pay_data, $proxy){
        $order = Order::model()->findByAttributes(array('out_id' => $pay_data['uuid'], 'restaurant_id' => $proxy->restaurant_id));
        if ($order){
            if ($order->total_price != $pay_data['amount']){
                throw new CException(self::getError(401), 401);
            }

//            $items = array();
//            foreach ($order->items as $item){
//                var_dump($item->position->out_id);
//            }

            $order->status_id = Order::STATUS_PAID;
            if (!$order->save()){
                if ($order->hasErrors()){
                    throw new CDbException(self::getError(402) . self::getFistValidationError($order), 402);
                }else{
                    throw new CDbException(self::getError(402) . 'Неизвестная ошибка сохранения в базу данных', 402);
                }
            }

        }else{
            throw new CException(self::getError(400), 400);
        }

        return $order;
    }

    public static function failParseHandler(RKeeperQueue $queue){
        $transaction = Yii::app()->db->beginTransaction();
        try{

            // отмена заказа
            if ($queue->getCMD() == 'CreateOrder'){
                $data = unserialize($queue->data);
                if ($data['order_id'] && $order = Order::model()->findByPk($data['order_id'])){
                    $order->status_id = Order::STATUS_CANCELED;
                    if ($order->save()){
                        $order->sendPush('cancel');
                    }
                }
            }

            // отмена блюд
            if ($queue->getCMD() == 'UpdateOrder'){
                $data = unserialize($queue->data);
                if ($data['order_id'] && $order = Order::model()->findByPk($data['order_id'])){
                    if (is_array($data['order_items']) && count($data['order_items'])){
                        if (OrderItem::model()->updateAll(array('deleted' => 1), 'id IN ('.implode(',', $data['order_items']).')')){
                            // обновление суммы заказа
                            $order->calculateTotalPrice();
                            // обновление кол-ва заказа
                            $order->calculateTotalCount();
                            $order->sendPush('fail_update_rk');
                        }
                    }
                }
            }
            //

            $transaction->commit();
        }catch(Exception $e) {
            $transaction->rollback();
        }
    }

}