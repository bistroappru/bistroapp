<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 18.03.15
 * Time: 16:30
 */

class RKeeperQueueParser {
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 2;
    const STATUS_DELETED = 3;

    private $queue;

    public function __construct(RKeeperQueue $queue){
        $this->queue = $queue;
    }

    public static function getIntStatus($key = 'rsDraft'){
        $list = array(
            'rsActive' => self::STATUS_ACTIVE,
            'rsDraft' => self::STATUS_DRAFT,
            'rsDeleted' => self::STATUS_DELETED,
        );

        if (!array_key_exists($key, $list))
            $key = 'rsDraft';

        return $list[$key];
    }

    public static function XMLParseResponse($input){
        //$this->queue->response = str_replace(array('&#1;'), array(''), $this->queue->response);
        $xml_object = simplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOERROR);

        if (!is_object($xml_object) || !($xml_object instanceof SimpleXMLElement)){
            throw new Exception('Не удалось проебразовать response в xml');
        }

        return $xml_object;
    }

    private function responsePrepare(){
        $response = $this->XMLParseResponse($this->queue->response);

        if ((string) $response['Status'] !== 'Ok'){
            throw new Exception('Статус должен быть: Ok'.((string)$response['ErrorText'] ? '. '.(string)$response['ErrorText'] : ''));
        }

        if ((string) $response['CMD'] !== $this->queue->getCMD()){
            throw new Exception('Команда запроса и ответа не совпадает');
        }

        switch ($this->queue->getRKNum()){
            case 6:
                $tmp = (string) $response->RK6Reference['Name'];
            break;
            case 7:
            default:
                $tmp = (string) $response->RK7Reference['Name'];
            break;
        }

        if ($tmp !== strtoupper($this->queue->getRefName())){
            throw new Exception('Справочник запроса и ответа не совпадает');
        }

        return $response;
    }

    public function parse(){
        switch($this->queue->getCMD()){
            case 'GetRefData':
                $method = 'parse'.ucfirst(str_replace(array('.dbf','.db'),'',strtolower($this->queue->getRefName()))).'RK'.$this->queue->getRKNum();
                if(method_exists($this, $method)){
                    $this->$method();
                }else{
                    throw new Exception('Не найден метод-обработчик '.$method);
                }
            break;
            case 'CreateOrder':
                $this->parseCreateOrder();
            break;
            case 'UpdateOrder':
                $this->parseUpdateOrder();
                break;
            case 'SaveOrder':
                $this->parseSaveOrder();
            break;

            default:
                throw new Exception('Не найден обработчик');
            break;
        }
    }

    // ============================================================================================================
    // ПАРСЕРЫ ЗАКАЗОВ
    // ============================================================================================================

    public function parseCreateOrder(){
        if ($this->queue->proxy->proxy_type == RKeeperProxy::TYPE_RK6){
            $this->parseCreateOrderRK6();
        }else{
            $this->parseCreateOrderRK7();
        }
    }

    public function parseUpdateOrder(){
        if ($this->queue->proxy->proxy_type == RKeeperProxy::TYPE_RK6){
            $this->parseUpdateOrderRK6();
        }else{
            $this->parseUpdateOrderRK7();
        }
    }

    public function parseSaveOrder(){

    }

    public function parseCreateOrderRK6(){
        $response = $this->responsePrepare();
        $data = array();
        $data['out_id'] = (string) $response['Guid'];
        $data['table_text'] = (string) $response['Table'];

        $response_data = unserialize($this->queue->data);
        // проверка Guid запроса и ответа
        $request = $this->XMLParseResponse($this->queue->body);
        if ((string) $response['Guid'] !== (string) $request->RK6CMD->Order['Guid']){
            throw new Exception('GUID запроса и ответа не совпадают!');
        }

        $model = Order::model()->findByPk($response_data['order_id']);

        if ($model === null){
            throw new Exception('Заказ с id = '.$response_data['order_id'].' не найден!');
        }

        if ($model->isFinishStatus()){
            throw new Exception('Заказ с id = '.$response_data['order_id'].' не доступен для изменения!');
        }

        $model->setAttributes($data, false);
        $model->setAttribute('status_id', Order::STATUS_ADOPTED);

        if (!$model->save(false)){
            throw new Exception('Не удалось обновить заказ с id = ' . $model->id);
        }

        if (is_array($response_data['order_items']) && count($response_data['order_items'])){
            OrderItem::model()->updateAll(array('status_id' => OrderItem::STATUS_ADOPTED), 'id IN ('.implode(',', $response_data['order_items']).')');
        }

        $model->sendPush('adopt_rk');
        $model->sendEmail('adopt_rk');
        $model->sendSMS('adopt_rk');
    }

    public function parseCreateOrderRK7(){
        $response = $this->responsePrepare();

        $data = array();
        $data['out_id'] = (string) $response['OrderID'];
        $data['out_visit'] = (string) $response['VisitID'];
        $data['status_id'] = Order::STATUS_ADOPTED;

        $response_data = unserialize($this->queue->data);
        $model = Order::model()->findByPk($response_data['order_id']);

        if ($model === null){
            throw new Exception('Заказ с id = '.$response_data['order_id'].' не найден!');
        }

        if ($model->isFinishStatus()){
            throw new Exception('Заказ с id = '.$response_data['order_id'].' не доступен для изменения!');
        }

        $model->setAttributes($data);

        if (!$model->save(false)){
            throw new Exception('Не удалось обновить заказ с id = ' . $model->id);
        }

        $model->sendPush('adopt_rk');
        $model->sendEmail('adopt_rk');
        $model->sendSMS('adopt_rk');

        // создать запрос на добавление позиций
        RKeeperQueue::createRKeeperOrderItems($model);
    }

    public function parseUpdateOrderRK6(){
        $response = $this->responsePrepare();
        $response_data = unserialize($this->queue->data);

        // проверка Guid запроса и ответа
        $request = $this->XMLParseResponse($this->queue->body);
        if ((string) $response['Guid'] !== (string) $request->RK6CMD->Order['Guid']){
            throw new Exception('GUID запроса и ответа не совпадают!');
        }

        $model = Order::model()->findByPk($response_data['order_id']);
        if ($model === null){
            throw new Exception('Заказ с id = '.$response_data['order_id'].' не найден!');
        }

        if (is_array($response_data['order_items']) && count($response_data['order_items'])){
            OrderItem::model()->updateAll(array('status_id' => OrderItem::STATUS_ADOPTED), 'id IN ('.implode(',', $response_data['order_items']).')');
        }

        $model->sendPush('update_rk');
        $model->sendEmail('update_rk');
        $model->sendSMS('update_rk');
    }


    public function parseUpdateOrderRK7(){}


    // ============================================================================================================
    // ПАРСЕРЫ СПРАВОЧНИКОВ RK6
    // ============================================================================================================
    public function parseMenuitemsRK6(){
        $response = $this->responsePrepare();

        if ($response->RK6Reference->Items->count()){
            $sort = Position::getMaxSort();
            foreach ($response->RK6Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['SIFR'] || !$item['PARENT']) continue;

                $criteria = new CDbCriteria();
                $criteria->addCondition('out_id = ' . $item['SIFR']);
                $criteria->addInCondition('menu_id', Menu::getCategoriesIds($this->queue->proxy->company_id));

                $model = Position::model()->find($criteria);
                if ($model === null){
                    $model = new Position();
                }

                if ($model->name == $model->name_orig){ // если оригинал отличается от названия не трогаем
                    $model->setAttribute('name', $item['NAME']);
                }

                if ($model->description == $model->description_orig){ // если оригинал отличается от названия не трогаем
                    $model->setAttribute('description', $item['DESC']);
                }

                $model->setAttributes(array(
                    'out_id' => $item['SIFR'],
                    'is_group' => 0, // TODO проверка на групповое
                    'name_orig' => $item['NAME'],
                    'description_orig' => $item['DESC'],
                    'menu_id' => Menu::getIdByOutId($item['PARENT'], true, $this->queue->proxy->company_id),
                    'sort' => $sort++,
                    'hidden' => 0,
                    
					//'deleted' => ($item['DEL'] == 'True' ? 1 : 0),
					
                    // обработать поле ABSENT (нет в наличии)
                    //'in_stock' => '', // TODO доработать определение наличия
                    'price' => $item['PRICE'],
                    'price_mode' => Position::PRICE_MODE_ONE,
                    'measure' => 'штук',
                ));

                $in_stock = array();
                if ($model->in_stock){
                    $in_stock = explode(',', $model->in_stock);
                }
                $in_stock[] = $this->queue->proxy->restaurant_id;
                $model->in_stock = implode(',', array_unique($in_stock));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить блюдо с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseCateglistRK6(){
        $response = $this->responsePrepare();
        if ($response->RK6Reference->Items->count()){
            $sort = Menu::getMaxSort($this->queue->proxy->company_id);
            foreach ($response->RK6Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['SIFR']) continue;

                $model = Menu::model()->findByAttributes(array('out_id' => $item['SIFR'], 'company_id' => $this->queue->proxy->company_id));
                if ($model === null){
                    $model = new Menu();
                }

                if ($model->name == $model->name_orig){ // если оригинал отличается от названия не трогаем
                    $model->setAttribute('name', $item['NAME']);
                }

                if ($model->description == $model->description_orig){ // если оригинал отличается от названия не трогаем
                    $model->setAttribute('description', $item['DESC']);
                }

                $model->setAttributes(array(
                    'out_id' => $item['SIFR'],
                    'name_orig' => $item['NAME'],
                    'description_orig' => $item['DESC'],
                    'company_id' => $this->queue->proxy->company_id,
                    'sort' => $sort++,
                    'hidden' => 0,

                    //'deleted' => ($item['DEL'] == 'True' ? 1 : 0),

                    'parent_id' => (int) Menu::getIdByOutId($item['PARENT'], true, $this->queue->proxy->company_id),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить категорию с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseEmployeesRK6(){
        $response = $this->responsePrepare();
        if ($response->RK6Reference->Items->count()){
            foreach ($response->RK6Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['SIFR']) continue;

                $model = User::model()->findByAttributes(array('out_id' => $item['SIFR'], 'company_id' => $this->queue->proxy->company_id));
                if ($model === null){
                    $model = new User();
                }

                $model->setAttributes(array(
                    'out_id' => $item['SIFR'],
                    'out_code' => $item['CODE'],
                    'company_id' => $this->queue->proxy->company_id ? $this->queue->proxy->company_id : $this->queue->proxy->restaurant->company_id,
                    'restaurant_id' => $this->queue->proxy->restaurant_id,
                ));

                if ($model->isNewRecord){
                    $model->setAttributes(array(
                        'username' => $this->queue->proxy->company->prefix . '_user' . $item['SIFR'],
                        'fullname' => $item['NAME'],
                        'email' => $this->queue->proxy->company->prefix . '_user' . $item['SIFR'] . '@bistroapp.ru',
                        'deleted' => ($item['DEL'] == 'True' ? 1 : 0),
                        'password' => '123',
                    ));

                    switch($item['TYPE']){
                        case 'B':
                            $role = User::ROLE_BARMAN;
                        break;
                        case 'K':
                            $role = User::ROLE_CASHIER;
                        break;
                        case 'M':
                            $role = User::ROLE_MANAGER;
                        break;
                        case 'W':
                        default:
                            $role = User::ROLE_GARCON;
                        break;
                    }
                    $model->setAttribute('role', $role);

                }

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить пользователя с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseAcheckRK6(){}
    public function parseAdcheckRK6(){}
    public function parseApcheckRK6(){}
    public function parseArcheckRK6(){}
    public function parseAvcheckRK6(){}
    public function parseCategRK6(){}
    public function parseChargesRK6(){}
    public function parseCombogRK6(){}
    public function parseCombomRK6(){}
    public function parseControlRK6(){}
    public function parseMenuRK6(){}
    public function parseModifyRK6(){}
    public function parseMoneyRK6(){}
    public function parsePersonalRK6(){}
    public function parseReasonsRK6(){}
    public function parseOrdersRK6(){}

    // ============================================================================================================
    // ПАРСЕРЫ СПРАВОЧНИКОВ RK7
    // ============================================================================================================

    public function parseCashesRK7(){
        $response = $this->responsePrepare();

        // оставить возможность при обновлении для учета полей которые необходимо обновить
        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){

                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident'] || !$item['MainParentIdent']) continue;

                $model = Cash::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new Cash();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'out_code' => $item['Code'],
                    'name' => $item['Name'],
                    'status' => self::getIntStatus($item['Status']),
                    'cashgroup_id' => CashGroups::getIdByOutId($item['MainParentIdent'], true),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить кассу с out_id = ' . $model->out_id);
                }

            }
        }
    }

    public function parseCashgroupsRK7(){
        $response = $this->responsePrepare();
        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident'] || !$item['MainParentIdent']) continue;

                $model = CashGroups::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new CashGroups();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'name' => $item['Name'],
                    'status' => self::getIntStatus($item['Status']),
                    'restaurant_id' => Restaurant::getIdByOutId($item['MainParentIdent'], true),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить кассовый сервер с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseCateglistRK7(){
        $response = $this->responsePrepare();
        if ($response->RK7Reference->Items->count()){
            $sort = Menu::getMaxSort($this->queue->proxy->company_id);
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident']) continue;

                $model = Menu::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new Menu();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'name' => $item['Name'],
                    'description' => $item['Comment'],
                    'company_id' => $this->queue->proxy->company_id,
                    'sort' => $sort++,
                    'hidden' => ($item['Status'] == 'rsDraft' ? 1 : 0),
                    'deleted' => ($item['Status'] == 'rsDeleted' ? 1 : 0),
                    'parent_id' => (int) Menu::getIdByOutId($item['MainParentIdent'], true, $this->queue->proxy->company_id),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить категорию с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseEmployeesRK7(){
        $response = $this->responsePrepare();
        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident']) continue;

                $model = User::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new User();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'out_code' => $item['Code'],
                    'company_id' => $this->queue->proxy->company_id,
                ));

                if ($model->isNewRecord){
                    $model->setAttributes(array(
                        'username' => $this->queue->proxy->company->prefix . '_user' . $item['Ident'],
                        'fullname' => $item['Name'],
                        'email' => $this->queue->proxy->company->prefix . '_user' . $item['Ident'] . '@bistroapp.ru',
                        'blocked' => 1,
                        'role' => User::ROLE_GARCON,
                        'password' => '123',
                    ));
                }

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить пользователя с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseHallplansRK7(){
        $response = $this->responsePrepare();
        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident']) continue;

                $model = RestaurantPlan::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new RestaurantPlan();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'name' => $item['Name'],
                    'hidden' => ($item['Status'] == 'rsDraft' ? 1 : 0),
                    'deleted' => ($item['Status'] == 'rsDeleted' ? 1 : 0),
                    'restaurant_id' => Restaurant::getIdByOutId($item['MainParentIdent'], true),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить план с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseMenuitemsRK7(){
        $response = $this->responsePrepare();
        if ($response->RK7Reference->Items->count()){
            $sort = Position::getMaxSort();
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident'] || !$item['MainParentIdent']) continue;

                $model = Position::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new Position();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'is_group' => 0, // TODO проверка на групповое
                    'name' => $item['Name'],
                    'description' => $item['Comment'],
                    'menu_id' => Menu::getIdByOutId($item['MainParentIdent'], true),
                    'sort' => $sort++,
                    //'depressed' => '',
                    //'label_id' => '',
                    //'image' => '',
                    'hidden' => ($item['Status'] == 'rsDraft' ? 1 : 0),
                    //'calories' => '',
                    'deleted' => ($item['Status'] == 'rsDeleted' ? 1 : 0),
                    //'in_stock' => '', // TODO доработать определение наличия
                    //'food_type' => '',
                    //'workplace' => '',
                    //'version' => '',
                    //'imageVersion' => '',
                    'price' => 0,
                    'price_mode' => Position::getPriceModeByKeeper($item['PriceMode']),
                    'measure' => $item['PortionName'],
                    'portion_weight' => $item['PortionWeight'],
                    'count_accuracy' => $item['QntDecDigits'],
                    'one_change_weight' => ($item['ChangeQntOnce'] == 'True' ? 1 : 0),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить блюдо с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseRestaurantsRK7(){
        $response = $this->responsePrepare();
        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AppHelper::simpleXmlAttrToArray($row);
                if (!$item['Ident']) continue;

                $model = Restaurant::model()->findByAttributes(array('out_id' => $item['Ident']));
                if ($model === null){
                    $model = new Restaurant();
                }

                $model->setAttributes(array(
                    'out_id' => $item['Ident'],
                    'name' => $item['Name'],
                    'company_id' => $this->queue->proxy->company_id,
                    'location' => $item['Address'],
                    'hidden' => ($item['Status'] == 'rsDraft' ? 1 : 0),
                    'deleted' => ($item['Status'] == 'rsDeleted' ? 1 : 0),
                ));

                if (!$model->save(false)){
                    throw new Exception('Не удалось создать/обновить ресторан с out_id = ' . $model->out_id);
                }
            }
        }
    }

    public function parseRolesRK7(){}

    public function parseTablesRK7(){}

    public function parseModifiersRK7(){}

    public function parseModigroupsRK7(){}

    public function parseModischemedetailsRK7(){}

    public function parseModischemesRK7(){}

}