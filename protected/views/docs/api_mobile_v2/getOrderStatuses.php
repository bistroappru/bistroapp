<h3>Получение статусов заказа и позиций заказа</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getOrderStatuses<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    -
</p>


<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
        {
        "status": "ok",
        "data": [
        {
        "id": "1",
        "name": "Создан",
        "type": "order"
        },
        {
        "id": "2",
        "name": "Передан",
        "type": "order"
        },
        {
        "id": "4",
        "name": "Оплачен",
        "type": "order"
        },
        {
        "id": "5",
        "name": "Отменен",
        "type": "order"
        },
        {
        "id": "6",
        "name": "Закрыт",
        "type": "order"
        },
        {
        "id": "8",
        "name": "Создан",
        "type": "item"
        },
        {
        "id": "9",
        "name": "Передан",
        "type": "item"
        },
        {
        "id": "10",
        "name": "Готовится",
        "type": "item"
        },
        {
        "id": "11",
        "name": "Готово",
        "type": "item"
        },
        {
        "id": "12",
        "name": "Доставлено",
        "type": "item"
        }
        ]
    }
    </code></pre>
<hr/>