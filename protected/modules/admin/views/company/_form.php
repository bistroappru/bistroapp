<?php
/* @var $this CompanyController */
/* @var $model Company */
/* @var $form CActiveForm */
?>

    <div class="product_section" id="company_form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'company-form',
            //'enableAjaxValidation' => true,
            'htmlOptions' => array(
                'data-id' => $model->id,
                'data-type' => $model->isNewRecord ? 'create' : 'update',
                'data-dirty' => $model->hasErrors() ? 'true' : 'false'
            )
        )); ?>

        <?php echo $form->hiddenField($model, 'hidden'); ?>
        <?php echo $form->hiddenField($model, 'deleted'); ?>

        <div class="company_section">
            <h1><?php echo $model->name; ?></h1>

            <div class="product_row">
                <label class="product_label">Наименование</label>

                <div class="product_title">
                    <div class="inp_wrap">
                        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>
                    <a href="#" class="edit-link"></a>
                </div>
            </div>
            <div class="product_row">
                <div class="product_desc">
                    <label class="product_label">Описание компании</label>

                    <div class="inp_wrap">
                        <?php echo $form->textArea($model, 'description'); ?>
                    </div>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
                <?php echo $form->hiddenField($model, 'logo', array('size' => 60, 'maxlength' => 500)); ?>
                <div class="product_thumb">
                    <label class="product_label">Логотип</label>
                    <img src="<?php echo $model->logo ? $model->logo : Yii::app()->params['emptyImage']; ?>" width="130"
                         height="130">
                    <a href="javascript:void(0);" class="refresh"></a>
                </div>
                <?php echo $form->error($model, 'logo'); ?>
            </div>
            <div class="product_row input_row">
                <label class="product_label">Префикс</label>
                <?
                if($model->isNewRecord) {
                    $pref_readonly = false;
                } else {
                    $pref_readonly = true;
                }
                ?>
                <div class="inp_wrap">
                    <?php echo $form->textField($model, 'prefix', array('readonly' => $pref_readonly, 'size' => 6, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'prefix'); ?>
                </div>
            </div>
            <div class="product_row input_row">
                <label class="product_label">Сайт</label>

                <div class="inp_wrap">
                    <?php echo $form->textField($model, 'website', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'website'); ?>
                </div>
            </div>

            <div class="product_row input_row">
                <label class="product_label">Email</label>

                <div class="inp_wrap">
                    <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
            </div>

            <div class="product_row inp_wrap">
                <label class="product_label_big">Отделы кухни</label>
                <div class="workplaces">
                    <?php
                    foreach(Company::getWorkplace() as $k=>$v){
                        ?>
                        <div class="spec_checkbox">
                            <input <?php if (in_array($k, $model->getActiveWorkplaces())) echo 'checked="checked"';?> type="checkbox" id="workplaces_<?php echo $k;?>" value="<?php echo $k;?>"><label for="workplaces_<?php echo $k;?>"><?php echo $v;?></label>
                            <div class="checkbox_bg"></div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <?php echo $form->hiddenField($model, 'workplaces');?>
                <?php echo $form->error($model,'workplaces'); ?>
            </div>

            <ul class="leftmenu_buttons dark">
                <li><?php echo CHtml::link('<img src="/images/leftmenu-4.png" width="50" height="50" alt=""><span>' . ($model->isNewRecord ? 'Создать' : 'Сохранить') . '</span>', 'javascript:void(0);', array('id' => 'company_save')); ?></li>
                <?php
                if (!$model->isNewRecord){
                ?>
                    <li><?php echo CHtml::link('<img src="/images/leftmenu-2'.($model->deleted ? '_act' : '').'.png" width="50" height="50" alt=""><span>'.($model->deleted ? 'Восстановить' : 'Удалить').'</span>', 'javascript:void(0);', array('id' => 'company_set_deleted', 'class' => $model->deleted ? 'active' : ''));?></li>
                    <li><?php echo CHtml::link('<img src="/images/leftmenu-6'.($model->hidden ? '_act' : '').'.png" width="50" height="50" alt=""><span>'.($model->hidden ? 'Включить' : 'Отключить').'</span>', 'javascript:void(0);', array('id' => 'company_set_hidden', 'class' => $model->hidden ? 'active' : ''));?></li>
                <?php
                }
                ?>
            </ul>
        </div>
        <?php $this->endWidget(); ?>
    </div>

<?php $this->renderPartial('/partials/_load_img', array('folder' => '/data/images/logos', 'selector' => 'Company_logo')); ?>