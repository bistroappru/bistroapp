<?php

/**
 * This is the model class for table "{{rkeeper_proxy}}".
 *
 * The followings are the available columns in table '{{rkeeper_proxy}}':
 * @property integer $id
 * @property integer $company_id
 * @property integer $restaurant_id
 * @property string $proxy_uuid
 * @property string $proxy_type
 * @property string $proxy_name
 * @property integer $create_time
 * @property integer $alive_interval
 * @property integer $last_request_time
 *
 */
class RKeeperProxy extends CActiveRecord
{
    const TYPE_REF = 'REF';
    const TYPE_MID = 'MID';
    const TYPE_RK6 = 'RK6';

    public static function getProxyType($key = NULL){
        $items = array(
            self::TYPE_REF => 'REF-сервер',
            self::TYPE_MID => 'MID-сервер',
            self::TYPE_RK6 => 'RK6-сервер',
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rkeeper_proxy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proxy_uuid, proxy_type, proxy_name, create_time', 'required', 'message' => 11),
			array('restaurant_id, company_id, create_time, alive_interval, last_request_time', 'numerical', 'integerOnly'=>true, 'message' => 12),

            // не было возможности задать свой код ошибки
			//array('proxy_uuid, proxy_type, proxy_name', 'length', 'max'=>255, 'message' => 13),
            array('proxy_uuid, proxy_type, proxy_name', 'ruleMaxLength', 'max'=>255, 'message' => 13),

            array('proxy_uuid', 'unique', 'message' => '{attribute} со значением {value} уже существует'),
            array('proxy_uuid', 'ruleUUID', 'message' => 20),
            array('proxy_type', 'in', 'range'=>array_keys(self::getProxyType()), 'message' => '{attribute} имеет недопустимое значение'),
            array('company_id', 'issetCompany'),
            array('restaurant_id', 'issetRestaurant'),
            array('company_id', 'checkCompany'),
            array('restaurant_id', 'checkRestaurant'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_id, restaurant_id, proxy_uuid, proxy_type, proxy_name, create_time, alive_interval, last_request_time', 'safe', 'on'=>'search'),
		);
	}


    // ---------    VALIDATE SECTION      ----------

    public function issetCompany($attribute, $params){
        if (empty($this->$attribute)) return;
        $model = Company::model()->findByPk($this->$attribute);
        if ($model === null){
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} с идентификатором {$this->$attribute} не существует");
        }
    }

    public function issetRestaurant($attribute, $params){
        if (empty($this->$attribute)) return;
        $model = Restaurant::model()->findByPk($this->$attribute);
        if ($model === null){
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} с идентификатором {$this->$attribute} не существует");
        }elseif(!empty($this->company_id) && $model->company_id != $this->company_id){
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} с идентификатором {$this->$attribute} для компании {$this->company_id} не существует");
        }
    }

    public function checkCompany($attribute, $params){
        if ($this->proxy_type == self::TYPE_REF && empty($this->$attribute) && empty($this->restaurant_id)){
            $this->addError($attribute, "Для прокси-сервера типа {$this->proxy_type} должна быть указана компания либо ресторан");
        }
    }

    public function checkRestaurant($attribute, $params){
        if (in_array($this->proxy_type, array(self::TYPE_MID, self::TYPE_RK6)) && empty($this->$attribute)){
            $this->addError($attribute, "Для прокси-сервера типа {$this->proxy_type} должен быть указан ресторан");
        }

        if ($this->proxy_type == self::TYPE_REF && empty($this->$attribute) && empty($this->restaurant_id)){
            $this->addError($attribute, "Для прокси-сервера типа {$this->proxy_type} должен быть указан ресторан либо компания");
        }
    }

    public function ruleUUID($attribute, $params){
        $pattern = '/^\{?[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}\}?$/';
        if (!preg_match($pattern, $this->$attribute)) {
            $this->addError($attribute, (string) $params['message']);
        }
    }

    public function ruleMaxLength($attribute, $params){
        if (isset($params['max']) && strlen($this->$attribute) > $params['max']){
            $this->addError($attribute, (string) $params['message']);
        }
    }

    // -----------------------

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'company'=>array(self::BELONGS_TO, 'Company', 'company_id'),
            'restaurant'=>array(self::BELONGS_TO, 'Restaurant', 'restaurant_id'),
            'turns'=>array(self::HAS_MANY, 'RKeeperTurn', 'proxy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_id' => 'Компания',
            'restaurant_id' => 'Ресторан',
			'proxy_uuid' => 'UUID прокси-сервера',
			'proxy_type' => 'Тип прокси-сервера',
			'proxy_name' => 'Имя прокси-сервера',
            'create_time' => 'Время создания',
            'alive_interval' => 'Интервал запросов',
            'last_request_time' => 'Время последнего запроса',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('proxy_uuid',$this->proxy,true);
        $criteria->compare('proxy_type',$this->proxy_type,true);
		$criteria->compare('proxy_name',$this->proxy_name,true);
        $criteria->compare('create_time',$this->create_time);
        $criteria->compare('alive_interval',$this->alive_interval);
        $criteria->compare('last_request_time',$this->last_request_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RKeeperProxy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    // ---------    CUSTOM OBJECT METHODS   ----------
    public function updateLastRequestTime(){
        $this->last_request_time = time();
        $this->save();
    }

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function getIdByProxy($proxy_uuid = null){
        $model = self::model()->findByAttributes(array('proxy_uuid' => $proxy_uuid));
        if ($model === null)
            return 0;
        else
            return $model->id;
    }

    public static function loadByProxy($proxy_uuid = null){
        return self::model()->findByAttributes(array('proxy_uuid' => $proxy_uuid));
    }

    public static function loadCompanyProxy($company_id, $type = self::TYPE_REF){
        return self::model()->findByAttributes(array('company_id' => $company_id, 'proxy_type' => $type));
    }

}
