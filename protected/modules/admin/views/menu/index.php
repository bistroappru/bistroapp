<?php
/* @var $this MenuController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle = Yii::app()->name . ' - Управление меню';
?>

<!--
<div id="container" class="admin_menu_container">
    <div class="menu_index">
        <section class="faq_section">
            <div class="center">
                <div class="faq_block">
                    <h2>КАК ПОЛЬЗОВАТЬСЯ МЕНЮ</h2>
                    <br/>
                    <p>&mdash; Выберите компанию меню которой Вы хотите просмотреть</p>
                    <p>&mdash; Слева располагается список разделов меню</p>
                </div>
            </div>
        </section>
    </div>
</div>
-->

<script>
    $(window).load(function(){
        $('#menu_sortable li:first a').click();
    });
</script>