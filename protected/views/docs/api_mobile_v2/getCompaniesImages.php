<h3>Получение изображений компании</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getCompaniesImages<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>


<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
    {
        "status": "ok",
        "data": [
        {
            "id": "1",
            "image": "\/9j\/4AAQSkZJRgABAQAAAQABAAD...",
            "extension": "jpeg",
            "size": {
                "width": 140,
                "height": 140
            }
        }
        ],
        "version": "0"
    }
    </code></pre>
<hr/>