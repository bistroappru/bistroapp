<h3>Получение списка компаний и ресторанов</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getCompanies<br/>
</p>

<hr/>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint">http://bistroapp.ru/api/getCompanies</code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
{
  "code": 0,
  "error": "\u0417\u0430\u043f\u0440\u043e\u0441 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d",
  "data": [
    {
      "id": "1",
      "name": "\u0422\u0430\u043d\u0443\u043a\u0438",
      "image": "\/9j\/4AAQSkZJRgABAQAAAQABAAD\/\/gA8Q1...9f0H+FSV7GPl93\/B8vz7s\/\/Z",
      "hidden": "0",
      "deleted": "0",
      "description": "\u0420\u0435\u0441\u0442\u043e\u0440\u0430\u043d\u044b \u0422\u0430\u043d\u0443\u043a\u0438 \u0437\u0430\u043c\u0435\u0442\u043d\u043e \u043e\u0442\u043b\u0438\u0447\u0430\u044e\u0442\u0441\u044f \u0441\u0432\u043e\u0438\u043c \u0430\u0443\u0442\u0435\u043d\u0442\u0438\u0447\u043d\u044b\u043c \u0438\u043d\u0442\u0435\u0440\u044c\u0435\u0440\u043e\u043c. \u0427\u0430\u0441\u0442\u043e\u043a\u043e\u043b \u0438\u0437 \u0431\u0430\u043c\u0431\u0443\u043a\u0430, \u0441\u0442\u0435\u043d\u044b \u0441 \u0438\u0435\u0440\u043e\u0433\u043b\u0438\u0444\u0430\u043c\u0438 \u0438 \u0441\u0442\u0430\u0442\u0443\u044d\u0442\u043a\u0430\u043c\u0438, \u0441\u0432\u0435\u0442\u0438\u043b\u044c\u043d\u0438\u043a\u0438 \u0438\u0437 \u0440\u0438\u0441\u043e\u0432\u043e\u0439 \u0431\u0443\u043c\u0430\u0433\u0438, \u0431\u043e\u043b\u044c\u0448\u0438\u0435 \u0441\u0442\u043e\u043b\u044b, \u0441\u0434\u0435\u043b\u0430\u043d\u043d\u044b\u0435 \u043d\u0435 \u0434\u043b\u044f \u044d\u043a\u043e\u043d\u043e\u043c\u0438\u0438 \u043c\u0435\u0441\u0442\u0430, \u0430 \u0434\u043b\u044f \u0443\u0434\u043e\u0431\u0441\u0442\u0432\u0430 \u0413\u043e\u0441\u0442\u0435\u0439, \u0432\u043f\u0440\u043e\u0447\u0435\u043c, \u043a\u0430\u043a \u0438 \u0432\u0441\u0451 \u0432 \u0440\u0435\u0441\u0442\u043e\u0440\u0430\u043d\u0430\u0445 \u0422\u0430\u043d\u0443\u043a\u0438",
      "restaurants": [
        {
          "id": "3",
          "deleted": "0",
          "location": "\u0433. \u041c\u043e\u0441\u043a\u0432\u0430, \u0443\u043b \u0411\u043e\u0440\u0438\u0441\u043e\u0432\u0441\u043a\u0438\u0435 \u041f\u0440\u0443\u0434\u044b, \u0434 10",
          "hidden": "0",
          "description": "\u041b\u0443\u0447\u0448\u0438\u0439 \u0440\u0435\u0441\u0442\u043e\u0440\u0430\u043d",
          "phone": "+7 (555) 100-02-30",
          "working_hours": "\u041f\u043d-\u041f\u0442 10:00-23:00",
          "company_id": "1",
          "coords": {
            "x": 55.635869,
            "y": 37.740974
          },
          "tables": [
            {
              "id": "11",
              "out_id": "",
              "name": "1",
              "restaurant_id": "3"
            },
            {
              "id": "12",
              "out_id": "",
              "name": "2",
              "restaurant_id": "3"
            }
          ]
        },
        {
          "id": "4",
          "deleted": "0",
          "location": "\u0433. \u041c\u043e\u0441\u043a\u0432\u0430, \u0448. \u0411\u043e\u0440\u043e\u0432\u0441\u043a\u043e\u0435, \u0434. 31",
          "hidden": "0",
          "description": "",
          "phone": "+7 (111) 222-33-44",
          "working_hours": "",
          "company_id": "1",
          "coords": {
            "x": 55.642453,
            "y": 37.361517
          },
          "tables": [
            {
              "id": "26",
              "out_id": "",
              "name": "1",
              "restaurant_id": "4"
            },
            {
              "id": "27",
              "out_id": "",
              "name": "2",
              "restaurant_id": "4"
            }
          ]
        },
        {
          "id": "6",
          "deleted": "0",
          "location": "\u0433. \u041c\u043e\u0441\u043a\u0432\u0430, \u0443\u043b. \u0410\u043a\u0430\u0434\u0435\u043c\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u0411., \u0434. 65",
          "hidden": "0",
          "description": "\u041f\u043e\u0441\u0430\u0434\u043e\u0447\u043d\u044b\u0445 \u043c\u0435\u0441\u0442: 198, \u041f\u0430\u0440\u043a\u043e\u0432\u043a\u0430: \u0415\u0441\u0442\u044c, Wi-fi: Free, \u041a\u0430\u043b\u044c\u044f\u043d\u043d\u0430\u044f \u043a\u0430\u0440\u0442\u0430: \u0415\u0441\u0442\u044c",
          "phone": "+7 (499) 153-81-44",
          "working_hours": "\u041f\u043d-\u0427\u0442 (11:30 \u2013 00:00), \u041f\u0442-\u0421\u0431 (11:30 \u2013 06:00), \u0412\u0441.11:30 \u2013 00:00",
          "company_id": "1",
          "coords": {
            "x": 55.840371,
            "y": 37.547477
          },
          "tables": []
        }
      ]
    },{
      "id": "8",
      "name": "\u0442\u0435\u0441\u0442\u043e\u0432\u0430\u044f",
      "image": "iVBORw0KGgoAAAANSUhEUgA....ABJRU5ErkJggg==",
      "hidden": "0",
      "deleted": "1",
      "description": "",
      "restaurants": []
    }
  ]
}
</code></pre>
<hr/>