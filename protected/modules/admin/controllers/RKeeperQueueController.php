<?php

class RKeeperQueueController extends Controller
{
    public $layout = 'admin_ref';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('inAdmin'),
            ),
            array(
                'deny',
                'roles'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }

}