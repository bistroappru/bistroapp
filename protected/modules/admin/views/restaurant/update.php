<?php
/* @var $this RestaurantController */
/* @var $model Restaurant */
$this->pageTitle = Yii::app()->name . ' - Редактирование ресторана';
?>

<div class="column_center2">
    <?php $this->widget('application.components.RestaurantList', array('company_id' => $model->company_id, 'restaurant_id' => $model->id));?>
</div>

<div class="column_right">
    <?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>