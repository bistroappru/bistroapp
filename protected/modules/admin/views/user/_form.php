<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'htmlOptions' => array(
        'data-id' => $model->id,
        'data-type' => $model->isNewRecord ? 'create' : 'update'
    )
)); ?>

<div class="company_section" id="user_form">
    <?php echo $form->hiddenField($model, 'deleted'); ?>

    <div class="product_thumb">
        <img src="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>" width="130" height="130">
        <a href="javascript:void(0);" class="refresh"></a>
    </div>
    <?php echo $form->hiddenField($model, 'image'); ?>


    <h1><?=$model->fullname?></h1>
    <div class="company_subtitle"><?=$model->role_name;?></div>

    <div class="product_row input_row">
        <label class="product_label">Компания: <b><?php echo $model->company->name; ?></b></label>
        <div class="inp_wrap">
            <?php echo $form->hiddenField($model, 'company_id'); ?>
            <?php echo $form->error($model, 'company_id'); ?>
        </div>
    </div>

    <div class="product_row input_row">
        <label class="product_label">Ресторан</label>
        <div class="inp_wrap">
            <?php
            if (Yii::app()->user->restaurant_id) {
                echo $form->hiddenField($model, 'restaurant_id');
                echo '<b>'.$model->restaurant->location.'</b>';
            } else {
                echo $form->dropDownList($model, 'restaurant_id', array('' => 'Не выбрано') + AbcHelper::allCompanyRestaurants($model->company_id));
            }
            ?>
            <?php echo $form->error($model, 'restaurant_id'); ?>
        </div>
    </div>

    <div class="product_row input_row">
        <label class="product_label">Логин</label>
        <div class="inp_wrap">
            <?php echo $form->hiddenField($model, 'username', array('size' => 60, 'maxlength' => 255)); ?>
            <span class="form_user_pref"><?=$model->company->prefix?>_</span>
            <div class="small_inp">
                <?php echo CHtml::textField('username', AbcHelper::clearUserPrefix($model->username), array('size' => 60, 'maxlength' => 255, 'data-prefix' => $model->company->prefix)); ?>
            </div>
            <div class="clear"></div>
            <?php echo $form->error($model, 'username'); ?>
        </div>
    </div>

    <div class="product_row input_row">
        <label class="product_label">ФИО</label>
        <div class="inp_wrap">
            <?php echo $form->textField($model, 'fullname', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'fullname'); ?>
        </div>
    </div>


    <div class="product_row input_row">
        <label class="product_label">Email</label>
        <div class="inp_wrap">
            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'autocomplete'=>'off')); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>
    </div>

    <div class="product_row input_row">
        <label class="product_label">Телефон</label>
        <div class="inp_wrap">
            <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 255, 'autocomplete'=>'off', 'class'=>'phone_mask')); ?>
            <?php echo $form->error($model, 'phone'); ?>
        </div>
    </div>

    <?php
    if ($model->isNewRecord) {
        ?>
        <div class="product_row input_row">
            <label class="product_label">Пароль</label>
            <div class="inp_wrap">
                <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 255, 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
        </div>
    <?php
    }
    ?>

    <div class="product_row input_row">
        <label class="product_label">Роль</label>
        <?php echo $form->dropDownList($model, 'role', AbcHelper::getAccessCreateRoles(Yii::app()->user->role)); ?>
    </div>

    <div class="product_row input_row">
        <label class="product_label">Заблокирован</label>
        <?php echo $form->dropDownList($model, 'blocked', array(1 => 'Да', 0 => 'Нет')); ?>
    </div>


    <?= $this->renderPartial('_user_tables', array('model'=>$model));?>

    <ul class="leftmenu_buttons">
        <li>
            <?php echo CHtml::submitButton('', array('class'=>'product_submit2', 'id' => 'user_save')); ?>
        </li>

        <?php
        if (!$model->isNewRecord){
            ?>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-2'.($model->deleted ? '_act' : '').'.png" width="50" height="50" alt=""><span>'.($model->deleted ? 'Восстановить' : 'Удалить').'</span>', 'javascript:void(0);', array('id' => 'user_set_deleted', 'class' => $model->deleted ? 'active' : ''));?></li>
            <?php
        }
        ?>
    </ul>
</div>
<?php $this->endWidget();?>
<?php $this->renderPartial('/partials/_load_img', array('folder' => '/data/images/users', 'selector' => 'User_image')); ?>


<?php
if ($model->id){
?>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'changepassword-form',
        'action' => '/admin/user/changepassword/id/'.$model->id
    ));?>
    <div class="password_box right_box">
        <div class="rightbox_title">
            <div class="rightbox_text">Пароль</div>
            <span class="pass_icon box_link"></span>
        </div>
        <div class="box_addit input_row" style="display:block;">
            <div class="inp_wrap"><input type="password" name="Password[new]" id="password_new" placeholder="Новый пароль" autocomplete="off"></div>
            <div class="inp_wrap"><input type="password" name="Password[confirm]" id="password_confirm" placeholder="Подтверждение пароля"></div>
            <div><input type="submit" value="" class="product_submit2"></div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
<?php
}
?>