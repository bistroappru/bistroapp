<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 24.04.14
 * Time: 16:05
 */

class WebUser extends CWebUser {
    private $_model = null;

    function getRole() {
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->role;
        }
    }

    public function getCompany_id(){
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->company_id;
        }
    }

    public function getCompany_id_full(){
        if($user = $this->getModel()){
            // в таблице User есть поле role
            if ($user->company_id) return $user->company_id;
            return (string)Yii::app()->request->cookies['company_id'];
        }
    }

    public function getRestaurant_id(){
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->restaurant_id;
        }
    }

    public function getRestaurant_id_full(){
        if($user = $this->getModel()){
            // в таблице User есть поле role
            if ($user->restaurant_id) return $user->restaurant_id;
            return (string)Yii::app()->request->cookies['restaurant_id'];
        }
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role, company_id, restaurant_id'));
        }
        return $this->_model;
    }
}