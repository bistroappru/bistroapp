<?php

/**
 * This is the model class for table "{{position}}".
 *
 * The followings are the available columns in table '{{position}}':
 * @property integer $id
 * @property string $out_id
 * @property integer $is_group
 * @property string $name
 * @property string $name_orig
 * @property string $description
 * @property string $description_orig
 * @property integer $menu_id
 * @property integer $sort
 * @property integer $depressed
 * @property integer $label_id
 * @property string $image
 * @property integer $hidden
 * @property string $calories
 * @property integer $deleted
 * @property string $in_stock
 * @property integer $food_type
 * @property integer $workplace
 * @property integer $version
 * @property integer $imageVersion
 * @property double $price
 * @property integer $price_mode
 * @property string $measure
 * @property double $portion_weight
 * @property integer $count_accuracy
 * @property integer $one_change_weight
 */
class Position extends CActiveRecord
{
    const PRICE_MODE_ONE = 1;
    const PRICE_MODE_PW = 2;
    const PRICE_MODE_PP = 3;
    const PRICE_MODE_DOSE = 4;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{position}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, menu_id, price, price_mode', 'required'),
			array('is_group, menu_id, sort, depressed, label_id, hidden, deleted, food_type, workplace, version, imageVersion, price_mode, count_accuracy, one_change_weight', 'numerical', 'integerOnly'=>true),
			array('price, portion_weight', 'numerical'),
			array('out_id, name, name_orig, image, measure', 'length', 'max'=>255),
			array('calories', 'length', 'max'=>50),
			array('in_stock', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, is_group, name, name_orig, description, description_orig, menu_id, sort, depressed, label_id, image, hidden, calories, deleted, in_stock, food_type, workplace, version, imageVersion, price, price_mode, measure, portion_weight, count_accuracy, one_change_weight', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
            'label' => array(self::BELONGS_TO, 'Label', 'label_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'out_id' => 'Out',
			'is_group' => 'Is Group',
			'name' => 'Название',
			'name_orig' => 'Название (исх)',
			'description' => 'Описание',
			'description_orig' => 'Описание (исх)',
			'menu_id' => 'Категория',
			'sort' => 'Порядковый номер',
			'depressed' => 'Подавление блюда',
			'label_id' => 'Спецвывод',
			'image' => 'Изображение',
			'hidden' => 'Скрыт',
			'calories' => 'Калории',
			'deleted' => 'Удален',
			'in_stock' => 'Наличие в ресторанах',
			'food_type' => 'Food Type',
			'workplace' => 'Workplace',
			'version' => 'Версия',
			'imageVersion' => 'Версия изображения',
			'price' => 'Цена',
			'price_mode' => 'Режим цены',
			'measure' => 'Ед.измерения',
			'portion_weight' => 'Вес порции',
			'count_accuracy' => 'Точность для количества',
			'one_change_weight' => '1 изменение веса',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Position the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

// ============================================================================================
// ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================

	protected function beforeSave() {
		if(parent::beforeSave()===false){
			return false;
		}

//		$version_image = Position::getLastVersion($this->menu->company_id, 'imageVersion');
//		$this->imageVersion = ++$version_image;
//
//		$version_data = Position::getLastVersion($this->menu->company_id);
//		$this->version = ++$version_data;

		return true;
	}

    public function isAvailability($restaurant_id){
        $restaurant_list = explode(',', $this->in_stock);
        if (in_array($restaurant_id, $restaurant_list))
            return true;
        else
            return false;
    }

// ============================================================================================
// СТАТИЧЕСКИЕ МЕТОДЫ МОДЕЛИ
// ============================================================================================
	/**
	 * получение индекса последней версии данных
	 */
	public static function getLastVersion($company_id, $field = 'version'){
		$menu_ids = Menu::getCategoriesIds($company_id);
		if (count($menu_ids)){
			$row = self::model()->find(array(
				'select' => $field,
				'condition' => 'menu_id IN ('.implode(',',$menu_ids).')',
				'order' => "{$field} DESC",
				'limit' => 1
			));
		}
		return is_object($row) ? $row->$field : 0;
	}


	public static function getPriceModeList($key = NULL){
        $items = array(
            self::PRICE_MODE_ONE => 'Штучное',
            self::PRICE_MODE_PW => 'Порционное за вес',
            self::PRICE_MODE_PP => 'Порционное за стд.Порцию',
            self::PRICE_MODE_DOSE => 'Дозируемое за стд.Дозу'
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }

    public static function getPriceModeByKeeper($key = ''){
        $items = array(
            'pmPerPiece' => self::PRICE_MODE_ONE,
            'pmPerUnitOfWeight' => self::PRICE_MODE_PW,
            'pmPerPortion' => self::PRICE_MODE_PP,
            'pmPerDose' => self::PRICE_MODE_DOSE
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return null;
    }

    public static function getMaxSort(){
        return 0;
    }

}


