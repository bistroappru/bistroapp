<?php

class AjaxController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'ajaxOnly + getRestJSON'
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('getRestJSON','getCompJSON','updateMenuList','updateCompanyList','updateDishList','updateCompanyUser','updateCompanyLogo'),
                'users'=>array('*'),
            ),
            array('deny', //deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionUpdateCompanyUser(){
        $company_id = Yii::app()->request->getParam('company_id', 0);
        $restaurant_id = Yii::app()->request->getParam('restaurant_id', 0);
        return $this->widget('CompanyUser', array('company_id'=>$company_id, 'restaurant_id'=>$restaurant_id));
    }


    public function actionUpdateMenuList(){
        $company_id = Yii::app()->request->getParam('company_id', 0);
        $menu_id = Yii::app()->request->getParam('menu_id', 0);
        return $this->widget('CompanyMenu', array('company_id'=>$company_id, 'menu_id'=>$menu_id, 'filter_key' => 'lc_menu'));
    }

    public function actionUpdateDishList(){
        $dish_id = Yii::app()->request->getParam('dish_id', 0);
        $menu_id = Yii::app()->request->getParam('menu_id', 0);
        $group = Yii::app()->request->getParam('group', 0);
        return $this->widget('DishList', array('dish_id'=>$dish_id, 'menu_id'=>$menu_id, 'group'=>$group));
    }

    public function actionUpdateCompanyList(){
        $company_id = Yii::app()->request->getParam('company_id', 0);
        return $this->widget('CompanyList', array('company_id'=>$company_id));
    }

    public function actionUpdateCompanyLogo(){
        return $this->widget('CompanyLogo');
    }


    public function actionGetCompJSON(){
        $fields = Yii::app()->request->getParam('fields', 'id,name');
        $restaurants = Yii::app()->request->getParam('restaurants', false);

        $list = array();

        $criteria = new CDbCriteria();
        $criteria->select = $fields;
        $criteria->order = 'name ASC';
        if ($cid = Yii::app()->user->company_id) $criteria->compare('id', $cid);

        $tmp = Company::model()->findAll($criteria);
        foreach($tmp as $k=>$v){
            $list['ids'][] = $v->id;
            $list['list'][$v->id] = $v->getAttributes($fields);
            if ($restaurants) $list['list'][$v->id]['restaurants'] = count($v->restaurants);
        }
        echo CJSON::encode($list);
    }

    public function actionGetRestJSON(){
        $id = Yii::app()->request->getParam('company_id', 0);
        $fields = Yii::app()->request->getParam('fields', 'id,name');
        $users = Yii::app()->request->getParam('users', false);

        $list = array();
        if ($id){
            $criteria = new CDbCriteria();
            $criteria->select = $fields;
            $criteria->order = 'name ASC';
            $criteria->addCondition('company_id = '.$id);

            $tmp = Restaurant::model()->findAll($criteria);
            foreach($tmp as $k=>$v){
                $list['ids'][] = $v->id;
                $list['list'][$v->id] = $v->getAttributes($fields);
                if ($users) $list['list'][$v->id]['users'] = count($v->users);
            }
        }
        echo CJSON::encode($list);
    }
}
