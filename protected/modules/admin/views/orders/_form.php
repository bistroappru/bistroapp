<?php
/* @var $this OrdersController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <br/>
	<p class="note">Обязательные <span class="required">*</span> поля.</p>
    <br/>
	<?php echo $form->errorSummary($model); ?>

    <?php //echo $form->textField($model,'date_create'); ?>
    <?php //echo $form->textField($model,'date_execute'); ?>
    <?php //echo $form->textField($model,'date_close'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'table'); ?>
		<?php echo $form->textField($model,'table',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'table'); ?>
	</div>
    <br/>
    <div class="row">
        <?php echo $form->labelEx($model,'garcon_id'); ?>
        <?php echo $form->dropDownList($model,'garcon_id', User::getGarconByRestaurant(Yii::app()->user->restaurant_id_full)); ?>
        <?php echo $form->error($model,'garcon_id'); ?>
    </div>
    <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'status_id'); ?>
		<?php echo $form->dropDownList($model,'status_id', OrderStatus::getAll()); ?>
		<?php echo $form->error($model,'status_id'); ?>
	</div>
    <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'payment_id'); ?>
        <?php echo $form->dropDownList($model,'payment_id', OrderPayment::getAll()); ?>
		<?php echo $form->error($model,'payment_id'); ?>
	</div>
    <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'attention'); ?>
        <?php echo $form->dropDownList($model,'attention',array(1=>'Да', 0=>'Нет')); ?>
		<?php echo $form->error($model,'attention'); ?>
	</div>
    <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'separated'); ?>
        <?php echo $form->dropDownList($model,'separated',array(1=>'Да', 0=>'Нет')); ?>
		<?php echo $form->error($model,'separated'); ?>
	</div>
    <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'separated_info'); ?>
		<?php echo $form->textArea($model,'separated_info',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'separated_info'); ?>
	</div>
    <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>
    <br/>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->