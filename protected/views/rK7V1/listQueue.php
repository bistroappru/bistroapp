<script src="/js/jquery-1.11.0.min.js"></script>
<script src="/js/google-code-prettify/run_prettify.js"></script>
<link rel="stylesheet" type="text/css" href="/js/google-code-prettify/prettify.css"/>
<link rel="stylesheet" type="text/css" href="/css/docs.css"/>
<link rel="stylesheet" type="text/css" href="/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script src="/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>

<script>
    $(function(){
        $('.fancybox').fancybox();
    });
</script>
<style>
    *{
        font-size: 12px!important;
    }

    table{
        border-top: 1px solid #808080;
        border-left: 1px solid #808080;
        border-collapse: collapse;
    }
    th, td{
        border-bottom: 1px solid #808080;
        border-right: 1px solid #808080;
        padding: 5px;
    }
    pre{
        padding: 0px;
        margin: 0px;
    }
    pre code{
        padding: 0px 5px;
        margin: 0px;
    }
</style>
<table>
    <tr>
        <th>id</th>
        <th>request_uuid</th>
        <th>proxy_id</th>
        <th>status</th>
        <th>type</th>
        <th>body</th>
        <th>response</th>
        <th>create_time</th>
        <th>request_time</th>
        <th>response_time</th>
        <th>error_description</th>
        <th>data</th>

    </tr>
    <?php
    if (count($list)){
        foreach($list as $proxy){
            $tmp = unserialize($proxy['data']);
        ?>
            <tr>
                <td><?=$proxy['id']?></td>
                <td><nobr><?=$proxy['uuid']?></nobr></td>
                <td><?=$proxy['proxy_id']?></td>
                <td><nobr><?=$proxy['status']?> / <?=RKeeperQueue::getStatus($proxy['status'])?></nobr></td>
                <td><?=$proxy['type']?></td>
                <td>
                    <div style="overflow: hidden; max-height: 100px; max-width: 200px;">
                        <div id="request_<?=$proxy['id']?>" style="display: none;">
                            <pre>
                            <code class="prettyprint1">
<?=CHtml::encode($proxy['body'])?>
                            </code>
                            </pre>
                        </div>
                    </div>
                    <a href="#request_<?=$proxy['id']?>" class="fancybox">подробнее</a>
                </td>
                <td>
                    <div style="overflow: hidden; max-height: 100px; max-width: 200px;">
                        <div id="body_<?=$proxy['id']?>" style="display: none;">
                            <pre>
                            <code class="prettyprint1">
<?=CHtml::encode($proxy['response'])?>
                            </code>
                            </pre>
                        </div>
                    </div>
                    <a href="#body_<?=$proxy['id']?>" class="fancybox">подробнее</a>
                </td>
                <td><nobr><?=date('d.m.Y H:i:s', $proxy['create_time'])?></nobr> <br/> <?=$proxy['create_time']?></td>
                <td><nobr><?=date('d.m.Y H:i:s', $proxy['request_time'])?></nobr> <br/> <?=$proxy['request_time']?></td>
                <td><nobr><?=date('d.m.Y H:i:s', $proxy['response_time'])?></nobr> <br/> <?=$proxy['response_time']?></td>
                <td><?=$proxy['error_description']?></td>
                <td><?= $tmp['order_id'] ? '<b>ID заказа: '.$tmp['order_id'].'</b><br>' : '';?><?=$proxy['data']?></td>
            </tr>
        <?php
        }
    }
    ?>
</table>