<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 18.08.14
 * Time: 16:05
 */
?>


<div class="left_search" id="user_company_search">
    <form>
        <input name="user_company_search" class="inp_left_srh" type="text" value="Поиск" onfocus="this.value=(this.value=='Поиск')? '' : this.value ;" onblur="this.value=(this.value=='')? 'Поиск' : this.value ;">
        <input class="submit_left_srh" type="submit" value=" ">
    </form>
</div>
<ul id="user_company_list" class="company_menu"></ul>

<script>
    var cu_json = <?=CJSON::encode($company_list)?>;
    var cu_id = <?=intval($company_id)?>;
    var cu_act = [];
    var ru_json = <?=CJSON::encode($restaurant_list)?>;
    var ru_id = <?=intval($restaurant_id)?>;
    var ru_act = 0;

    function searchUserCompanies(){
        var search = $('#user_company_search input[name=user_company_search]').val();
        if (!search || search == 'Поиск') search = '';
        var ul_html = '';
        if (cu_json['ids']){
            $.each(cu_json['ids'], function(k, v){
                pos = cu_json['list'][v].name.toLowerCase().indexOf(search.toLowerCase());
                if (pos != -1){
                    ul_html += '<li class="'
                        +(cu_json['list'][v].deleted == 1 ? 'style_del' : '')+' '
                        +(cu_json['list'][v].hidden == 1 ? 'style_hid' : '')+'" data-id="'+ cu_json['list'][v].id+'" id="ucl_'+ cu_json['list'][v].id+'"> \
                    <a href="#" data-id="'+ cu_json['list'][v].id+'" class="ex_link_comp"> \
                        <span class="company_ico"><img src="'+(cu_json['list'][v].logo ? cu_json['list'][v].logo : '<?=Yii::app()->params['emptyImage']?>')+'" width="40" height="41"></span> \
                        <span class="company_name">'+cu_json['list'][v].name+'</span> \
                        <span class="company_rest">ресторанов ('+ cu_json['list'][v].restaurants+')</span> \
                        <span class="company_rest">пользователей ('+cu_json['list'][v].users+')</span> \
                    </a> \
                </li>';
                }
            });
        }
        $('#user_company_list').html(ul_html);

        $.each(cu_act, function(i,v){
            expandUserCompany(v);
        });
    }

    function searchUserRestaurants(id){
        var search = $('#ucl_'+id+' input[type=text]').val();
        if (!search || search == 'Поиск') search = '';
        var ul_html = '';

        if (ru_json[id]['ids']){
            $.each(ru_json[id]['ids'], function(k, v){
                pos = ru_json[id]['list'][v].name.toLowerCase().indexOf(search.toLowerCase());
                if (pos != -1){
                    ul_html += '<li class="'
                        +(ru_json[id]['list'][v].deleted == 1 ? 'style_del' : '')+' '
                        +(ru_json[id]['list'][v].hidden == 1 ? 'style_hid' : '')+'">\
                    <a href="#" data-id="'+ ru_json[id]['list'][v].id+'" class="ex_link_rest ' + (ru_json[id]['list'][v].id == ru_act ? 'active' : '') + '"> \
                        <span class="company_name">'+ru_json[id]['list'][v].name+'</span> \
                        <span class="company_rest">'+ru_json[id]['list'][v].location+'</span>\
                        <span class="company_rest">пользователей ('+ru_json[id]['list'][v].users+')</span> \
                    </a>\
                </li>';
                }
            });
        }
        $('#ucl_'+id+' .company_submenu').html(ul_html);
    }

    function expandUserCompany(id){
        var li = $('#ucl_'+id);
        if (li.hasClass('opened')){
            li.removeClass('opened').find('.company_subbox').remove();
        }else{
            li.addClass('opened').append('<div class="company_subbox">\
                <div class="left_search">\
                    <form>\
                        <input class="inp_left_srh" type="text" value="Поиск" onfocus="this.value=(this.value==\'Поиск\')? \'\' : this.value ;" onblur="this.value=(this.value==\'\')? \'Поиск\' : this.value ;">\
                        <input name="ucl_'+id+'_search" class="submit_left_srh" type="submit" value="">\
                    </form>\
                </div>\
                <ul class="company_submenu"></ul>\
            </div>');
            searchUserRestaurants(id);
        }
    }

    function updateActiveCompanies(){
        cu_act = [];
        $('#user_company_list li.opened').each(function(){
            cu_act.push(parseInt($(this).attr('data-id')));
        });
    }

    searchUserCompanies();
    $('#user_company_search input[name=user_company_search]').on('keyup', function(){
        searchUserCompanies();
    });
    $('#user_company_search form').on('submit', function(e){
        e.preventDefault();
        searchUserCompanies();
    });

    $('#user_company_list').on('click', 'a.ex_link_comp', function(e){
        e.preventDefault();
        var link = $(this);
        var id = parseInt(link.attr('data-id'));
        if (ru_json[id]){
            expandUserCompany(id);
            updateActiveCompanies();
        }
    });

    $('#user_company_list').on('keyup', 'div.company_subbox form input[type=text]', function(){
        var id = parseInt($(this).closest('li.opened').attr('data-id'));
        if (id) searchUserRestaurants(id);
    });

    $('#user_company_list').on('submit', 'div.company_subbox form', function(e){
        e.preventDefault();
        var id = parseInt($(this).closest('li.opened').attr('data-id'));
        if (id) searchUserRestaurants(id);
    });

    if (cu_id){
        expandUserCompany(cu_id);
        if (ru_id) $('.ex_link_rest[data-id='+ru_id+']').addClass('active');
    }
</script>
