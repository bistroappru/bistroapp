<?php

/**
 * This is the model class for table "{{restaurant}}".
 *
 * The followings are the available columns in table '{{restaurant}}':
 * @property integer $id
 * @property string $out_id
 * @property integer $company_id
 * @property integer $deleted
 * @property string $name
 * @property string $location
 * @property string $description
 * @property string $phone
 * @property string $working_hours
 * @property integer $hidden
 * @property integer $map
 * @property integer $photos
 * @property integer $lock_order_transfer
 */
class Restaurant extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{restaurant}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location, name', 'required'),
			array('hidden, deleted, company_id, lock_order_transfer', 'numerical', 'integerOnly'=>true),
			array('out_id, deleted, company_id, location, name, phone, working_hours', 'length', 'max'=>255),
            array('description, map', 'length', 'max'=>500),
            array('map', 'ruleMap'),
            array('phone', 'rulePhone'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, deleted, company_id, location, name, hidden, phone, working_hours, description, photos, lock_order_transfer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'company'=>array(self::BELONGS_TO, 'Company', 'company_id'),
            'users' => array(self::HAS_MANY, 'User', 'restaurant_id'),
            'tables' => array(self::HAS_MANY, 'RestaurantTable', 'restaurant_id'),
        );
	}

    public function ruleMap(){
        $tmp = CJSON::decode($this->map);
        if (!isset($tmp['place']['street']['id'])) $this->addError('kladr_street', 'Поле содержит некорректные данные');
        if (!isset($tmp['place']['building']['id'])) $this->addError('kladr_building', 'Поле содержит некорректные данные');
    }

    public function rulePhone(){
        $tmp = explode(',', $this->phone);
        if (is_array($tmp)){
            foreach($tmp as $k=>$v){
                if (!preg_match("/^\+7 \([0-9]{3}\) [0-9]{3}-[0-9]{2}\-[0-9]{2}/", $v)) {
                    $this->addError('phone_'.$k, 'Необходимо указать телефон в формате +7 (ххх) ххх-хх-хх');
                }
            }
        }
    }

    /**
     * подготовка модели по данным POST
     */
    public function prepareWithPostData(){
        $_POST['Restaurant']['phone'] = implode(',',$_POST['Restaurant']['phone']);
        $_POST['Restaurant']['photos'] = CJSON::encode($_POST['Restaurant']['photos']);

        $this->attributes = $_POST['Restaurant'];

        $tables_list = array();
        $error_list = array();
        $tmp = CJSON::decode($_POST['Restaurant']['tables']);
        if (is_array($tmp)){
            foreach($tmp as $v){
                if($v['id']){
                    $table = RestaurantTable::model()->findByPk($v['id']);
                }else{
                    $table = new RestaurantTable();
                }
                $table->action = $v['action'];
                $table->attributes = $v;
                $tables_list[] = $table;
                if ($table->action == 'remove' && $table->hasUsers()){
                    $error_list[] = $table->name;
                }
            }
        }
        if (count($error_list))
            $this->addError('tables', 'Некоторые столики не могут быть удалены по причине наличия привязанных пользователей. <br/>Список столиков:<br/>'.implode(', ', $error_list));
        $this->tables = $tables_list;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_id' => 'Компания',
            'deleted' => 'Удален',
            'name' => 'Название',
			'location' => 'Адрес',
			'hidden' => 'Скрыт',
            'description' => 'Описание',
            'phone' => 'Телефон',
            'working_hours' => 'Рабочие часы',
            'map' => 'Координаты',
            'photos' => 'Фотографии',
            'lock_order_transfer' => 'Запретить передачу заказа'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('deleted',$this->deleted);
        $criteria->compare('company_id',$this->company_id);
		$criteria->compare('name',$this->name,true);
        $criteria->compare('location',$this->location,true);
		$criteria->compare('hidden',$this->hidden);
        $criteria->compare('description',$this->description);
        $criteria->compare('phone',$this->phone);
        $criteria->compare('working_hours',$this->working_hours);
        $criteria->compare('photos',$this->photos);
        $criteria->compare('lock_order_transfer',$this->lock_order_transfer);

        if (Yii::app()->user->company_id) $criteria->addCondition('company_id = '.Yii::app()->user->company_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Restaurant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave()
    {
        parent::afterSave();
        // обновление или создание столиков
        foreach($this->tables as $one) {
            $one->restaurant_id = $this->id;
            switch($one->action){
                case 'new':
                case 'update':
                    $one->save();
                break;
                case 'remove':
                    $one->delete();
                break;
            }
        }
    }

    // ---------    CUSTOM OBJECT METHODS   ----------
    public function getTitle(){
        if ($this->name)
            return $this->name;
        if ($this->location)
            return $this->location;

        return 'Ресторан #' . $this->id;
    }

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function getIdByOutId($out_id, $create = false){
        var_dump($out_id);
        if ($out_id){
            $model = Restaurant::model()->findByAttributes(array('out_id' => $out_id));
            if ($model === null && $create){
                $model = new Restaurant();
                $model->setAttributes(array(
                    'out_id' => $out_id,
                    'name' => '#'.$out_id,
                    'deleted' => 1
                ));
                if ($model->save(false)){
                    return $model->id;
                }
            }elseif(is_object($model)){
                return $model->id;
            }
        }
        return null;
    }

    public static function parseKeeperData(RKeeperQueue $queue){
        $response = @simplexml_load_string($queue->response);

        if (!$response){
            $queue->setFail('Не удалось обработать xml-ответ');
            return;
        }

        if ((string) $response['Status'] !== 'Ok'){
            $queue->setFail('Статус должен быть: Ok');
            return;
        }

        if ((string) $response['CMD'] !== $queue->getCMD()){
            $queue->setFail('Команда запроса и ответа не совпадает');
            return;
        }

        if ((string) $response->RK7Reference['Name'] !== strtoupper($queue->getRefName())){
            $queue->setFail('Справочник запроса и ответа не совпадает');
            return;
        }

        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AbcHelper::simple_xml_attr_to_array($row);

                if (!$item['Ident']) continue;
                $data = array();
                $data['out_id'] = $item['Ident'];
                $data['name'] = $item['Name'];
                $data['company_id'] = $queue->proxy->company_id;
                $data['location'] = $item['Address'];
                $data['hidden'] = ($item['Status'] == 'rsDraft' ? 1 : 0);
                $data['deleted'] = ($item['Status'] == 'rsDeleted' ? 1 : 0);

                $model = Restaurant::model()->findByAttributes(array('out_id' => $data['out_id']));
                if ($model === null){
                    $model = new Restaurant();
                }
                $model->setAttributes($data);
                if (!$model->save(false)){
                    $queue->setFail('Не удалось создать/обновить ресторан с out_id = ' . $model->out_id);
                    return;
                }
            }
            $queue->setFinished();
        }
    }








// ============================================================================================
// ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================


// ============================================================================================
// СТАТИЧЕСКИЕ МЕТОДЫ МОДЕЛИ
// ============================================================================================
    public static function getCompanyRestaurants($company_id){
        $list = array();
        if ($company_id){
            $result = Restaurant::model()->findAllByAttributes(array('company_id' => $company_id));
            foreach($result as $v){
                $list[$v->id] = $v->name;
            }
        }
        return $list;
    }

    /**
     * получение индекса последней версии данных
     */
    public static function getLastVersion($company_id, $field = 'version'){
        $row = self::model()->find(array(
            'select' => $field,
            'condition' => 'company_id = :company_id',
            'params' => array(':company_id' => $company_id),
            'order' => "{$field} DESC",
            'limit' => 1
        ));
        return is_object($row) ? $row->$field : 0;
    }

}


