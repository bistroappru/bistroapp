<?php

/**
 * This is the model class for table "{{menu}}".
 *
 * The followings are the available columns in table '{{menu}}':
 * @property integer $id
 * @property string $out_id
 * @property string $name
 * @property string $name_orig
 * @property integer $company_id
 * @property integer $parent_id
 * @property integer $sort
 * @property string $description
 * @property string $description_orig
 * @property string $image
 * @property integer $hidden
 * @property integer $deleted
 * @property integer $version
 * @property integer $imageVersion
 */
class Menu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{menu}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, company_id', 'required'),
			array('company_id, parent_id, sort, hidden, deleted', 'numerical', 'integerOnly'=>true),
			array('out_id, name, name_orig, image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, company_id, parent_id, sort, description, description_orig, image, hidden, deleted, version', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'positions' => array(self::HAS_MANY, 'Position', 'menu_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Наименование',
            'name_orig' => 'Наименование (исх)',
			'company_id' => 'Компания',
			'parent_id' => 'Родительская категория',
			'sort' => 'Сортировка',
			'description' => 'Описание',
            'description_orig' => 'Описание (исх)',
			'image' => 'Изображение',
			'hidden' => 'Скрытый',
            'deleted' => 'Удален',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


// ============================================================================================
// ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================
    protected function beforeSave() {
        if(parent::beforeSave()===false){
            return false;
        }

        $version_image = Menu::getLastVersion($this->company_id, 'imageVersion');
        $this->imageVersion = ++$version_image;

        $version_data = Menu::getLastVersion($this->company_id);
        $this->version = ++$version_data;

        return true;
    }

// ============================================================================================
// СТАТИЧЕСКИЕ МЕТОДЫ МОДЕЛИ
// ============================================================================================
    public static function getMenuList($company_id){
        $out = array();
        if ($company_id){
            $criteria = new CDbCriteria();
            $criteria->order = 'name ASC';
            $criteria->addCondition('company_id = ' . $company_id);
            foreach(self::model()->findAll($criteria) as $one){
                $out[$one->id] = $one->name;
            }
        }

        return $out;
    }



// =================================================================================================
// =================================================================================================
// СТАРОЕ
// =================================================================================================
// =================================================================================================

    /**
     * получить папку для бизнес ланчей
     */
    public static function getBusinessLunchRoot($company_id = 0){
        if (!$company_id)
            return false;

        $model = CompanyTools::model()->findByPk($company_id);
        if ($model === null)
            return false;

        return $model->business_lunch_ids;
    }

    /**
     * проверить является ли категория родительской для бизнес ланча
    */
    public function isBusinessLunchRoot(){
        $root = self::getBusinessLunchRoot($this->company_id);
        if ($root == $this->id)
            return true;
        return false;
    }

    /**
     * получение индекса последней версии данных
     */
    public static function getLastVersion($company_id, $field = 'version'){
        $row = self::model()->find(array(
            'select' => $field,
            'condition' => 'company_id = :company_id',
            'params' => array(':company_id' => $company_id),
            'order' => "{$field} DESC",
            'limit' => 1
        ));
        return is_object($row) ? $row->$field : 0;
    }

    public static function getCategoriesIds($company_id){
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->order = 'sort ASC';
        $criteria->addCondition('company_id = ' . $company_id);

        $result = self::model()->findAll($criteria);
        $list = array();
        foreach($result as $one){
            $list[] = $one['id'];
        }
        return $list;
    }

    // ---------    CUSTOM OBJECT METHODS   ----------
    /**
     * получить категории меню заданной компании
     */
    public function getCompanyCategories($bl_only = null, $array = true){
        $out = array();

        if ($this->company_id){
            $criteria = new CDbCriteria();
            $criteria->order = 'sort ASC';
            $criteria->addCondition('company_id = ' . $this->company_id);

            if ($bl_only !== null){
                $blid = (int) CompanyTools::model()->getBusinessLunchID($this->company_id);
                if ($bl_only === true){
                    $criteria->addCondition('id = ' . $blid);
                }else{
                    $criteria->addCondition('id != ' . $blid);
                }
            }

            foreach(self::model()->findAll($criteria) as $one){
                if ($array){
                    $out[$one->id] = $one->name;
                }else{
                    $out[] = $one;
                }
            }
        }

        return $out;
    }

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function getIdByOutId($out_id, $create = false, $company_id){
        if ($out_id){
            $model = Menu::model()->findByAttributes(array('out_id' => $out_id, 'company_id' => $company_id));
            if ($model === null && $create){
                $model = new Menu();
                $model->setAttributes(array(
                    'out_id' => $out_id,
                    'name' => '#'.$out_id,
                    'name_orig' => '#'.$out_id,
                    'company_id' => $company_id,
                    'deleted' => 1
                ));

                if ($model->save(false)){
                    return $model->id;
                }
            }elseif(is_object($model)){
                return $model->id;
            }
        }
        return null;
    }

    public static function getMaxSort($company_id){
        $model = self::model()->find(array('condition' => 'company_id = :company_id', 'order' => 'sort DESC', 'limit' => 1, 'params' => array(':company_id' => $company_id)));
        if ($model === null)
            return 0;
        else
            return $model->sort;
    }

}
