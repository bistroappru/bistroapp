/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50538
Source Host           : localhost:3306
Source Database       : bistroappru

Target Server Type    : MYSQL
Target Server Version : 50538
File Encoding         : 65001

Date: 2014-12-28 15:39:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `app_auth_assignment`;
CREATE TABLE `app_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `app_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_auth_assignment
-- ----------------------------
INSERT INTO `app_auth_assignment` VALUES ('admin', '1', null, 'N;');
INSERT INTO `app_auth_assignment` VALUES ('owner', '2', null, 'N;');

-- ----------------------------
-- Table structure for `app_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `app_auth_item`;
CREATE TABLE `app_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_auth_item
-- ----------------------------
INSERT INTO `app_auth_item` VALUES ('admin', '2', 'СуперАдмин', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('barman', '2', 'Бармен', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyCreate', '0', 'Создание компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyDelete', '0', 'Удаление компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyOwnTools', '1', 'Настройки своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('companyOwnUpdate', '1', 'Обновление своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('companyOwnView', '1', 'Просмотр своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('companyTools', '0', 'Настройки компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyUpdate', '0', 'Обновление компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyView', '0', 'Просмотр компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('cook', '2', 'Повар', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('garcon', '2', 'Оффициант', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('guest', '2', 'Гость', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('inAdmin', '0', 'Доступ к админке', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('manager', '2', 'Администратор ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryCreate', '0', 'Создание категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryDelete', '0', 'Удаление категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryUpdate', '0', 'Обновление категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryView', '0', 'Просмотр категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishCreate', '0', 'Создание блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishDelete', '0', 'Удаление блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishUpdate', '0', 'Обновление блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishView', '0', 'Просмотр блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryCreate', '1', 'Создание категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryDelete', '1', 'Удаление категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryUpdate', '1', 'Обновление категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryView', '1', 'Просмотр категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishCreate', '1', 'Создание блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishDelete', '1', 'Удаление блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishUpdate', '1', 'Обновление блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishView', '1', 'Просмотр блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('mobile', '2', 'Девайс', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('mobile_guest', '2', 'Девайс (гость)', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('owner', '2', 'Владелец сети', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyCreate', '1', 'Создание ресторанов в своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyDelete', '1', 'Удаление ресторанов в своей компании', 'return Yii::app()->user->company_id==$params[\"restaurant\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyUpdate', '1', 'Обновление ресторанов своей компании', 'return Yii::app()->user->company_id==$params[\"restaurant\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyView', '1', 'Просмотр ресторанов своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCreate', '0', 'Создание ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantDelete', '0', 'Удаление ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantOwnUpdate', '1', 'Обновление своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"restaurant\"]->id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantUpdate', '0', 'Обновление ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantView', '0', 'Просмотр ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userCreate', '0', 'Создание пользователя', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userDelete', '0', 'Удаление пользователя', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyCreate', '1', 'Создание пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyDelete', '1', 'Удаление пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyUpdate', '1', 'Редактирование пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyView', '1', 'Просмотр пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantCreate', '1', 'Создание пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantDelete', '1', 'Удаление пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantUpdate', '1', 'Редактирование пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantView', '1', 'Просмотр пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userUpdate', '0', 'Обновление пользователя', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userView', '0', 'Просмотр пользователя', null, 'N;');

-- ----------------------------
-- Table structure for `app_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `app_auth_item_child`;
CREATE TABLE `app_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `app_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_auth_item_child
-- ----------------------------
INSERT INTO `app_auth_item_child` VALUES ('manager', 'barman');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'companyOwnTools');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'companyOwnUpdate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'companyOwnView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyTools');
INSERT INTO `app_auth_item_child` VALUES ('companyOwnTools', 'companyTools');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('companyOwnUpdate', 'companyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyView');
INSERT INTO `app_auth_item_child` VALUES ('companyOwnView', 'companyView');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'cook');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'garcon');
INSERT INTO `app_auth_item_child` VALUES ('barman', 'inAdmin');
INSERT INTO `app_auth_item_child` VALUES ('cook', 'inAdmin');
INSERT INTO `app_auth_item_child` VALUES ('garcon', 'inAdmin');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'manager');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryCreate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryCreate', 'menuCategoryCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryDelete');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryDelete', 'menuCategoryDelete');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryUpdate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryUpdate', 'menuCategoryUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryView');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryView', 'menuCategoryView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishCreate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishCreate', 'menuDishCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishDelete');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishDelete', 'menuDishDelete');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishUpdate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishUpdate', 'menuDishUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishView');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishView', 'menuDishView');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryUpdate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryView');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishUpdate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishView');
INSERT INTO `app_auth_item_child` VALUES ('mobile', 'mobile_guest');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'owner');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'restaurantCompanyCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'restaurantCompanyDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'restaurantCompanyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'restaurantCompanyView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantCreate');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyCreate', 'restaurantCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantDelete');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyDelete', 'restaurantDelete');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'restaurantOwnUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyUpdate', 'restaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('restaurantOwnUpdate', 'restaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantView');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyView', 'restaurantView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userCreate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyCreate', 'userCreate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantCreate', 'userCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userDelete');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyDelete', 'userDelete');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantDelete', 'userDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyView');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantCreate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantDelete');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userUpdate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyUpdate', 'userUpdate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantUpdate', 'userUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userView');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyView', 'userView');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantView', 'userView');

-- ----------------------------
-- Table structure for `app_cash`
-- ----------------------------
DROP TABLE IF EXISTS `app_cash`;
CREATE TABLE `app_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `cashgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_cash
-- ----------------------------
INSERT INTO `app_cash` VALUES ('1', '15002', '1', 'F_ST', '1', '1');
INSERT INTO `app_cash` VALUES ('2', '15003', '3', 'rasp_rasp_STO1', '3', '1');
INSERT INTO `app_cash` VALUES ('3', '15004', '1', 'rasp_rasp_ST01', '3', '1');
INSERT INTO `app_cash` VALUES ('4', '15006', '4', 'ST02', '3', '2');
INSERT INTO `app_cash` VALUES ('5', '15008', '3', 'ST01', '3', '3');
INSERT INTO `app_cash` VALUES ('6', '15010', '5', 'ST04', '3', '4');
INSERT INTO `app_cash` VALUES ('7', '15012', '6', 'ST05', '3', '5');
INSERT INTO `app_cash` VALUES ('8', '15014', '7', 'ST06', '3', '6');
INSERT INTO `app_cash` VALUES ('9', '15016', '8', 'ST07', '3', '7');
INSERT INTO `app_cash` VALUES ('10', '15018', '9', 'ST08', '3', '8');
INSERT INTO `app_cash` VALUES ('11', '15020', '10', 'ST09', '3', '9');
INSERT INTO `app_cash` VALUES ('12', '15022', '11', 'ST10', '3', '15');
INSERT INTO `app_cash` VALUES ('13', '15025', '2', 'F_ST', '3', '11');
INSERT INTO `app_cash` VALUES ('14', '15028', '1', 'F_ST', '3', '12');
INSERT INTO `app_cash` VALUES ('15', '15030', '1', 'F_ST', '3', '13');
INSERT INTO `app_cash` VALUES ('16', '15033', '1', 'F_ST', '3', '14');

-- ----------------------------
-- Table structure for `app_cash_groups`
-- ----------------------------
DROP TABLE IF EXISTS `app_cash_groups`;
CREATE TABLE `app_cash_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_cash_groups
-- ----------------------------
INSERT INTO `app_cash_groups` VALUES ('1', '15001', '45', 'F_MIDSERV', '1');
INSERT INTO `app_cash_groups` VALUES ('2', '15005', '45', 'MIDSERV2', '3');
INSERT INTO `app_cash_groups` VALUES ('3', '15007', '45', 'MIDSERV3', '3');
INSERT INTO `app_cash_groups` VALUES ('4', '15009', '45', 'MIDSERV4', '3');
INSERT INTO `app_cash_groups` VALUES ('5', '15011', '45', 'MIDSERV5', '3');
INSERT INTO `app_cash_groups` VALUES ('6', '15013', '45', 'MIDSERV6', '3');
INSERT INTO `app_cash_groups` VALUES ('7', '15015', '45', 'MIDSERV7', '3');
INSERT INTO `app_cash_groups` VALUES ('8', '15017', '45', 'MIDSERV8', '3');
INSERT INTO `app_cash_groups` VALUES ('9', '15019', '45', 'MIDSERV9', '3');
INSERT INTO `app_cash_groups` VALUES ('11', '15024', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('12', '15027', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('13', '15029', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('14', '15031', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('15', '15021', '45', 'MIDSERV10', '3');

-- ----------------------------
-- Table structure for `app_company`
-- ----------------------------
DROP TABLE IF EXISTS `app_company`;
CREATE TABLE `app_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(500) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `type` varchar(255) NOT NULL,
  `kitchen` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `workplaces` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_company
-- ----------------------------
INSERT INTO `app_company` VALUES ('1', '', 'Тануки', '/data/images/logos/1401926957_c8b85841d570f75773a922451541676d.jpeg', '0', '0', 'Рестораны Тануки заметно отличаются своим аутентичным интерьером. Частокол из бамбука, стены с иероглифами и статуэтками, светильники из рисовой бумаги, большие столы, сделанные не для экономии места, а для удобства Гостей, впрочем, как и всё в ресторанах Тануки', '', '', 'http://www.tanuki.ru/', 'hotline@tanuki.ru', 'tanuki', '1,2', '0');
INSERT INTO `app_company` VALUES ('2', '', 'Япоша', '/data/images/logos/yaposha.jpg', '0', '0', '«Япоша» — сеть популярных демократичных японских ресторанов с яркой, жизнеутверждающей концепцией, с оригинальным двойным меню – суши и антисуши. Мы подарим Вам счастливые минуты беззаботного отдыха в кругу семьи и друзей.', '', '', '', '', 'yapo', '', '0');
INSERT INTO `app_company` VALUES ('3', '', 'Якитория', '/data/images/logos/yakitoriya.jpg', '0', '0', 'Сеть культовых кафе авторской японской кухни «Якитория» – самый масштабный проект ассоциации ресторанов \"Веста-центр интернешнл\". Городские кафе, удобно расположенные вблизи станций метро в Москве, предлагают лучшую японскую кухню в городе.', '', '', '', '', 'yaki', '', '0');
INSERT INTO `app_company` VALUES ('4', '', 'IL PATIO', '/data/images/logos/il_patio.jpg', '0', '0', '', '', '', '', '', 'patio', '1', '0');
INSERT INTO `app_company` VALUES ('6', '', 'Кофе Хауз', '/data/images/logos/1403777097_b54435daaf447e4d08027640ee117ff2.jpeg', '0', '0', '', '', '', '', '', 'house', '', '0');
INSERT INTO `app_company` VALUES ('7', '', 'шоколадница', '/data/images/logos/1403786730_a08d079201683af1763fae7e6f6e6270.jpeg', '1', '1', '', '', '', '', '', 'choko', '', '0');
INSERT INTO `app_company` VALUES ('8', '', 'тестовая', '/data/images/logos/1404304731_6c88a64d5f598c51d6a8e9e9aa69fc3b.png', '0', '1', '', '', '', '', '', 'test', '', '0');
INSERT INTO `app_company` VALUES ('9', '', 'Тест RKeeper7', '', '0', '0', '', '', '', '', '', 'rkeeper', '1,2', '0');

-- ----------------------------
-- Table structure for `app_company_tools`
-- ----------------------------
DROP TABLE IF EXISTS `app_company_tools`;
CREATE TABLE `app_company_tools` (
  `company_id` int(11) NOT NULL,
  `business_lunch_ids` int(11) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_company_tools
-- ----------------------------
INSERT INTO `app_company_tools` VALUES ('1', '13');

-- ----------------------------
-- Table structure for `app_company_type`
-- ----------------------------
DROP TABLE IF EXISTS `app_company_type`;
CREATE TABLE `app_company_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_company_type
-- ----------------------------
INSERT INTO `app_company_type` VALUES ('1', 'Ресторан');
INSERT INTO `app_company_type` VALUES ('2', 'Кафе');
INSERT INTO `app_company_type` VALUES ('3', 'Бар');
INSERT INTO `app_company_type` VALUES ('4', 'Кофейня');
INSERT INTO `app_company_type` VALUES ('5', 'Пиццерия');
INSERT INTO `app_company_type` VALUES ('6', 'Суши-бар');

-- ----------------------------
-- Table structure for `app_condition`
-- ----------------------------
DROP TABLE IF EXISTS `app_condition`;
CREATE TABLE `app_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `position_id` int(11) NOT NULL,
  `measure` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `action_on` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_condition
-- ----------------------------
INSERT INTO `app_condition` VALUES ('16', '', '9', '1000 мл.', '0', '150', '0');
INSERT INTO `app_condition` VALUES ('25', '', '8', '200 гр.', '0', '250', '1');
INSERT INTO `app_condition` VALUES ('30', '', '17', '8 шт.', '0', '95', '0');
INSERT INTO `app_condition` VALUES ('31', '', '18', '8 шт.', '0', '145', '0');
INSERT INTO `app_condition` VALUES ('32', '', '19', '1 шт.', '0', '355', '0');
INSERT INTO `app_condition` VALUES ('33', '', '20', '1 шт.', '0', '325', '0');
INSERT INTO `app_condition` VALUES ('34', '', '21', '6 шт.', '0', '95', '0');
INSERT INTO `app_condition` VALUES ('35', '', '22', '1 шт.', '0', '235', '0');
INSERT INTO `app_condition` VALUES ('38', '', '25', '1 шт.', '0', '290', '0');
INSERT INTO `app_condition` VALUES ('39', '', '26', '1000 мл.', '0', '110', '0');
INSERT INTO `app_condition` VALUES ('40', '', '27', '330 мл.', '0', '145', '0');
INSERT INTO `app_condition` VALUES ('41', '', '28', '250 мл.', '0', '90', '0');
INSERT INTO `app_condition` VALUES ('42', '', '29', '1000 мл.', '0', '120', '0');
INSERT INTO `app_condition` VALUES ('43', '', '30', '1 шт.', '0', '310', '0');
INSERT INTO `app_condition` VALUES ('44', '', '31', '1 шт.', '0', '245', '0');
INSERT INTO `app_condition` VALUES ('45', '', '32', '1 шт.', '0', '360', '0');
INSERT INTO `app_condition` VALUES ('46', '', '33', '1 шт.', '0', '235', '0');
INSERT INTO `app_condition` VALUES ('47', '', '34', '1 шт.', '0', '290', '0');
INSERT INTO `app_condition` VALUES ('48', '', '35', '1 шт.', '0', '1230', '0');
INSERT INTO `app_condition` VALUES ('49', '', '36', '1 шт.', '0', '2790', '0');
INSERT INTO `app_condition` VALUES ('50', '', '37', '1 шт.', '0', '1110', '0');
INSERT INTO `app_condition` VALUES ('51', '', '38', '1 шт.', '0', '310', '0');
INSERT INTO `app_condition` VALUES ('52', '', '39', '1 шт.', '0', '440', '0');
INSERT INTO `app_condition` VALUES ('53', '', '40', '1 шт.', '0', '35', '0');
INSERT INTO `app_condition` VALUES ('54', '', '41', '1 шт.', '0', '75', '0');
INSERT INTO `app_condition` VALUES ('55', '', '42', '1 шт.', '0', '70', '0');
INSERT INTO `app_condition` VALUES ('56', '', '43', '1 шт.', '0', '90', '0');
INSERT INTO `app_condition` VALUES ('57', '', '44', '1 шт.', '0', '80', '0');
INSERT INTO `app_condition` VALUES ('69', '', '48', '500 мл.', '0', '150', '0');
INSERT INTO `app_condition` VALUES ('70', '', '49', '150 мл.', '0', '50', '0');
INSERT INTO `app_condition` VALUES ('71', '', '49', '250 мл.', '0', '100', '0');
INSERT INTO `app_condition` VALUES ('72', '', '49', '500 мл.', '0', '200', '0');
INSERT INTO `app_condition` VALUES ('74', '', '51', 'Основная', '0', '115', '0');
INSERT INTO `app_condition` VALUES ('75', '', '52', 'Основная', '0', '150', '0');
INSERT INTO `app_condition` VALUES ('76', '', '53', 'Основная', '0', '220', '0');
INSERT INTO `app_condition` VALUES ('77', '', '54', 'Основная', '0', '245', '0');
INSERT INTO `app_condition` VALUES ('78', '', '55', 'Основная', '0', '247', '0');
INSERT INTO `app_condition` VALUES ('79', '', '56', 'Основная', '0', '299', '0');
INSERT INTO `app_condition` VALUES ('80', '', '57', 'Основная', '0', '287', '0');
INSERT INTO `app_condition` VALUES ('81', '', '58', 'Основная', '0', '237', '0');
INSERT INTO `app_condition` VALUES ('82', '', '59', 'Основная', '0', '100', '0');
INSERT INTO `app_condition` VALUES ('83', '', '60', 'комбо', '0', '250', '0');
INSERT INTO `app_condition` VALUES ('88', '', '24', '1 шт.', '0', '260', '0');
INSERT INTO `app_condition` VALUES ('90', '', '63', '1 шт.', '1', '100', '0');
INSERT INTO `app_condition` VALUES ('91', '', '64', 'Общая стоимость', '1', '250', '0');
INSERT INTO `app_condition` VALUES ('92', '', '65', '1 шт.', '1', '20', '0');
INSERT INTO `app_condition` VALUES ('93', '', '66', '1 шт.', '1', '345', '0');
INSERT INTO `app_condition` VALUES ('101', '', '23', '1 шт.', '1410877406', '1', '0');
INSERT INTO `app_condition` VALUES ('102', '', '67', 'Общая стоимость', '1415782139', '123', '0');
INSERT INTO `app_condition` VALUES ('103', '', '142', '1 шт.', '1419233612', '150', '0');
INSERT INTO `app_condition` VALUES ('104', '', '143', '1 шт.', '1419233944', '98', '0');

-- ----------------------------
-- Table structure for `app_group_details`
-- ----------------------------
DROP TABLE IF EXISTS `app_group_details`;
CREATE TABLE `app_group_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `condition_id` int(11) NOT NULL,
  `pg_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_group_details
-- ----------------------------
INSERT INTO `app_group_details` VALUES ('26', '67', '0', '25', '2');
INSERT INTO `app_group_details` VALUES ('27', '67', '1', '35', '2');
INSERT INTO `app_group_details` VALUES ('63', '64', '0', '88', '1');
INSERT INTO `app_group_details` VALUES ('64', '64', '1', '38', '1');
INSERT INTO `app_group_details` VALUES ('65', '64', '0', '35', '2');
INSERT INTO `app_group_details` VALUES ('66', '64', '0', '25', '3');
INSERT INTO `app_group_details` VALUES ('67', '64', '1', '101', '3');

-- ----------------------------
-- Table structure for `app_kitchen`
-- ----------------------------
DROP TABLE IF EXISTS `app_kitchen`;
CREATE TABLE `app_kitchen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_kitchen
-- ----------------------------
INSERT INTO `app_kitchen` VALUES ('1', 'Авторская');
INSERT INTO `app_kitchen` VALUES ('2', 'Азиатская');
INSERT INTO `app_kitchen` VALUES ('3', 'Американская');
INSERT INTO `app_kitchen` VALUES ('4', 'Арабская');
INSERT INTO `app_kitchen` VALUES ('5', 'Армянская');
INSERT INTO `app_kitchen` VALUES ('6', 'Армянская');
INSERT INTO `app_kitchen` VALUES ('7', 'Белорусская');
INSERT INTO `app_kitchen` VALUES ('8', 'Вегетарианская');
INSERT INTO `app_kitchen` VALUES ('9', 'Восточная');
INSERT INTO `app_kitchen` VALUES ('10', 'Грузинская');
INSERT INTO `app_kitchen` VALUES ('11', 'Домашняя');
INSERT INTO `app_kitchen` VALUES ('12', 'Европейская');
INSERT INTO `app_kitchen` VALUES ('13', 'Индийская');
INSERT INTO `app_kitchen` VALUES ('14', 'Испанская');
INSERT INTO `app_kitchen` VALUES ('15', 'Итальянская');
INSERT INTO `app_kitchen` VALUES ('16', 'Кавказская');
INSERT INTO `app_kitchen` VALUES ('17', 'Китайская');
INSERT INTO `app_kitchen` VALUES ('18', 'Кубинская');
INSERT INTO `app_kitchen` VALUES ('19', 'Кухня народов СССР');
INSERT INTO `app_kitchen` VALUES ('20', 'Мексиканская');
INSERT INTO `app_kitchen` VALUES ('21', 'Народная');
INSERT INTO `app_kitchen` VALUES ('22', 'Немецкая');
INSERT INTO `app_kitchen` VALUES ('23', 'Русская');
INSERT INTO `app_kitchen` VALUES ('24', 'Северная');
INSERT INTO `app_kitchen` VALUES ('25', 'Славянская');
INSERT INTO `app_kitchen` VALUES ('26', 'Средиземноморская');
INSERT INTO `app_kitchen` VALUES ('27', 'Старобелорусская');
INSERT INTO `app_kitchen` VALUES ('28', 'Старославянская');
INSERT INTO `app_kitchen` VALUES ('29', 'Тайская');
INSERT INTO `app_kitchen` VALUES ('30', 'Узбекская');
INSERT INTO `app_kitchen` VALUES ('31', 'Украинская');
INSERT INTO `app_kitchen` VALUES ('32', 'Французская');
INSERT INTO `app_kitchen` VALUES ('33', 'Чешская');
INSERT INTO `app_kitchen` VALUES ('34', 'Японская');

-- ----------------------------
-- Table structure for `app_label`
-- ----------------------------
DROP TABLE IF EXISTS `app_label`;
CREATE TABLE `app_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `style_id` varchar(125) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_label
-- ----------------------------
INSERT INTO `app_label` VALUES ('1', 'ХИТ', 'hit');
INSERT INTO `app_label` VALUES ('2', 'НОВИНКА', 'new');

-- ----------------------------
-- Table structure for `app_label_style`
-- ----------------------------
DROP TABLE IF EXISTS `app_label_style`;
CREATE TABLE `app_label_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classname` varchar(100) NOT NULL,
  `style` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_label_style
-- ----------------------------

-- ----------------------------
-- Table structure for `app_menu`
-- ----------------------------
DROP TABLE IF EXISTS `app_menu`;
CREATE TABLE `app_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_menu
-- ----------------------------
INSERT INTO `app_menu` VALUES ('1', '', 'Закуски', '1', '0', '0', 'Описание \"Закуски\"\r\nЗдесь могла бы быть  ваша реклама.', '/data/images/menu/1404301480_40b5de0c3abc86700b15cd62a6aa0793.png', '0', '0', '15', '5');
INSERT INTO `app_menu` VALUES ('2', '', 'Напитки', '1', '0', '1', 'Описание \"Напитки\"', '/data/images/menu/74f4f3fba4992673c6568afaa6e8748f.png', '0', '0', '21', '0');
INSERT INTO `app_menu` VALUES ('3', '', 'Роллы', '1', '0', '2', 'Описание \"Роллы\"', '/data/images/menu/ee927ac9dd447449d100a297c55a7030.png', '0', '0', '17', '0');
INSERT INTO `app_menu` VALUES ('4', '', 'Салаты', '1', '0', '5', 'Описание \"Салаты\"', '/data/images/menu/a0bb357344944233001a7aa70df48faf.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('5', '', 'Сашими', '1', '0', '4', 'Описание \"Сашими\"', '/data/images/menu/5acfabbff1963fe83ec0a5b671625f44.png', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('6', '', 'Сеты', '1', '0', '6', 'Описание \"Сети\"', '/data/images/menu/c094dfc66865c2bca0e109286ee7ca1c.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('7', '', 'Суши', '1', '0', '7', 'Описание \"Суши\"', '/data/images/menu/a00e1192e3e7cd2c07a28c9974952063.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('9', '', 'Антисуши', '2', '0', '0', '', '/data/images/menu/1403777336_952582388a081cda41d8fb3ab6a7f448.png', '0', '0', '16', '0');
INSERT INTO `app_menu` VALUES ('10', '', 'Супы', '3', '0', '0', 'Супы японской кухни', '/data/images/menu/1403778236_f89f3b28186a4fc7b2a495315f14535d.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('11', '', 'Шоколад', '7', '0', '0', 'вкусно и питательно', '/data/images/menu/1403786888_9502a5ed48137020cfe8ff8f50b52730.jpeg', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('12', '', 'Супы', '7', '0', '0', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('13', '', 'Бизнес-ланчи', '1', '0', '3', 'djkfhkjd', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('14', '', 'Супы', '6', '0', '0', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('15', '', 'закуски', '6', '0', '0', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('18', '', 'первая', '8', '0', '0', '', '', '0', '0', '1', '1');
INSERT INTO `app_menu` VALUES ('19', '', 'вторые блюда', '3', '0', '1', '', '', '0', '0', '1', '1');
INSERT INTO `app_menu` VALUES ('44', '1000018', 'Напитки', '9', '65', '62', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('45', '1000063', 'Групповой выбор', '9', '0', '64', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('46', '1000073', 'Бизнес ланч', '9', '65', '65', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('47', '1000078', 'Салаты', '9', '46', '66', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('48', '1000082', 'Супы', '9', '46', '67', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('49', '1000086', 'Напитки', '9', '46', '68', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('50', '1000104', 'Десерты2', '9', '65', '69', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('51', '1000112', 'Салаты', '9', '65', '70', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('52', '1000191', 'Вторые блюда', '9', '46', '71', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('53', '1000207', 'Вторые блюда', '9', '65', '72', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('54', '1000221', 'Супы', '9', '65', '73', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('55', '1000227', 'Горячие закуски', '9', '65', '74', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('56', '1000233', 'Коньяк', '9', '66', '75', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('57', '1000474', 'фисв5', '9', '73', '89', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('58', '1000239', 'Пиво бутылочное', '9', '65', '76', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('59', '1000346', 'Новое комбо', '9', '65', '77', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('60', '1000468', 'тест 300500', '9', '69', '84', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('61', '1000490', 'комбо', '9', '0', '91', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('62', '1000504', 'тестовая группа', '9', '54', '92', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('63', '1000062', 'Групповой выбор', '9', '44', '63', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('64', '1000459', 'НЕ БУДЕТ ТАКОГО 2', '9', '65', '78', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('65', '1000463', 'Корень', '9', '0', '79', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('66', '1000464', 'Еще 1 корень', '9', '65', '80', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('67', '1000465', 'Не будет такого 3', '9', '64', '81', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('68', '1000466', 'тест 100500', '9', '65', '82', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('69', '1000467', 'тест 200500', '9', '68', '83', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('70', '1000470', 'фисв', '9', '68', '85', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('71', '1000471', 'фисв2', '9', '70', '86', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('72', '1000472', 'фисв3', '9', '71', '87', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('73', '1000473', 'фисв4', '9', '65', '88', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('74', '1000489', 'тест', '9', '0', '90', '', '', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('75', '1000505', 'хаха папка', '9', '73', '93', '', '', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for `app_order`
-- ----------------------------
DROP TABLE IF EXISTS `app_order`;
CREATE TABLE `app_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_visit` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `date_create` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `attention` tinyint(1) NOT NULL DEFAULT '0',
  `separated` tinyint(1) NOT NULL DEFAULT '0',
  `separated_info` text NOT NULL,
  `date_execute` int(11) NOT NULL,
  `date_close` int(11) NOT NULL,
  `comment` text NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `garcon_id` int(11) NOT NULL,
  `count_items` int(11) NOT NULL,
  `total_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order
-- ----------------------------
INSERT INTO `app_order` VALUES ('30', '', '', '1', '12', '2', '1401187411', '1', '0', '0', '', '0', '0', '', '3', '0', '1', '1000');
INSERT INTO `app_order` VALUES ('31', '', '', '1', '12', '6', '1401187433', '2', '0', '0', '', '0', '1413209166', '', '3', '0', '1', '1000');
INSERT INTO `app_order` VALUES ('32', '', '', '1', '12', '2', '1401187490', '1', '0', '0', '', '0', '0', '', '3', '0', '1', '250');
INSERT INTO `app_order` VALUES ('35', '', '', '1', '12', '2', '1401193230', '1', '0', '0', '', '0', '0', '', '3', '0', '1', '180');
INSERT INTO `app_order` VALUES ('38', '', '', '1', '12', '2', '1405688493', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '5', '2', '720');
INSERT INTO `app_order` VALUES ('39', '', '', '1', '11', '2', '1405689407', '2', '0', '0', '', '0', '0', '', '3', '5', '2', '595');
INSERT INTO `app_order` VALUES ('40', '', '', '1', '11', '2', '1405689418', '1', '0', '0', '', '0', '0', '', '3', '5', '2', '260');
INSERT INTO `app_order` VALUES ('41', '', '', '1', '11', '2', '1405689478', '2', '0', '0', '', '0', '0', '', '3', '5', '2', '350');
INSERT INTO `app_order` VALUES ('42', '', '', '1', '11', '2', '1405689506', '1', '0', '0', '', '0', '0', '', '3', '5', '2', '260');
INSERT INTO `app_order` VALUES ('43', '', '', '1', '11', '2', '1405689660', '2', '0', '0', '', '0', '0', '', '3', '5', '2', '645');
INSERT INTO `app_order` VALUES ('44', '', '', '1', '11', '2', '1406146020', '1', '0', '0', '', '0', '0', '', '3', '5', '1', '285');
INSERT INTO `app_order` VALUES ('45', '', '', '1', '11', '2', '1406190524', '2', '0', '0', '', '0', '0', '', '3', '5', '1', '1230');
INSERT INTO `app_order` VALUES ('46', '', '', '1', '11', '2', '1406622304', '2', '0', '0', '', '0', '0', '', '3', '5', '2', '1235');
INSERT INTO `app_order` VALUES ('47', '', '', '1', '11', '2', '1406624905', '1', '0', '0', '', '0', '0', '', '3', '5', '2', '345');
INSERT INTO `app_order` VALUES ('48', '', '', '1', '11', '2', '1407277854', '2', '0', '0', '', '0', '0', '', '3', '5', '2', '690');
INSERT INTO `app_order` VALUES ('49', '', '', '1', '11', '2', '1407779319', '2', '0', '0', '', '0', '0', '', '3', '5', '1', '190');
INSERT INTO `app_order` VALUES ('50', '', '', '1', '11', '2', '1407780478', '1', '0', '0', '', '0', '0', '', '3', '5', '1', '580');
INSERT INTO `app_order` VALUES ('51', '', '', '1', '11', '2', '1407850965', '2', '0', '0', '', '0', '0', '', '3', '5', '2', '500');
INSERT INTO `app_order` VALUES ('52', '', '', '1', '11', '2', '1408142013', '2', '0', '0', '', '0', '0', '', '3', '5', '7', '3090');
INSERT INTO `app_order` VALUES ('56', '', '', '1', '12', '2', '1414600164', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '9', '1', '450');
INSERT INTO `app_order` VALUES ('57', '', '', '1', '12', '2', '1414604328', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '9', '1', '450');
INSERT INTO `app_order` VALUES ('58', '', '', '1', '12', '2', '1414604367', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '9', '1', '450');
INSERT INTO `app_order` VALUES ('59', '', '', '1', '12', '2', '1414604404', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '9', '1', '450');
INSERT INTO `app_order` VALUES ('60', '', '', '1', '12', '2', '1414604474', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '9', '1', '450');
INSERT INTO `app_order` VALUES ('61', '', '', '1', '12', '2', '1414604576', '1', '1', '1', 'текст раздельного заказа', '0', '0', '', '3', '9', '1', '450');
INSERT INTO `app_order` VALUES ('62', '256', '655362330', '1', '28', '1', '1419239553', '1', '0', '0', '', '0', '0', '', '45', '12', '2', '248');
INSERT INTO `app_order` VALUES ('63', '256', '655362330', '1', '28', '1', '1419239753', '1', '0', '0', '', '0', '0', '', '45', '12', '1', '300');

-- ----------------------------
-- Table structure for `app_order_item`
-- ----------------------------
DROP TABLE IF EXISTS `app_order_item`;
CREATE TABLE `app_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order_item
-- ----------------------------
INSERT INTO `app_order_item` VALUES ('20', '30', '25', '9', '250', '4');
INSERT INTO `app_order_item` VALUES ('21', '31', '25', '9', '250', '4');
INSERT INTO `app_order_item` VALUES ('22', '32', '25', '8', '250', '1');
INSERT INTO `app_order_item` VALUES ('25', '35', '41', '11', '90', '2');
INSERT INTO `app_order_item` VALUES ('29', '38', '41', '9', '90', '3');
INSERT INTO `app_order_item` VALUES ('30', '38', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('31', '39', '25', '9', '250', '2');
INSERT INTO `app_order_item` VALUES ('32', '39', '36', '9', '95', '1');
INSERT INTO `app_order_item` VALUES ('35', '40', '39', '8', '110', '1');
INSERT INTO `app_order_item` VALUES ('36', '40', '16', '8', '150', '1');
INSERT INTO `app_order_item` VALUES ('37', '41', '83', '9', '250', '1');
INSERT INTO `app_order_item` VALUES ('38', '41', '82', '9', '100', '1');
INSERT INTO `app_order_item` VALUES ('39', '42', '39', '9', '110', '1');
INSERT INTO `app_order_item` VALUES ('40', '42', '16', '9', '150', '1');
INSERT INTO `app_order_item` VALUES ('41', '43', '31', '9', '145', '2');
INSERT INTO `app_order_item` VALUES ('43', '43', '32', '9', '355', '1');
INSERT INTO `app_order_item` VALUES ('44', '44', '36', '9', '95', '3');
INSERT INTO `app_order_item` VALUES ('45', '45', '48', '9', '1230', '1');
INSERT INTO `app_order_item` VALUES ('46', '46', '35', '9', '235', '1');
INSERT INTO `app_order_item` VALUES ('47', '46', '25', '9', '250', '4');
INSERT INTO `app_order_item` VALUES ('48', '47', '36', '9', '95', '1');
INSERT INTO `app_order_item` VALUES ('49', '47', '91', '9', '250', '1');
INSERT INTO `app_order_item` VALUES ('50', '48', '36', '9', '95', '2');
INSERT INTO `app_order_item` VALUES ('51', '48', '25', '9', '250', '2');
INSERT INTO `app_order_item` VALUES ('52', '49', '36', '9', '95', '2');
INSERT INTO `app_order_item` VALUES ('53', '50', '38', '9', '290', '2');
INSERT INTO `app_order_item` VALUES ('55', '51', '25', '9', '250', '1');
INSERT INTO `app_order_item` VALUES ('56', '51', '25', '9', '250', '1');
INSERT INTO `app_order_item` VALUES ('57', '52', '35', '9', '235', '1');
INSERT INTO `app_order_item` VALUES ('59', '52', '38', '9', '290', '2');
INSERT INTO `app_order_item` VALUES ('60', '52', '91', '9', '250', '1');
INSERT INTO `app_order_item` VALUES ('61', '52', '48', '9', '1230', '1');
INSERT INTO `app_order_item` VALUES ('62', '52', '16', '9', '150', '1');
INSERT INTO `app_order_item` VALUES ('63', '52', '31', '9', '145', '2');
INSERT INTO `app_order_item` VALUES ('64', '52', '32', '9', '355', '1');
INSERT INTO `app_order_item` VALUES ('68', '56', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('69', '57', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('70', '58', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('71', '59', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('72', '60', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('73', '61', '16', '9', '150', '3');
INSERT INTO `app_order_item` VALUES ('74', '62', '103', '9', '150', '1');
INSERT INTO `app_order_item` VALUES ('75', '62', '104', '9', '98', '1');
INSERT INTO `app_order_item` VALUES ('76', '63', '103', '9', '150', '2');

-- ----------------------------
-- Table structure for `app_order_payment`
-- ----------------------------
DROP TABLE IF EXISTS `app_order_payment`;
CREATE TABLE `app_order_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order_payment
-- ----------------------------
INSERT INTO `app_order_payment` VALUES ('1', 'Наличными');
INSERT INTO `app_order_payment` VALUES ('2', 'Картой');

-- ----------------------------
-- Table structure for `app_order_status`
-- ----------------------------
DROP TABLE IF EXISTS `app_order_status`;
CREATE TABLE `app_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'order',
  `prev_id` tinyint(1) NOT NULL,
  `next_id` tinyint(1) NOT NULL,
  `roles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order_status
-- ----------------------------
INSERT INTO `app_order_status` VALUES ('1', 'Новый', 'order', '0', '0', 'admin,owner,manager');
INSERT INTO `app_order_status` VALUES ('2', 'Принят', 'order', '0', '0', 'admin,owner,manager');
INSERT INTO `app_order_status` VALUES ('3', 'Оплачен', 'order', '0', '0', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('6', 'Закрыт', 'order', '0', '0', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('8', 'Новый', 'item', '0', '9', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('9', 'Принято', 'item', '8', '10', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('10', 'Готовится', 'item', '9', '11', 'admin,owner,manager,barman,cook');
INSERT INTO `app_order_status` VALUES ('11', 'Готово', 'item', '10', '12', 'admin,owner,manager,barman,cook');
INSERT INTO `app_order_status` VALUES ('12', 'Доставлено', 'item', '11', '0', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('13', 'Отменен', 'order', '0', '0', 'admin,owner,manager');

-- ----------------------------
-- Table structure for `app_position`
-- ----------------------------
DROP TABLE IF EXISTS `app_position`;
CREATE TABLE `app_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `is_group` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `depressed` tinyint(1) NOT NULL DEFAULT '0',
  `label_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `calories` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `in_stock` varchar(500) NOT NULL,
  `food_type` tinyint(1) NOT NULL DEFAULT '1',
  `workplace` tinyint(1) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_position
-- ----------------------------
INSERT INTO `app_position` VALUES ('8', '', '0', 'Калифорния рору', 'Мясо краба, авокадо, руккола', '1', '3', '0', '2', '/data/images/positions/1401927643_297cc5cc806b05ef6c055377170e81f8.jpeg', '0', '', '0', '3,4', '1', '1', '77', '3');
INSERT INTO `app_position` VALUES ('9', '', '0', 'Сок апельсиновый', 'Пакетированный апельсиновый сок\r\n\r\n21313234', '2', '1', '0', '0', '/data/images/positions/e5c0c07963cc3d3d0b75bb704091eda2.jpeg', '0', '', '0', '', '2', '1', '0', '0');
INSERT INTO `app_position` VALUES ('17', '', '0', 'Абокадо ролл', 'Классический ролл с авокадо', '3', '1', '0', '0', '/data/images/positions/f0837c2e091cd50ce59b12b03a585bcd.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('18', '', '0', 'Сякэ ролл', 'Классический ролл с лососем\r\n\r\nСостав:(рис, водоросли)', '3', '0', '0', '0', '/data/images/positions/1b63e7273a45181773e764dd46468ae7.jpeg', '0', '', '0', '3,4,6', '1', '1', '124', '0');
INSERT INTO `app_position` VALUES ('19', '', '0', 'Калифорния', 'Ролл с мясом краба, авокадо, огурцом, тобико и майонезом', '3', '2', '0', '0', '/data/images/positions/89a1127cd2d987cc4918949480db00dc.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('20', '', '0', 'Филадельфия', 'Ролл с лососем, мягким сыром, зеленым луком, кунжутом, огурцом и авокадо', '3', '3', '0', '0', '/data/images/positions/d3b5ac1126fd045fb69c591d789cd343.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('21', '', '0', 'Каппа ролл', 'Классический ролл с огурцом', '3', '4', '0', '0', '/data/images/positions/83d648b0a2638c769651c334e5c5fad9.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('22', '', '0', 'Гюнику татаки', 'Маринованная говядина с перцем и чесноком, дайконом, салатом, лимоном и луком, украшается петрушкой \r\n\r\n+ соус Каниши', '1', '4', '0', '2', '/data/images/positions/709d251f56858dff4da693b1a46337a2.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('23', '', '0', 'Кимучи', 'Острый салат из капусты, моркови, яблока, лука-порей и дайкона', '1', '2', '0', '1', '/data/images/positions/1404307274_2fb3eec1550ba464fbd732c607568722.jpeg', '0', '', '0', '', '1', '1', '118', '4');
INSERT INTO `app_position` VALUES ('24', '', '0', 'Сякэ усугири', 'Ломтики лосося с перцем чили и соусом \"Юзу\"', '1', '5', '0', '0', '/data/images/positions/cb43416d18773ec2bc624ae03e839a0c.jpeg', '0', '', '0', '', '1', '1', '98', '0');
INSERT INTO `app_position` VALUES ('25', '', '0', 'Торинику соба', 'Гречневая лапша с курицей, морковью, кунжутом и соусом \"Юзу-понзу\"', '1', '6', '0', '0', '/data/images/positions/dacc7b13857bc54a87ce851b3734ede7.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('26', '', '0', 'Спрайт', 'Газированный напиток Спрайт', '2', '0', '0', '0', '/data/images/positions/bfd78bb7f13c5cae729e72ea1ab008f9.jpeg', '0', '', '0', '', '2', '1', '120', '0');
INSERT INTO `app_position` VALUES ('27', '', '0', ' Перье', 'Минеральная вода Перье', '2', '2', '0', '0', '/data/images/positions/2bc6f91d3cef70ab4805a42e43829ebc.jpeg', '0', '', '0', '', '2', '1', '104', '0');
INSERT INTO `app_position` VALUES ('28', '', '0', 'Кока-кола', 'Газированный напиток Кока-кола', '2', '3', '0', '0', '/data/images/positions/31ab3caf52db4a6f0cae3b9cebbb7407.jpeg', '0', '', '0', '', '2', '1', '105', '0');
INSERT INTO `app_position` VALUES ('29', '', '0', 'Нести Лимон', 'Холодный чай Нести Лимон', '2', '4', '0', '0', '/data/images/positions/2461b677fe0d18d2f665937a687e23b2.jpeg', '0', '', '0', '', '2', '1', '106', '0');
INSERT INTO `app_position` VALUES ('30', '', '0', ' Рифу сарада', 'Кальмар, мидии, креветки с листьями салата корн, базиликом, кинзой и острым соусом', '4', '0', '0', '2', '/data/images/positions/c7d2f2cc2c8558421a038ca84e7e4e92.jpeg', '0', '', '0', '', '1', '1', '7', '0');
INSERT INTO `app_position` VALUES ('31', '', '0', '«Цезарь-сан» с курицей', 'Салат с курицей, салатом айсберг, чесночными гренками, отварным яйцом, черри и сыром грана падано', '4', '0', '0', '0', '/data/images/positions/db6fda642133122a0a9390263e465106.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('32', '', '0', 'Дайкани сарада', 'Салат с мясом краба, апельсином и тобико', '4', '0', '0', '0', '/data/images/positions/3033a1c3b159c0d0e7a3369353187302.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('33', '', '0', 'Осэй сарада', 'Салат из пикантной курицы с салатом айсберг, сельдереем, авокадо, огурцами и сладким перцем', '4', '0', '0', '0', '/data/images/positions/7f2c9de3808410e00f15c0040849c4f1.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('34', '', '0', 'Гюнику сарада', 'Говядина с томатами, огурцом и зелёным луком', '4', '0', '0', '0', '/data/images/positions/dc407fad2b77d7b9ca81fcdca7b576d8.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('35', '', '0', 'Тануки сет', 'Сяке темпура - 1\r\nФиладельфия -1\r\nЭби сирогома -1\r\nСяке кадо - 1\r\nХотате караи - 1\r\nПорция васаби XL (30г) - 1\r\nПорция имбиря XL (60г) - 1', '6', '0', '0', '0', '/data/images/positions/75322a8622bee9eaba6e554ded571ec7.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('36', '', '0', 'Харакири сет', 'Дракон ролл - 1\r\nОвара ролл - 1\r\nЙонака ролл - 1\r\nОкинава ролл - 1\r\nКалифорния - 2\r\nФиладельфия - 1\r\nХигата ролл - 1\r\nМексиканский ролл - 1\r\nХияши унаги - 1\r\nУнаги урамаки - 1\r\nПорция васаби XL (30 г) - 2\r\nПорция имбиря XL (60 г) - 2\r\n', '6', '0', '0', '0', '/data/images/positions/04279f8326e750e85a75a50571878969.jpeg', '0', '300', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('37', '', '0', 'Суши сет', 'Суши с угрем - 5\r\nСуши с лососем - 5\r\nСуши с тунцом - 5\r\nСуши с лакедрой - 2\r\nПорция васаби (10г) - 2\r\nПорция имбиря (20г) - 2\r\n', '6', '0', '0', '0', '/data/images/positions/251fdbc662899a16ed50ca94cc548864.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('38', '', '0', 'Якинику мориавасэ', 'Ассорти мясное: шашлычки из куриного фарша, говядины, свинины и кукурузы', '6', '0', '0', '0', '/data/images/positions/7981d6b7a38d03d12b639fc0d9fa7c1e.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('39', '', '0', 'Сифудо мориавасэ', 'Ассорти из морепродуктов: шашлычки из креветок, морского гребешка, лосося и осьминожек', '6', '0', '0', '0', '/data/images/positions/a04ea60470ca58ddfc36804ab98ce963.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('40', '', '0', 'Сякэ', 'Суши нигири с лососем', '7', '1', '0', '0', '/data/images/positions/76a954a2f113df9109946ce5c9d10250.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('41', '', '0', 'Хотатэ', 'Суши нигири с морским гребешком', '7', '2', '0', '0', '/data/images/positions/cdf8a124550b9cf2c472c5c5dcbd6b2d.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('42', '', '0', 'Эби', 'Суши нигири с вареной сладкой креветкой, маринованной в имбирном соусе', '7', '0', '0', '0', '/data/images/positions/b4e6e929f2c157942f4c90bede5e707b.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('43', '', '0', 'Икура', 'Суши гункан с икрой лосося', '7', '3', '0', '0', '/data/images/positions/d4b9f8240ae9d09674b50e5276aeece5.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('44', '', '0', 'Спайси сякэ', 'Острый суши гункан с лососем', '7', '4', '0', '0', '/data/images/positions/4780ead67ef309276efd0327436c4cef.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('48', '', '0', 'Напиток для ланча 1', '', '2', '5', '1', '0', '', '0', '', '0', '4', '2', '1', '107', '0');
INSERT INTO `app_position` VALUES ('49', '', '0', 'Напиток для ланча 2', '', '2', '6', '1', '0', '', '0', '', '0', '3,4', '2', '1', '108', '0');
INSERT INTO `app_position` VALUES ('51', '', '0', 'Вареники с картошкой', 'Домашние вареники ручной лепки с картофелем. Подаются со сметаной.', '9', '1', '0', '0', '/data/images/positions/1403777530_9f2df9e3815e570d992c3d3f9066ecd3.jpeg', '0', '', '0', '7', '1', '1', '1', '0');
INSERT INTO `app_position` VALUES ('52', '', '0', 'Сибирские пельмени', 'Домашние пельмени, ручной лепки, с нежным мясом. Подаются со сметаной.', '9', '0', '0', '0', '/data/images/positions/1403777673_7f785013ba9c808fd9d1569cea05a442.jpeg', '0', '', '0', '7', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('53', '', '0', 'Жареная картошечка с грибами', 'Вкус знакомый с детства, настоящая картошка с грибами по домашнему.', '9', '2', '0', '0', '/data/images/positions/1403777736_14e2e8157345fb67f3c3abbca2563b00.jpeg', '0', '', '0', '7', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('54', '', '0', 'Домашние котлеты с курицей', 'Домашние котлеты подаются с картофелем и соусом.', '9', '3', '0', '0', '/data/images/positions/1403777806_fbe1fa3d940d51939a8037f9a9ee4641.jpeg', '0', '', '0', '7', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('55', '', '0', 'Ао чиз', 'крем-суп из голубого сыра, с креветками', '10', '0', '0', '0', '/data/images/positions/1403778356_4eb753196945f76f362eb00e77b04036.jpeg', '0', '', '0', '8', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('56', '', '0', 'Янагава набэ', 'набэ из копченого угря', '10', '0', '0', '0', '/data/images/positions/1403778489_21cb40c0718c77d529d29825a946a7fb.jpeg', '0', '', '0', '8', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('57', '', '0', 'Том Ям набэ', 'традиционный кокосовый суп с креветками и грибами. Сервируется рисом \"Гохан\"', '10', '0', '0', '0', '/data/images/positions/1403778545_dfa04c8886ee9729a5378a8063be92d8.jpeg', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('58', '', '0', 'Нику рамэн', 'суп-лапша со свининой и яйцом', '10', '0', '0', '0', '/data/images/positions/1403778650_c0fdcdd3a8f8f4ff71504a7e34659070.jpeg', '0', '', '0', '8', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('59', '', '0', 'Борщ', '', '12', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('60', '', '1', 'Бизнес ланч 1', '', '12', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('63', '', '0', 'Борщ', '', '14', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('64', '', '1', 'Лайт', '', '13', '0', '0', '0', '', '0', '', '0', '', '1', '1', '134', '0');
INSERT INTO `app_position` VALUES ('65', '', '0', 'рпг', '', '13', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0');
INSERT INTO `app_position` VALUES ('66', '', '0', '345345', '', '18', '0', '0', '0', '', '0', '', '0', '', '1', '1', '1', '1');
INSERT INTO `app_position` VALUES ('67', '', '1', '123', '', '13', '0', '0', '0', '', '0', '', '0', '', '1', '0', '123', '5');
INSERT INTO `app_position` VALUES ('142', '1000019', '0', 'Кола', '', '44', '0', '0', '0', '', '0', '', '0', '43,44,45,46', '1', '1', '2', '0');
INSERT INTO `app_position` VALUES ('143', '1000020', '0', 'Фанта', '', '44', '1', '0', '0', '', '0', '', '0', '43,44,45,46', '1', '1', '3', '0');
INSERT INTO `app_position` VALUES ('144', '1000064', '0', 'Бизнес-ланч', '', '45', '2', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('145', '1000074', '0', 'Ланч 3 позиции', '', '46', '3', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('146', '1000079', '0', 'Овощной', '', '47', '4', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('147', '1000080', '0', 'Мясной', '', '47', '5', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('148', '1000081', '0', 'Рыбный', '', '47', '6', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('149', '1000083', '0', 'Борщ', '', '48', '7', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('150', '1000084', '0', 'Уха', '', '48', '8', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('151', '1000085', '0', 'Харчо', '', '48', '9', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('152', '1000087', '0', 'Молочный коктель', '', '49', '10', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('153', '1000088', '0', 'Морс', '', '49', '11', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('154', '1000089', '0', 'Чай', '', '49', '12', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('155', '1000105', '0', 'Мороженое', '', '50', '13', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('156', '1000110', '0', 'Тирамису', '', '50', '14', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('157', '1000111', '0', 'Медовик', '', '50', '15', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('158', '1000113', '0', 'Греческий', '', '51', '16', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('159', '1000114', '0', 'Цезарь', '', '51', '17', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('160', '1000175', '0', 'Пиво', '', '44', '18', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('161', '1000190', '0', 'Ланч 4 позиции', '', '46', '19', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('162', '1000192', '0', 'Котлеты по киевски', '', '52', '20', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('163', '1000193', '0', 'Рагу из овощей', '', '52', '21', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('164', '1000208', '0', '\"Медальон\"', '', '53', '22', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('165', '1000209', '0', 'Мясо \"По- мексикански\"', '', '53', '23', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('166', '1000210', '0', 'Шашлык из свинины', '', '53', '24', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('167', '1000211', '0', 'Говядина «Фламбэ» с белыми грибами', '', '53', '25', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('168', '1000212', '0', 'Стейк говяжий на решетке', '', '53', '26', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('169', '1000213', '0', '\"Капрезе\"', '', '51', '27', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('170', '1000214', '0', '\"Экзотика\"', '', '51', '28', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('171', '1000215', '0', '\"Парма ди Руккола\"', '', '51', '29', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('172', '1000216', '0', 'Штрудель яблочный', '', '50', '30', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('173', '1000217', '0', 'Салат \"Фруктовый\"', '', '50', '31', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('174', '1000218', '0', 'Соки \"RICH\" в ассортименте 200 мл.', '', '44', '32', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('175', '1000219', '0', 'Брусничный морс', '', '44', '33', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('176', '1000220', '0', 'Акваминерале', '', '44', '34', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('177', '1000222', '0', 'Борщ \"Московский\"', '', '54', '35', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('178', '1000223', '0', 'Солянка по-домашнему', '', '54', '36', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('179', '1000224', '0', 'Уха ростовская', '', '54', '37', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('180', '1000225', '0', 'Крем-суп с белыми грибами', '', '54', '38', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('181', '1000226', '0', '\"Сицилия\"', '', '54', '39', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('182', '1000228', '0', 'Сыр в кляре', '', '55', '40', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('183', '1000229', '0', 'Крылышки куриные на углях', '', '55', '41', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('184', '1000230', '0', 'Креветки тигровые в кляре', '', '55', '42', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('185', '1000231', '0', 'Копченый сыр к пиву', '', '55', '43', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('186', '1000232', '0', 'Гренки острые с сыром', '', '55', '44', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('187', '1000234', '0', '\"Ахтамар\" 0,050', '', '56', '45', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('188', '1000235', '0', '\"Арарат\" 0,050', '', '56', '46', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('189', '1000236', '0', '\"Remy Martin\" V.S. 0,050', '', '56', '47', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('190', '1000237', '0', '\"Hennessy\" V.S. 0,050', '', '57', '48', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('191', '1000238', '0', '\"Праздничный\" 0,050', '', '56', '49', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('192', '1000240', '0', '\"Miller\" 0,33', '', '58', '50', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('193', '1000241', '0', '\"Corona Extra\" 0,33', '', '58', '51', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('194', '1000242', '0', '\"Beck\'s\" безалкогольное 0,33', '', '58', '52', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('195', '1000243', '0', '\"Hoegaarden\" 0,5', '', '58', '53', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('196', '1000244', '0', 'Сибирская Корона 0,5', '', '58', '54', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('197', '1000305', '0', 'Новый напиток', '', '44', '55', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('198', '1000306', '0', 'Новый напиток', '', '44', '56', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('199', '1000347', '0', 'модикомбо', '', '59', '57', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('200', '1000391', '0', '111', '', '59', '58', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('201', '1000404', '0', 'вкусняшка', '', '54', '59', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('202', '1000469', '0', '111', '', '60', '60', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('203', '1000475', '0', 'приветик', '', '56', '61', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('204', '1000482', '0', 'хахаха', '', '56', '62', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('205', '1000483', '0', 'xxx', '', '56', '63', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('206', '1000484', '0', 'куку черновик', '', '56', '64', '0', '0', '', '0', '', '1', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('207', '1000493', '0', 'комбо', '', '61', '65', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('208', '1000503', '0', 'Фокус тест', '', '62', '66', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('209', '1000520', '0', 'Новый десерт', '', '50', '67', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('210', '1000521', '0', 'Блюдо1', '', '61', '68', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');
INSERT INTO `app_position` VALUES ('211', '1000529', '0', 'Блюдо2', '', '61', '69', '0', '0', '', '0', '', '0', '', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for `app_position_groups`
-- ----------------------------
DROP TABLE IF EXISTS `app_position_groups`;
CREATE TABLE `app_position_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_position_groups
-- ----------------------------
INSERT INTO `app_position_groups` VALUES ('1', '', '1', 'Первое');
INSERT INTO `app_position_groups` VALUES ('2', '', '1', 'Второе');
INSERT INTO `app_position_groups` VALUES ('3', '', '1', 'Десерт');

-- ----------------------------
-- Table structure for `app_restaurant`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant`;
CREATE TABLE `app_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `working_hours` varchar(255) NOT NULL,
  `map` varchar(500) NOT NULL,
  `photos` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant
-- ----------------------------
INSERT INTO `app_restaurant` VALUES ('3', '', '0', 'Борисовские пруды', 'г. Москва, ул Борисовские Пруды, д 10', '0', '1', 'Лучший ресторан', '+7 (555) 100-02-30', 'Пн-Пт 10:00-23:00', '{\"zoom\":11,\"coords\":{\"x\":55.635869,\"y\":37.740974},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000047100\",\"name\":\"Борисовские Пруды\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000004710001\",\"name\":\"10\",\"zip\":115211,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45296557000\",\"contentType\":\"building\"}}}', '[\"\\/data\\/images\\/restaurants\\/1406195747_0cf8570408c51a95107b73e5d6844e8b.jpeg\"]');
INSERT INTO `app_restaurant` VALUES ('4', '', '0', 'Боровское шоссе, д.31', 'г. Москва, ш. Боровское, д. 31', '0', '1', '', '+7 (111) 222-33-44', '', '{\"zoom\":13,\"coords\":{\"x\":55.642453,\"y\":37.361517},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000089000\",\"name\":\"Боровское\",\"zip\":null,\"type\":\"Шоссе\",\"typeShort\":\"ш\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000008900030\",\"name\":\"31\",\"zip\":119633,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45268577000\",\"contentType\":\"building\"}}}', '');
INSERT INTO `app_restaurant` VALUES ('6', '', '0', 'ул. Большая Академическая, д.65', 'г. Москва, ул. Академическая Б., д. 65', '0', '1', 'Посадочных мест: 198, Парковка: Есть, Wi-fi: Free, Кальянная карта: Есть', '+7 (499) 153-81-44', 'Пн-Чт (11:30 – 00:00), Пт-Сб (11:30 – 06:00), Вс.11:30 – 00:00', '{\"zoom\":11,\"coords\":{\"x\":55.840371,\"y\":37.547477},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000072300\",\"name\":\"Академическая Б.\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000007230016\",\"name\":\"65\",\"zip\":125183,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45277580000\",\"contentType\":\"building\"}}}', '');
INSERT INTO `app_restaurant` VALUES ('7', '', '0', 'шоссе Энтузиастов, 20', 'шоссе Энтузиастов, 20', '0', '2', 'Wi-Fi', '+7(495) 362-27-07', '11:00-00:00 ежедневно (до 6:00 только по праздникам), 11:00-16:00 ланч', '', '');
INSERT INTO `app_restaurant` VALUES ('8', '', '0', 'Авиамоторная ул., 41', 'Авиамоторная ул., 41', '0', '3', 'Одно из лучших кафе нашей сети,расположенное в 1-ой минуте ходьбы от метро, с курящим и отдельным некурящим залами. На летний период — веранда на крыше.', '+7 (495) 514-02-70', 'с 10:00 до 06:00 .бронирование: с 10:00 до 12:00  (только на текущий день, в Пт, Сб, Вс, а так же праздничные дни резервы не принимаются )', '', '');
INSERT INTO `app_restaurant` VALUES ('9', '', '0', 'Лениградка', 'Лениградка', '0', '6', '', '', '', '', '');
INSERT INTO `app_restaurant` VALUES ('10', '', '0', 'какой-то адрес', 'какой-то адрес', '0', '7', '', '', '', '', '');
INSERT INTO `app_restaurant` VALUES ('11', '', '0', 'маяквока', 'маяквока', '0', '6', '', '', '', '', '');
INSERT INTO `app_restaurant` VALUES ('12', '', '0', '6666', 'г. Москва, ул. 8 Марта 4-я, д. 5', '0', '4', '', '+7 (666) 666-66-66', '', '{\"zoom\":11,\"coords\":{\"x\":55.804218,\"y\":37.548887},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000103000\",\"name\":\"8 Марта 4-я\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45277553000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000010300002\",\"name\":\"5\",\"zip\":125319,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45277553000\",\"contentType\":\"building\"}}}', '');
INSERT INTO `app_restaurant` VALUES ('13', '', '0', '44', 'г. Москва, ул. 8 Марта 4-я, д. 4к1', '0', '4', '', '+7 (444) 444-44-44', '', '{\"zoom\":11,\"coords\":{\"x\":55.803398,\"y\":37.548896},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000103000\",\"name\":\"8 Марта 4-я\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45277553000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000010300001\",\"name\":\"4к1\",\"zip\":125167,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45277553000\",\"contentType\":\"building\"}}}', '');
INSERT INTO `app_restaurant` VALUES ('43', '1', '0', 'Наименование Ресторана', '', '0', '9', '', '+7 (586) 854-52-65', '', '{\"zoom\":11,\"coords\":{\"x\":55.71767,\"y\":37.449201},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000149800\",\"name\":\"Козлова\",\"zip\":\"121357\",\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45268569000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000014980001\",\"name\":\"29стр1\",\"zip\":121357,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45268569000\",\"contentType\":\"building\"}}}', '');
INSERT INTO `app_restaurant` VALUES ('44', '2', '0', 'Центральный Офис', '', '0', '9', '', '', '', '', '');
INSERT INTO `app_restaurant` VALUES ('45', '1000010', '0', 'RAV', '', '0', '9', '', '', '', '', '');
INSERT INTO `app_restaurant` VALUES ('46', '1000506', '1', 'Центральный офис', '', '0', '9', '', '', '', '', '');

-- ----------------------------
-- Table structure for `app_restaurant_plan`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant_plan`;
CREATE TABLE `app_restaurant_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant_plan
-- ----------------------------
INSERT INTO `app_restaurant_plan` VALUES ('3', '1000015', '1', '45', '0', '0');

-- ----------------------------
-- Table structure for `app_restaurant_table`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant_table`;
CREATE TABLE `app_restaurant_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant_table
-- ----------------------------
INSERT INTO `app_restaurant_table` VALUES ('8', '', '', '44', '0', '13', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('9', '', '', '44', '0', '13', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('10', '', '', '66', '0', '13', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('11', '', '', '1', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('12', '', '', '2', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('13', '', '', '3', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('14', '', '', '4', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('15', '', '', '5', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('16', '', '', '6', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('17', '', '', '7', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('18', '', '', '8', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('19', '', '', '9', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('20', '', '', '10', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('21', '', '', '11', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('22', '', '', '12', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('23', '', '', '13', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('24', '', '', '14', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('25', '', '', '15', '0', '3', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('26', '', '', '1', '0', '4', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('27', '', '', '2', '0', '4', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('28', '1000017', '2', '2', '3', '45', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('29', '1000027', '3', '3', '3', '45', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('30', '1000028', '1', '1', '3', '45', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('31', '1000132', '6', 'Pool1', '3', '45', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('32', '1000206', '4', 'Игра', '3', '45', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('33', '1000328', '111', 'Д-ка', '3', '45', '0', '0');

-- ----------------------------
-- Table structure for `app_restaurant_tools`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant_tools`;
CREATE TABLE `app_restaurant_tools` (
  `restaurant_id` int(11) NOT NULL,
  `cash_id` int(11) NOT NULL,
  PRIMARY KEY (`restaurant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant_tools
-- ----------------------------
INSERT INTO `app_restaurant_tools` VALUES ('45', '1');

-- ----------------------------
-- Table structure for `app_rkeeper_proxy`
-- ----------------------------
DROP TABLE IF EXISTS `app_rkeeper_proxy`;
CREATE TABLE `app_rkeeper_proxy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `proxy_uuid` varchar(255) NOT NULL,
  `proxy_type` varchar(255) NOT NULL,
  `proxy_name` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `alive_interval` int(11) NOT NULL,
  `last_request_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_rkeeper_proxy
-- ----------------------------

-- ----------------------------
-- Table structure for `app_rkeeper_queue`
-- ----------------------------
DROP TABLE IF EXISTS `app_rkeeper_queue`;
CREATE TABLE `app_rkeeper_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `proxy_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `body` longtext NOT NULL,
  `response` longtext NOT NULL,
  `create_time` int(11) NOT NULL,
  `request_time` int(11) NOT NULL,
  `response_time` int(11) NOT NULL,
  `error_description` varchar(255) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_rkeeper_queue
-- ----------------------------

-- ----------------------------
-- Table structure for `app_sale_price`
-- ----------------------------
DROP TABLE IF EXISTS `app_sale_price`;
CREATE TABLE `app_sale_price` (
  `condition_id` int(11) NOT NULL,
  `date_on` int(11) NOT NULL,
  `date_off` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_sale_price
-- ----------------------------

-- ----------------------------
-- Table structure for `app_user`
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `restaurant_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('1', '', '', 'ssa', 'e10adc3949ba59abbe56e057f20f883e', 'birlaver@gmail.com', 'Eugene Lepeshko', 'admin', '0', '0', '0');
INSERT INTO `app_user` VALUES ('2', '', '', 'tanuki_owner', 'e10adc3949ba59abbe56e057f20f883e', 'ssa@bistroapp.ru', 'Владелец Тануки', 'owner', '0', '0', '1');
INSERT INTO `app_user` VALUES ('3', '', '', 'tanuki_manager', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_manager@bistroapp.ru', 'Администратор Тануки', 'manager', '0', '3', '1');
INSERT INTO `app_user` VALUES ('4', '', '', 'tanuki_cook', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_cook@bistroapp.ru', 'Повар Тануки', 'cook', '0', '3', '1');
INSERT INTO `app_user` VALUES ('5', '', '', 'tanuki_garcon', 'e10adc3949ba59abbe56e057f20f883e', 'kalin@abcwww.ru', 'Оффициант Тануки', 'garcon', '0', '3', '1');
INSERT INTO `app_user` VALUES ('6', '', '', 'tanuki_barman', 'e10adc3949ba59abbe56e057f20f883e', 'lunatik_37@mail.ru', 'Бармен', 'barman', '0', '3', '1');
INSERT INTO `app_user` VALUES ('7', '', '', 'tanuki_wer', 'e10adc3949ba59abbe56e057f20f883e', 'wer@bistroapp.ru', 'wer', 'barman', '0', '3', '1');
INSERT INTO `app_user` VALUES ('8', '', '', 'tanuki_ert', 'e10adc3949ba59abbe56e057f20f883e', 'ert@bistroapp.ru', 'ert', 'manager', '0', '7', '2');
INSERT INTO `app_user` VALUES ('9', '', '', 'tanuki_1234323', 'e10adc3949ba59abbe56e057f20f883e', 'abc-coder@mail.ru', '6324234', 'garcon', '0', '3', '1');
INSERT INTO `app_user` VALUES ('10', '', '', 'tanuki_234243', 'e10adc3949ba59abbe56e057f20f883e', 'ssa23423@ewrwer.ui', '234234234', 'garcon', '0', '3', '1');
INSERT INTO `app_user` VALUES ('11', '', '', 'rkeeper_owner', '202cb962ac59075b964b07152d234b70', 'rkeeper_owner@bistroapp.ru', 'Владелец компании', 'owner', '0', '0', '9');
INSERT INTO `app_user` VALUES ('12', '1', '7', 'rkeeper_user1', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1@bistroapp.ru', 'Администратор', 'garcon', '1', '45', '9');
INSERT INTO `app_user` VALUES ('13', '9001', '9001', 'rkeeper_user9001', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9001@bistroapp.ru', 'Система', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('14', '9002', '9002', 'rkeeper_user9002', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9002@bistroapp.ru', '777', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('15', '9003', '9003', 'rkeeper_user9003', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9003@bistroapp.ru', 'Контроль Кодов', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('16', '9004', '9004', 'rkeeper_user9004', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9004@bistroapp.ru', 'RK6 Импорт', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('17', '9005', '9005', 'rkeeper_user9005', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9005@bistroapp.ru', 'Веб-Резервирование', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('18', '1000002', '2', 'rkeeper_user1000002', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000002@bistroapp.ru', 'Tester', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('19', '1000003', '3', 'rkeeper_user1000003', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000003@bistroapp.ru', 'Крылов', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('20', '1000005', '4', 'rkeeper_user1000005', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000005@bistroapp.ru', 'Официант №2', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('21', '1000006', '5', 'rkeeper_user1000006', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000006@bistroapp.ru', 'Официант №1', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('22', '1000007', '6', 'rkeeper_user1000007', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000007@bistroapp.ru', 'Кассир №1', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('23', '1000008', '8', 'rkeeper_user1000008', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000008@bistroapp.ru', 'Кассир №2', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('24', '1000009', '12', 'rkeeper_user1000009', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000009@bistroapp.ru', 'Спец работник', 'garcon', '1', '0', '9');

-- ----------------------------
-- Table structure for `app_user_table`
-- ----------------------------
DROP TABLE IF EXISTS `app_user_table`;
CREATE TABLE `app_user_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_user_table
-- ----------------------------
INSERT INTO `app_user_table` VALUES ('6', '12', '9');
INSERT INTO `app_user_table` VALUES ('7', '14', '9');
INSERT INTO `app_user_table` VALUES ('8', '20', '9');
INSERT INTO `app_user_table` VALUES ('9', '24', '9');
