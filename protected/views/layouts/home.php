<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta charset="UTF-8">
    <base href="<?php echo Yii::app()->request->getBaseUrl(true); ?>">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jscrollpane.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.backstretch.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.placeholder.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.selectBox.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body class="homepage">
<div id="wrapper">
    <div id="home_header">
        <div class="center">
            <?php $this->renderPartial('/partials/_top_menu',array()); ?>
            <div class="header_device"></div>
            <a href="/" id="logo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo_home.png" width="160" height="157" alt=""></a>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery(window).resize(function(){
                $('#home_header').backstretch("<?php echo Yii::app()->request->baseUrl; ?>/images/header.jpg");
            });
            $('#home_header').backstretch("<?php echo Yii::app()->request->baseUrl; ?>/images/header.jpg");
        });
    </script>
    <section class="benefits_section">
        <div class="pattern_top"></div>
        <div class="pattern_bottom"></div>
        <div class="center">
            <ul class="benefits_list">
                <li>
                    <div class="thumb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/benefits1.png" width="121" height="121" alt=""></div>
                    <p>Приложение само определит ваши координаты и предложит ознакомиться с меню того ресторана, в котором вы находитесь.</p>
                </li>
                <li>
                    <div class="thumb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/benefits4.png" width="121" height="121" alt=""></div>
                    <p>Вы можете выбрать понравившиеся блюда и отправить заказ прямо со своего мобильного устройства.</p>
                </li>
                <li>
                    <div class="thumb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/benefits5.png" width="121" height="121" alt=""></div>
                    <p>Вы можете выбрать способ оплаты, и оплатить счет не дожидаясь официанта.</p>
                </li>
            </ul>
        </div>
    </section>
    <section class="home_gray">
        <div class="big_device">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/device.jpg" width="1040" height="473" alt="">
        </div>
        <div class="pattern_bottom gray"></div>
    </section>
    <section class="simple-easy">
        <div class="simple_inner">
            <div class="simple_text">
                <h2>ПРОСТО и ЛЕГКО</h2>
                <p>Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
            </div>
            <div class="simple_device"></div>
        </div>
    </section>
    <script type="text/javascript">
        $('.simple-easy').backstretch("<?php echo Yii::app()->request->baseUrl; ?>/images/simple-easy.jpg");
        jQuery(document).ready(function(){jQuery(window).resize(function(){
            $('.simple-easy').backstretch("<?php echo Yii::app()->request->baseUrl; ?>/images/simple-easy.jpg");
        });})
    </script>
    <section class="clients_section">
        <div class="pattern_top"></div>
        <div class="center">
            <h2>ГДЕ НАЙТИ</h2>
            <div class="clients_list_wrap">
                <ul class="clients_list">
                    <li><span><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/appstore.png" width="263" height="88"></a></span></li>
                    <li><span><a href="javascript:void(0);"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/google_play.png" width="272" height="88"></a></span></li>
                </ul>
            </div>
        </div>
    </section>
</div>
</body>
</html>
