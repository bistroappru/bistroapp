<?php

class DefaultController extends Controller
{
    public $layout = 'admin_main';
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions'=>array('test'),
                'roles'=>array('guest'),
            ),
            array(
                'deny',
                'roles'=>array('*'),
            ),
        );
    }

	public function actionIndex()
	{
        $this->render('index');
	}

    public function actionReferences()
    {
        $this->layout = 'admin_ref';
        $this->render('references');
    }

    public function actionBuildAuth(){
        $auth=Yii::app()->authManager;
        $auth->clearAll();
        // - описание операций RBAC ------------------------------------------------------------------------------------

        $auth->createOperation('inAdmin', 'Доступ к админке');

        // ------------------------   КОМПАНИИ  ------------------------------------------
        // операции
        $auth->createOperation('companyView', 'Просмотр компании');
        $auth->createOperation('companyCreate', 'Создание компании');
        $auth->createOperation('companyUpdate', 'Обновление компании');
        $auth->createOperation('companyDelete', 'Удаление компании');
        $auth->createOperation('companyTools', 'Настройки компании');

        // задачи (таски)
        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('companyOwnUpdate', 'Обновление своей компании', $bizRule);
        $task->addChild('companyUpdate');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('companyOwnView', 'Просмотр своей компании', $bizRule);
        $task->addChild('companyView');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('companyOwnTools', 'Настройки своей компании', $bizRule);
        $task->addChild('companyTools');

        // ------------------------  РЕСТОРАНЫ  ------------------------------------------
        $auth->createOperation('restaurantView', 'Просмотр ресторана');
        $auth->createOperation('restaurantCreate', 'Создание ресторана');
        $auth->createOperation('restaurantUpdate', 'Обновление ресторана');
        $auth->createOperation('restaurantDelete', 'Удаление ресторана');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('restaurantCompanyView', 'Просмотр ресторанов своей компании', $bizRule);
        $task->addChild('restaurantView');

        $bizRule='return Yii::app()->user->restaurant_id==$params["restaurant"]->id;';
        $task=$auth->createTask('restaurantOwnUpdate', 'Обновление своего ресторана', $bizRule);
        $task->addChild('restaurantUpdate');

        $bizRule='return Yii::app()->user->company_id==$params["restaurant"]->company_id;';
        $task=$auth->createTask('restaurantCompanyUpdate', 'Обновление ресторанов своей компании', $bizRule);
        $task->addChild('restaurantUpdate');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('restaurantCompanyCreate', 'Создание ресторанов в своей компании', $bizRule);
        $task->addChild('restaurantCreate');

        $bizRule='return Yii::app()->user->company_id==$params["restaurant"]->company_id;';
        $task=$auth->createTask('restaurantCompanyDelete', 'Удаление ресторанов в своей компании', $bizRule);
        $task->addChild('restaurantDelete');

        // ------------------------   МЕНЮ  ------------------------------------------
        // операции
        $auth->createOperation('menuCategoryView', 'Просмотр категории меню');
        $auth->createOperation('menuCategoryCreate', 'Создание категории меню');
        $auth->createOperation('menuCategoryUpdate', 'Обновление категории меню');
        $auth->createOperation('menuCategoryDelete', 'Удаление категории меню');

        // задачи (таски)
        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnCategoryView', 'Просмотр категории меню своей компании', $bizRule);
        $task->addChild('menuCategoryView');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnCategoryCreate', 'Создание категории меню своей компании', $bizRule);
        $task->addChild('menuCategoryCreate');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnCategoryUpdate', 'Обновление категории меню своей компании', $bizRule);
        $task->addChild('menuCategoryUpdate');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnCategoryDelete', 'Удаление категории меню своей компании', $bizRule);
        $task->addChild('menuCategoryDelete');

        // ------------------------   БЛЮДА  ------------------------------------------
        // операции
        $auth->createOperation('menuDishView', 'Просмотр блюда меню');
        $auth->createOperation('menuDishCreate', 'Создание блюда меню');
        $auth->createOperation('menuDishUpdate', 'Обновление блюда меню');
        $auth->createOperation('menuDishDelete', 'Удаление блюда меню');

        // задачи (таски)
        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnDishView', 'Просмотр блюда меню своей компании', $bizRule);
        $task->addChild('menuDishView');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnDishCreate', 'Создание блюда меню своей компании', $bizRule);
        $task->addChild('menuDishCreate');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnDishUpdate', 'Обновление блюда меню своей компании', $bizRule);
        $task->addChild('menuDishUpdate');

        $bizRule='return Yii::app()->user->company_id==$params["company_id"];';
        $task=$auth->createTask('menuOwnDishDelete', 'Удаление блюда меню своей компании', $bizRule);
        $task->addChild('menuDishDelete');


        // ------------------------   ПОЛЬЗОВАТЕЛИ  ------------------------------------------
        // операции
        $auth->createOperation('userView', 'Просмотр пользователя');
        $auth->createOperation('userCreate', 'Создание пользователя');
        $auth->createOperation('userUpdate', 'Обновление пользователя');
        $auth->createOperation('userDelete', 'Удаление пользователя');

        // задачи (таски)
        $bizRule='return Yii::app()->user->company_id==$params["user"]->company_id;';
        $task=$auth->createTask('userOwnCompanyView', 'Просмотр пользователя своей компании', $bizRule);
        $task->addChild('userView');

        $bizRule='return Yii::app()->user->company_id==$params["user"]->company_id;';
        $task=$auth->createTask('userOwnCompanyCreate', 'Создание пользователя своей компании', $bizRule);
        $task->addChild('userCreate');

        $bizRule='return Yii::app()->user->company_id==$params["user"]->company_id;';
        $task=$auth->createTask('userOwnCompanyUpdate', 'Редактирование пользователя своей компании', $bizRule);
        $task->addChild('userUpdate');

        $bizRule='return Yii::app()->user->company_id==$params["user"]->company_id;';
        $task=$auth->createTask('userOwnCompanyDelete', 'Удаление пользователя своей компании', $bizRule);
        $task->addChild('userDelete');

        $bizRule='return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;';
        $task=$auth->createTask('userOwnRestaurantView', 'Просмотр пользователя своего ресторана', $bizRule);
        $task->addChild('userView');

        $bizRule='return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;';
        $task=$auth->createTask('userOwnRestaurantCreate', 'Создание пользователя своего ресторана', $bizRule);
        $task->addChild('userCreate');

        $bizRule='return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;';
        $task=$auth->createTask('userOwnRestaurantUpdate', 'Редактирование пользователя своего ресторана', $bizRule);
        $task->addChild('userUpdate');

        $bizRule='return Yii::app()->user->restaurant_id==$params["user"]->restaurant_id;';
        $task=$auth->createTask('userOwnRestaurantDelete', 'Удаление пользователя своего ресторана', $bizRule);
        $task->addChild('userDelete');


        // ------------------------  РОЛИ  ------------------------------------------
        $role=$auth->createRole(User::ROLE_GUEST, User::getRoles(User::ROLE_GUEST));

        $role=$auth->createRole(User::ROLE_MOBILE_GUEST, User::getRoles(User::ROLE_MOBILE_GUEST));

        $role=$auth->createRole(User::ROLE_MOBILE, User::getRoles(User::ROLE_MOBILE));
        $role->addChild(User::ROLE_MOBILE_GUEST);

        $role=$auth->createRole(User::ROLE_COOK, User::getRoles(User::ROLE_COOK));
        $role->addChild('inAdmin');

        $role=$auth->createRole(User::ROLE_BARMAN, User::getRoles(User::ROLE_BARMAN));
        $role->addChild('inAdmin');

        $role=$auth->createRole(User::ROLE_GARCON, User::getRoles(User::ROLE_GARCON));
        $role->addChild('inAdmin');

        $role=$auth->createRole(User::ROLE_MANAGER, User::getRoles(User::ROLE_MANAGER));
        $role->addChild(User::ROLE_COOK);
        $role->addChild(User::ROLE_BARMAN);
        $role->addChild(User::ROLE_GARCON);
        $role->addChild('companyOwnView');
        $role->addChild('restaurantCompanyView');
        $role->addChild('restaurantOwnUpdate');
        $role->addChild('userOwnRestaurantCreate');
        $role->addChild('userOwnRestaurantView');
        $role->addChild('userOwnRestaurantUpdate');
        $role->addChild('userOwnRestaurantDelete');

        $role=$auth->createRole(User::ROLE_OWNER, User::getRoles(User::ROLE_OWNER));
        $role->addChild(User::ROLE_MANAGER);
        $role->addChild('companyOwnUpdate');
        $role->addChild('companyOwnTools');
        $role->addChild('restaurantCompanyUpdate');
        $role->addChild('restaurantCompanyCreate');
        $role->addChild('restaurantCompanyDelete');
        $role->addChild('menuOwnCategoryCreate');
        $role->addChild('menuOwnCategoryView');
        $role->addChild('menuOwnCategoryUpdate');
        $role->addChild('menuOwnCategoryDelete');
        $role->addChild('menuOwnDishCreate');
        $role->addChild('menuOwnDishView');
        $role->addChild('menuOwnDishUpdate');
        $role->addChild('menuOwnDishDelete');
        $role->addChild('userOwnCompanyCreate');
        $role->addChild('userOwnCompanyView');
        $role->addChild('userOwnCompanyUpdate');
        $role->addChild('userOwnCompanyDelete');

        $role=$auth->createRole(User::ROLE_ADMIN, User::getRoles(User::ROLE_ADMIN));
        $role->addChild(User::ROLE_OWNER);
        $role->addChild('companyCreate');
        $role->addChild('companyView');
        $role->addChild('companyUpdate');
        $role->addChild('companyDelete');
        $role->addChild('companyTools');
        $role->addChild('restaurantCreate');
        $role->addChild('restaurantView');
        $role->addChild('restaurantUpdate');
        $role->addChild('restaurantDelete');
        $role->addChild('menuCategoryCreate');
        $role->addChild('menuCategoryView');
        $role->addChild('menuCategoryUpdate');
        $role->addChild('menuCategoryDelete');
        $role->addChild('menuDishCreate');
        $role->addChild('menuDishView');
        $role->addChild('menuDishUpdate');
        $role->addChild('menuDishDelete');
        $role->addChild('userCreate');
        $role->addChild('userView');
        $role->addChild('userUpdate');
        $role->addChild('userDelete');

        // - конец описания RBAC ---------------------------------------------------------------------------------------
        $auth->save();
        echo 'all OK!';
    }

}