<?php

/**
 * This is the model class for table "{{restaurant_plan}}".
 *
 * The followings are the available columns in table '{{restaurant_plan}}':
 * @property integer $id
 * @property string $out_id
 * @property string $name
 * @property integer $restaurant_id
 */
class RestaurantPlan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{restaurant_plan}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('restaurant_id', 'numerical', 'integerOnly'=>true),
			array('out_id, name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, name, restaurant_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'restaurant' => array(self::BELONGS_TO, 'Restaurant', 'restaurant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'out_id' => 'Out',
			'name' => 'Name',
			'restaurant_id' => 'Restaurant',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('out_id',$this->out_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('restaurant_id',$this->restaurant_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RestaurantPlan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave()
    {
        parent::afterSave();
        if ($this->restaurant_id){
            RestaurantTable::model()->updateAll(array('restaurant_id' => $this->restaurant_id), 'plan_id = :plan_id', array(':plan_id' => $this->id));
        }
    }

    // ---------    CUSTOM OBJECT METHODS   ----------

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function getIdByOutId($out_id, $create = false, $data = array()){
        $model = RestaurantPlan::model()->findByAttributes(array('out_id' => $out_id));
        if ($model === null && $create){
            $model = new RestaurantPlan();
            $model->out_id = $out_id;
            $model->setAttributes($data);
            if ($model->save(false))
                return $model->id;
        }elseif(is_object($model)){
            return $model->id;
        }
        return null;
    }

}
