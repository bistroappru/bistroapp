<?php

/**
 * This is the model class for table "{{order}}".
 *
 * The followings are the available columns in table '{{order}}':
 * @property integer $id
 * @property string $out_id
 * @property string $out_visit
 * @property integer $device_id
 * @property integer $restaurant_id
 * @property integer $table_id
 * @property string $table_text
 * @property string $guests
 * @property integer $status_id
 * @property integer $date_create
 * @property integer $payment_id
 * @property integer $attention
 * @property integer $separated
 * @property string $separated_info
 * @property integer $date_execute
 * @property integer $date_close
 * @property string $comment
 * @property integer $count_items
 * @property float $total_price
 * @property integer $garcon_id
 * @property integer $version
 */
class Order extends CActiveRecord
{
    const STATUS_ERROR = 0; // ошибка создания заказа
    const STATUS_CREATE = 1; // создан из приложения
    const STATUS_ADOPTED = 2; // успешно передан в keeper || принят в обработку на сайте
    const STATUS_PAID = 4; // оплачен
    const STATUS_CANCELED = 5; // отменен
    const STATUS_CLOSED = 6; // закрыт

    public $positions = array();
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('device_id, status_id, date_create, payment_id, restaurant_id, separated', 'required'),
			array('device_id, table_id, status_id, date_create, payment_id, restaurant_id, attention, separated, date_execute, date_close', 'numerical', 'integerOnly'=>true),
            array('out_id, table_text, guests, out_visit', 'length', 'max'=>255),
            array('restaurant_id', 'issetRestaurant'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, device_id, table_id, table_text, guests, status_id, date_create, payment_id, restaurant_id, garcon_id, attention, separated, separated_info, date_execute, date_close, comment, price_total, version, count_items', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'items' => array(self::HAS_MANY, 'OrderItem', 'order_id'),
            'status' => array(self::BELONGS_TO, 'OrderStatus', 'status_id'),
            'payment' => array(self::BELONGS_TO, 'OrderPayment', 'payment_id'),
            'restaurant' => array(self::BELONGS_TO, 'Restaurant', 'restaurant_id'),
            'garcon' => array(self::BELONGS_TO, 'User', 'garcon_id'),
            'table' => array(self::BELONGS_TO, 'RestaurantTable', 'table_id'),
            'device' => array(self::BELONGS_TO, 'Device', 'device_id'),
		);
	}

    public function issetRestaurant($attribute, $params){
        $model = Restaurant::model()->findByPk($this->$attribute);
        if ($model === null){
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} с идентификатором {$this->$attribute} не существует");
        }
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'device_id' => 'Мобильное устройство',
			'table_id' => 'Столик',
            'table_text' => 'Столик (текст)',
            'guests' => 'Гости',
			'status_id' => 'Статус заказа',
			'date_create' => 'Дата создания',
			'payment_id' => 'Тип оплаты',
            'restaurant_id' => 'Ресторан (ID)',
			'attention' => 'Внимание оффицианта',
			'separated' => 'Раздельный заказ',
			'separated_info' => 'Информация раздельного заказа',
            'date_execute' => 'Дата принятия',
            'date_close' => 'Дата закрытия',
            'comment' => 'Комментарий',
            'total_price' => 'Сумма заказа',
            'count_items' => 'Количество позиций',
            'garcon_id' => 'Оффициант',
            'version' => 'Версия',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('table_id',$this->table_id);
        $criteria->compare('table_text',$this->table_text);
        $criteria->compare('guests',$this->guests);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('date_create',$this->date_create);
		$criteria->compare('payment_id',$this->payment_id);
        $criteria->compare('restaurant_id',$this->restaurant_id);
        $criteria->compare('attention',$this->attention);
		$criteria->compare('separated',$this->separated);
		$criteria->compare('separated_info',$this->separated_info, true);
		$criteria->compare('date_execute',$this->date_execute);
		$criteria->compare('date_close',$this->date_close);
        $criteria->compare('comment',$this->comment);
        $criteria->compare('garcon_id',$this->garcon_id);
        $criteria->compare('total_price',$this->total_price);
        $criteria->compare('count_items',$this->count_items);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave(){
        parent::afterSave();
        foreach($this->positions as $item){
            $item->order_id = $this->id;
            $item->save();
        }
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        OrderItem::model()->deleteAll('order_id = '.$this->id);
    }

// ============================================================================================
// ВАЛИДАЦИОННЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================


// ============================================================================================
// ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================
    public function isFinishStatus(){
        return in_array($this->status_id, array(Order::STATUS_ERROR, Order::STATUS_PAID, Order::STATUS_CANCELED, Order::STATUS_CLOSED));
    }

    public function isOutOrder(){
        return !empty($this->out_id);
    }

    public function calculateTotalPrice(){
        $this->total_price = 0;
        foreach($this->items as $item){
            if ($item->deleted) continue;
            $this->total_price += $item->calcPrice();
        }
        return $this->save();
    }

    public function calculateTotalCount(){
        $this->count_items = 0;
        foreach($this->items as $item){
            if ($item->deleted) continue;
            $this->count_items += $item->calcCount();
        }
        return $this->save();
    }

    public function getNotificationEmails(){
        //$emails = array('a.evtyushin@gmail.com', 'aksyaitov@gmail.com', 'aksyaitov@mail.ru', 'abc-coder@mail.ru');
        $emails = array('a.evtyushin@gmail.com', 'abc-coder@mail.ru');
        //$emails = array('abc-coder@mail.ru');

        $validator = new CEmailValidator;

        // оффициант
        if ($this->garcon_id && $validator->validateValue($this->garcon->email)){
            $emails[] = $this->garcon->email;
        }

        // администраторы
        $admin = User::model()->findAllByAttributes(array(
            'role'=> User::ROLE_MANAGER,
            'restaurant_id' => $this->restaurant_id
        ));
        foreach($admin as $person){
            if ($person->email && $validator->validateValue($person->email)) {
                $emails[] = $person->email;
            }
        }
        return $emails;
    }

    public function getNotificationPhones(){
        $numbers = array();
        // оффициант
        if ($this->garcon_id && $this->garcon->phone){
            $numbers[] = str_replace(array('+',' ','(',')','-'), '', $this->garcon->phone);
        }
        return $numbers;
    }

    public function sendEmail($type){
        try {
            $mailer = Yii::app()->mailer;
            foreach($this->getNotificationEmails() as $email){
                if ($email)
                    $mailer->AddAddress($email);
            }

            switch ($type){
                case 'adopt_rk':
                    $subject = 'Новый заказ (#'.$this->id.')';
                    $tpl = '_mail_new_order';
                break;
                case 'update_rk':
                    $subject = 'Заказ (#'.$this->id.') обновлен';
                    $tpl = '_mail_update_order';
                break;
            }

            $mailer->Subject = $subject;
            $mailer->Body = Yii::app()->controller->renderPartial('application.views.email.'.$tpl, array('model' => $this), true);
            $mailer->Send();

        } catch (Exception $e) {
            // @TODO логируем ошибку
        }
    }

    public function createOutOrder(){
        $type = $this->getProxyType();

        if ($type == RKeeperProxy::TYPE_RK6){
            RKeeperQueue::createRKeeper6Order($this);
        }else{
            RKeeperQueue::createRKeeper7Order($this);
        }
    }

    public function addOutOrderPosition($positions){
        $type = $this->getProxyType();

        if ($type == RKeeperProxy::TYPE_RK6){
            RKeeperQueue::addPositionRKeeper6Order($this, $positions);
        }else{
            RKeeperQueue::addPositionRKeeper7Order($this, $positions);
        }
    }

    public function sendPush($type){
        $device = Device::model()->findByPk($this->device_id);

        if (!$device->push_token) return;

        $data = null;
        $alert = null;

        switch ($type){
            case 'create':
                $alert = 'Ваш заказ зарегистрирован';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array();
                $data['message'] = 'Ваш заказ #'.$this->id.' успешно зарегистрирован на bistroApp.ru';
            break;
            case 'update':
                $alert = 'Обновление заказа зарегистрировано';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array();
                $data['message'] = 'Обновление заказа #'.$this->id.' успешно зарегистрировано на bistroApp.ru';
                break;
            case 'adopt_rk':
                $alert = 'Ваш заказ передан в ресторан';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array('show_notification', 'get_order');
                $data['message'] = 'Ваш заказ #'.$this->id.' успешно передан в '.$this->restaurant->company->name;
            break;
            case 'update_rk':
                $alert = 'Ваш заказ обновлен';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array('show_notification', 'get_order');
                $data['message'] = 'Ваш заказ #'.$this->id.' успешно обновлен';
            break;
            case 'fail_update_rk':
                $alert = 'Изменение заказа отменено';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array('show_notification');
                $data['message'] = 'Не удалось внести изменения в заказ #'.$this->id.', сервер ресторана не доступен';
                break;
            case 'cancel':
                $alert = 'Ваш заказ отменен';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array('show_notification', 'get_order');
                $data['message'] = 'Ваш заказ #'.$this->id.' был отменен, сервер ресторана не доступен';
            break;
            case 'pay_rk':
                $alert = 'Ваш заказ был оплачен';
                $data['data']['object_id'] = $this->id;
                $data['data']['actions'] = array('show_notification', 'get_order');
                $data['message'] = 'Ваш заказ #'.$this->id.' успешно оплачен в ресторане '.$this->restaurant->company->name;
            break;
        }
        if (!$alert) return;
        try {
            $apns = Yii::app()->apns;
            $result = $apns->send(str_replace(" ", "", $device->push_token), $alert, $data, array('sound' => 'default'));
        } catch (Exception $e) {
             // @TODO логируем ошибку   
        }
        return $result;
    }

    public function sendSMS($type, $comment = null){
        $msg = null;
        $from = 'BistroApp';
        switch ($type){
            case 'create':
                $msg = 'Создан новый заказ #'.$this->id;
            break;
            case 'update':
                $msg = 'Стол #'.$this->table->name.' обновил заказ #'.$this->table_text;
                break;
            case 'adopt_rk':
                $msg = 'Стол #'.$this->table->name.' сделал заказ #'.$this->table_text;
            break;
            case 'update_rk':
                $msg = 'Стол #'.$this->table->name.' обновил заказ #'.$this->table_text;
            break;
            case 'pay_rk':
                $msg = 'Заказ #'.$this->table_text.' полностью оплачен';
                break;
            case 'canPay':
                $msg = 'Стол #'.$this->table->name.' готов расплатиться ('.$this->payment->name.') за заказ #'.$this->table_text;
            break;
            case 'comeToTable':
                $msg = 'Стол #'.$this->table->name.' просит подойти официанта';
            break;
        }

        if (!$msg) return;

        if (!empty($comment)) $msg .= ' ('.$comment.')';

        foreach($this->getNotificationPhones() as $number){
            try {
                Yii::app()->sms->sms_send($number, $msg, $from, null, false);
            } catch (Exception $e) {
                // @TODO логируем ошибку
            }
        }

        // тест для Андрея
        try {
            Yii::app()->sms->sms_send('79851663398', '[test] '.$msg, $from, null, false);
        } catch (Exception $e) {
            // @TODO логируем ошибку
        }
    }


// ============================================================================================
// СТАТИЧЕСКИЕ МЕТОДЫ МОДЕЛИ
// ============================================================================================

    /**
     * получение индекса последней версии данных
     */
    public static function getLastVersion($device_id, $field = 'version'){
        $row = self::model()->find(array(
            'select' => $field,
            'condition' => 'device_id = :device_id',
            'params' => array(':device_id' => $device_id),
            'order' => "{$field} DESC",
            'limit' => 1
        ));
        return is_object($row) ? $row->$field : 0;
    }

    public static function getNextSubOrder($order_id){
        $row = OrderItem::model()->find(array(
            'select' => 'sub_order',
            'condition' => 'order_id = :order_id',
            'params' => array(':order_id' => $order_id),
            'order' => "sub_order DESC",
            'limit' => 1
        ));
        return is_object($row) ? $row->sub_order+1 : 1;
    }

    public static function updateTotalPrice($id=null){
        $order = self::model()->findByPk($id);
        if (!is_object($order)) return false;
        $tp = 0;
        foreach($order->items as $one){
            if ($one->deleted) continue;
            $tp += $one->price*$one->count;
        }
        $order->total_price = $tp;
        if ($order->save())
            return $tp;
        else
            return false;
    }

    public static function updateCountItems($id=null){
        $order = self::model()->findByPk($id);
        if (!is_object($order)) return false;
        $tc = 0;
        foreach($order->items as $one){
            if ($one->deleted) continue;
            $tc++;
        }

        $order->count_items = $tc;
        $order->save();
        if ($order->save())
            return $tc;
        else
            return false;
    }

    public function getTotalPrice(){
        $tp = 0;
        foreach($this->items as $one){
            $tp += $one->price*$one->count;
        }
        return $tp;
    }

    public function notificationMessages(){
        //$emails = array('a.evtyushin@gmail.com', 'aksyaitov@gmail.com', 'aksyaitov@mail.ru', 'abc-coder@mail.ru');
        $emails = array('abc-coder@mail.ru');

        // оффициант
        if ($this->garcon->email){
            $emails[] = $this->garcon->email;
        }

        // администраторы
        $admin = User::model()->findAllByAttributes(array(
            'role'=> User::ROLE_ADMIN,
            'restaurant_id' => $this->restaurant_id
        ));

        foreach($admin as $person){
            $emails[] = $person->email;
        }

        // генерация письма
        $mailer = Yii::app()->mailer;
        foreach($emails as $v){
            if (!$v) continue;
            $mailer->AddAddress($v);
        }
        $mailer->Subject = 'Новый заказ (#'.$this->id.')';
        $mailer->Body = Yii::app()->controller->renderPartial('application.views.email._mail_new_order', array('model' => $this), true);
        $mailer->Send();
    }

    // ---------    CUSTOM OBJECT METHODS   ----------
    public function getProxyId($type = null){
        $company = $this->restaurant->company;
        $attributes = array(
            'company_id' => $company->id
        );

        if ($type){
            $attributes['proxy_type'] = $type;
        }

        $proxy = RKeeperProxy::model()->findByAttributes($attributes, array('order' => 'id DESC'));
        if ($proxy === null)
            return;
        return $proxy->id;
    }

    public function getProxyType(){
        $company = $this->restaurant->company;
        $proxy = RKeeperProxy::model()->findByAttributes(array(
            'company_id' => $company->id,
        ),array('order' => 'id DESC'));

        if ($proxy === null)
            return;
        return $proxy->proxy_type;
    }

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------

    public static function parseKeeperData(RKeeperQueue $queue){
        $response = @simplexml_load_string($queue->response);

        if (!$response){
            $queue->setFail('Не удалось обработать xml-ответ');
            return;
        }

        if ((string) $response['Status'] !== 'Ok'){
            $queue->setFail('Статус должен быть: Ok');
            return;
        }

        if ((string) $response['CMD'] !== $queue->getCMD()){
            $queue->setFail('Команда запроса и ответа не совпадает');
            return;
        }

        if (!$response['OrderID'] || !$response['VisitID']){
            $queue->setFail('Значения OrderID, VisitID не корректны');
            return;
        }

        $data = array();
        $data['out_id'] = (string) $response['OrderID'];
        $data['out_visit'] = (string) $response['VisitID'];

        $response_data = unserialize($queue->data);
        $model = Order::model()->findByPk($response_data['order_id']);

        if ($model === null){
            $queue->setFail('Заказ с id = '.$response_data['order_id'].' не найден!');
            return;
        }

        $model->setAttributes($data);

        if (!$model->save(false)){
            $queue->setFail('Не удалось обновить заказ с id = ' . $model->id);
            return;
        }

        // TODO отправить PUSH что заказ создан в keeper

        // создать запрос на добавление позиций
        RKeeperQueue::createRKeeperOrderItems($model);

        $queue->setFinished();
    }

}
