<?php
/* @var $this PositionController */
/* @var $model Position */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'position-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'data-id' => $model->id,
        'data-type' => $model->isNewRecord ? 'create' : 'update'
    )
)); ?>
<?php echo $form->hiddenField($model, 'is_group'); ?>
<?php echo $form->hiddenField($model, 'hidden'); ?>
<?php echo $form->hiddenField($model, 'deleted'); ?>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('name')?></label>
        <div class="product_title inp_wrap">
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'required' => 'required')); ?>
            <a href="#" class="edit-link"></a>
            <?php echo $form->error($model, 'name'); ?>
        </div>
        <p class="orig_text"><?=$model->name_orig?></p>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('menu_id')?></label>
        <div class="product_title input_row">
            <?php echo $form->dropDownList($model, 'menu_id', Menu::getMenuList($model->menu->company_id));?>
        </div>
        <?php echo $form->error($model, 'menu_id'); ?>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('description')?></label>

        <div class="product_desc">
            <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
            <a href="#" class="edit-link"></a>
        </div>
        <div class="product_thumb">
            <a href="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>" class="fancybox_image">
                <img src="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>" width="130" height="130"/>
            </a>
            <a href="javascript:void(0);" class="refresh"></a>
        </div>
        <p class="orig_text"><?=$model->description_orig?></p>

        <?php echo $form->hiddenField($model, 'image'); ?>
        <?php echo $form->error($model, 'description'); ?>
        <div class="clear"></div>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('calories')?></label>
        <div class="product_title">
            <?php echo $form->textField($model, 'calories', array('size' => 50, 'maxlength' => 50)); ?>
            <a href="#" class="edit-link"></a>
        </div>
        <?php echo $form->error($model, 'calories'); ?>
    </div>

<?php
if (!$model->is_group) {
    ?>
    <div class="product_row">
        <?php echo $form->hiddenField($model, 'depressed'); ?>
        <div class="spec_checkbox">
            <input <?php if ($model->depressed) echo 'checked="checked"'; ?> type="checkbox" id="depressed"><label
                for="depressed"><?php echo $model->getAttributeLabel('depressed')?></label>

            <div class="checkbox_bg"></div>
        </div>
    </div>
<?php
}
?>


    <div class="product_row">
        <label class="product_label_big"><?php echo $model->getAttributeLabel('label_id')?></label>
        <div class="special_out">
            <?php
            foreach (Label::model()->findAll() as $v) {
                ?>
                <div class="spec_checkbox">
                    <input <?php if ($v->id == $model->label_id) echo 'checked="checked"'; ?> type="checkbox" id="special_<?php echo $v->id; ?>" value="<?php echo $v->id; ?>">
                    <label for="special_<?php echo $v->id; ?>"><?php echo $v->text; ?></label>
                    <div class="checkbox_bg"></div>
                </div>
            <?php
            }
            ?>
        </div>
        <?php echo $form->hiddenField($model, 'label_id'); ?>
    </div>

    <div class="product_row">
        <label class="product_label_big"><?php echo $model->getAttributeLabel('in_stock')?></label>
        <div class="instock">
            <?php
            $rests = Restaurant::getCompanyRestaurants(Yii::app()->user->company_id_full);
            $in_stock = explode(',', $model->in_stock);
            foreach ($rests as $k => $v) {
                ?>
                <div class="spec_checkbox">
                    <input <?php if (in_array($k, $in_stock) || $model->isNewRecord) echo 'checked="checked"'; ?>
                        type="checkbox" id="instock_<?php echo $k; ?>" value="<?php echo $k; ?>"><label
                        for="instock_<?php echo $k; ?>"><?php echo $v; ?></label>

                    <div class="checkbox_bg"></div>
                </div>
            <?php
            }
            ?>
        </div>
        <?php echo $form->hiddenField($model, 'in_stock'); ?>
    </div>

    <label class="product_label_big">Порции</label>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('price_mode')?></label>
        <div class="product_title input_row">
            <?php echo $form->dropDownList($model, 'price_mode', Position::getPriceModeList());?>
        </div>
        <?php echo $form->error($model, 'price_mode'); ?>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('price')?></label>
        <div class="product_title inp_wrap">
            <?php echo $form->textField($model, 'price', array('size' => 60, 'maxlength' => 255, 'required' => 'required')); ?>
            <a href="#" class="edit-link"></a>
            <?php echo $form->error($model, 'price'); ?>
        </div>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('measure')?></label>
        <div class="product_title inp_wrap">
            <?php echo $form->textField($model, 'measure', array('size' => 60, 'maxlength' => 255)); ?>
            <a href="#" class="edit-link"></a>
            <?php echo $form->error($model, 'measure'); ?>
        </div>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('portion_weight')?></label>
        <div class="product_title inp_wrap">
            <?php echo $form->textField($model, 'portion_weight', array('size' => 60, 'maxlength' => 255)); ?>
            <a href="#" class="edit-link"></a>
            <?php echo $form->error($model, 'portion_weight'); ?>
        </div>
    </div>

    <div class="product_row">
        <label class="product_label"><?php echo $model->getAttributeLabel('count_accuracy')?></label>
        <div class="product_title input_row">
            <?php echo $form->dropDownList($model, 'count_accuracy', range(0,3));?>
        </div>
        <?php echo $form->error($model, 'count_accuracy'); ?>
    </div>

    <div class="product_row">
        <?php echo $form->hiddenField($model, 'one_change_weight'); ?>
        <div class="spec_checkbox">
            <input <?php if ($model->one_change_weight) echo 'checked="checked"'; ?> type="checkbox" id="one_change_weight">
            <label for="one_change_weight"><?php echo $model->getAttributeLabel('one_change_weight')?></label>
            <div class="checkbox_bg"></div>
        </div>
    </div>



    <br/>* @property integer $food_type
    <br/>* @property integer $workplace
    <br/>* @property integer $version
    <br/>* @property integer $imageVersion

    <ul class="leftmenu_buttons dark">
        <li><?php echo CHtml::link('<img src="/images/leftmenu-4.png" width="50" height="50" alt=""><span>' . ($model->isNewRecord ? 'Создать' : 'Сохранить') . '</span>', '#', array('id' => 'position_save')); ?></li>
        <?php
        if (!$model->isNewRecord) {
            ?>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-2' . ($model->deleted ? '_act' : '') . '.png" width="50" height="50" alt=""><span>' . ($model->deleted ? 'Восстановить' : 'Удалить') . '</span>', '#', array('id' => 'position_set_deleted', 'class' => $model->deleted ? 'active' : '')); ?></li>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-6' . ($model->hidden ? '_act' : '') . '.png" width="50" height="50" alt=""><span>' . ($model->hidden ? 'Включить' : 'Отключить') . '</span>', '#', array('id' => 'position_set_hidden', 'class' => $model->hidden ? 'active' : '')); ?></li>
        <?php
        }
        ?>
    </ul>
<?php $this->endWidget(); ?>

<?php $this->renderPartial('/partials/_load_img', array('folder' => '/data/images/positions', 'selector' => 'Position_image')); ?>