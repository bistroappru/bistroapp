<h3>Передача запроса на оплату</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/r_keeper_api/v1/payOrder<br/>
</p>

<hr/>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF9}" ServerTimeStamp="UTC_DATETIME"/>
    <R_KEEPER>
        <REQUEST>
        <![CDATA[
            <?xml version="1.0" encoding="UTF-8"?>
            <RK6Query>
                <RK6Query>
                    <RK6CMD CMD="PayOrder"/>
                    <Order Guid="{55260C19-F771-68D1-0D3E-F2B7B96854D8}"/>
                    <Station Code="21"/>
                    <Payments>
                        <Payment Code="40" Amount="27030" Name="Карта"/>
                    </Payments>
                    <Dishes>
                        <Dish Id="4" Code="1" Quantity="3000" Sum="27030"/>
                    </Dishes>
                    <Persons>
                        <Person Id="9" Code="1234" Name="BACashier" Role="W"/>
                        <Person Id="7" Code="2222" Name="kassir" Role="K"/>
                    </Persons>
                </RK6Query>
            </RK6Query>
        ]]>
        </REQUEST>
    </R_KEEPER>
</BA_PROXY_REQUEST>');?>
    </code></pre>
<hr/>


<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK"></BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="ERROR">
        <ERROR code="17" description="Proxy с UUID {96896BD3-1D9F-45D5-B87C-9B2E6A924AF4} не зарегистрирован"/>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>