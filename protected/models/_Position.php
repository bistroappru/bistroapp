<?php

/**
 * This is the model class for table "{{position}}".
 *
 * The followings are the available columns in table '{{position}}':
 * @property integer $id
 * @property string $out_id
 * @property integer $is_group
 * @property string $name
 * @property string $description
 * @property integer $menu_id
 * @property integer $sort
 * @property integer $depressed
 * @property integer $label_id
 * @property string $image
 * @property integer $hidden
 * @property integer $deleted
 * @property integer $calories
 * @property string $in_stock
 * @property integer $food_type
 * @property integer $workplace
 * @property string $version
 * @property string $imageVersion
 */
class _Position extends CActiveRecord
{
    const TYPE_KITCHEN = 1;
    const TYPE_BAR = 2;

    public $delete_ids;
    public $sagr = 0;

    /*
    * Геттер списка всех типов блюда
    */
    public function getFoodTypes($key = NULL)
    {
        $items = array(
            self::TYPE_KITCHEN => 'Кухня',
            self::TYPE_BAR => 'Бар'
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{position}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, menu_id', 'required'),
			array('is_group, menu_id, sort, depressed, label_id, hidden, deleted, food_type, workplace', 'numerical', 'integerOnly'=>true),
            array('in_stock', 'length', 'max'=>500),
            array('food_type', 'ruleFoodType'),
            array('workplace', 'ruleWorkplace'),
            array('out_id, name, image', 'length', 'max'=>255),
            array('calories', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, is_group, name, description, menu_id, sort, depressed, label_id, image, hidden, calories, in_stock, food_type, workplace, version', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'conditions' => array(self::HAS_MANY, 'Condition', 'position_id', 'order'=>'conditions.sort ASC'),
            'group' => array(self::HAS_MANY, 'GroupDetails', 'group_id', 'order'=>'group.sort ASC'),
            'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
            'label' => array(self::BELONGS_TO, 'Label', 'label_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'is_group' => 'Групповая позиция',
			'name' => 'Наименование',
            'food_type' => 'Место приготовления',
			'description' => 'Описание',
			'menu_id' => 'Меню',
			'sort' => 'Индекс',
			'depressed' => 'Подавить вывод',
			'label_id' => 'Спецвывод',
			'image' => 'Изображение',
			'hidden' => 'Скрыт',
            'deleted' => 'Удален',
            'calories' => 'Каллории',
            'in_stock' => 'В наличии',
            'workplace' => 'Отдел кухни'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('is_group',$this->is_group);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('depressed',$this->depressed);
		$criteria->compare('label_id',$this->label_id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('hidden',$this->hidden);
        $criteria->compare('deleted',$this->deleted);
        $criteria->compare('calories',$this->calories);
        $criteria->compare('in_stock',$this->in_stock);
        $criteria->compare('food_type',$this->food_type);
        $criteria->compare('workplace',$this->workplace);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * подготовка модели по данным POST
     */
    public function prepareWithPostData(){
        $image_old = $this->image;
        $this->attributes = $_POST['Position'];
        $this->setAttribute('description', $_POST['Position']['description']); // TODO почемуто не подхватывается в предыдущей строке
        $this->delete_ids = $_POST['Position']['delete_ids'];

        $condition_list = array();
        if(isset($_POST['Position']['conditions']) && is_array($_POST['Position']['conditions'])){
            foreach($_POST['Position']['conditions'] as $k=>$v){
                if($v['id']){
                    $condition = Condition::model()->findByPk($v['id']);
                }else{
                    $condition = new Condition();
                }
                if (!is_object($condition)) throw new HttpException('Ошибка создания условия', 400);
                $condition->position_id = intval($this->id);
                $condition->measure = $v['measure'];
                $condition->sort = $v['sort'];
                $condition->price = $v['price'];
                $condition->action_on = $v['action_on'];

                if ($condition->action_on){
                    if (!is_object($condition->action)) $condition->action = new SalePrice();
                    $condition->action->condition_id = intval($condition->id);
                    $condition->action->sale_price = $v['action']['sale_price'];
                    $condition->action->date_on = $v['action']['date_on'] ? strtotime($v['action']['date_on']) : '';
                    $condition->action->date_off = $v['action']['date_off'] ? strtotime($v['action']['date_off']) : '';
                    $condition->action->validate();
                    if ($condition->action->hasErrors()){
                        $this->addError('action', 'Есть ошибки в заполнении акции для условий');
                    }
                }

                $condition->validate();
                if ($condition->hasErrors()){
                    $this->addError('conditions', 'Есть ошибки в заполнении условий');
                }
                $condition_list[] = $condition;
            }
        }else{
            $this->addError('conditions_count', 'Необходимо задать хотя бы одно условие');
        }
        $this->conditions = $condition_list;

        //подготовка деталей групповой позиции
        if ($this->is_group){
            $group_list = array();
            $ingroup = CJSON::decode($_POST['Position']['ingroup']); //$_POST['Position']['ingroup'] ? explode(',', $_POST['Position']['ingroup']) : array();
            if (!is_array($ingroup))
                $ingroup = array();

            if (count($ingroup)){
                foreach ($ingroup as $v){
                    // TODO переделать чтобы не создавать каждый раз новый
                    $gd = new GroupDetails();
                    $gd->setAttributes(array(
                        'group_id' => intval($this->id),
                        'sort' => $v['sort'],
                        'condition_id' => $v['condition_id'],
                        'pg_item_id' => $v['groups_id']
                    ));
                    $group_list[] = $gd;
                }
            }else{
                $this->addError('details_count', 'Необходимо выбрать хотя бы одну позицию');
            }
            $this->group = $group_list;
        }

        // - проверка версионирования
        if($this->isNewRecord || $image_old != $this->image){
            $version_image = Position::model()->getLastVersion('imageVersion', $this->menu->company_id);
            $this->imageVersion = ++$version_image;
        }

        $version_data = Position::model()->getLastVersion('version', $this->menu->company_id);
        $this->version = ++$version_data;
        $this->sagr = 1; // указываем что сохраняем удаляем

        return true;
    }


    /**
     * получение индекса последней версии данных
     */
    public static function getLastVersion($field = 'version', $company_id){
        $companies_menu = Menu::model()->findAll(array(
            'condition' => 'company_id = :company_id',
            'params' => array(':company_id' => $company_id),
            'select' => 'id'
        ));

        $menu_ids = array();
        foreach($companies_menu as $one){
            $menu_ids[] = $one->id;
        }

        $criteria = new CDbCriteria();
        $criteria->select = $field;
        $criteria->order = $field.' DESC';
        $criteria->addInCondition('menu_id', $menu_ids);
        $row = self::model()->find($criteria);

        return is_object($row) ? $row->$field : 0;
    }

    public function getCompanyWorkplaces(){
        $company = Company::model()->findByPk($this->menu->company_id);
        if($company === null)
            throw new CHttpException(404, 'Ошибка загрузки модели компании');

        return $company->getActiveWorkplaces();
    }

    /*
     * RULES
     */

    /*
     * validate food_type
     */
    public function ruleFoodType($attribute, $params){
        if (!$this->is_group && !in_array($this->food_type, array_keys(self::getFoodTypes()))) $this->addError('food_type', 'Неправильно задано место приготовления блюда');
    }

    /*
     * validate workplace
     */
    public function ruleWorkplace($attribute, $params){
        if (!$this->is_group && $this->food_type == Position::TYPE_KITCHEN && !$this->workplace) $this->addError('workplace', 'Не выбран отдел кухни для приготовления');
    }

    public function preparePositionGroupOut(){
        $out = $this->group;
        $groups = array();
        if (is_array($out)){
            foreach($out as $v){
                $groups[$v->pg_item_id]['name'] = $v->groups->name;
                $groups[$v->pg_item_id]['children'][] = $v;
            }
        }
        return $groups;
    }

    public function setIsGroupByMenuId(){
        if (!$this->menu_id)
            return false;

        $menu = Menu::model()->findByPk($this->menu_id);
        if ($menu === null)
            return false;

        if ($menu->isBusinessLunchRoot()){
            $this->is_group = 1;
            return true;
        }
        return false;
    }



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Position the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave()
    {
        parent::afterSave();
        if ($this->sagr){
            // удаление условий
            if ($this->delete_ids){
                $del_ids = explode(',', $this->delete_ids);
                $del_ids = array_filter($del_ids);
                if (is_array($del_ids) && count($del_ids)){
                    $list = Condition::model()->findAll('id IN ('.implode(',', $del_ids).') AND position_id = '.$this->id);
                    foreach($list as $one){
                        $one->delete();
                    }
                }
            }

            // создание деталей групповых позиций
            if ($this->is_group){
                GroupDetails::model()->deleteAll('group_id = '.$this->id);
                foreach($this->group as $one){
                    //$one->group_id = $this->id;
                    $one->save();
                }
            }

            // обновление или создание условий
            foreach($this->conditions as $one) {
                $one->position_id = $this->id;
                $one->save();
            }

            //
        }
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        foreach($this->conditions as $one){
            $one->delete();
        }

        // создание деталей групповых позиций
        if ($this->is_group){
            foreach($this->group as $one){
                $one->delete();
            }
        }
    }


    // ---------    CUSTOM OBJECT METHODS   ----------


    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function getMaxSort(){
        return 0;
    }

    public static function parseKeeperData(RKeeperQueue $queue){
        $response = @simplexml_load_string($queue->response);

        if (!$response){
            $queue->setFail('Не удалось обработать xml-ответ');
            return;
        }

        if ((string) $response['Status'] !== 'Ok'){
            $queue->setFail('Статус должен быть: Ok');
            return;
        }

        if ((string) $response['CMD'] !== $queue->getCMD()){
            $queue->setFail('Команда запроса и ответа не совпадает');
            return;
        }

        if ((string) $response->RK7Reference['Name'] !== strtoupper($queue->getRefName())){
            $queue->setFail('Справочник запроса и ответа не совпадает');
            return;
        }

        if ($response->RK7Reference->Items->count()){
            $sort = Position::getMaxSort();
            foreach ($response->RK7Reference->Items->children() as $k=>$row){
                $item = AbcHelper::simple_xml_attr_to_array($row);
                if (!$item['Ident'] || !$item['MainParentIdent']) continue;

                $data = array();
                $data['out_id'] = $item['Ident'];
                $data['name'] = $item['Name'];
                $data['description'] = '';
                //$data['is_group'] = 'признак бизнес-ланча';

                // данные для создания категории, если понадобится
                $menu_data = array(
                    'company_id' => $queue->proxy->company_id,
                    'name' => 'Категория #' . $item['MainParentIdent']
                );
                $data['menu_id'] = Menu::getIdByOutId($item['MainParentIdent'], true, $menu_data);

                $data['sort'] = 0;
                $data['hidden'] = ($item['Status'] == 'rsDraft' ? 1 : 0);
                $data['deleted'] = ($item['Status'] == 'rsDeleted' ? 1 : 0);
                //$data['in_stock'] = 'определить наличие в ресторанах';
                //$data['food_type'] = 'определить тип бар/кухня';
                //$data['workplace'] = 'определить цех';
                //$data['imageVersion'] = '';

                $model = Position::model()->findByAttributes(array('out_id' => $data['out_id']));
                if ($model === null){
                    $model = new Position();
                }
                $model->setAttributes($data);

                $version = self::getLastVersion('version', $queue->proxy->company_id);
                $model->version = ++$version;

                if (!$model->save(false)){
                    $queue->setFail('Не удалось создать/обновить блюдо с out_id = ' . $model->out_id);
                    return;
                }
                $queue->setFinished();
            }
        }
    }
}
