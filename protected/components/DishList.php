<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class DishList extends CWidget{
    public $dish_id = 0;
    public $menu_id;
    public $group;
    public $fields = array('id','name','depressed','label_id','image','hidden','deleted', 'price', 'price_mode', 'measure');
    public function run(){
        if (!$this->menu_id) return;

        $category = Menu::model()->findByPk($this->menu_id);
        if ($category === null) return;

        if ($this->group){
            $categories = $category->getCompanyCategories(false, false);
            if ($category->isBusinessLunchRoot())
                $category = clone $categories[0];
        }else{
            $categories = array();
        }

        $criteria = new CDbCriteria();
        $criteria->select = $this->fields;
        $criteria->order = 'sort ASC';
        $criteria->compare('menu_id', $category->id);
        if ($this->group)
            $criteria->addCondition('is_group != 1');


        $dishes = array();
        $temp = Position::model()->findAll($criteria);
        foreach($temp as $k=>$v){
            $dishes['ids'][] = $v->id;
            $dishes['list'][$v->id] = $v->getAttributes($this->fields);
            if (in_array('label_id', $this->fields) && $v->label_id) $dishes['list'][$v->id]['style_id'] = $v->label->style_id;
        }

        $this->render('_dish_list_'.intval($this->group),array(
            'dishes' => $dishes,
            'category' => $category,
            'categories' => $categories,
            'dish_id' => $this->dish_id,
        ));
    }
}