<?php
list($queryCount, $queryTime) = Yii::app()->db->getStats();
?>

<head>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.0.min.js"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta charset="UTF-8">
    <base href="<?php echo Yii::app()->request->getBaseUrl(true); ?>">
    <link rel="icon" href="/favicon.png" type="image/x-icon">

    <!--  CSS  -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/media-queries.css">
    <link rel="stylesheet" media="print" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/kladrapi-jquery-master/examples/css/example4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui/jquery-ui.min.css" media="screen" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jQuery-File-Upload-9.5.7/css/jquery.fileupload.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jcrop/css/jquery.Jcrop.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/baron-scrollbar/baron.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.css"/>
    <!--  JS  -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jscrollpane.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/baron-scrollbar/baron.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/baron-scrollbar/src/autoUpdate.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.selectBox.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.placeholder.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modal.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.inputmask.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-cookie/src/jquery.cookie.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQuery-File-Upload-9.5.7/js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQuery-File-Upload-9.5.7/js/jquery.iframe-transport.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jQuery-File-Upload-9.5.7/js/jquery.fileupload.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jcrop/js/jquery.Jcrop.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <style>
        body {
            background: url(../../../../../images/bg.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>

    <script src="http://api-maps.yandex.ru/2.0.18/?load=package.full&lang=ru_RU"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/kladrapi-jquery-master/jquery.primepix.kladr.min.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/alerts/jquery.alerts.js" type="text/javascript"></script>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/js/alerts/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />

    <!--  APPLICATION SCRIPTS  -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/company.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/restaurant.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/menu.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/users.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/orders.js"></script>
</head>
<!--
<center style="color: #fff;">Запросов к БД <?=$queryCount?>. Время запросов <?=sprintf('%0.3f',$queryTime)?>. Отработало за <?=sprintf('%0.3f',Yii::getLogger()->getExecutionTime())?> с. Скушано памяти: <?=round(memory_get_peak_usage()/(1024*1024),2)."MB"?></center>
-->
