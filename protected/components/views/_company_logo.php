<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:25
 */
?>
<div class="logo_box" id="company_logobox">
    <div class="logo_img">
        <a href="javascript:void(0);" id="logo"><img src="<?php echo $company->logo ? $company->logo : Yii::app()->params['emptyImage'];?>" width="100" height="100" alt=""></a>
    </div>
    <div class="logo_text">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'restaurant-choose-form',
            'enableAjaxValidation'=>false,
        ));
        echo CHtml::hiddenField('empty', 1);
        ?>

        <?php if (Yii::app()->user->company_id){?>
            <div class="site_title"><?php echo $company->name;?></div>
        <?php }else{
            echo CHtml::dropDownList('company_id', Yii::app()->user->company_id_full, array('0'=>'Выберите компанию') + AbcHelper::allCompanies(Yii::app()->user->company_id),array('id'=>'cr_company','class' => 'logo_select',));
        } ?>

        <?php if (Yii::app()->user->restaurant_id){?>
            <div class="site_address"><?php echo $restaurant->location;?></div>
        <?php }else{
            echo CHtml::dropDownList('restaurant_id', Yii::app()->user->restaurant_id_full, array('0'=>'Выберите ресторан') + AbcHelper::allCompanyRestaurants(Yii::app()->user->company_id_full), array('id'=>'cr_restaurant','class' => 'logo_select'));
        } ?>
        <?php $this->endWidget();?>
    </div>
</div>
