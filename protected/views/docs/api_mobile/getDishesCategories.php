<h3>Получение категорий меню</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getDishesCategories<br/>
</p>

<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>company_id</em></strong>: ID компании (сеть ресторанов) <b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/getDishesCategories?company_id=1&version=3 ');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "status": "ok",
  "version": "21",
  "data": [
    {
      "id": "1",
      "name": "\u0417\u0430\u043a\u0443\u0441\u043a\u0438",
      "company_id": "1",
      "parent_id": "0",
      "sort": "0",
      "description": "\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \"\u0417\u0430\u043a\u0443\u0441\u043a\u0438\"\r\n\u0417\u0434\u0435\u0441\u044c \u043c\u043e\u0433\u043b\u0430 \u0431\u044b \u0431\u044b\u0442\u044c  \u0432\u0430\u0448\u0430 \u0440\u0435\u043a\u043b\u0430\u043c\u0430.",
      "hidden": "0",
      "deleted": "0"
    },
    {
      "id": "2",
      "name": "\u041d\u0430\u043f\u0438\u0442\u043a\u0438",
      "company_id": "1",
      "parent_id": "0",
      "sort": "1",
      "description": "\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \"\u041d\u0430\u043f\u0438\u0442\u043a\u0438\"",
      "hidden": "0",
      "deleted": "0"
    },
    {
      "id": "3",
      "name": "\u0420\u043e\u043b\u043b\u044b",
      "company_id": "1",
      "parent_id": "0",
      "sort": "2",
      "description": "\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \"\u0420\u043e\u043b\u043b\u044b\"",
      "hidden": "0",
      "deleted": "0"
    }
  ]
}');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "status": "error",
  "error": {
    "code": 1,
    "message": "\u041d\u0435 \u0437\u0430\u0434\u0430\u043d \u043e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440 company_id"
  }
}');?>
    </code></pre>
<hr/>

