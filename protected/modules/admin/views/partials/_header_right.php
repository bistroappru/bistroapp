<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 21.05.14
 * Time: 9:36
 */
?>

<div class="header_right">
    <div class="search">
        <form action="/admin/search">
            <input name="string" class="inp_search" type="text" value="<?=Yii::app()->request->getParam('string', 'Поиск');?>"
                   onfocus="this.value=(this.value=='Поиск')? '' : this.value ;"
                   onblur="this.value=(this.value=='')? 'Поиск' : this.value ;">
            <input class="submit_search" type="submit" value="">
        </form>
    </div>
    <div class="right_links">
        <a href="mailto:<?=Yii::app()->params['adminEmail']?>" class="mail_ic">&nbsp;</a>
        <a href="/admin/settings" class="set_ic">&nbsp;</a>
    </div>
    <div class="login_box">
        <div class="login_entry">
            <p><?php echo Yii::app()->user->name;?></p>
            <?php echo CHtml::link('Выход', array('/site/logout'), array('class'=>'login-link'))?>
        </div>
        <?php echo CHtml::link('', array('/admin'), array('class'=>'login_ic'))?>
    </div>
</div>