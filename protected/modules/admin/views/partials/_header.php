<header>
    <div class="header_line">
        <div class="header_center">
            <div class="column_left">
                <?php $this->widget('application.components.CompanyLogo');?>
            </div>
            <?php $this->renderPartial('/partials/_admin_menu'); ?>
        </div>
    </div>
    <div class="header_center">
        <?php $this->renderPartial('/partials/_header_right', array()); ?>
        <?php if ($this->id == 'orders') $this->renderPartial('/partials/_order_table_menu');?>
    </div>
</header>
<div class="clear"></div>