<?php
/**
 * Created by PhpStorm.
 * User: birlaver
 * Date: 07.09.14
 * Time: 11:18
 */
?>
<div class="dish_list_wrap">
    <div class="dishes_head">
        <div class="dish_box">
        <span class="dish_entry">
            <span class="dish_title"><?= $category->name; ?></span>
            <span class="dish_desc"><?= AbcHelper::trimString($category->description, 80); ?></span>
        </span>
            <span class="dish_icon"><img
                    src="<?= $category->image ? $category->image : Yii::app()->params['emptyImage']; ?>" width="80"
                    height="80"></span>
        </div>
    </div>

    <div class="rest_search" id="dish_search">
        <form>
            <input class="inp_rest_srh" autocomplete="off" type="text" name="dish_search" value="Поиск"
                   onfocus="this.value=(this.value=='Поиск')? '' : this.value ;"
                   onblur="this.value=(this.value=='')? 'Поиск' : this.value ;">
            <input class="submit_rest_srh" type="submit" value=" ">
        </form>
    </div>

    <ul class="dishes_list" id="dishes_sortable"></ul>

    <ul class="leftmenu_buttons">
        <?php
        if (!$category->isBusinessLunchRoot()) {
            ?>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-3.png" width="50" height="50" alt=""><span>Добавить блюдо</span>', array('/admin/position/create/menu_id/' . $category->id), array('id' => 'position_create')); ?></li>
        <?php
        } else {
            ?>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-3.png" width="50" height="50" alt=""><span>Добавить бизнес-ланч</span>', array('/admin/position/create/menu_id/' . $category->id), array('id' => 'position_create_group')); ?></li>
        <?php
        }
        ?>
    </ul>

    <script>
        var dishes_json = <?= CJSON::encode($dishes);?>;
        var dishes_id = <?=$dish_id;?>;

        function searchDishes() {
            var search = $('#dish_search input[name=dish_search]').val();
            if (!search || search == 'Поиск') search = '';
            var ul_html = '';
            if (dishes_json['ids'] && dishes_json['ids'].length) {
                $.each(dishes_json['ids'], function (k, v) {
                    pos = dishes_json['list'][v].name.toLowerCase().indexOf(search.toLowerCase());
                    if (pos != -1) {
                        ul_html += '<li data-id="' + v + '" class="'
                            + (dishes_json['list'][v].deleted == 1 ? 'style_del' : '') + ' '
                            + (dishes_json['list'][v].hidden == 1 ? 'style_hid' : '') + ' '
                            + (dishes_json['list'][v].depressed == 1 ? 'depressed' : '') + '">\
                    ' + (dishes_json['list'][v].label_id ? '<div class="spec_' + dishes_json['list'][v].style_id + '"></div>' : '') + '\
                    <div href="/admin/position/update/dish_id/' + v + '" class="dish_box ' + (dishes_json['list'][v].id == dishes_id ? 'active' : '') + '">\
                        <span class="dish_entry">\
                            <span class="dish_title">' + dishes_json['list'][v].name + '</span>\
                            <span class="dish_desc"><span class="black">' + dishes_json['list'][v].price + ' р.</span></span>\
                        </span>\
                        <span class="dish_icon">\
                            <a href="' + (dishes_json['list'][v].image ? dishes_json['list'][v].image : '<?=Yii::app()->params['emptyImage'];?>') + '" class="fancybox_image" title="' + dishes_json['list'][v].name + '" rel="dish_gallery">\
                                <img src="' + (dishes_json['list'][v].image ? dishes_json['list'][v].image : '<?=Yii::app()->params['emptyImage'];?>') + '" width="80" height="80">\
                            </a>\
                        </span>\
                    </div>\
                </li>';
                    }
                });
            }
            $('#dishes_sortable').replaceWith('<ul class="dishes_list" id="dishes_sortable">' + ul_html + '</ul>');
            if (search == '') activateDishesSortable();
        }

        searchDishes();

        $('#dish_search input[name=dish_search]').on('keyup', function () {
            searchDishes();
        });

        $('#dish_search form').on('submit', function (e) {
            e.preventDefault();
            searchDishes();
        });
    </script>
</div>
