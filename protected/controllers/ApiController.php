<?php
class ApiController extends Controller
{
    public function filters()
    {
        return array(
            //'postOnly + registrationProxy', // we only allow deletion via POST request
        );
    }

    private $_codeDescription = array(
        0 => 'Запрос успешно выполнен',
        1 => 'Не задан обязательный параметр company_id',
        2 => 'Заказ должен содержать хотя бы одну позицию',
        3 => 'Позиция (%position%) не существует или удалена',
        4 => 'Цена для (%position%) не актуальна, обновите приложение',
        5 => 'Ресторан не является дочерним заданной компании',
        6 => 'Не удалось распарсить тело запроса. Проверьте тело запроса на наличие синтакисческих ошибок',
        7 => 'Прокси-сервер с таким UUID - уже существует',
        8 => 'Некорректное название прокси-сервера',
        9 => 'Некорректно задан тип прокси-сервера',
        10 => 'Для REF сервера некоректно задан ID компании',
    );

	public function actionIndex(){
        $this->redirect($this->createUrl('/docs/api_mobile'));
		$this->layout = 'api';
		$this->render('index');
	}

    public function actionForm(){
        $this->layout = 'api';
        $this->render('form');
    }

    public function actionGetCompanies()
    {
        header('Content-type: application/json');
        $data = array(
            'code' => 0,
            'error' => $this->_codeDescription[0],
            'data' => array()
        );
        //---------------------

        // ----- действия метода
        $condition = array();

        $tmp = Company::model()->findAllByAttributes($condition);
        foreach($tmp as $one){
            $restaurants = array();
            foreach($one->restaurants as $item){
                $tables = array();
                foreach($item->tables as $table){
                    $tables[] = $table->attributes;
                }

                $coords = CJSON::decode($item['map']);
                $restaurants[] = array(
                    'id' => $item->id,
                    'deleted' => $item->deleted,
                    'location' =>  $item->location,
                    'hidden' =>  $item->hidden,
                    'description' =>  $item->description,
                    'phone' =>  $item->phone,
                    'working_hours' =>  $item->working_hours,
                    'company_id' =>  $item->company_id,
                    'coords' => $coords['coords'],
                    'tables' => $tables
                );
            }

            $data['data'][] = array(
                'id' => $one->id,
                'name' => $one->name,
                //'image' => Yii::app()->request->hostInfo.'/'.$one->logo,
                'image' => $one->logo ? base64_encode(file_get_contents(Yii::app()->request->hostInfo.'/'.$one->logo)) : '',
                'hidden' => $one->hidden,
                'deleted' => $one->deleted,
                'description' =>  $one->description,
                'restaurants' => $restaurants
            );
        }
        // ----- конец действий

        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetDishes_(){
        header('Content-type: application/json');
        $data = array(
            'code' => 0,
            'data' => array(),
            'error' => $this->_codeDescription[0]
        );
        //---------------------

        // ----- действия метода
        $company_id = Yii::app()->request->getParam('company_id', 0);
        if ($company_id){
            $condition = array();
            $condition['company_id'] = $company_id;
            $tmp = Menu::model()->findAllByAttributes($condition);
            $menu_tree = array();
            foreach($tmp as $one){
                $dishes = array();
                foreach($one->positions as $item){
                    $conditions = array();
                    foreach($item->conditions as $row){
                        $conditions[] = array(
                            'id' => $row->id,
                            'measure' => $row->measure,
                            'sort' => $row->sort,
                            'price' => $row->price,
                            'dish_id' => $item->id,
                            'action' => is_object($row->action) ? $row->action->getAttributes() : ''
                        );
                    }

                    $dishes[] = array(
                        'id' => $item->id,
                        'is_group' => $item->is_group,
                        'name' => $item->name,
                        'description' => $item->description,
                        'menu_id' => $item->menu_id,
                        'sort' => $item->sort,
                        'depressed' => $item->depressed,
                        'label_id' => $item->label_id,
                        'image' => $item->image ? base64_encode(file_get_contents(Yii::app()->request->hostInfo.$item->image)) : '',
                        'conditions' => $conditions,
                        'hidden' => $item->hidden,
                        'calories' => $item->calories,
                        'in_stock' => $item->in_stock,
                    );
                }

                $menu_tree[$one->id] = array(
                    'id' => $one->id,
                    'name' => $one->name,
                    'company_id' => $one->company_id,
                    'parent_id' => $one->parent_id,
                    'sort' => $one->sort,
                    'description' => $one->description,
                    'image' => $one->image ? base64_encode(file_get_contents(Yii::app()->request->hostInfo.$one->image)) : '',
                    'hidden' => $one->hidden,
                    'dishes' => $dishes
                );
            }

            $data['data'] = $this->build_tree($menu_tree);
            $this->array_keys_clean($data['data']);
        }else{
            $data['code'] = 1;
            $data['error'] = $this->_codeDescription[1];
        }

        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetOrders(){
        header('Content-type: application/json');
        $data = array(
            'code' => 0,
            'data' => array(),
            'error' => $this->_codeDescription[0]
        );
        //---------------------

        $token = Yii::app()->request->getParam('token', 0);
        $company_id = intval(Yii::app()->request->getParam('company_id', 0));
        $restaurant_id = intval(Yii::app()->request->getParam('restaurant_id', 0));
        $user_id = 1;

        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id = '.$user_id);

        if ($company_id){
            $restaurant_list = array();
            $rst_temp = Restaurant::model()->findAll('company_id = '.$company_id);
            foreach($rst_temp as $v){
                $restaurant_list[] = $v->id;
            }
            if(!count($restaurant_list)) $restaurant_list[] = -1;
            $criteria->addInCondition('restaurant_id', $restaurant_list);
        }

        if ($restaurant_id && isset($restaurant_list) && !in_array($restaurant_id, $restaurant_list)){
            $data['code'] = 5;
            $data['error'] = $this->_codeDescription[5];
            echo CJSON::encode($data);
            Yii::app()->end();
        }

        if ($restaurant_id) $criteria->addCondition('restaurant_id = '.intval($restaurant_id));

        $tmp = Order::model()->findAll($criteria);
        foreach($tmp as $one){
            $items = array();
            foreach($one->items as $item){
                $items[] = $item->getAttributes();
            }

            $order = $one->getAttributes();
            $order['company_id'] = $one->restaurant->company_id;
            $order['items'] = $items;
            $data['data'][] = $order;
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionPostOrder(){
        //header('Content-type: application/json');
        $data = array(
            'code' => 0,
            'error' => $this->_codeDescription[0]
        );
        //---------------------
        $token = Yii::app()->request->getParam('token', 0);
        $request = file_get_contents('php://input');
        $body = CJSON::decode($request);


        try {
            $mailer = Yii::app()->mailer;
            $mailer->AddAddress('abc-coder@mail.ru');
            $mailer->Subject = 'JSON заказа';
            $mailer->Body = $request;
            $mailer->Send();
        } catch (Exception $e){
            //
        }

        //$body = CJSON::decode(Yii::app()->request->getParam('body', null));

        $order = new Order();
        $order->attention = isset($body['attention']) ? $body['attention'] : '';
        $order->payment_id = isset($body['payment_id']) ? $body['payment_id'] : '';
        $order->restaurant_id = isset($body['restaurant_id']) ? $body['restaurant_id'] : 3;
        $order->separated = isset($body['separated']) ? $body['separated'] : '';
        $order->separated_info = isset($body['separated_info']) ? $body['separated_info'] : '';

        $order->table_id = isset($body['table']) ? $body['table'] : '';
        //$order->table_id = isset($body['table']) ? RestaurantTable::getIdByCode($body['table'], $order->restaurant_id) : '';

        $userTable = UserTable::model()->findByAttributes(array('table_id'=>$order->table_id));
        if ($userTable) $order->garcon_id = $userTable->user_id;

        $order->user_id = 1; // TODO заменить на настоящий
        $order->status_id = 1;// TODO заменить на по дефолту из конфига
        $order->date_create = time();
        $order->total_price = 0;
        $order->count_items = 0;

        // TODO проверить нет ли открытого заказа на этот столик

        if (isset($body['positions']) && is_array($body['positions']) && count($body['positions'])){
            foreach($body['positions'] as $v){
                $sp = Condition::model()->findByPk($v['condition_id']);
                if (is_object($sp)){
                    if (isset($sp->price) && $sp->price != $v['price']){
                        $data['code'] = 4;
                        $data['error'] = str_replace('%position%', $sp->position->name.', '.$sp->measure, $this->_codeDescription[4]);
                        echo CJSON::encode($data);
                        Yii::app()->end();
                    }
                    $item = new OrderItem();
                    $item->condition_id = $sp->id;
                    $item->status_id = 8;// TODO заменить на по дефолту из конфига
                    $item->price = $sp->price;
                    $item->count = $v['count'];
                    $order->positions[] = $item;
                    $order->total_price += $item->price*$item->count;
                    $order->count_items++;
                }else{
                    $data['code'] = 3;
                    $data['error'] = str_replace('%position%', $v['condition_id'], $this->_codeDescription[3]);
                    echo CJSON::encode($data);
                    Yii::app()->end();
                }
            }
        }

        if(!$order->count_items){
            $data['code'] = 2;
            $data['error'] = $this->_codeDescription[2];
            echo CJSON::encode($data);
            Yii::app()->end();
        }

        if(!$order->save()){
            $data['error'] = $this->splitErrors($order->getErrors());
        }else{
            // send messages when all OK
            $order->notificationMessages();
            RKeeperQueue::createRKeeperOrder($order);
        }

        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetOrderStatuses(){
        header('Content-type: application/json');
        $data = array(
            'code' => 0,
            'data' => array(),
            'error' => $this->_codeDescription[0]
        );
        //---------------------
        $tmp = OrderStatus::model()->findAllByAttributes(array());
        foreach($tmp as $one){
            $data['data'][] = array(
                'id' => $one->id,
                'name' => $one->name,
                'type' => $one->type,
            );
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetOrderPayments(){
        header('Content-type: application/json');
        $data = array(
            'code' => 0,
            'data' => array(),
            'error' => $this->_codeDescription[0]
        );
        //---------------------
        $tmp = OrderPayment::model()->findAllByAttributes(array());
        foreach($tmp as $one){
            $data['data'][] = array(
                'id' => $one->id,
                'name' => $one->name
            );
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetDishesCategories()
    {
        header('Content-type: application/json');
        $data = array();
        //---------------------
        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        if ($company_id){
            $tmp = Menu::model()->findAll(array(
                'condition' => 'company_id = :company_id AND version > :version',
                'params' => array(':company_id' => $company_id, ':version' => $version)
            ));
            $menu_tree = array();
            foreach($tmp as $one){
                $menu_tree[$one->id] = array(
                    'id' => $one->id,
                    'name' => $one->name,
                    'company_id' => $one->company_id,
                    'parent_id' => $one->parent_id,
                    'sort' => $one->sort,
                    'description' => $one->description,
                    'hidden' => $one->hidden,
                    'deleted' => $one->deleted
                );
            }
            $data['status'] = 'ok';
            $data['version'] = Menu::model()->getLastVersion($company_id);
            $data['data'] = $menu_tree;
            $this->array_keys_clean($data['data']);
        }else{
            $data['status'] = 'error';
            $data['error'] = array(
                'code' => 1,
                'message' => $this->_codeDescription[1]
            );
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetDishesCategoriesImages()
    {
        header('Content-type: application/json');
        $data = array();
        //---------------------
        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        if ($company_id){
            $tmp = Menu::model()->findAll(array(
                //'select' => array('image'),
                'condition' => 'company_id = :company_id AND imageVersion > :version',
                'params' => array(':company_id' => $company_id, ':version' => $version)
            ));
            $menu_tree = array();
            foreach($tmp as $one){
                if (!file_exists($_SERVER['DOCUMENT_ROOT'].$one->image)) continue;
                $image_info = getimagesize(Yii::app()->request->hostInfo.$one->image);
                $path_info = pathinfo(Yii::app()->request->hostInfo.$one->image);
                $menu_tree[$one->id] = array(
                    'id' => $one->id,
                    'image' => $one->image ? base64_encode(file_get_contents(Yii::app()->request->hostInfo.$one->image)) : '',
                    'extension' => $one->image ? $path_info['extension'] : '',
                    'size' => array(
                        'width' => $one->image ? $image_info[0] : '',
                        'height' => $one->image ? $image_info[1] : ''
                    )
                );
            }
            $data['status'] = 'ok';
            $data['version'] = Menu::model()->getLastVersion($company_id, 'imageVersion');
            $data['data'] = $menu_tree;
            $this->array_keys_clean($data['data']);
        }else{
            $data['status'] = 'error';
            $data['error'] = array(
                'code' => 1,
                'message' => $this->_codeDescription[1]
            );
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetDishes()
    {
        header('Content-type: application/json');
        $data = array();
        //---------------------
        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        if ($company_id){
            $criteria = new CDbCriteria();
            $criteria->select = 'id';
            $criteria->order = 'sort ASC';
            $criteria->addCondition('company_id = ' . $company_id);
            $menu_list_sql = Menu::model()->findAll($criteria);
            $menu_list = array();
            foreach($menu_list_sql as $one){
                $menu_list[] = $one['id'];
            }
            $dishes = array();
            if (count($menu_list)){
                $criteria = new CDbCriteria();
                $criteria->condition = 'version > :version';
                $criteria->params = array(':version' => $version);
                $criteria->addInCondition('menu_id', $menu_list);
                $tmp = Position::model()->findAll($criteria);
                foreach($tmp as $item){
                    $conditions = array();
                    foreach($item->conditions as $row){
                        $conditions[] = array(
                            'id' => $row->id,
                            'measure' => $row->measure,
                            'sort' => $row->sort,
                            'price' => $row->price,
                            'dish_id' => $item->id,
                            'action' => is_object($row->action) ? $row->action->getAttributes() : ''
                        );
                    }

                    $dishes[] = array(
                        'id' => $item->id,
                        'is_group' => $item->is_group,
                        'name' => $item->name,
                        'description' => $item->description,
                        'menu_id' => $item->menu_id,
                        'sort' => $item->sort,
                        'depressed' => $item->depressed,
                        'label_id' => $item->label_id,
                        //'image' => $item->image ? base64_encode(file_get_contents(Yii::app()->request->hostInfo.$item->image)) : '',
                        'conditions' => $conditions,
                        'hidden' => $item->hidden,
                        'deleted' => $item->deleted,
                        'calories' => $item->calories,
                        'in_stock' => $item->in_stock
                    );
                }
            }
            $data['status'] = 'ok';
            $data['version'] = Position::model()->getLastVersion('version', $company_id);
            $data['data'] = $dishes;
            $this->array_keys_clean($data['data']);
        }else{
            $data['status'] = 'error';
            $data['error'] = array(
                'code' => 1,
                'message' => $this->_codeDescription[1]
            );
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionGetDishesImages()
    {
        header('Content-type: application/json');
        $data = array();
        //---------------------
        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        if ($company_id){
            $criteria = new CDbCriteria();
            $criteria->select = 'id';
            $criteria->order = 'sort ASC';
            $criteria->addCondition('company_id = ' . $company_id);
            $menu_list_sql = Menu::model()->findAll($criteria);
            $menu_list = array();
            foreach($menu_list_sql as $one){
                $menu_list[] = $one['id'];
            }
            $dishes = array();
            if (count($menu_list)){
                $criteria = new CDbCriteria();
                $criteria->select = array('id','image');
                $criteria->condition = 'imageVersion > :version';
                $criteria->params = array(':version' => $version);
                $criteria->addInCondition('menu_id', $menu_list);
                $tmp = Position::model()->findAll($criteria);
                foreach($tmp as $item){
                    if (!file_exists($_SERVER['DOCUMENT_ROOT'].$item->image)) continue;
                    $image_info = getimagesize(Yii::app()->request->hostInfo.$item->image);
                    $path_info = pathinfo(Yii::app()->request->hostInfo.$item->image);
                    $dishes[] = array(
                        'id' => $item->id,
                        'image' => $item->image ? base64_encode(file_get_contents(Yii::app()->request->hostInfo.$item->image)) : '',
                        'extension' => $item->image ? $path_info['extension'] : '',
                        'size' => array(
                            'width' => $item->image ? $image_info[0] : '',
                            'height' => $item->image ? $image_info[1] : ''
                        )
                    );
                }
            }
            $data['status'] = 'ok';
            $data['version'] = Position::model()->getLastVersion('imageVersion', $company_id);
            $data['data'] = $dishes;
            $this->array_keys_clean($data['data']);
        }else{
            $data['status'] = 'error';
            $data['error'] = array(
                'code' => 1,
                'message' => $this->_codeDescription[1]
            );
        }
        //---------------------
        header('Content-Length: '.strlen(CJSON::encode($data)));
        echo CJSON::encode($data);
        Yii::app()->end();
    }

//    public function actionXXX()
//    {
//        header('Content-type: application/json');
//        $data = array(
//            'code' => 0,
//            'data' => array(),
//            'error' => $this->_codeDescription[0]
//        );
//        //---------------------
//        // TODO код метода
//        //---------------------
//        echo CJSON::encode($data);
//        Yii::app()->end();
//    }

// -----------------------------------------------------------------------------------------------
// --------------------             PRIVATE FUNCTIONS             --------------------------------
// -----------------------------------------------------------------------------------------------

    private function splitErrors($list){
        $errors = array();
        if (is_array($list)){
            foreach($list as $k=>$v){
                $errors = array_merge($errors, $v);
            }
        }
        return implode('; ', $errors);
    }

    private function array_keys_clean(&$array) {
        $array = array_values($array);
        foreach($array as $key => &$item) {
            if(isset($item["childrens"]))
                $this->array_keys_clean($item["childrens"], $array, $key);
        }
    }

    private function build_tree($dataset){
        $tree = array();
        foreach ($dataset as $id=>&$node) {
            if (!$node['parent_id']) {
                $tree[$id] = &$node;
            }
            else {
                $dataset[$node['parent_id']]['childrens'][$id] = &$node;
            }
        }
        return $tree;
    }
}