<h3>Получение списка ресторанов</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getRestaurants<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>company_id</em></strong>: ID компании, рестораны которой нужно вернуть<b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
    {
        "status": "ok",
        "data": [
            {
                "id": "45",
                "name": "Кантемировская, Каширская",
                "company_id": "9",
                "location": "г. Москва, ш. Каширское, д. 46к1",
                "description": "198 посадочных мест, парковка, Wi-Fi, кальянная карта",
                "hidden": "0",
                "deleted": "0",
                "phone": "+7 (499) 324-71-91",
                "working_hours": "Пн-Чт,Вс(11:30-00:00), Пт-Сб(11:30-02:00)"
            },
            {
                "id": "47",
                "name": "Волгоградский пр-т, Пролетарская",
                "company_id": "9",
                "location": "г. Москва, пр-кт. Волгоградский, д. 17",
                "description": "150 посадочных мест, парковка, Wi-Fi, кальянная карта",
                "hidden": "0",
                "deleted": "0",
                "phone": "+7 (495) 676-99-22",
                "working_hours": "Пн-Чт,Вс(11:30-00:00), Пт-Сб(11:30-02:00)"
            }
        ],
        "version": "0"
    }
    </code></pre>
<hr/>