<h3>Создание заказа</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/postOrder<br/>
</p>

<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>token</em></strong>: появится при появлении функции авторизации <b>(обязательный)</b><br/>
</p>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/postOrder?token=d41d8cd98f00b204e9800998ecf8427e');?>
    </code></pre>
<hr/>

<h4>Тело запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{ "attention" : 1,
  "payment_id" : 1,
  "restaurant_id" : 3,
  "positions" : [{
        "condition_id" : 16,
        "count" : 3,
        "price" : 150,
      }],
  "separated" : 1,
  "separated_info" : "текст раздельного заказа",
  "table" : 2
}');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "code": 0,
  "error": "\u0417\u0430\u043f\u0440\u043e\u0441 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d"
}');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "code": 0,
  "error": "\u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u043f\u043e\u043b\u0435 \u00ab\u0421\u0442\u043e\u043b\u0438\u043a\u00bb."
}');?>
    </code></pre>
<hr/>

