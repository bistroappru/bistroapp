<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name . ' - О проекте';
?>
<div id="home_header">
    <div class="center">
        <?php $this->renderPartial('/partials/_top_menu',array()); ?>
        <div class="header_device2"></div>
        <a href="/" id="logo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo_home.png" width="160" height="157" alt=""></a>
    </div>
</div>

<section class="benefits_section">
    <div class="pattern_top"></div>
    <div class="pattern_bottom"></div>
    <div class="center">
        <ul class="benefits_list">
            <li>
                <div class="thumb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/benefits1.png" width="121" height="121" alt=""></div>
                <p>Приложение само определит ваши координаты и предложит ознакомиться с меню того ресторана, в котором вы находитесь.</p>
            </li>
            <li>
                <div class="thumb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/benefits4.png" width="121" height="121" alt=""></div>
                <p>Вы можете выбрать понравившиеся блюда и отправить заказ прямо со своего мобильного устройства.</p>
            </li>
            <li>
                <div class="thumb"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/benefits5.png" width="121" height="121" alt=""></div>
                <p>Вы можете выбрать способ оплаты, и оплатить счет не дожидаясь официанта.</p>
            </li>
        </ul>
    </div>
</section>
<section class="home_gray">
    <div class="big_device2">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/device2.jpg" width="949" height="540" alt="">
    </div>
    <div class="pattern_bottom gray"></div>
</section>
<section class="simple-easy">
    <div class="simple_inner">
        <div class="simple_text">
            <h2>ПРОСТО и ЛЕГКО</h2>
            <p>Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
        </div>
        <div class="simple_device2"></div>
    </div>
</section>
<script type="text/javascript">
    $('.simple-easy').backstretch("images/simple-easy.jpg");
    jQuery(document).ready(function(){jQuery(window).resize(function(){
        $('.simple-easy').backstretch("images/simple-easy.jpg");
    });})
</script>
<section class="clients_section">
    <div class="pattern_top"></div>
    <div class="center"></div>
</section>