<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 29.05.14
 * Time: 18:08
 */

?>

<div style="display: none;">
    <div id="load-image">
        <span class="btn btn-success fileinput-button">
            <span>Выберите файл...</span>
            <input id="fileupload" type="file" name="files[]" multiple>
        </span>
        <br>
        <!-- The global progress bar -->
        <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <!-- The container for the uploaded files -->
        <div id="files" class="files"></div>
        <div id="crop_img">
            <div class="inline-labels">
                <input type="hidden" size="4" id="x1" name="x1"/>
                <input type="hidden" size="4" id="y1" name="y1"/>
                <input type="hidden" size="4" id="x2" name="x2"/>
                <input type="hidden" size="4" id="y2" name="y2"/>
                <input type="hidden" size="4" id="w" name="w" />
                <input type="hidden" size="4" id="h" name="h" />
                <input type="hidden" size="4" id="fw" name="fw" />
                <input type="hidden" size="4" id="fh" name="fh" />
                <br/><span id="crop_img_release" class="btn btn-success">Обрезать</span>
            </div>
            <br/>
        </div>
        <br>
    </div>
</div>

<script>
    var jcrop_api;
    var paste_selector = '<?php echo $selector;?>';
    $('.refresh').on('click', function(){
        $('#files').html('');
        $('.inline-labels').hide();
        if (jcrop_api) jcrop_api.destroy();
        $('#crop_img img').remove();
        $('#progress .progress-bar').css('width', '0%');
        $.fancybox.open('#load-image');
    });

    $('.ph_refresh').on('click', function(){
        $('#files').html('');
        $('.inline-labels').hide();
        if (jcrop_api) jcrop_api.destroy();
        $('#crop_img img').remove();
        $('#progress .progress-bar').css('width', '0%');
        $.fancybox.open('#load-image');
        paste_selector = $('.photo_list li.selected input').attr('id');
    });

    $('#crop_img_release').on('click', function(){
        $.ajax({
            url: '/site/cropimg',
            beforeSend: function(){
                $('#files').html('');
            },
            data: {
                x1:$('#x1').val(),
                y1:$('#y1').val(),
                x2:$('#x2').val(),
                y2:$('#y2').val(),
                w:$('#w').val(),
                h:$('#h').val(),
                fw:$('#fw').val(),
                fh:$('#fh').val(),
                url:$('#crop_img img').attr('src'),
                folder:'<?php echo $folder;?>'
            },
            dataType: 'html',
            error: function(jqXHR, textStatus){
                $('#files').html('Не удалось обрезать изображение!');
            },
            success: function(data){
                $('.product_thumb img').attr('src', data);
                $('.product_thumb .fancybox_image').attr('href', data);

                $('#'+paste_selector+'_img').attr('src', data);
                $('#'+paste_selector).attr('value', data);
                setDirty($('#'+paste_selector).closest('form'));

                $.fancybox.close();
            },
            type: 'post'
        });
    });

    function showCoords(c){
        $('#x1').val(c.x);
        $('#y1').val(c.y);
        $('#x2').val(c.x2);
        $('#y2').val(c.y2);
        $('#w').val(c.w);
        $('#h').val(c.h);
        $('#fw').val($('#crop_img img').width());
        $('#fh').val($('#crop_img img').height());
    };

    function clearCoords(){
        $('#crop_img input').val('');
    };

    $(function () {
        'use strict';
        $('#fileupload').fileupload({
            url: '/site/loadimg',
            dataType: 'json',
            done: function (e, data) {
                var errors = 0;
                $.each(data.result.files, function (index, file) {
                    if (file.error){
                        $('#files').html(file.error);
                        errors = 1;
                    }else{
                        $('#files').html('');
                        //$('.product_thumb img').attr('src', file.thumbnailUrl);
                        //$('#'+paste_selector).attr('value', file.url);
                        $('#crop_img').append('<img src="' + file.url + '" alt=""/>');
                        $(window).resize();
                        $('#crop_img img').Jcrop({
                            onChange:   showCoords,
                            onSelect:   showCoords,
                            onRelease:  clearCoords,
                            setSelect:   [0, 0, 640, 640],
                            aspectRatio: 1,
                            bgColor: '#F9F9F9',
                            minSize:   [60, 60],
                            maxSize:   [640, 640]
                        },function(){
                            jcrop_api = this;
                        });
                        $('.inline-labels').show();
                    }
                });
                //if (!errors) $.fancybox.close();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css('width', progress + '%');
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>