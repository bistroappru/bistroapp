-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 30 2014 г., 11:51
-- Версия сервера: 5.5.40
-- Версия PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `bistroappru`
--

-- --------------------------------------------------------

--
-- Структура таблицы `app_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `app_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_auth_assignment`
--

INSERT INTO `app_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;'),
('owner', '2', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `app_auth_item`
--

CREATE TABLE IF NOT EXISTS `app_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_auth_item`
--

INSERT INTO `app_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'СуперАдмин', NULL, 'N;'),
('barman', 2, 'Бармен', NULL, 'N;'),
('companyCreate', 0, 'Создание компании', NULL, 'N;'),
('companyDelete', 0, 'Удаление компании', NULL, 'N;'),
('companyOwnUpdate', 1, 'Обновление своей компании', 'return Yii::app()->user->company_id==$params["company"]->id;', 'N;'),
('companyUpdate', 0, 'Обновление компании', NULL, 'N;'),
('cook', 2, 'Повар', NULL, 'N;'),
('garcon', 2, 'Оффициант', NULL, 'N;'),
('guest', 2, 'Гость', NULL, 'N;'),
('manager', 2, 'Администратор ресторана', NULL, 'N;'),
('mobile', 2, 'Девайс', NULL, 'N;'),
('mobile_guest', 2, 'Девайс (гость)', NULL, 'N;'),
('owner', 2, 'Владелец сети', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `app_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `app_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_auth_item_child`
--

INSERT INTO `app_auth_item_child` (`parent`, `child`) VALUES
('manager', 'barman'),
('admin', 'companyCreate'),
('admin', 'companyDelete'),
('owner', 'companyOwnUpdate'),
('admin', 'companyUpdate'),
('companyOwnUpdate', 'companyUpdate'),
('manager', 'cook'),
('manager', 'garcon'),
('owner', 'manager'),
('mobile', 'mobile_guest'),
('admin', 'owner');

-- --------------------------------------------------------

--
-- Структура таблицы `app_company`
--

CREATE TABLE IF NOT EXISTS `app_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(500) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `type` varchar(255) NOT NULL,
  `kitchen` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `workplaces` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `app_company`
--

INSERT INTO `app_company` (`id`, `name`, `logo`, `hidden`, `deleted`, `description`, `type`, `kitchen`, `website`, `email`, `prefix`, `workplaces`) VALUES
(1, 'Тануки', '/data/images/logos/1401926957_c8b85841d570f75773a922451541676d.jpeg', 0, 0, 'Рестораны Тануки заметно отличаются своим аутентичным интерьером. Частокол из бамбука, стены с иероглифами и статуэтками, светильники из рисовой бумаги, большие столы, сделанные не для экономии места, а для удобства Гостей, впрочем, как и всё в ресторанах Тануки', '', '', 'http://www.tanuki.ru/', 'hotline@tanuki.ru', 'tanuki', '1,2'),
(2, 'Япоша', '/data/images/logos/yaposha.jpg', 0, 0, '«Япоша» — сеть популярных демократичных японских ресторанов с яркой, жизнеутверждающей концепцией, с оригинальным двойным меню – суши и антисуши. Мы подарим Вам счастливые минуты беззаботного отдыха в кругу семьи и друзей.', '', '', '', '', 'yapo', ''),
(3, 'Якитория', '/data/images/logos/yakitoriya.jpg', 0, 0, 'Сеть культовых кафе авторской японской кухни «Якитория» – самый масштабный проект ассоциации ресторанов "Веста-центр интернешнл". Городские кафе, удобно расположенные вблизи станций метро в Москве, предлагают лучшую японскую кухню в городе.', '', '', '', '', 'yaki', ''),
(4, 'IL PATIO', '/data/images/logos/il_patio.jpg', 0, 0, '', '', '', '', '', 'patio', ''),
(6, 'Кофе Хауз', '/data/images/logos/1403777097_b54435daaf447e4d08027640ee117ff2.jpeg', 0, 0, '', '', '', '', '', 'house', ''),
(7, 'шоколадница', '/data/images/logos/1403786730_a08d079201683af1763fae7e6f6e6270.jpeg', 1, 1, '', '', '', '', '', 'choko', ''),
(8, 'тестовая', '/data/images/logos/1404304731_6c88a64d5f598c51d6a8e9e9aa69fc3b.png', 0, 1, '', '', '', '', '', 'test', '');

-- --------------------------------------------------------

--
-- Структура таблицы `app_company_type`
--

CREATE TABLE IF NOT EXISTS `app_company_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `app_company_type`
--

INSERT INTO `app_company_type` (`id`, `name`) VALUES
(1, 'Ресторан'),
(2, 'Кафе'),
(3, 'Бар'),
(4, 'Кофейня'),
(5, 'Пиццерия'),
(6, 'Суши-бар');

-- --------------------------------------------------------

--
-- Структура таблицы `app_condition`
--

CREATE TABLE IF NOT EXISTS `app_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_id` int(11) NOT NULL,
  `measure` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `action_on` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=102 ;

--
-- Дамп данных таблицы `app_condition`
--

INSERT INTO `app_condition` (`id`, `position_id`, `measure`, `sort`, `price`, `action_on`) VALUES
(16, 9, '1000 мл.', 0, 150, 0),
(25, 8, '200 гр.', 0, 250, 1),
(30, 17, '8 шт.', 0, 95, 0),
(31, 18, '8 шт.', 0, 145, 0),
(32, 19, '1 шт.', 0, 355, 0),
(33, 20, '1 шт.', 0, 325, 0),
(34, 21, '6 шт.', 0, 95, 0),
(35, 22, '1 шт.', 0, 235, 0),
(38, 25, '1 шт.', 0, 290, 0),
(39, 26, '1000 мл.', 0, 110, 0),
(40, 27, '330 мл.', 0, 145, 0),
(41, 28, '250 мл.', 0, 90, 0),
(42, 29, '1000 мл.', 0, 120, 0),
(43, 30, '1 шт.', 0, 310, 0),
(44, 31, '1 шт.', 0, 245, 0),
(45, 32, '1 шт.', 0, 360, 0),
(46, 33, '1 шт.', 0, 235, 0),
(47, 34, '1 шт.', 0, 290, 0),
(48, 35, '1 шт.', 0, 1230, 0),
(49, 36, '1 шт.', 0, 2790, 0),
(50, 37, '1 шт.', 0, 1110, 0),
(51, 38, '1 шт.', 0, 310, 0),
(52, 39, '1 шт.', 0, 440, 0),
(53, 40, '1 шт.', 0, 35, 0),
(54, 41, '1 шт.', 0, 75, 0),
(55, 42, '1 шт.', 0, 70, 0),
(56, 43, '1 шт.', 0, 90, 0),
(57, 44, '1 шт.', 0, 80, 0),
(69, 48, '500 мл.', 0, 150, 0),
(70, 49, '150 мл.', 0, 50, 0),
(71, 49, '250 мл.', 0, 100, 0),
(72, 49, '500 мл.', 0, 200, 0),
(74, 51, 'Основная', 0, 115, 0),
(75, 52, 'Основная', 0, 150, 0),
(76, 53, 'Основная', 0, 220, 0),
(77, 54, 'Основная', 0, 245, 0),
(78, 55, 'Основная', 0, 247, 0),
(79, 56, 'Основная', 0, 299, 0),
(80, 57, 'Основная', 0, 287, 0),
(81, 58, 'Основная', 0, 237, 0),
(82, 59, 'Основная', 0, 100, 0),
(83, 60, 'комбо', 0, 250, 0),
(88, 24, '1 шт.', 0, 260, 0),
(90, 63, '1 шт.', 1, 100, 0),
(91, 64, 'Общая стоимость', 1, 250, 0),
(92, 65, '1 шт.', 1, 20, 0),
(93, 66, '1 шт.', 1, 345, 0),
(101, 23, '1 шт.', 1410877406, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `app_group_details`
--

CREATE TABLE IF NOT EXISTS `app_group_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `condition_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `app_group_details`
--

INSERT INTO `app_group_details` (`id`, `group_id`, `sort`, `condition_id`) VALUES
(20, 64, 0, 92);

-- --------------------------------------------------------

--
-- Структура таблицы `app_kitchen`
--

CREATE TABLE IF NOT EXISTS `app_kitchen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `app_kitchen`
--

INSERT INTO `app_kitchen` (`id`, `name`) VALUES
(1, 'Авторская'),
(2, 'Азиатская'),
(3, 'Американская'),
(4, 'Арабская'),
(5, 'Армянская'),
(6, 'Армянская'),
(7, 'Белорусская'),
(8, 'Вегетарианская'),
(9, 'Восточная'),
(10, 'Грузинская'),
(11, 'Домашняя'),
(12, 'Европейская'),
(13, 'Индийская'),
(14, 'Испанская'),
(15, 'Итальянская'),
(16, 'Кавказская'),
(17, 'Китайская'),
(18, 'Кубинская'),
(19, 'Кухня народов СССР'),
(20, 'Мексиканская'),
(21, 'Народная'),
(22, 'Немецкая'),
(23, 'Русская'),
(24, 'Северная'),
(25, 'Славянская'),
(26, 'Средиземноморская'),
(27, 'Старобелорусская'),
(28, 'Старославянская'),
(29, 'Тайская'),
(30, 'Узбекская'),
(31, 'Украинская'),
(32, 'Французская'),
(33, 'Чешская'),
(34, 'Японская');

-- --------------------------------------------------------

--
-- Структура таблицы `app_label`
--

CREATE TABLE IF NOT EXISTS `app_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `style_id` varchar(125) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `app_label`
--

INSERT INTO `app_label` (`id`, `text`, `style_id`) VALUES
(1, 'ХИТ', 'hit'),
(2, 'НОВИНКА', 'new');

-- --------------------------------------------------------

--
-- Структура таблицы `app_label_style`
--

CREATE TABLE IF NOT EXISTS `app_label_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classname` varchar(100) NOT NULL,
  `style` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `app_label_style`
--


-- --------------------------------------------------------

--
-- Структура таблицы `app_menu`
--

CREATE TABLE IF NOT EXISTS `app_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `app_menu`
--

INSERT INTO `app_menu` (`id`, `out_id`, `name`, `company_id`, `parent_id`, `sort`, `description`, `image`, `hidden`, `deleted`, `version`, `imageVersion`) VALUES
(1, '', 'Закуски', 1, 0, 0, 'Описание "Закуски"\r\nЗдесь могла бы быть  ваша реклама.', '/data/images/menu/1404301480_40b5de0c3abc86700b15cd62a6aa0793.png', 0, 0, 15, 5),
(2, '', 'Напитки', 1, 0, 1, 'Описание "Напитки"', '/data/images/menu/74f4f3fba4992673c6568afaa6e8748f.png', 0, 0, 16, 0),
(3, '', 'Роллы', 1, 0, 2, 'Описание "Роллы"', '/data/images/menu/ee927ac9dd447449d100a297c55a7030.png', 0, 0, 17, 0),
(4, '', 'Салаты', 1, 0, 5, 'Описание "Салаты"', '/data/images/menu/a0bb357344944233001a7aa70df48faf.png', 0, 0, 0, 0),
(5, '', 'Сашими', 1, 0, 4, 'Описание "Сашими"', '/data/images/menu/5acfabbff1963fe83ec0a5b671625f44.png', 0, 1, 0, 0),
(6, '', 'Сеты', 1, 0, 6, 'Описание "Сети"', '/data/images/menu/c094dfc66865c2bca0e109286ee7ca1c.png', 0, 0, 0, 0),
(7, '', 'Суши', 1, 0, 7, 'Описание "Суши"', '/data/images/menu/a00e1192e3e7cd2c07a28c9974952063.png', 0, 0, 0, 0),
(9, '', 'Антисуши', 2, 0, 0, '', '/data/images/menu/1403777336_952582388a081cda41d8fb3ab6a7f448.png', 0, 0, 16, 0),
(10, '', 'Супы', 3, 0, 0, 'Супы японской кухни', '/data/images/menu/1403778236_f89f3b28186a4fc7b2a495315f14535d.png', 0, 0, 0, 0),
(11, '', 'Шоколад', 7, 0, 0, 'вкусно и питательно', '/data/images/menu/1403786888_9502a5ed48137020cfe8ff8f50b52730.jpeg', 0, 0, 0, 0),
(12, '', 'Супы', 7, 0, 0, '', '', 0, 0, 0, 0),
(13, '', 'Бизнес-ланчи', 1, 0, 3, 'djkfhkjd', '', 0, 0, 0, 0),
(14, '', 'Супы', 6, 0, 0, '', '', 0, 0, 0, 0),
(15, '', 'закуски', 6, 0, 0, '', '', 0, 0, 0, 0),
(18, '', 'первая', 8, 0, 0, '', '', 0, 0, 1, 1),
(19, '', 'вторые блюда', 3, 0, 1, '', '', 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_order`
--

CREATE TABLE IF NOT EXISTS `app_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `table` varchar(50) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `date_create` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `attention` tinyint(1) NOT NULL DEFAULT '0',
  `separated` tinyint(1) NOT NULL DEFAULT '0',
  `separated_info` text NOT NULL,
  `date_execute` int(11) NOT NULL,
  `date_close` int(11) NOT NULL,
  `comment` text NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `garcon_id` int(11) NOT NULL,
  `count_items` int(11) NOT NULL,
  `total_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Дамп данных таблицы `app_order`
--

INSERT INTO `app_order` (`id`, `user_id`, `table`, `status_id`, `date_create`, `payment_id`, `attention`, `separated`, `separated_info`, `date_execute`, `date_close`, `comment`, `restaurant_id`, `garcon_id`, `count_items`, `total_price`) VALUES
(30, 1, '2', 2, 1401187411, 1, 0, 0, '', 0, 0, '', 3, 0, 1, 1000),
(31, 1, '2', 6, 1401187433, 2, 0, 0, '', 0, 1413209166, '', 3, 0, 1, 1000),
(32, 1, '2', 2, 1401187490, 1, 0, 0, '', 0, 0, '', 3, 0, 1, 250),
(35, 1, '2', 2, 1401193230, 1, 0, 0, '', 0, 0, '', 3, 0, 1, 180),
(38, 1, '2', 2, 1405688493, 1, 1, 1, 'текст раздельного заказа', 0, 0, '', 3, 5, 2, 720),
(39, 1, '1', 2, 1405689407, 2, 0, 0, '', 0, 0, '', 3, 5, 2, 595),
(40, 1, '1', 2, 1405689418, 1, 0, 0, '', 0, 0, '', 3, 5, 2, 260),
(41, 1, '1', 2, 1405689478, 2, 0, 0, '', 0, 0, '', 3, 5, 2, 350),
(42, 1, '1', 2, 1405689506, 1, 0, 0, '', 0, 0, '', 3, 5, 2, 260),
(43, 1, '1', 2, 1405689660, 2, 0, 0, '', 0, 0, '', 3, 5, 2, 645),
(44, 1, '1', 2, 1406146020, 1, 0, 0, '', 0, 0, '', 3, 5, 1, 285),
(45, 1, '1', 2, 1406190524, 2, 0, 0, '', 0, 0, '', 3, 5, 1, 1230),
(46, 1, '1', 2, 1406622304, 2, 0, 0, '', 0, 0, '', 3, 5, 2, 1235),
(47, 1, '1', 2, 1406624905, 1, 0, 0, '', 0, 0, '', 3, 5, 2, 345),
(48, 1, '1', 2, 1407277854, 2, 0, 0, '', 0, 0, '', 3, 5, 2, 690),
(49, 1, '1', 2, 1407779319, 2, 0, 0, '', 0, 0, '', 3, 5, 1, 190),
(50, 1, '1', 2, 1407780478, 1, 0, 0, '', 0, 0, '', 3, 5, 1, 580),
(51, 1, '1', 2, 1407850965, 2, 0, 0, '', 0, 0, '', 3, 5, 2, 500),
(52, 1, '1', 1, 1408142013, 2, 0, 0, '', 0, 0, '', 3, 5, 7, 3090),
(53, 1, '1', 1, 1413834451, 2, 0, 0, '', 0, 0, '', 3, 5, 3, 530),
(54, 1, '1', 1, 1413834511, 2, 0, 0, '', 0, 0, '', 3, 5, 3, 515),
(55, 1, '1', 1, 1413834565, 2, 0, 0, '', 0, 0, '', 3, 5, 4, 505),
(56, 1, '1', 1, 1413834584, 2, 0, 0, '', 0, 0, '', 3, 5, 3, 530),
(57, 1, '2', 1, 1414597564, 1, 1, 1, 'текст раздельного заказа', 0, 0, '', 3, 5, 1, 450);

-- --------------------------------------------------------

--
-- Структура таблицы `app_order_item`
--

CREATE TABLE IF NOT EXISTS `app_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Дамп данных таблицы `app_order_item`
--

INSERT INTO `app_order_item` (`id`, `order_id`, `condition_id`, `status_id`, `price`, `count`) VALUES
(20, 30, 25, 9, 250, 4),
(21, 31, 25, 9, 250, 4),
(22, 32, 25, 8, 250, 1),
(25, 35, 41, 11, 90, 2),
(29, 38, 41, 9, 90, 3),
(30, 38, 16, 9, 150, 3),
(31, 39, 25, 9, 250, 2),
(32, 39, 36, 9, 95, 1),
(35, 40, 39, 8, 110, 1),
(36, 40, 16, 8, 150, 1),
(37, 41, 83, 9, 250, 1),
(38, 41, 82, 9, 100, 1),
(39, 42, 39, 9, 110, 1),
(40, 42, 16, 9, 150, 1),
(41, 43, 31, 9, 145, 2),
(43, 43, 32, 9, 355, 1),
(44, 44, 36, 9, 95, 3),
(45, 45, 48, 9, 1230, 1),
(46, 46, 35, 9, 235, 1),
(47, 46, 25, 9, 250, 4),
(48, 47, 36, 9, 95, 1),
(49, 47, 91, 9, 250, 1),
(50, 48, 36, 9, 95, 2),
(51, 48, 25, 9, 250, 2),
(52, 49, 36, 9, 95, 2),
(53, 50, 38, 9, 290, 2),
(55, 51, 25, 9, 250, 1),
(56, 51, 25, 9, 250, 1),
(57, 52, 35, 9, 235, 1),
(59, 52, 38, 8, 290, 2),
(60, 52, 91, 8, 250, 1),
(61, 52, 48, 8, 1230, 1),
(62, 52, 16, 8, 150, 1),
(63, 52, 31, 8, 145, 2),
(64, 52, 32, 8, 355, 1),
(65, 53, 40, 1, 145, 2),
(66, 53, 41, 1, 90, 1),
(67, 53, 16, 1, 150, 1),
(68, 54, 39, 1, 110, 2),
(69, 54, 16, 1, 150, 1),
(70, 54, 40, 1, 145, 1),
(71, 55, 41, 1, 90, 1),
(72, 55, 42, 1, 120, 1),
(73, 55, 40, 1, 145, 1),
(74, 55, 16, 1, 150, 1),
(75, 56, 16, 1, 150, 1),
(76, 56, 40, 1, 145, 2),
(77, 56, 41, 1, 90, 1),
(78, 57, 16, 1, 150, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `app_order_payment`
--

CREATE TABLE IF NOT EXISTS `app_order_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `app_order_payment`
--

INSERT INTO `app_order_payment` (`id`, `name`) VALUES
(1, 'Наличными'),
(2, 'Картой');

-- --------------------------------------------------------

--
-- Структура таблицы `app_order_status`
--

CREATE TABLE IF NOT EXISTS `app_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'order',
  `prev_id` tinyint(1) NOT NULL,
  `next_id` tinyint(1) NOT NULL,
  `roles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `app_order_status`
--

INSERT INTO `app_order_status` (`id`, `name`, `type`, `prev_id`, `next_id`, `roles`) VALUES
(1, 'Новый', 'order', 0, 0, 'admin,owner,manager'),
(2, 'Принят', 'order', 0, 0, 'admin,owner,manager'),
(3, 'Оплачен', 'order', 0, 0, 'admin,owner,manager,garcon'),
(6, 'Закрыт', 'order', 0, 0, 'admin,owner,manager,garcon'),
(8, 'Новый', 'item', 0, 9, 'admin,owner,manager,garcon'),
(9, 'Принято', 'item', 8, 10, 'admin,owner,manager,garcon'),
(10, 'Готовится', 'item', 9, 11, 'admin,owner,manager,barman,cook'),
(11, 'Готово', 'item', 10, 12, 'admin,owner,manager,barman,cook'),
(12, 'Доставлено', 'item', 11, 0, 'admin,owner,manager,garcon');

-- --------------------------------------------------------

--
-- Структура таблицы `app_position`
--

CREATE TABLE IF NOT EXISTS `app_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `is_group` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `depressed` tinyint(1) NOT NULL DEFAULT '0',
  `label_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `calories` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `in_stock` varchar(500) NOT NULL,
  `food_type` tinyint(1) NOT NULL DEFAULT '1',
  `workplace` tinyint(1) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Дамп данных таблицы `app_position`
--

INSERT INTO `app_position` (`id`, `out_id`, `is_group`, `name`, `description`, `menu_id`, `sort`, `depressed`, `label_id`, `image`, `hidden`, `calories`, `deleted`, `in_stock`, `food_type`, `workplace`, `version`, `imageVersion`) VALUES
(8, '', 0, 'Калифорния рору', 'Мясо краба, авокадо, руккола', 1, 3, 0, 2, '/data/images/positions/1401927643_297cc5cc806b05ef6c055377170e81f8.jpeg', 0, '', 0, '3,4', 1, 1, 77, 3),
(9, '', 0, 'Сок апельсиновый', 'Пакетированный апельсиновый сок\r\n\r\n21313234', 2, 1, 0, 0, '/data/images/positions/e5c0c07963cc3d3d0b75bb704091eda2.jpeg', 0, '', 0, '', 2, 1, 0, 0),
(17, '', 0, 'Абокадо ролл', 'Классический ролл с авокадо', 3, 1, 0, 0, '/data/images/positions/f0837c2e091cd50ce59b12b03a585bcd.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(18, '', 0, 'Сякэ ролл', 'Классический ролл с лососем\r\n\r\nСостав:(рис, водоросли)', 3, 0, 0, 0, '/data/images/positions/1b63e7273a45181773e764dd46468ae7.jpeg', 0, '', 0, '', 1, 1, 7, 0),
(19, '', 0, 'Калифорния', 'Ролл с мясом краба, авокадо, огурцом, тобико и майонезом', 3, 2, 0, 0, '/data/images/positions/89a1127cd2d987cc4918949480db00dc.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(20, '', 0, 'Филадельфия', 'Ролл с лососем, мягким сыром, зеленым луком, кунжутом, огурцом и авокадо', 3, 3, 0, 0, '/data/images/positions/d3b5ac1126fd045fb69c591d789cd343.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(21, '', 0, 'Каппа ролл', 'Классический ролл с огурцом', 3, 4, 0, 0, '/data/images/positions/83d648b0a2638c769651c334e5c5fad9.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(22, '', 0, 'Гюнику татаки', 'Маринованная говядина с перцем и чесноком, дайконом, салатом, лимоном и луком, украшается петрушкой \r\n\r\n+ соус Каниши', 1, 4, 0, 2, '/data/images/positions/709d251f56858dff4da693b1a46337a2.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(23, '', 0, 'Кимучи', 'Острый салат из капусты, моркови, яблока, лука-порей и дайкона', 1, 2, 0, 1, '/data/images/positions/1404307274_2fb3eec1550ba464fbd732c607568722.jpeg', 0, '', 0, '', 1, 1, 118, 4),
(24, '', 0, 'Сякэ усугири', 'Ломтики лосося с перцем чили и соусом "Юзу"', 1, 5, 0, 0, '/data/images/positions/cb43416d18773ec2bc624ae03e839a0c.jpeg', 0, '', 0, '', 1, 1, 98, 0),
(25, '', 0, 'Торинику соба', 'Гречневая лапша с курицей, морковью, кунжутом и соусом "Юзу-понзу"', 1, 6, 0, 0, '/data/images/positions/dacc7b13857bc54a87ce851b3734ede7.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(26, '', 0, 'Спрайт', 'Газированный напиток Спрайт', 2, 0, 0, 0, '/data/images/positions/bfd78bb7f13c5cae729e72ea1ab008f9.jpeg', 0, '', 0, '', 2, 1, 103, 0),
(27, '', 0, ' Перье', 'Минеральная вода Перье', 2, 2, 0, 0, '/data/images/positions/2bc6f91d3cef70ab4805a42e43829ebc.jpeg', 0, '', 0, '', 2, 1, 104, 0),
(28, '', 0, 'Кока-кола', 'Газированный напиток Кока-кола', 2, 3, 0, 0, '/data/images/positions/31ab3caf52db4a6f0cae3b9cebbb7407.jpeg', 0, '', 0, '', 2, 1, 105, 0),
(29, '', 0, 'Нести Лимон', 'Холодный чай Нести Лимон', 2, 4, 0, 0, '/data/images/positions/2461b677fe0d18d2f665937a687e23b2.jpeg', 0, '', 0, '', 2, 1, 106, 0),
(30, '', 0, ' Рифу сарада', 'Кальмар, мидии, креветки с листьями салата корн, базиликом, кинзой и острым соусом', 4, 0, 0, 2, '/data/images/positions/c7d2f2cc2c8558421a038ca84e7e4e92.jpeg', 0, '', 0, '', 1, 1, 7, 0),
(31, '', 0, '«Цезарь-сан» с курицей', 'Салат с курицей, салатом айсберг, чесночными гренками, отварным яйцом, черри и сыром грана падано', 4, 0, 0, 0, '/data/images/positions/db6fda642133122a0a9390263e465106.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(32, '', 0, 'Дайкани сарада', 'Салат с мясом краба, апельсином и тобико', 4, 0, 0, 0, '/data/images/positions/3033a1c3b159c0d0e7a3369353187302.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(33, '', 0, 'Осэй сарада', 'Салат из пикантной курицы с салатом айсберг, сельдереем, авокадо, огурцами и сладким перцем', 4, 0, 0, 0, '/data/images/positions/7f2c9de3808410e00f15c0040849c4f1.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(34, '', 0, 'Гюнику сарада', 'Говядина с томатами, огурцом и зелёным луком', 4, 0, 0, 0, '/data/images/positions/dc407fad2b77d7b9ca81fcdca7b576d8.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(35, '', 0, 'Тануки сет', 'Сяке темпура - 1\r\nФиладельфия -1\r\nЭби сирогома -1\r\nСяке кадо - 1\r\nХотате караи - 1\r\nПорция васаби XL (30г) - 1\r\nПорция имбиря XL (60г) - 1', 6, 0, 0, 0, '/data/images/positions/75322a8622bee9eaba6e554ded571ec7.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(36, '', 0, 'Харакири сет', 'Дракон ролл - 1\r\nОвара ролл - 1\r\nЙонака ролл - 1\r\nОкинава ролл - 1\r\nКалифорния - 2\r\nФиладельфия - 1\r\nХигата ролл - 1\r\nМексиканский ролл - 1\r\nХияши унаги - 1\r\nУнаги урамаки - 1\r\nПорция васаби XL (30 г) - 2\r\nПорция имбиря XL (60 г) - 2\r\n', 6, 0, 0, 0, '/data/images/positions/04279f8326e750e85a75a50571878969.jpeg', 0, '300', 0, '', 1, 1, 0, 0),
(37, '', 0, 'Суши сет', 'Суши с угрем - 5\r\nСуши с лососем - 5\r\nСуши с тунцом - 5\r\nСуши с лакедрой - 2\r\nПорция васаби (10г) - 2\r\nПорция имбиря (20г) - 2\r\n', 6, 0, 0, 0, '/data/images/positions/251fdbc662899a16ed50ca94cc548864.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(38, '', 0, 'Якинику мориавасэ', 'Ассорти мясное: шашлычки из куриного фарша, говядины, свинины и кукурузы', 6, 0, 0, 0, '/data/images/positions/7981d6b7a38d03d12b639fc0d9fa7c1e.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(39, '', 0, 'Сифудо мориавасэ', 'Ассорти из морепродуктов: шашлычки из креветок, морского гребешка, лосося и осьминожек', 6, 0, 0, 0, '/data/images/positions/a04ea60470ca58ddfc36804ab98ce963.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(40, '', 0, 'Сякэ', 'Суши нигири с лососем', 7, 1, 0, 0, '/data/images/positions/76a954a2f113df9109946ce5c9d10250.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(41, '', 0, 'Хотатэ', 'Суши нигири с морским гребешком', 7, 2, 0, 0, '/data/images/positions/cdf8a124550b9cf2c472c5c5dcbd6b2d.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(42, '', 0, 'Эби', 'Суши нигири с вареной сладкой креветкой, маринованной в имбирном соусе', 7, 0, 0, 0, '/data/images/positions/b4e6e929f2c157942f4c90bede5e707b.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(43, '', 0, 'Икура', 'Суши гункан с икрой лосося', 7, 3, 0, 0, '/data/images/positions/d4b9f8240ae9d09674b50e5276aeece5.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(44, '', 0, 'Спайси сякэ', 'Острый суши гункан с лососем', 7, 4, 0, 0, '/data/images/positions/4780ead67ef309276efd0327436c4cef.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(48, '', 0, 'Напиток для ланча 1', '', 2, 5, 1, 0, '', 0, '', 0, '4', 2, 1, 107, 0),
(49, '', 0, 'Напиток для ланча 2', '', 2, 6, 1, 0, '', 0, '', 0, '3,4', 2, 1, 108, 0),
(51, '', 0, 'Вареники с картошкой', 'Домашние вареники ручной лепки с картофелем. Подаются со сметаной.', 9, 1, 0, 0, '/data/images/positions/1403777530_9f2df9e3815e570d992c3d3f9066ecd3.jpeg', 0, '', 0, '7', 1, 1, 1, 0),
(52, '', 0, 'Сибирские пельмени', 'Домашние пельмени, ручной лепки, с нежным мясом. Подаются со сметаной.', 9, 0, 0, 0, '/data/images/positions/1403777673_7f785013ba9c808fd9d1569cea05a442.jpeg', 0, '', 0, '7', 1, 1, 0, 0),
(53, '', 0, 'Жареная картошечка с грибами', 'Вкус знакомый с детства, настоящая картошка с грибами по домашнему.', 9, 2, 0, 0, '/data/images/positions/1403777736_14e2e8157345fb67f3c3abbca2563b00.jpeg', 0, '', 0, '7', 1, 1, 0, 0),
(54, '', 0, 'Домашние котлеты с курицей', 'Домашние котлеты подаются с картофелем и соусом.', 9, 3, 0, 0, '/data/images/positions/1403777806_fbe1fa3d940d51939a8037f9a9ee4641.jpeg', 0, '', 0, '7', 1, 1, 0, 0),
(55, '', 0, 'Ао чиз', 'крем-суп из голубого сыра, с креветками', 10, 0, 0, 0, '/data/images/positions/1403778356_4eb753196945f76f362eb00e77b04036.jpeg', 0, '', 0, '8', 1, 1, 0, 0),
(56, '', 0, 'Янагава набэ', 'набэ из копченого угря', 10, 0, 0, 0, '/data/images/positions/1403778489_21cb40c0718c77d529d29825a946a7fb.jpeg', 0, '', 0, '8', 1, 1, 0, 0),
(57, '', 0, 'Том Ям набэ', 'традиционный кокосовый суп с креветками и грибами. Сервируется рисом "Гохан"', 10, 0, 0, 0, '/data/images/positions/1403778545_dfa04c8886ee9729a5378a8063be92d8.jpeg', 0, '', 0, '', 1, 1, 0, 0),
(58, '', 0, 'Нику рамэн', 'суп-лапша со свининой и яйцом', 10, 0, 0, 0, '/data/images/positions/1403778650_c0fdcdd3a8f8f4ff71504a7e34659070.jpeg', 0, '', 0, '8', 1, 1, 0, 0),
(59, '', 0, 'Борщ', '', 12, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0),
(60, '', 1, 'Бизнес ланч 1', '', 12, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0),
(63, '', 0, 'Борщ', '', 14, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0),
(64, '', 1, 'Лайт', '', 13, 0, 0, 0, '', 0, '', 0, '', 1, 1, 93, 0),
(65, '', 0, 'рпг', '', 13, 0, 0, 0, '', 0, '', 0, '', 1, 1, 0, 0),
(66, '', 0, '345345', '', 18, 0, 0, 0, '', 0, '', 0, '', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `app_restaurant`
--

CREATE TABLE IF NOT EXISTS `app_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `working_hours` varchar(255) NOT NULL,
  `map` varchar(500) NOT NULL,
  `photos` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `app_restaurant`
--

INSERT INTO `app_restaurant` (`id`, `out_id`, `deleted`, `name`, `location`, `hidden`, `company_id`, `description`, `phone`, `working_hours`, `map`, `photos`) VALUES
(3, '', 0, 'Борисовские пруды', 'г. Москва, ул Борисовские Пруды, д 10', 0, 1, 'Лучший ресторан', '+7 (555) 100-02-30', 'Пн-Пт 10:00-23:00', '{"zoom":11,"coords":{"x":55.635869,"y":37.740974},"place":{"city":"7700000000000","street":{"id":"77000000000047100","name":"Борисовские Пруды","zip":null,"type":"Улица","typeShort":"ул","okato":null,"contentType":"street"},"building":{"id":"7700000000004710001","name":"10","zip":115211,"type":"дом","typeShort":"д","okato":"45296557000","contentType":"building"}}}', '["\\/data\\/images\\/restaurants\\/1406195747_0cf8570408c51a95107b73e5d6844e8b.jpeg"]'),
(4, '', 0, 'Боровское шоссе, д.31', 'г. Москва, ш. Боровское, д. 31', 0, 1, '', '+7 (111) 222-33-44', '', '{"zoom":13,"coords":{"x":55.642453,"y":37.361517},"place":{"city":"7700000000000","street":{"id":"77000000000089000","name":"Боровское","zip":null,"type":"Шоссе","typeShort":"ш","okato":null,"contentType":"street"},"building":{"id":"7700000000008900030","name":"31","zip":119633,"type":"дом","typeShort":"д","okato":"45268577000","contentType":"building"}}}', ''),
(6, '', 0, 'ул. Большая Академическая, д.65', 'г. Москва, ул. Академическая Б., д. 65', 0, 1, 'Посадочных мест: 198, Парковка: Есть, Wi-fi: Free, Кальянная карта: Есть', '+7 (499) 153-81-44', 'Пн-Чт (11:30 – 00:00), Пт-Сб (11:30 – 06:00), Вс.11:30 – 00:00', '{"zoom":11,"coords":{"x":55.840371,"y":37.547477},"place":{"city":"7700000000000","street":{"id":"77000000000072300","name":"Академическая Б.","zip":null,"type":"Улица","typeShort":"ул","okato":null,"contentType":"street"},"building":{"id":"7700000000007230016","name":"65","zip":125183,"type":"дом","typeShort":"д","okato":"45277580000","contentType":"building"}}}', ''),
(7, '', 0, 'шоссе Энтузиастов, 20', 'шоссе Энтузиастов, 20', 0, 2, 'Wi-Fi', '+7(495) 362-27-07', '11:00-00:00 ежедневно (до 6:00 только по праздникам), 11:00-16:00 ланч', '', ''),
(8, '', 0, 'Авиамоторная ул., 41', 'Авиамоторная ул., 41', 0, 3, 'Одно из лучших кафе нашей сети,расположенное в 1-ой минуте ходьбы от метро, с курящим и отдельным некурящим залами. На летний период — веранда на крыше.', '+7 (495) 514-02-70', 'с 10:00 до 06:00 .бронирование: с 10:00 до 12:00  (только на текущий день, в Пт, Сб, Вс, а так же праздничные дни резервы не принимаются )', '', ''),
(9, '', 0, 'Лениградка', 'Лениградка', 0, 6, '', '', '', '', ''),
(10, '', 0, 'какой-то адрес', 'какой-то адрес', 0, 7, '', '', '', '', ''),
(11, '', 0, 'маяквока', 'маяквока', 0, 6, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `app_sale_price`
--

CREATE TABLE IF NOT EXISTS `app_sale_price` (
  `condition_id` int(11) NOT NULL,
  `date_on` int(11) NOT NULL,
  `date_off` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `app_sale_price`
--


-- --------------------------------------------------------

--
-- Структура таблицы `app_user`
--

CREATE TABLE IF NOT EXISTS `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `restaurant_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `app_user`
--

INSERT INTO `app_user` (`id`, `username`, `password`, `email`, `fullname`, `role`, `blocked`, `restaurant_id`, `company_id`) VALUES
(1, 'ssa', 'e10adc3949ba59abbe56e057f20f883e', 'abc-coder@mail.ru', 'Eugene Lepeshko', 'admin', 0, 0, 0),
(2, 'tanuki_owner', 'e10adc3949ba59abbe56e057f20f883e', 'ssa@bistroapp.ru', 'Владелец Тануки', 'owner', 0, 0, 1),
(3, 'tanuki_manager', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_manager@bistroapp.ru', 'Администратор Тануки', 'manager', 0, 3, 1),
(4, 'tanuki_cook', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_cook@bistroapp.ru', 'Повар Тануки', 'cook', 0, 3, 1),
(5, 'tanuki_garcon', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_garcon@bistroapp.ru', 'Оффициант Тануки', 'garcon', 0, 3, 1),
(6, 'tanuki_barman', 'e10adc3949ba59abbe56e057f20f883e', 'lunatik_37@mail.ru', 'Бармен', 'barman', 0, 3, 1),
(7, 'tanuki_wer', 'e10adc3949ba59abbe56e057f20f883e', 'wer@bistroapp.ru', 'wer', 'barman', 0, 3, 1),
(8, 'tanuki_ert', 'e10adc3949ba59abbe56e057f20f883e', 'ert@bistroapp.ru', 'ert', 'manager', 0, 7, 2),
(9, 'tanuki_1234323', 'e10adc3949ba59abbe56e057f20f883e', '23424@mail.ru', '6324234', 'garcon', 0, 3, 1),
(10, 'tanuki_234243', 'e10adc3949ba59abbe56e057f20f883e', 'ssa23423@ewrwer.ui', '234234234', 'garcon', 0, 3, 1);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `app_auth_assignment`
--
ALTER TABLE `app_auth_assignment`
  ADD CONSTRAINT `app_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `app_auth_item_child`
--
ALTER TABLE `app_auth_item_child`
  ADD CONSTRAINT `app_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `app_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
