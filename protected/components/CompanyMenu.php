<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class CompanyMenu extends CWidget{
    public $company_id;
    public $menu_id;
    public $filter_key;
    public function run(){
        if (!$this->company_id) return;

        $criteria = new CDbCriteria();
        $criteria->select = 'id,name,image,hidden,deleted';
        $criteria->order = 'sort ASC';
        $criteria->compare('company_id', $this->company_id);

        // ---- filter
        $filter_data = explode('||', (string) Yii::app()->request->cookies[$this->filter_key]->value);
        if (!in_array('hidden', $filter_data))
            $criteria->addCondition("hidden = 0");
        if (!in_array('deleted', $filter_data))
            $criteria->addCondition("deleted = 0");


        $categories = Menu::model()->findAll($criteria);

        $this->render('_company_menu', array(
            'menu' => $categories,
            'filter_key' => $this->filter_key,
            'menu_id' => $this->menu_id,
            'company_id' => $this->company_id,
        ));
    }
}