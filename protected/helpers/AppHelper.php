<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 19.03.15
 * Time: 16:48
 */

class AppHelper {

    public static function simpleXmlAttrToArray($item){
        $out = array();
        if (is_object($item) && $item->attributes()->count()){
            foreach($item->attributes() as $k=>$v){
                $out[$k] = (string) $v;
            }
        }
        return $out;
    }
} 