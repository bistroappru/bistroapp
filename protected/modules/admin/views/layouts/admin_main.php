<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <?php $this->renderPartial('/partials/_head'); ?>
    <body>
        <div id="wrapper" class="ctrl_<?= $this->id?> act_<?= $this->action->id?>">
            <?php $this->renderPartial('/partials/_header'); ?>
            <div id="root">
                <?php echo $content; ?>
            </div>
        </div>
    </body>
</html>