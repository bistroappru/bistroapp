<?php
/* @var $this DefaultController */
$this->pageTitle = Yii::app()->name . ' - Панель управления';
?>
<section class="panel_section">
    <div class="center">
        <h1>ПАНЕЛЬ УПРАВЛЕНИЯ</h1>
        <?php $this->widget('zii.widgets.CMenu', array(
            'encodeLabel' => false,
            'htmlOptions' => array(
                'class' => 'panel_list',
            ),
            'items' => array(
                array(
                    'label' => '<span class="thumb"><img src="/images/panel-1.png" width="141" height="140" alt=""></span><span class="item_text">КОМПАНИИ</span>',
                    'url' => array('/admin/company'),
                    'visible' => Yii::app()->user->checkAccess('companyView', array('company_id' => Yii::app()->user->company_id))
                ),
                array(
                    'label' => '<span class="thumb"><img src="/images/panel-4.png" width="141" height="140" alt=""></span><span class="item_text">ПОЛЬЗОВАТЕЛИ</span>',
                    'url' => array('/admin/user'),
                    'visible' => Yii::app()->user->checkAccess('userView', array('user' => Yii::app()->user))
                ),
                array(
                    'label' => '<span class="thumb"><img src="/images/panel-2.png" width="141" height="140" alt=""></span><span class="item_text">МЕНЮ</span>',
                    'url' => array('/admin/menu'),
                    'visible' => Yii::app()->user->checkAccess('menuCategoryView', array('company_id' => Yii::app()->user->company_id))
                ),
                array(
                    'label' => '<span class="thumb"><img src="/images/panel-3.png" width="141" height="140" alt=""></span><span class="item_text">ЗАКАЗЫ</span>',
                    'url' => array('/admin/orders'),
                    'visible' => Yii::app()->user->checkAccess('inAdmin')
                ),
                array(
                    'label' => '<span class="thumb"><img src="/images/panel-5.png" width="141" height="140" alt=""></span><span class="item_text">СПРАВОЧНИКИ</span>',
                    'url' => array('/admin/references'),
                    'visible' => Yii::app()->user->checkAccess('inAdmin')
                ),
            )
        ));?>
    </div>
</section>

<section class="faq_section">
    <div class="center">
        <div class="faq_block">
            <h2>КАК ПОЛЬЗОВАТЬСЯ ПРИЛОЖЕНИЕМ</h2>
            <p><a href="#" class="faq_download">FAQ</a> по пользованию приложением</p>
        </div>
    </div>
</section>