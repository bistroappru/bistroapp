<?php
/* @var $this PositionController */
/* @var $model Position */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'position-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'data-id' => $model->id,
        'data-type' => $model->isNewRecord ? 'create' : 'update'
    )
)); ?>
<?php echo $form->hiddenField($model, 'is_group'); ?>
<?php echo $form->hiddenField($model, 'sort'); ?>
<?php echo $form->hiddenField($model, 'delete_ids'); ?>
<?php echo $form->hiddenField($model, 'hidden'); ?>
<?php echo $form->hiddenField($model, 'deleted'); ?>

    <div class="product_row">
        <label class="product_label">Наименование</label>

        <div class="product_title inp_wrap">
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'required' => 'required')); ?>
            <a href="#" class="edit-link"></a>
            <?php echo $form->error($model, 'name'); ?>
        </div>
    </div>

    <div class="product_row">
        <label class="product_label">Категория</label>

        <div class="product_title input_row">
            <?php
                if ($model->is_group){
                    echo $form->dropDownList($model, 'menu_id', $model->menu->getCompanyCategories(true));
                }else{
                    echo $form->dropDownList($model, 'menu_id', $model->menu->getCompanyCategories(false));
                }
            ?>
        </div>
        <?php echo $form->error($model, 'menu_id'); ?>

    </div>

    <div class="product_row">
        <label class="product_label">Описание</label>

        <div class="product_desc">
            <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
            <a href="#" class="edit-link"></a>
        </div>
        <div class="product_thumb"><a
                href="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>"
                class="fancybox_image"><img
                    src="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>" width="130"
                    height="130"></a><a href="javascript:void(0);" class="refresh"></a></div>
        <?php echo $form->hiddenField($model, 'image'); ?>
        <?php echo $form->error($model, 'description'); ?>
        <div class="clear"></div>
    </div>

    <div class="product_row">
        <label class="product_label">Каллории</label>

        <div class="product_title">
            <?php echo $form->textField($model, 'calories', array('size' => 50, 'maxlength' => 50)); ?>
            <a href="#" class="edit-link"></a>
        </div>
        <?php echo $form->error($model, 'calories'); ?>
    </div>


<?php
if ($model->is_group) {
?>
    <div class="product_row">
        <?php echo $model->hasErrors('details_count') ? CHtml::tag('div', array('class' => 'inp_wrap details_count_error'), $form->error($model, 'details_count')) : ''; ?>
        <label class="product_label_big">Входящие позиции:</label>
        <?php foreach ($model->preparePositionGroupOut() as $k=>$v) { ?>
            <div class="groups_root" data-id="<?=$k;?>">
                <div class="title"><?=$v['name']?></div>
                <div class="ingroup" id="ingroup_sortable_<?=$k;?>">
                    <?php foreach ($v['children'] as $item) { ?>
                        <div position-id="<?php echo $item->condition->position->id;?>" condition-id="<?php echo $item->condition_id; ?>" class="lanch_item">
                            <span class="name"><?php echo $item->condition->position->name; ?></span>
                            <span class="measure"><?php echo $item->condition->measure; ?>
                                <span class="black">(<?php echo $item->condition->price; ?> р.)</span>
                            </span>
                            <span class="delete">X</span>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <?php echo CHtml::textArea('Position[ingroup]', GroupDetails::model()->getGroupPositionList($model->id), array('style'=>'display: none;')); ?>
    </div>
<?php
}
?>

<?php
if (!$model->is_group) {
?>
    <div class="product_row">
        <?php echo $form->hiddenField($model, 'depressed'); ?>
        <div class="spec_checkbox">
            <input <?php if ($model->depressed) echo 'checked="checked"'; ?> type="checkbox" id="depressed"><label
                for="depressed">Подавить вывод</label>

            <div class="checkbox_bg"></div>
        </div>
    </div>
<?php
}
?>


    <?php
        if (!$model->is_group){
    ?>
            <div class="product_row inp_wrap">
                <label class="product_label_big">Место приготовления</label>

                <div class="food_type_out">
                    <?php
                    foreach ($model->getFoodTypes() as $k => $v) {
                        ?>
                        <div class="spec_checkbox">
                            <input <?php if ($k == $model->food_type) echo 'checked="checked"'; ?> type="checkbox"
                                                                                                   id="food_type_<?php echo $k; ?>"
                                                                                                   value="<?php echo $k; ?>"><label
                                for="food_type_<?php echo $k; ?>"><?php echo $v; ?></label>

                            <div class="checkbox_bg"></div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <?php echo $form->hiddenField($model, 'food_type'); ?>
                <?php echo $form->error($model, 'food_type'); ?>
            </div>

            <div id="workplace_list" class="product_row inp_wrap" style="<?php if (Position::TYPE_KITCHEN != $model->food_type) echo 'display: none;';?>">
                <label class="product_label_big">Отдел кухни</label>
                <div class="workplace_list">
                    <?php
                    foreach ($model->getCompanyWorkplaces() as $k) {
                        ?>
                        <div class="spec_checkbox">
                            <input <?php if ($k == $model->workplace) echo 'checked="checked"'; ?> type="checkbox" id="workplace_<?php echo $k; ?>" value="<?php echo $k; ?>">
                            <label for="workplace_<?php echo $k; ?>"><?php echo Company::getWorkplace($k); ?></label>
                            <div class="checkbox_bg"></div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <?php echo $form->hiddenField($model, 'workplace'); ?>
                <?php echo $form->error($model, 'workplace'); ?>
            </div>
    <?php
        }
    ?>


    <div class="product_row">
        <label class="product_label_big">Спецвывод</label>

        <div class="special_out">
            <?php
            foreach (Label::model()->findAll() as $v) {
                ?>
                <div class="spec_checkbox">
                    <input <?php if ($v->id == $model->label_id) echo 'checked="checked"'; ?> type="checkbox"
                                                                                              id="special_<?php echo $v->id; ?>"
                                                                                              value="<?php echo $v->id; ?>"><label
                        for="special_<?php echo $v->id; ?>"><?php echo $v->text; ?></label>

                    <div class="checkbox_bg"></div>
                </div>
            <?php
            }
            ?>
        </div>
        <?php echo $form->hiddenField($model, 'label_id'); ?>
    </div>

    <label class="product_label_big">Условия</label>
<?php echo $model->hasErrors('conditions_count') ? CHtml::tag('div', array('class' => 'inp_wrap conditions_count_error'), $form->error($model, 'conditions_count')) : ''; ?>
    <div id="condition_sortable">
        <?php
        foreach ($model->conditions as $k => $data) {
            echo $this->renderPartial('_condition', array('key' => $k, 'data' => $data, 'form' => $form));
        }
        ?>
    </div>

    <div class="product_row">
        <div class="new_serving">Добавить условие<a href="#" class="serving_add"></a></div>
    </div>

    <div class="product_row">
        <label class="product_label_big">Наличие в ресторанах</label>

        <div class="instock">
            <?php
            $rests = AbcHelper::allCompanyRestaurants(Yii::app()->user->company_id_full);
            $in_stock = explode(',', $model->in_stock);
            foreach ($rests as $k => $v) {
                ?>
                <div class="spec_checkbox">
                    <input <?php if (in_array($k, $in_stock) || $model->isNewRecord) echo 'checked="checked"'; ?>
                        type="checkbox" id="instock_<?php echo $k; ?>" value="<?php echo $k; ?>"><label
                        for="instock_<?php echo $k; ?>"><?php echo $v; ?></label>

                    <div class="checkbox_bg"></div>
                </div>
            <?php
            }
            ?>
        </div>
        <?php echo $form->hiddenField($model, 'in_stock'); ?>
    </div>


    <ul class="leftmenu_buttons dark">
        <li><?php echo CHtml::link('<img src="/images/leftmenu-4.png" width="50" height="50" alt=""><span>' . ($model->isNewRecord ? 'Создать' : 'Сохранить') . '</span>', '#', array('id' => 'position_save')); ?></li>
        <?php
        if (!$model->isNewRecord) {
            ?>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-2' . ($model->deleted ? '_act' : '') . '.png" width="50" height="50" alt=""><span>' . ($model->deleted ? 'Восстановить' : 'Удалить') . '</span>', '#', array('id' => 'position_set_deleted', 'class' => $model->deleted ? 'active' : '')); ?></li>
            <li><?php echo CHtml::link('<img src="/images/leftmenu-6' . ($model->hidden ? '_act' : '') . '.png" width="50" height="50" alt=""><span>' . ($model->hidden ? 'Включить' : 'Отключить') . '</span>', '#', array('id' => 'position_set_hidden', 'class' => $model->hidden ? 'active' : '')); ?></li>
        <?php
        }
        ?>
    </ul>
<?php $this->endWidget(); ?>

<?php $this->renderPartial('/partials/_load_img', array('folder' => '/data/images/positions', 'selector' => 'Position_image')); ?>