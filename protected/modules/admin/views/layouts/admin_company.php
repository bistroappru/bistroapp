<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_main'); ?>
    <div id="page">
        <div class="column_left">
            <?php
            // TODO это надо как то красиво обыграть
            switch(Yii::app()->controller->id){
                case 'company':
                    $company_id = Yii::app()->request->getParam('id', 0);
                    break;
                case 'restaurant':
                    $restaurant_id = Yii::app()->request->getParam('id', 0);
                    $company_id = Restaurant::model()->findByPk($restaurant_id)->company_id;
                    if (!$company_id) $company_id = Yii::app()->request->getParam('company_id', 0);
                    break;
            }
            ?>

            <?php $this->widget('application.components.CompanyList', array('company_id' => $company_id));?>
        </div>
        <div id="content_wrapper">
            <?php echo $content; ?>
        </div>
        <div class="clear"></div>
    </div>
<?php $this->endContent(); ?>