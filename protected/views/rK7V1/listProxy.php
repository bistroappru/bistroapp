<style>
    table{
        border-top: 1px solid #808080;
        border-left: 1px solid #808080;
        border-collapse: collapse;
    }
    th, td{
        border-bottom: 1px solid #808080;
        border-right: 1px solid #808080;
        padding: 5px;
    }
</style>

<table>
    <tr>
        <th>id</th>
        <th>company_id</th>
        <th>restaurant_id</th>
        <th>proxy_uuid</th>
        <th>proxy_type</th>
        <th>proxy_name</th>
        <th>create_time</th>
        <th>alive_interval</th>
        <th>last_request_time</th>
    </tr>
    <?php
    if (count($list)){
        foreach($list as $proxy){
        ?>
            <tr>
                <td><?=$proxy['id']?></td>
                <td><?=$proxy['company_id']?></td>
                <td><?=$proxy['restaurant_id']?></td>
                <td><?=$proxy['proxy_uuid']?></td>
                <td><?=$proxy['proxy_type']?></td>
                <td><?=$proxy['proxy_name']?></td>
                <td><?=date('d.m.Y H:i:s', $proxy['create_time'])?> <br/> <?=$proxy['create_time']?></td>
                <td><?=$proxy['alive_interval']?></td>
                <td><?=date('d.m.Y H:i:s', $proxy['last_request_time'])?> <br/> <?=$proxy['last_request_time']?></td>
            </tr>
        <?php
        }
    }
    ?>
</table>