/**
 * Created by TWT on 05.09.14.
 */
function updateMenuList(company_id, menu_id){
    $.ajax({
        url: '/admin/ajax/updateMenuList',
        beforeSend: showLoading,
        data: {
            company_id: company_id,
            menu_id: menu_id
        },
        dataType: 'html',
        error: function(jqXHR, textStatus){
            hideLoading();
            jAlert(jqXHR.responseText);
        },
        success: function(data){
            $('#menu_categories').replaceWith(data);
            activateMenuSortable();
            hideLoading();
        },
        type: 'get'
    });
}

function updateDishList(menu_id, group, dish_id){
    $.ajax({
        url: '/admin/ajax/updateDishList',
        beforeSend: showLoading,
        data: {
            menu_id: menu_id,
            dish_id: dish_id,
            group: group
        },
        dataType: 'html',
        error: function(jqXHR, textStatus){
            hideLoading();
            jAlert(jqXHR.responseText);
        },
        success: function(data){
            $('#content_wrapper .column_center .dish_list_wrap').replaceWith(data);
            inputChecked($('input[type=checkbox]'));
            $('.scroll-pane').jScrollPane();
            setActiveGroupDishes();
            hideLoading();
        },
        type: 'get'
    });
}

function activateMenuSortable(){
    if($("#menu_sortable").length){
        $("#menu_sortable").sortable({
            placeholder: "menu_sortable_placeholder",
            stop: function(event, ui){
                setMenuSort();
            }
        });
    }
}

function setMenuSort(){
    var list = [];
    $('#menu_sortable li').each(function(index){
        list[index] = $(this).attr('data-id');
    });
    $.ajax({
        url: '/admin/menu/resort',
        beforeSend: showLoading,
        data: {list: list},
        dataType: 'html',
        error: function(jqXHR, textStatus){
            jAlert(jqXHR.responseText);
            hideLoading();
        },
        success: function(data){
            hideLoading();
        },
        type: 'post'
    });
}

function activateDishesSortable(){
    if ($("#dishes_sortable").length){
        $("#dishes_sortable").sortable({
            placeholder: "dishes_sortable_placeholder",
            stop: function(event, ui){
                setDishesSort();
            }
        });
    }
}

function setDishesSort(){
    var list = [];
    $('#dishes_sortable li').each(function(index){
        list[index] = $(this).attr('data-id');
    });

    $.ajax({
        url: '/admin/position/resort',
        beforeSend: showLoading,
        data: {list: list},
        dataType: 'html',
        error: function(jqXHR, textStatus){
            hideLoading();
            jAlert(jqXHR.responseText);
        },
        success: function(data){
            hideLoading();
        },
        type: 'post'
    });
}

function activateDishPageScript(){
    inputChecked($('input[type=radio]'));
    inputChecked($('input[type=checkbox]'));
    $('.datepicker').datepicker({dateFormat: "dd.mm.yy"});
    if ($('.conditions_count_error').length == 0 && $('#condition_sortable .condition_row').length == 0) $('.serving_add').trigger('click');
    if ($('#Position_is_group').val() == 1){
        $('.serving_add').closest('.product_row').hide();
        $('.serving_del').hide();
        $('.product_serving').html('Общая стоимость');
        activateInGroupSortable();
        setActiveGroupDishes();
        setActiveGroups();
        setActiveGroupsRoot();
        $('.scroll-pane').jScrollPane();
    }else{
        activateConditionSortable();
        activateMenuSortable();
    }
}

function setActiveGroupsRoot(id){
    var index,
        act_root_i;
    if (id){
        index = id;
    }else{
        act_root_i = $('.groups_root .title i');
        if (act_root_i.length){
            index = parseInt(act_root_i.closest('.groups_root').attr('data-id'), 10);
        }else{
            index = parseInt($('.groups_root:first').attr('data-id'));
        }
    }

    console.log(index);

    $('.groups_root').removeClass('active');
    $('.groups_root').find('.title i').remove();

    $('.groups_root[data-id='+index+']').addClass('active');
    $('.groups_root[data-id='+index+'] .title').append('<i class="fa fa-check"></i>');
}

function setActiveGroups(){
    var list = $('#groups_list_wrap .group_item');
    var group_id = 0;

    if (!list.length){
        return false;
    }

    list.each(function(){
        group_id = parseInt($(this).attr('data-id'), 10);
        $(this).find('i').remove();
        if ($('.groups_root[data-id='+group_id+']').length){
            $(this).append('<i class="fa fa-check"></i>');
        }
    });
}

function activateInGroupSortable(){
    var id;
    $('.ingroup').each(function(){
        id = $(this).attr('id');
        if (id){
            $("#"+id).sortable({
                placeholder: "ingroup_sortable_placeholder",
                stop: function (event, ui) {
                    setActiveGroupValue();
                }
            });
        }
    });
}

function activateConditionSortable(){
    if($("#condition_sortable").length){
        $("#condition_sortable").sortable({
            placeholder: "condition_sortable_placeholder",
            stop: function(event, ui){
                setConditionSort();
            }
        });
    }
}

function setConditionSort(){
    $('#condition_sortable .condition_row').each(function(index){
        $(this).find('input.sort').val(index);
    });
}

function setActiveGroupDishes(){
    if (!$('.dishes_list2').length) return;
    $('.dishes_list2 input[type=checkbox]').prop('checked', '').removeClass('checked');
    $('.dish_desc').removeClass('active');
    $('.ingroup .lanch_item').each(function(){
        var position = $(this).attr('position-id');
        var condition = $(this).attr('condition-id');
        if ($('#dishes_'+position).length > 0){
            $('#dishes_'+position).prop('checked', 'checked').addClass('checked');
            $('.dish_desc[cond-id='+condition+']').addClass('active');
        }
    });
}

function setActiveGroupValue(){
    var list = [],
        root;
    $('.groups_root').each(function(){
        root = $(this).attr('data-id');
        $(this).find('.ingroup .lanch_item').each(function(){
            list.push({
                'condition_id': $(this).attr('condition-id'),
                'position_id': $(this).attr('position-id'),
                'groups_id': root,
                'sort': $(this).index()
            });
        });
    });
    $('#Position_ingroup').val(JSON.stringify(list));
}

$(document).ready(function(){
    $(window).on('logobox_company', function(){
        if ($('.ctrl_menu').length > 0){
            ajaxRequest('/admin/menu',function(data){
                history.pushState({},'','/admin/menu');
                $('#root').html($(data).find('#page'));
                $('#menu_sortable li:first a').trigger('click');
            });
        }
    });

    $('#root').on('click', '#menu_categories .category_item a', function(e) {
        e.preventDefault();
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                $('#menu_categories .category_item').removeClass('active');
                link.closest('li').addClass('active');
                history.pushState({},'',link.attr('href'));
                hideLoading();
            },
            type: 'get'
        });
    });

    $('#root').on('click', '#menu_create', function(e){
        e.preventDefault();
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                $('#menu_categories .category_item').removeClass('active');
                history.pushState({},'',link.attr('href'));
                hideLoading();
            },
            type: 'get'
        });
    });

    $('#root').on('submit', '#menu-form', function(e){
        e.preventDefault();
        var form = $(this);
        var type_before = form.attr('data-type');
        var type_after = '';
        $.ajax({
            url: form.attr('action'),
            beforeSend: showLoading,
            data: form.serialize(),
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                var newform = $('#menu-form');
                type_after = newform.attr('data-type');
                if (type_before != type_after) history.pushState({},'',newform.attr('action'));
                if (newform.find('.errorMessage').length == 0){
                    updateMenuList($('#Menu_company_id').val(), newform.attr('data-id'));
                }
                hideLoading();
            },
            type: 'POST'
        });
    });

    $('#root').on('click', '#menu_save', function(e){
        e.preventDefault();
        $('#menu-form').trigger('submit');
    });

    $('#root').on('click', '#menu_set_deleted', function(e){
        e.preventDefault();
        if ($('#Menu_deleted').val() == 1){
            $('#Menu_deleted').val(0);
        }else{
            $('#Menu_deleted').val(1);
        }
        $('#menu_save').trigger('click');
    });

    $('#root').on('click', '#menu_set_hidden', function(e){
        e.preventDefault();
        if ($('#Menu_hidden').val() == 1){
            $('#Menu_hidden').val(0);
        }else{
            $('#Menu_hidden').val(1);
        }
        $('#menu_save').trigger('click');
    });

    $('#root').on('click', '#position_create, #position_create_group', function(e){
        e.preventDefault();
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                $('#dishes_sortable .dish_box').removeClass('active');
                history.pushState({},'',link.attr('href'));
                activateDishPageScript();
                hideLoading();
            },
            type: 'get'
        });
    });

    $('#root').on('click', '#position_groups_create', function(e){
        e.preventDefault();
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                console.log(data);
                hideLoading();
            },
            type: 'get'
        });
    });

    $('#root').on('click', '.groups_root .title', function(){
        setActiveGroupsRoot(parseInt($(this).closest('.groups_root').attr('data-id'),10));
    });

    $('#root').on('click', '.groups_list_wrap .group_item', function(){
        var group_id = parseInt($(this).attr('data-id'));
        var group_title = $(this).attr('data-title');
        var root = $('.groups_root[data-id='+group_id+']');
        if (root.length){
            jConfirm('При отключении этой группы будут отключены все входящие позиции', 'Отключение группы', 'warning', function(res){
                if (res){
                    root.remove();
                    setActiveGroupDishes();
                    setActiveGroupValue();
                    setActiveGroups();
                    setActiveGroupsRoot();
                }
            });
        }else{
            $('#Position_ingroup').before('<div class="groups_root" data-id="'+group_id+'">\
                <div class="title">'+group_title+'</div>\
                <div class="ingroup" id="ingroup_sortable_'+group_id+'"></div>\
            </div>');
            activateInGroupSortable();
            setActiveGroups();
            setActiveGroupsRoot();
        }
    });

    $('#root').on('click', '#position_save', function(e){
        e.preventDefault();
        $('#position-form').trigger('submit');
    });

    $('#root').on('click', '#position_set_deleted', function(e){
        e.preventDefault();
        if ($('#Position_deleted').val() == 1){
            $('#Position_deleted').val(0);
        }else{
            $('#Position_deleted').val(1);
        }
        $('#position_save').trigger('click');
    });

    $('#root').on('click', '#position_set_hidden', function(e){
        e.preventDefault();
        if ($('#Position_hidden').val() == 1){
            $('#Position_hidden').val(0);
        }else{
            $('#Position_hidden').val(1);
        }
        $('#position_save').trigger('click');
    });

    $('#root').on('submit', '#position-form', function(e){
        e.preventDefault();
        var form = $(this);
        var type_before = form.attr('data-type');
        var type_after = '';
        $.ajax({
            url: form.attr('action'),
            beforeSend: showLoading,
            data: form.serialize(),
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                var newform = $('#position-form');
                type_after = newform.attr('data-type');
                if (type_before != type_after) history.pushState({},'',newform.attr('action'));
                activateDishPageScript();
                hideLoading();
            },
            type: 'POST'
        });
    });

    $('#root').on('click', '#dishes_sortable .dish_box', function(){
        var url = $(this).attr('href');
        ajaxRequest(url, function(data){
            history.pushState({},'',url);
            $('#content_wrapper').html(data);
            activateDishPageScript();
        });
    });

    $('#root').on('click', '#position-form .serving_del', function(e){
        e.preventDefault();
        var link = $(this);
        jConfirm('Условие будет удалено без возможности восстановить', 'Удалить условие?', 'question', function(r){
            // TODO ajax запрос на наличие этого условия в незакрытых заказах
            if (r){
                var delid = parseInt(link.attr('data-id'));
                if (delid){
                    var delids = $('#Position_delete_ids');
                    delids.val(delids.val() + (delids.val() ? ',' : '') + delid);
                }
                link.closest('.product_row').remove();
                setConditionSort();
            }
        });
    });

    $('#root').on('click', '#position-form .serving_add', function(e){
        e.preventDefault();
        $('.conditions_count_error').remove();
        var serv_ind = time();
        $('#condition_sortable').append('<div class="product_row condition_row"><div class="product_left"><input type="hidden" name="Position[conditions][' + serv_ind + '][id]" value=""><input class="sort" type="hidden" name="Position[conditions][' + serv_ind + '][sort]" value="' + serv_ind + '"><textarea  name="Position[conditions][' + serv_ind + '][measure]" class="product_serving">1 шт.</textarea><a href="#" class="serving_del"></a></div><div class="product_right"><ul class="prices_list radio_wrap"><li class="cond_price_def"><div class="prices_left"><div class="prices_cell"><p>ЦЕНА:</p></div></div><div class="check_item"><input type="radio" id="action_off_' + serv_ind + '" checked="checked" value="0" name="Position[conditions][' + serv_ind + '][action_on]"><label for="action_off_' + serv_ind + '" class="label_radio"></label><input type="text" value="" name="Position[conditions][' + serv_ind + '][price]" class="inp_price"></div></li><li class="cond_price_act"><div class="prices_left"><div class="prices_cell"><p>ЦЕНА:</p><p class="action">Акция</p></div></div><div class="check_item"><input type="radio" id="action_on_' + serv_ind + '" value="1" name="Position[conditions][' + serv_ind + '][action_on]"><label for="action_on_' + serv_ind + '" class="label_radio"></label><input type="text" value="" name="Position[conditions][' + serv_ind + '][action][sale_price]" class="inp_price"></div><div class="action_period" style="display: none;"><p class="action_time">срок действия акции:</p><input type="text" value="" name="Position[conditions][' + serv_ind + '][action][date_on]" class="action_date datepicker" readonly="readonly"/> - <input type="text" value="" name="Position[conditions][' + serv_ind + '][action][date_off]" class="action_date datepicker" readonly="readonly"/></div></li></ul></div><div class="clear"></div></div>');
        inputChecked($('input[type=radio]'));
        $('.datepicker').datepicker({dateFormat: "dd.mm.yy"});
        activateConditionSortable();
    });

    $('#root').on('change', '#position-form .instock input[type=checkbox]', function(){
        var string = '';
        $('.instock input[type=checkbox]:checked').each(function(){
            string += ','+$(this).val();
        });
        $('#Position_in_stock').val(string.substr(1));
    });

    $('#root').on('change', '#depressed', function(){
        var string = 0;
        if ($(this).prop("checked")) string = 1;
        $('#Position_depressed').val(string);
    });

    $('#root').on('change', '#position-form .food_type_out input[type=checkbox]', function(){
        var value = 0;
        if ($(this).prop('checked')){
            value = $(this).val();
        }
        $('.food_type_out input[type=checkbox][value!='+value+']').prop('checked', false).removeClass('checked');
        $('#Position_food_type').val(value);
        if (value == 1){
            $('#workplace_list').show();
        }else{
            $('.workplace_list input[type=checkbox]').prop('checked', false).removeClass('checked');
            $('#Position_workplace').val(0);
            $('#workplace_list').hide();
        }
    });

    $('#root').on('change', '#position-form .workplace_list input[type=checkbox]', function(){
        var value = 0;
        if ($(this).prop('checked')){
            value = $(this).val();
        }
        $('.workplace_list input[type=checkbox][value!='+value+']').prop('checked', false).removeClass('checked');
        $('#Position_workplace').val(value);
    });

    $('#root').on('change', '#position-form .special_out input[type=checkbox]', function(){
        var value = 0;
        if ($(this).prop('checked')){
            value = $(this).val();
        }
        $('.special_out input[type=checkbox][value!='+value+']').prop('checked', false).removeClass('checked');
        $('#Position_label_id').val(value);
    });

    $('#root').on('change', '#position-form .intypes input[type=checkbox]', function(){
        var string = '';
        $('.intypes input[type=checkbox]:checked').each(function(){
            string += ','+$(this).val();
        });
        $('#Company_type').val(string.substr(1));
    });

    $('#root').on('change', '#position-form .inkitchen input[type=checkbox]', function(){
        var string = '';
        $('.inkitchen input[type=checkbox]:checked').each(function(){
            string += ','+$(this).val();
        });
        $('#Company_kitchen').val(string.substr(1));
    });

    $('#root').on('change', '#dish_choose select[name=dish_choose]', function(){
        var is_group = $('#Position_is_group').val() || 0;
        updateDishList($(this).val(), is_group);
    });

    // TODO переписать наверное надо
    $('#root').on('change', '#condition_sortable .condition_row input[type=radio]', function(){
        var row_index = $(this).closest('.condition_row').index();
        if ($(this).val() == 0){
            $('#condition_sortable .condition_row:eq('+row_index+') .cond_price_act .action_period').hide();
        }else{
            $('#condition_sortable .condition_row:eq('+row_index+') .cond_price_act .action_period').show();
        }
    });

    $('#root').on('click', '.ingroup .lanch_item .delete', function(){
        $(this).parent().remove();
        setActiveGroupValue();
        setActiveGroupDishes();
    });

    $('#root').on('change', '.dishes_list2 input[type=checkbox]', function(){
        if ($(this).prop('checked')){
            if (!$(this).parent().find('.dish_desc.active').length) $(this).parent().find('.dish_desc:first').addClass('active');
            $('.groups_root.active .ingroup').append('<div position-id="'+$(this).attr('data-id')+'" condition-id="'+$(this).parent().find('.dish_desc.active').attr('cond-id')+'" class="lanch_item"><span class="name">'+$(this).parent().find('.dish_title').html()+'</span> <span class="measure">'+$(this).parent().find('.dish_desc.active').html()+'</span><span class="delete">X</span></div>');
            $('.details_count_error').remove();
        }else{
            $(this).parent().find('.dish_desc').removeClass('active');
            $('.ingroup .lanch_item[position-id='+$(this).attr('data-id')+']').remove();
        }
        setActiveGroupValue();
        setActiveGroupDishes();
    });

    $('#root').on('click', '.dishes_list2 .dish_desc', function(){
        $(this).parent().find('.dish_desc').removeClass('active');
        $(this).addClass('active');
        var checkbox = $(this).parents('li').find('input[type=checkbox]');
        if(checkbox.prop('checked')){
            $('.lanch_item[position-id=' + checkbox.attr('data-id') + ']').attr('condition-id',$(this).attr('cond-id')).find('.measure').html($(this).html());
            setActiveGroupValue();
        }else{
            checkbox.next('label').trigger('click');
        }
        return false;
    });
});
//**************************************************************
//*****     OLD BLOCK     **************************************
//**************************************************************


$(document).ready(function(){
    //jQuery('.scroll-pane').jScrollPane();
    $('.fancybox_image').fancybox();
    activateDishPageScript();
});