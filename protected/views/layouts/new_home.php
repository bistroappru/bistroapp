<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <script src="/html/front/js/jquery-1.11.0.min.js"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <base href="<?php echo Yii::app()->request->getBaseUrl(true); ?>">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="/html/front/css/style.css">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="/html/front/js/jquery.mousewheel.js"></script>
    <script src="/html/front/js/jquery.jscrollpane.min.js"></script>
    <script src="/html/front/js/jquery.backstretch.min.js"></script>
    <script src="/html/front/js/jquery.selectBox.js"></script>
    <script src="/html/front/js/jquery.placeholder.js"></script>
    <script src="/html/front/js/modal.js"></script>
    <script src="/html/front/js/script.js"></script>
    <script>
        $(document).ready(function(){
            if ($('#login-form input.error').length) $('.login-link').trigger('click');
        });
    </script>
    <style>
        .errorMessage{
            text-transform: lowercase;
            color: #ff0000;
        }
    </style>
</head>
<body class="homepage">
    <?php echo $content; ?>
</body>
</html>