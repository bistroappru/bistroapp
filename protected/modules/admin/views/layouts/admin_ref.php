<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_main'); ?>

<div id="page">
    <div class="column_left">
        <?php $this->renderPartial('/partials/_ref_list')?>
    </div>
    <div id="content_wrapper">
        <?php echo $content; ?>
    </div>
    <div class="clear"></div>
</div>

<?php $this->endContent(); ?>