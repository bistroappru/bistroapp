/**
 * Created by birlaver on 30.08.14.
 */
// FUNCTIONS

// изменения в форме
function setDirty(form){
    form.attr('data-dirty', true);
}

function getDirtyCount(){
    return $('form[data-dirty=true]').length;
}

function checkFormModified(callback){
    if (getDirtyCount() > 0){
        jConfirm('Данные формы будут потеряны. Продолжить?', 'Внимание!', 'question', function(result){
            if (result){
                callback();
            }
        });
    }else{
        callback();
    }
}


function inputChecked(input) {
    input.each(function(){
        if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
        $(this).change(function(){
            input.each(function(){
                if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
            });
        });
    });
}

function ajaxRequest(url, callback, data, dataType, type){
    if (!url) return;
    if (!type) type = 'get';
    if (!dataType) dataType = 'html';
    if (!data) data = {};

    $.ajax({
        url: url,
        beforeSend: showLoading,
        data: data,
        dataType: dataType,
        type: type,
        error: function (jqXHR, textStatus) {
            jAlert(jqXHR.responseText);
            hideLoading();
        },
        success: function (data){
            if(callback) callback(data);
            hideLoading();
        }
    });
}

function showLoading(){
    $.fancybox.showLoading();
}

function hideLoading(){
    $.fancybox.hideLoading();
}

function time(){
    return parseInt(new Date().getTime()/1000);
}

// READY
$(document).ready(function(){
    inputChecked($('input[type=radio]'));
    inputChecked($('input[type=checkbox]'));

    if ($.datepicker) {
        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }

    $('input[placeholder], textarea[placeholder]').placeholder();

    //LOGO BOX
    $(document).on('change', '#cr_company', function(){
        var val = $(this).val();
        $.cookie('company_id', val, {expires: 7, path: '/'});
        $.cookie('restaurant_id', '0', {expires: 7, path: '/'});
        ajaxRequest('/admin/ajax/updateCompanyLogo',function(data){
            $('#company_logobox').replaceWith(data);
            $(window).trigger('logobox_company');
        });
    });

    $(document).on('change', '#cr_restaurant', function(){
        $.cookie('restaurant_id', $(this).val(), {expires: 7, path: '/'});
        $(window).trigger('logobox_restaurant');
    });

    $('.table_menu_top li:not(.menu_site) a').on('click', function(e){
        return;
        jConfirm('Данные формы будут потеряны. Продолжить?', 'Внимание!', 'question', function(result){
            if (result){
                alert('уходим!');
                // как сделать так чтобы он снова начал выполнять стандартное действие? которое было заложено в других обработчиках?
            }else{
                e.preventDefault();
                alert('остаемся!');
            }
        });
        // перехода по ссылке уже не будет
    });

    // TOOLS page --------------------------------------------------------
    $(document).on('change', '#select_tools_company', function(){
        var val = $(this).val();
        checkFormModified(function(){
            if (!val){
                var url = '/admin/settings/';
                ajaxRequest(url, function(data){
                    $('#content_wrapper').html(data);
                    history.pushState({}, '', url);
                });
            }else{
                var url = '/admin/settings/company/id/'+val;
                ajaxRequest(url, function(data){
                    $('#content_wrapper').html(data);
                    history.pushState({}, '', url);
                });
            }
        });
    });

    // изменение полей формы
    $('#root').on('change', '#company-tools-form input, #company-tools-form textarea, #company-tools-form select', function () {
        setDirty($(this).closest('form'));
    });

    $('#root').on('click', '#company_tools_save', function () {
        $('#company-tools-form').submit();
        return false;
    });

    $('#root').on('submit', '#company-tools-form', function (e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            beforeSend: showLoading,
            data: form.serialize(),
            dataType: 'html',
            error: function (jqXHR, textStatus) {
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function (data) {
                $('#content_wrapper').html(data);
                inputChecked($('input[type=checkbox]'));
                hideLoading();
            },
            type: 'POST'
        });
    });

    // -------------------------------------------------------------------


});