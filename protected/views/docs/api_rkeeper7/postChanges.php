<h3>Передача результата выполнения запроса</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/r_keeper_api/v1/postChanges<br/>
</p>

<hr/>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BA_PROXY_REQUEST>
    <BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF9}" ServerTimeStamp="UTC_DATETIME"/>
    <BISTRO_APP requestID="{08692D19-4DC0-6953-0218-66375B217C29}"/>
    <R_KEEPER>
        <RESPONSE>
        <![CDATA[
            <RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="GetRefList" ErrorText="" DateTime="2014-11-13T16:21:15" WorkTime="0" Processed="1">
                <RK7RefList Count="2">
                    <RK7Reference RefName="BONUSTYPES" Count="1" DataVersion="2"/>
                    <RK7Reference RefName="BRIGADES" Count="0" DataVersion="1"/>
                </RK7RefList>
            </RK7QueryResult>
        ]]>
        </RESPONSE>
    </R_KEEPER>
</BA_PROXY_REQUEST>');?>
    </code></pre>
<hr/>



<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK"></BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('<?xml version="1.0" encoding="UTF-8" ?>
<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="ERROR">
        <ERROR code="17" description="Proxy с UUID {96896BD3-1D9F-45D5-B87C-9B2E6A924AF4} не зарегистрирован"/>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>');?>
    </code></pre>
<hr/>