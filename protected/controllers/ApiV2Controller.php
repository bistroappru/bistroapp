<?php
class ApiV2Controller extends Controller
{
    public $layout = 'clear';

    public function filters()
    {
        return array(
            'postOnly - test,createRandomOrder,addOrderPosition,updateOrder',
        );
    }

    private function sendData($data = array()){
        $response = array_merge(array('status' => 'ok', 'data' => array()), $data);
        $output = json_encode($response, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 256);

        header('Accept-Encoding: ');
        header('Content-type: application/json; charset=utf-8;');
        header('Content-length: '.strlen($output));

        echo $output;
        Yii::app()->end();
    }

    private function sendError($code, $message = ''){
        $response = array(
            'status' => 'error',
            'error' => array(
                'code' => $code,
                'message' => $message
            )
        );

        $output = json_encode($response, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 256);

        header('Accept-Encoding: ');
        header('Content-type: application/json; charset=utf-8;');
        header('Content-length: '.strlen($output));

        echo $output;
        Yii::app()->end();
    }

    // ============================================================================================
    //          ACTIONS
    // ============================================================================================
    public function actionIndex(){
        $this->redirect($this->createUrl('/docs/api_mobile_v2'));
    }

    /**
     * ПОЛУЧЕНИЕ КОМПАНИЙ
     */
    public function actionGetCompanies(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getCompanies($version);

            ApiV2Service::log($device, array('version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }


    /**
     * ПОЛУЧЕНИЕ КАРТИНОК КОМПАНИЙ
     */
    public function actionGetCompaniesImages(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getCompaniesImages($version);

            ApiV2Service::log($device, array('version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ РЕСТОРАНОВ КОМПАНИИ
     */
    public function actionGetRestaurants(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getRestaurants($company_id, $version);

            ApiV2Service::log($device, array('company_id' => $company_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ ФОТОГРАФИЙ РЕСТОРАНА
     */
    public function actionGetRestaurantPhotos(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getRestaurantsPhotos($company_id, $version);

            ApiV2Service::log($device, array('company_id' => $company_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ СТОЛИКОВ РЕСТОРАНА
     */
    public function actionGetRestaurantTables(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $restaurant_id = Yii::app()->request->getParam('restaurant_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getRestaurantTables($restaurant_id, $version);

            ApiV2Service::log($device, array('restaurant_id' => $restaurant_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ КАТЕГОРИЙ МЕНЮ
     */
    public function actionGetDishesCategories(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getMenuCategories($company_id, $version);

            ApiV2Service::log($device, array('company_id' => $company_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ ИЗОБРАЖЕНИЙ КАТЕГОРИЙ МЕНЮ
     */
    public function actionGetDishesCategoriesImages(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getMenuCategoriesImages($company_id, $version);

            ApiV2Service::log($device, array('company_id' => $company_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ БЛЮД
     */
    public function actionGetDishes(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getDishes($company_id, $version);

            ApiV2Service::log($device, array('company_id' => $company_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ КАРТИНОК БЛЮД
     */
    public function actionGetDishesImages(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $company_id = Yii::app()->request->getParam('company_id', 0);
        $version = Yii::app()->request->getParam('version', -1);
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getDishesImages($company_id, $version);

            ApiV2Service::log($device, array('company_id' => $company_id, 'version' => $version));

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionGetOrder(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $order_id = Yii::app()->request->getParam('order_id', 0);

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getOrder($order_id, $device);

            ApiV2Service::log($device);

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionGetOrders(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        $version = Yii::app()->request->getParam('version', -1);

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getOrders($version, $device);

            ApiV2Service::log($device);

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ СПОСОБОВ ОПЛАТЫ
     */
    public function actionGetOrderPayments(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getOrderPayments();

            ApiV2Service::log($device);

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionGetOrderStatuses(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}}';

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getOrderStatuses();

            ApiV2Service::log($device);

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionPostOrder(){
        $input = file_get_contents('php://input');

        $transaction = Yii::app()->db->beginTransaction();
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);
            $order = ApiV2Service::createOrder($data['order'], $device->id);

            // добавить в очередь к киперу
            $restaurant = Restaurant::model()->findByPk($order->restaurant_id);
            if ($restaurant !== null && !$restaurant->lock_order_transfer){
                $order->createOutOrder();
            }
            // ---------------------------
            ApiV2Service::log($device, '', $data['order']);

            $order_data = array('data' => ApiV2Service::prepareOrderData($order));

            $transaction->commit();
            $this->sendData($order_data);
        } catch (Exception $e) {
            $transaction->rollback();
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionAddOrderPosition(){
        $input = file_get_contents('php://input');
        //$input = '{"device": {"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"},"order": {"id": 167,"positions":[{"position_id": 274,"count": 2,"price": 80,"comment": "без лука"}]}}';

        $transaction = Yii::app()->db->beginTransaction();
        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);
            $order = ApiV2Service::loadOrder($data['order']['id'], $device);
            $positions = ApiV2Service::addOrderPosition($order, $data['order']['positions']);

            $order_data = array('data' => ApiV2Service::prepareOrderData($order));

            // добавить в очередь к киперу
            $restaurant = Restaurant::model()->findByPk($order->restaurant_id);
            if ($restaurant !== null && !$restaurant->lock_order_transfer){
                $order->addOutOrderPosition($positions);
            }
            // ---------------------------

            ApiV2Service::log($device, '', $data['order']);
            $transaction->commit();
            $this->sendData($order_data);
        } catch (Exception $e) {
            $transaction->rollback();
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    /**
     * ПОЛУЧЕНИЕ МЕТОК ВЫДЕЛЕНИЯ
     */
    public function actionGetLabels(){
        $input = file_get_contents('php://input');

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $list = ApiV2Service::getLabels();

            ApiV2Service::log($device);

            $this->sendData($list);
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionSetPushToken(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}, "push_token":"f5950b10 eb5e8888 ce549b49 0d24cbb1 a0202f49 929ca991 bd37a6c2 7070dbea"}';

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            ApiV2Service::setPushToken($device, $data['push_token']);

            // тестовая проверка
            $apns = Yii::app()->apns;
            $apns->send(str_replace(" ", "", $device->push_token), 'Test of verification token. Sent '.date('d-m-Y H:i:s'), array('custom_prop' => 'test'), array('sound' => 'default', 'badge' => 1));

            ApiV2Service::log($device);

            $this->sendData();
        } catch (Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionPostFeedBack(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}, "feedback":{"title":"Название ошибки","description":"Описание ошибки","author":"Иванов Иван"}}';

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);

            $data['feedback']['device_id'] = $device->id;

            $feedback = ApiV2Service::createFeedback($data['feedback']);

            // отправка письма
            $emails = FeedbackEmail::model()->findAll();
            if (count($emails)){
                $mailer = Yii::app()->mailer;
                foreach($emails as $email){
                    if ($email->email) $mailer->AddAddress($email->email);
                }
                $mailer->Subject = 'BistroApp (Обратная связь) - ' . $feedback->title;
                $mailer->Body = Yii::app()->controller->renderPartial('application.views.email._mail_new_feedback', array('model' => $feedback), true);
                $mailer->Send();
            }

            ApiV2Service::log($device);
            $this->sendData();
        }catch(Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }

    public function actionSendNotificationToWaiter(){
        $input = file_get_contents('php://input');
        //$input = '{"device":{"uuid": "550e8400-e29b-41d4-a716-446655440000","platform": "iOS","name": "iPhone 5","version": "7.1"}, "notification":{"type": "comeToTable","order_id": 1,"comment": "текст коммента"}}';

        try {
            $data = ApiV2Service::parseData($input);
            $device = ApiV2Service::loadDevice($data['device']);
            $order = ApiV2Service::loadOrder($data['notification']['order_id'], $device);

            $order->sendSMS($data['notification']['type'], $data['notification']['comment']);

            ApiV2Service::log($device);
            $this->sendData();
        }catch(Exception $e) {
            $this->sendError($e->getCode(), $e->getMessage());
        }
    }




    /*
     *      МЕТОДЫ ДЛЯ СОЗДАНИЯ ИЗ БРАУЗЕРА
     */

    public function actionCreateRandomOrder(){
        $proxy_id = Yii::app()->request->getParam('proxy_id');
        $table = Yii::app()->request->getParam('table', 2);
        $guests = rand(1,5);

        if (empty($proxy_id)) throw new CHttpException('Не задан параметр proxy_id');
        $proxy = RKeeperProxy::model()->findByPk($proxy_id);
        if (empty($proxy)) throw new CHttpException('Не найден прокси-сервер с id ' . $proxy_id);
        if (empty($proxy->restaurant_id)) throw new CHttpException('Не задан ресторан у прокси-сервера с id ' . $proxy_id);

        $criteria = new CDbCriteria();
        $criteria->addCondition('FIND_IN_SET('.$proxy->restaurant_id.', in_stock)');
        $criteria->addCondition('deleted = 0');
        $criteria->order = 'RAND()';
        $criteria->limit = rand(1, 5);
        $list = Position::model()->findAll($criteria);

        if (!count($list)) die('Нет доступных блюд');

        $positions = array();

        foreach($list as $one){
            $positions[] = array(
                'position_id' => $one->id,
                'count' => rand(1,4),
                'price' => $one->price
            );
        }

        $device_id = Yii::app()->request->getParam('device_id', 7);
        $device = Device::model()->findByPk($device_id);
        if (empty($device)) throw new CHttpException('Не найдено устройство с id '.$device_id);

        $device_data = array(
            'uuid' => $device->uuid,
            'platform' => $device->platform,
            'name' => $device->name,
            'version' => $device->version
        );

        $input = CJSON::encode(array(
            'device' => $device_data,
            'order' => array('attention' => 1, 'payment_id' => 1, 'restaurant_id' => $proxy->restaurant_id, 'positions' => $positions, 'separated' => 0, 'table' => $table, 'guests' => $guests)
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['SERVER_NAME'].'/api/v2/postOrder');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_exec($ch);
        curl_close($ch);
    }

    public function actionUpdateOrder(){
        $order_id = Yii::app()->request->getParam('order_id');
        if (empty($order_id)) throw new CHttpException('Не задан order_id');
        $order = Order::model()->findByPk($order_id);
        if (empty($order)) throw new CHttpException('Не найден заказ с id ' . $order_id);

        $criteria = new CDbCriteria();
        $criteria->addCondition('FIND_IN_SET('.$order->restaurant_id.', in_stock)');
        $criteria->addCondition('deleted = 0');
        $criteria->order = 'RAND()';
        $criteria->limit = rand(1,3);
        $list = Position::model()->findAll($criteria);

        if (!count($list)) die('Нет доступных блюд');

        $positions = array();

        foreach($list as $one){
            $positions[] = array(
                'position_id' => $one->id,
                'count' => rand(1,4),
                'price' => $one->price
            );
        }

        $device_id = Yii::app()->request->getParam('device_id', 7);
        $device = Device::model()->findByPk($device_id);
        if (empty($device)) throw new CHttpException('Не найдено устройство с id '.$device_id);

        $device_data = array(
            'uuid' => $device->uuid,
            'platform' => $device->platform,
            'name' => $device->name,
            'version' => $device->version
        );

        $input = CJSON::encode(array(
            'device' => $device_data,
            'order' => array('id' => $order_id, 'positions' => $positions)
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['SERVER_NAME'].'/api/v2/addOrderPosition');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_exec($ch);
        curl_close($ch);
    }

    public function actionTest(){
        $order = Order::model()->findByPk(61);
        echo Order::getNextSubOrder($order->id);
    }

}