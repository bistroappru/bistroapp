<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */
$this->pageTitle = Yii::app()->name . ' - Вход в систему';
?>
<div id="home_header">
    <div class="center">
        <?php $this->renderPartial('/partials/_top_menu', array()); ?>
        <div class="header_device2"></div>
        <a href="/" id="logo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo_home.png" width="160"
                                   height="157" alt=""></a>
    </div>
</div>

<section class="benefits_section">
    <div class="pattern_top"></div>
    <div class="pattern_bottom"></div>
    <div class="center">
        <div class="login_form">
            <h2>ВХОД В СИСТЕМУ</h2>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            )); ?>
            <div class="login_entry">
                <div class="login_row">
                    <label>ЛОГИН:<span class="required">*</span></label>
                    <?php echo $form->textField($model, 'username'); ?>
                    <?php echo $form->error($model, 'username'); ?>
                </div>
                <div class="login_row">
                    <label>ПАРОЛЬ:<span class="required">*</span></label>
                    <?php echo $form->passwordField($model, 'password'); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
                <?php echo CHtml::submitButton('Войти', array('value'=>'', 'class'=>'login_submit'));?>
                <div class="login_check">
                    <?php echo $form->checkBox($model, 'rememberMe', array('id'=>'remember'));?>
                    <label class="check_label" for="remember">Запомнить меня</label>
                    <?php echo $form->error($model, 'rememberMe'); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
<section class="home_gray">
    <div class="big_device2">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/device2.jpg" width="949" height="540" alt="">
    </div>
    <div class="pattern_bottom gray"></div>
</section>
<section class="simple-easy">
    <div class="simple_inner">
        <div class="simple_text">
            <h2>ПРОСТО и ЛЕГКО</h2>

            <p>Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum
                tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus
                molestie.</p>
        </div>
        <div class="simple_device2"></div>
    </div>
</section>
<script type="text/javascript">
    $('.simple-easy').backstretch("images/simple-easy.jpg");
    jQuery(document).ready(function () {
        jQuery(window).resize(function () {
            $('.simple-easy').backstretch("images/simple-easy.jpg");
        });
    })
</script>
<section class="clients_section">
    <div class="pattern_top"></div>
    <div class="center"></div>
</section>
