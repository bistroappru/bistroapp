<h3>Получение статусов заказа</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getOrderStatuses <br/>
</p>

<hr/>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/getOrderStatuses ');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "code": 0,
  "data": [
    {
      "id": "1",
      "name": "\u041d\u043e\u0432\u044b\u0439",
      "type": "order"
    },
    {
      "id": "2",
      "name": "\u041f\u0440\u0438\u043d\u044f\u0442",
      "type": "order"
    },
    {
      "id": "3",
      "name": "\u041e\u043f\u043b\u0430\u0447\u0435\u043d",
      "type": "order"
    },
    {
      "id": "6",
      "name": "\u0417\u0430\u043a\u0440\u044b\u0442",
      "type": "order"
    },
    {
      "id": "8",
      "name": "\u041d\u043e\u0432\u044b\u0439",
      "type": "item"
    },
    {
      "id": "9",
      "name": "\u041f\u0440\u0438\u043d\u044f\u0442\u043e",
      "type": "item"
    },
    {
      "id": "10",
      "name": "\u0413\u043e\u0442\u043e\u0432\u0438\u0442\u0441\u044f",
      "type": "item"
    },
    {
      "id": "11",
      "name": "\u0413\u043e\u0442\u043e\u0432\u043e",
      "type": "item"
    },
    {
      "id": "12",
      "name": "\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d\u043e",
      "type": "item"
    }
  ],
  "error": "\u0417\u0430\u043f\u0440\u043e\u0441 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d"
}');?>
    </code></pre>
<hr/>

