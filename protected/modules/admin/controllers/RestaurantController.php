<?php

class RestaurantController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin_company';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('inAdmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($company_id)
	{
        if(!Yii::app()->user->checkAccess('restaurantCreate', array('company_id'=>$company_id))){
            throw new CHttpException(403, 'Действие недоступно для вашей учетной записи');
        }

        $ajax = Yii::app()->request->isAjaxRequest;
		$model=new Restaurant;

        if (!$company_id)
            throw new CHttpException(404, 'Некорректный запрос');
        $model->company_id = $company_id;

		if(isset($_POST['Restaurant']))
		{
            $model->prepareWithPostData();
            if($model->validate(null, false) && $model->save(false))
             	$this->redirect(array('update','id'=>$model->id));
		}

        // подготовка массива телефонов для view
        $model->phone = explode(',', $model->phone);

        // подготовка массива фотографий для view
        $model->photos = CJSON::decode($model->photos);
        if(!is_array($model->photos)) $model->photos = array();

        if ($ajax){
            $this->renderPartial('create',array(
                'model'=>$model
            ));
        }else{
            $this->render('create',array(
                'model'=>$model
            ));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $ajax = Yii::app()->request->isAjaxRequest;
		$model=$this->loadModel($id);

        if(!Yii::app()->user->checkAccess('restaurantView', array('company_id'=>$model->company_id))){
            throw new CHttpException(403, 'Вы не можете просматривать рестораны этой компании');
        }

        if(isset($_POST['Restaurant']))
		{
            if(!Yii::app()->user->checkAccess('restaurantUpdate', array('restaurant'=>$model))){
                throw new CHttpException(403, 'Вы не можете редактировать этот ресторан');
            }
            $model->prepareWithPostData();
            if($model->validate(null, false) && $model->save(false))
                $this->redirect(array('update','id'=>$model->id));
		}

        // подготовка массива телефонов для view
        $model->phone = explode(',', $model->phone);

        // подготовка массива фотографий для view
        $model->photos = CJSON::decode($model->photos);
        if(!is_array($model->photos)) $model->photos = array();

        if ($ajax){
            $this->renderPartial('update',array(
                'model'=>$model
            ));
        }else{
            $this->render('update',array(
                'model'=>$model
            ));
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
        $model = $this->loadModel($id);

        if(!Yii::app()->user->checkAccess('restaurantDelete', array('restaurant'=>$model))){
            throw new CHttpException(403, 'Вы не можете удалить этот ресторан');
        }

        $model->setAttribute('deleted', 1);
        $model->save();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('restaurant/update/id/'.$model->id));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Restaurant the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Restaurant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Restaurant $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='restaurant-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
