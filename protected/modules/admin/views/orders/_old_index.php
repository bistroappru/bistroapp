<?php
/* @var $this OrdersController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs = array(
    'Orders',
);

$this->beginClip('order_table_menu');
if (in_array(Yii::app()->user->role, array('admin','owner','manager','garcon'))){
?>
<ul class="table_menu" style="display: none;">
    <li class="menu_send"><a href="javascript:void(0);" class="set_status" data-id="5"><span class="menu_icon"></span>Отправить</a></li>
    <li class="menu_close"><a href="javascript:void(0);" class="set_status" data-id="6"><span class="menu_icon"></span>Закрыть</a></li>
    <li class="menu_del"><a href="javascript:void(0);"  class="delete_order"><span class="menu_icon"></span>Удалить</a></li>
    <li class="menu_check"><a href="javascript:void(0);"><span class="menu_icon"></span>Чек</a>
        <ul class="table_submenu">
            <li><a href="javascript:void(0);" class="set_payment" data-id="1">Наличными</a></li>
            <li><a href="javascript:void(0);" class="set_payment" data-id="2">Карта</a></li>
        </ul>
    </li>
    <li class="menu_cancel"><a href="javascript:void(0);" class="set_status" data-id="8"><span class="menu_icon"></span>Отменить</a></li>
</ul>
<?php
}else{
?>
<ul class="table_menu" style="display: none;">
    <li class="menu_send"><a href="javascript:void(0);" class="set_status" data-id="3"><span class="menu_icon"></span>Кухня</a></li>
    <li class="menu_send"><a href="javascript:void(0);" class="set_status" data-id="4"><span class="menu_icon"></span>Приготовлен</a></li>
</ul>
<?php
}
$this->endClip();
?>

<div class="order_meta">
    <div class="pages_filter">
        <span class="pf_text">Перейти к странице</span>
        <input class="pf_input" type="text">
        <a class="pf_prev" href="#"></a>
        <a class="pf_number" href="#">1</a>
        <a class="pf_number" href="#">2</a>
        <a class="pf_next" href="#"></a>
    </div>
    <div class="pages_visible">Элементы 1-11 из 25</div>
</div>
<div class="scroll-pane table-scroll">
<table class="order_table">
    <tr class="header_row">
        <th>&nbsp;</th>
        <th>
            <div class="th_inner">ID<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">ID<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th>
            <div class="th_inner">Стол<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Стол<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th>
            <div class="th_inner">Статус<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Статус<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th style="font-size:16px; line-height:16px;">
            <div class="th_inner">Время заказа<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Время заказа<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th>
            <div class="th_inner">Оплата<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Оплата<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th>
            <div class="th_inner" style="font-size:0; line-height:0;"><img src="/images/exclam_gray.png" width="22"
                                                                           height="22" alt=""><span
                    class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip"><img src="/images/exclam_white.png" width="22" height="22" alt=""><span
                        class="tooltip_corner"></span></div>
            </div>
        </th>
        <th>
            <div class="th_inner">Позиций<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Позиций<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th>
            <div class="th_inner">С/S<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">С/S<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th style="font-size:16px;">
            <div class="th_inner">Отправлен<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Отправлен<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <th style="font-size:16px;">
            <div class="th_inner">Закрыт<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Закрыт<span class="tooltip_corner"></span></div>
            </div>
        </th>
        <!--
        <th><div class="th_inner">Оффициант<span class="deploy_plus"></span></div><div class="deploy_hidden"><span class="deploy_minus"></span><div class="deploy_tooltip">Оффициант<span class="tooltip_corner"></span></div></div></th>
        -->
        <th>
            <div class="th_inner">Сумма<span class="deploy_plus"></span></div>
            <div class="deploy_hidden"><span class="deploy_minus"></span>

                <div class="deploy_tooltip">Сумма<span class="tooltip_corner"></span></div>
            </div>
        </th>
    </tr>
    <tr class="manage_row">
    <td>
        <div class="td_inner"></div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord_search">
                <input type="text" class="inp_ord-srh">
                <input type="submit" class="submit_ord-srh" value="">
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select" style="width:60px;">
                <option value="0" selected></option>
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select">
                <option value="0" selected></option>
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a href="#" class="sort_up"></a></div>
                <div class="ord-btn"><a href="#" class="sort_down"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date"></a>

                    <div class="date_tooltip">
                        <h6>Период времени:</h6>

                        <div class="date_tooltip_row">
                            <label>от</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select">
                <option value="0" selected></option>
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner"></div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select">
                <option value="0" selected></option>
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner"></div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a href="#" class="sort_up"></a></div>
                <div class="ord-btn"><a href="#" class="sort_down"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date"></a>

                    <div class="date_tooltip">
                        <h6>Период времени:</h6>

                        <div class="date_tooltip_row">
                            <label>от</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a href="#" class="sort_up"></a></div>
                <div class="ord-btn"><a href="#" class="sort_down"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date"></a>

                    <div class="date_tooltip">
                        <h6>Период времени:</h6>

                        <div class="date_tooltip_row">
                            <label>от</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a href="#" class="sort_up"></a></div>
                <div class="ord-btn"><a href="#" class="sort_down"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date"></a>

                    <div class="date_tooltip">
                        <h6>Период времени:</h6>

                        <div class="date_tooltip_row">
                            <label>от</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <select class="order_select" style="width:36px;">
                                <option value="22" selected>22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                            <select class="order_select" style="width:68px;">
                                <option value="8" selected>август</option>
                                <option value="9">сентябрь</option>
                                <option value="10">октябрь</option>
                                <option value="11">ноябрь</option>
                            </select>
                            <select class="order_select" style="width:46px;">
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014" selected>2014</option>
                            </select>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    </tr>
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',

    )); ?>
</table>
</div>
<div class="food_detail" style="display: none;">
    <div class="detail_meta">
        <div class="food_buttons_left">
            <!--
            <a href="#" class="food_plus"></a>
            <a href="#" class="food_minus"></a>
            -->
            <a href="javascript:void(0);" class="food_del"></a>
        </div>
        <a href="#" class="detail_arrow">Детально</a>
    </div>
    <div class="food_table_wrap" style="display:none;"></div>
</div>
