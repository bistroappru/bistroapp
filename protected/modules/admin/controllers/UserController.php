<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $user_company_id = 0;
    public $user_restaurant_id = 0;
    public $layout='admin_users';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
            'ajaxOnly + changepassword',

		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',
                'roles'=>array('inAdmin'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($restaurant_id)
	{
        $ajax = Yii::app()->request->isAjaxRequest;
        $restaurant = Restaurant::model()->findByPk($restaurant_id);

		$model = new User;
        $model->company_id = $restaurant->company->id;
        $model->restaurant_id = $restaurant->id;
        $model->role = 'garcon';

        if(!Yii::app()->user->checkAccess('userCreate', array('user'=>$model))){
            throw new CHttpException(403, 'Вы не можете создавать пользователя для этого заведения');
        }

        $this->user_company_id = $model->company_id;
        $this->user_restaurant_id = $model->restaurant_id;

		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->prepareWithPostData();
			if($model->save()){
                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->From = Yii::app()->params['from'];
                $mailer->IsHTML(true);
                $mailer->AddAddress($model->email);
                $mailer->FromName = Yii::app()->params['from_name'];
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Регистрационные данные';
                $mailer->Body = $this->renderPartial('create_update_mail', array('model' => $model, 'labels' => $model->attributeLabels(), 'password' => $_POST['User']['password'], 'update' => 0), true);
                $mailer->Send();
                $this->redirect(array('update','id'=>$model->id));
            }
		}

        if ($ajax){
            $this->renderPartial('create',array(
                'model'=>$model,
            ));
        }else{
            $this->render('create',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $ajax = Yii::app()->request->isAjaxRequest;

		$model=$this->loadModel($id);

        if(!Yii::app()->user->checkAccess('userView', array('user'=>$model))){
            throw new CHttpException(403, 'Вы не можете просматривать данного пользователя');
        }

        $this->user_company_id = $model->company_id;
        $restaurant_id = $model->restaurant_id ? $model->restaurant_id : Yii::app()->request->getParam('restaurant_id', 0);
        if (!$restaurant_id){
            $criteria = new CDbCriteria();
            $criteria->select = 'id';
            $criteria->limit = 1;
            $criteria->order = 'name ASC';
            $criteria->compare('company_id', $this->user_company_id);
            $tmp_rest = Restaurant::model()->find($criteria);
            $restaurant_id = intval($tmp_rest->id);
        }
        $this->user_restaurant_id = $restaurant_id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        if(isset($_POST['User']))
		{
            if(!Yii::app()->user->checkAccess('userUpdate', array('user'=>$model))){
                throw new CHttpException(403, 'Вы не можете редактировать данного пользователя');
            }
            $model->prepareWithPostData();
            if($model->save()) {
                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->From = Yii::app()->params['from'];
                $mailer->IsHTML(true);
                $mailer->AddAddress($model->email);
                $mailer->FromName = Yii::app()->params['from_name'];
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Изменение данных';
                $mailer->Body = $this->renderPartial('create_update_mail', array('model' => $model, 'labels' => $model->attributeLabels(), 'password' => $_POST['User']['password'], 'update' => 1), true);
                $mailer->Send();
                $this->redirect(array('update','id'=>$model->id));
            }
		}

        if ($ajax){
            $this->renderPartial('update',array(
                'model'=>$model,
                'restaurant_id'=>$restaurant_id
            ));
        }else{
            $this->render('update',array(
                'model'=>$model,
                'restaurant_id'=>$restaurant_id
            ));
        }
	}

    public function actionTableList($restaurant_id){
        $user_id = Yii::app()->request->getParam('user_id', 0);
        $role = Yii::app()->request->getParam('role', 0);
        if ($user_id){
            $model = $this->loadModel($user_id);
        }else{
            $model = new User;
        }
        $model->restaurant_id = $restaurant_id;
        $model->role = $role;

        $this->renderPartial('_user_tables',array(
            'model'=>$model,
        ));
    }

    public function actionChangepassword($id)
    {
        if (!isset($_POST['Password']) || !$_POST['Password']['new']) {
            echo 'Пароль не может быть пустым!';
            Yii::app()->end();
        }

        if ($_POST['Password']['new'] !== $_POST['Password']['confirm']) {
            echo 'Неверное подтверждение пароля!';
            Yii::app()->end();
        }

        $model = $this->loadModel($id);

        if(!Yii::app()->user->checkAccess('userUpdate', array('user'=>$model))){
            throw new CHttpException(403, 'Вы не можете редактировать данного пользователя');
        }

        $model->password = md5($_POST['Password']['new']);
        if ($model->save()){
            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->From = Yii::app()->params['from'];
            $mailer->IsHTML(true);
            $mailer->AddAddress($model->email);
            $mailer->FromName = Yii::app()->params['from_name'];
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Изменение пароля';
            $mailer->Body = $this->renderPartial('update_pass_mail', array('model' => $model, 'labels' => $model->attributeLabels(), 'password' => $_POST['Password']['new']), true);
            $mailer->Send();
            echo 'Пароль успешно изменен! <br/>Новый пароль '.$_POST['Password']['new'];
            Yii::app()->end();
        }
        echo 'Не удалось сменить пароль <br/> Попробуйте повторить попытку';
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        if ($id == Yii::app()->user->id) throw new CHttpException(403, 'Нельзя удалить самого себя.');



		$model = $this->loadModel($id);

        if(!Yii::app()->user->checkAccess('userUpdate', array('user'=>$model))){
            throw new CHttpException(403, 'Вы не можете удалить данного пользователя');
        }

        $model->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        if (Yii::app()->request->isAjaxRequest){
            $id = Yii::app()->request->getParam('restaurant_id', 0);
            $this->renderPartial('index', array('restaurant_id'=>$id));
        }else{
            $this->render('index');
        }
	}

    public function actionGetUserList($restaurant_id)
    {
        $this->renderPartial('_getUserList', array('restaurant_id' => $restaurant_id));
    }

    public function actionGetrestaurants(){
        $restaurants = AbcHelper::allCompanyRestaurants(isset($_POST['User']['company_id']) ? $_POST['User']['company_id'] : 0);
        $this->renderPartial('_getrestaurants',array('restaurants'=>$restaurants));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
