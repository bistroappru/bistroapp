<?php
$this->beginClip('sidebar');
?>
<li><a href="#getCompanies">getCompanies</a></li>
<li><a href="#getDishesCategories">getDishesCategories</a></li>
<li><a href="#getDishesCategoriesImages">getDishesCategoriesImages</a></li>
<li><a href="#getDishes">getDishes</a></li>
<li><a href="#getDishesImages">getDishesImages</a></li>
<li><a href="#getOrders">getOrders</a></li>
<li><a href="#getOrderPayments">getOrderPayments</a></li>
<li><a href="#getOrderStatuses">getOrderStatuses</a></li>
<li><a href="#postOrder">postOrder</a></li>
<?php
$this->endClip();
?>
<!-- getCompanies -->
<div class="section">
    <h2 id="getCompanies">getCompanies</h2>
    <div class="method">
        <div class="title">Получение списка всех ресторанов системы:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li>без параметров</li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getCompanies
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{ "code" : 0,
  "data" : [
        {"deleted" : "0",
        "hidden" : "0",
        "id" : "1",
        "image" : "/9j/4AAQSkZJRgABAQEAZABkAAD/2wBDAAMCAgMCAgMDAgMDAwMDB...",
        "name" : "Тануки",
        "description" : "Ресторан японской кухни",
        "restaurants" : [
            { "deleted" : "0",
              "hidden" : "0",
              "id" : "3",
              "location" : "Борисовские пруды, д.10",
              "company_id" : "1",
              "description" : "лучший ресторан",
              "phone" : "+7(495)123-45-67",
              "working_hours" : "Пн-Пт 10:00-23:00"
            },
            { "deleted" : "0",
              "hidden" : "0",
              "id" : "4",
              "location" : "Боровское шоссе, д.31",
              "company_id" : "1",
              "description" : "лучший ресторан",
              "phone" : "+7(495)123-45-67",
              "working_hours" : "Пн-Пт 10:00-23:00"
            }
          ]
      },
      ....
    ],
  "error" : "correct (успешный результат запроса)"
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>


<!-- getDishesCategories -->
<div class="section">
    <h2 id="getDishesCategories">getDishesCategories</h2>
    <div class="method">
        <div class="title">Получение категорий меню для выбранного ресторана:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li><b>company_id</b> &mdash; ID компании(сеть ресторанов), меню которой нужно получить. <b>обязательный</b></li>
            <li><b>version</b> &mdash; номер последней версии полученной приложением <b>необязательный, по умолчанию вернет всё</b></li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getDishesCategories?company_id=1&version=3
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{
  "status": "ok",
  "version": "5",
  "data": [
    {
      "id": "1",
      "name": "\u0417\u0430\u043a\u0443\u0441\u043a\u0438",
      "company_id": "1",
      "parent_id": "0",
      "sort": "0",
      "description": "\u041e\u043f\u0438\u0441\u0430\u043d\...",
      "hidden": "0"
    },
    {
      "id": "17",
      "name": "345345",
      "company_id": "1",
      "parent_id": "0",
      "sort": "0",
      "description": "",
      "hidden": "0"
    }
  ]
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>

<!-- getDishesCategoriesImages -->
<div class="section">
    <h2 id="getDishesCategoriesImages">getDishesCategoriesImages</h2>
    <div class="method">
        <div class="title">Получение изображений категорий меню для выбранного ресторана:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li><b>company_id</b> &mdash; ID компании(сеть ресторанов), меню которой нужно получить. <b>обязательный</b></li>
            <li><b>version</b> &mdash; номер последней версии полученной приложением <b>необязательный, по умолчанию вернет всё</b></li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getDishesCategoriesImages?company_id=1&version=3
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{
  "status": "ok",
  "version": "6",
  "data": [
    {
      "id": "1",
      "image": "iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA...",
      "extension": "png",
      "size": {
        "width": 60,
        "height": 60
      }
    }
  ]
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>


<!-- getDishes -->
<div class="section">
    <h2 id="getDishes">getDishes</h2>
    <div class="method">
        <div class="title">Получение блюд для выбранного ресторана:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li><b>company_id</b> &mdash; ID компании(сеть ресторанов), блюда которой нужно получить. <b>обязательный</b></li>
            <li><b>version</b> &mdash; номер последней версии полученной приложением <b>необязательный, по умолчанию вернет всё</b></li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getDishes?company_id=1
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{
  "status": "ok",
  "version": "6",
  "data": [
    {
      "id": "8",
      "is_group": "0",
      "name": "\u041a\u0430\u043b\u0438\u0444\u043e\...",
      "description": "\u041c\u044f\u0441\u043e \u043a...",
      "menu_id": "1",
      "sort": "1",
      "depressed": "0",
      "label_id": "2",
      "conditions": [
        {
          "id": "25",
          "measure": "200 \u0433\u0440.",
          "sort": "0",
          "price": "250",
          "dish_id": "8",
          "action": ""
        }
      ],
      "hidden": "0",
      "calories": "",
      "in_stock": "3,4"
    },
    {
      "id": "23",
      "is_group": "0",
      "name": "\u041a\u0438\u043c\u0443\u0447\u0438",
      "description": "\u041e\u0441\u0442\u0440\u044b\u0439...",
      "menu_id": "1",
      "sort": "0",
      "depressed": "0",
      "label_id": "1",
      "conditions": [
        {
          "id": "36",
          "measure": "1 \u0448\u0442.",
          "sort": "0",
          "price": "95",
          "dish_id": "23",
          "action": ""
        }
      ],
      "hidden": "0",
      "calories": "",
      "in_stock": ""
    }
  ]
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>

<!-- getDishesImages -->
<div class="section">
    <h2 id="getDishesImages">getDishesImages</h2>
    <div class="method">
        <div class="title">Получение изображений блюд для выбранного ресторана:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li><b>company_id</b> &mdash; ID компании(сеть ресторанов), изображения блюд которой нужно получить. <b>обязательный</b></li>
            <li><b>version</b> &mdash; номер последней версии полученной приложением <b>необязательный, по умолчанию вернет всё</b></li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getDishesImages?company_id=1&version=3
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{
  "status": "ok",
  "version": "4",
  "data": [
    {
      "id": 8,
      "image": "\/9j\/4AAQSkZJRgABAQAAAQABAAD...",
      "extension": "jpeg",
      "size": {
        "width": 640,
        "height": 640
      }
    }
    ....
  ]
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>

<!-- getOrders -->
<div class="section">
    <h2 id="getOrders">getOrders</h2>
    <div class="method">
        <div class="title">Получение заказов пользователя:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li><b>token</b> &mdash; появится при появлении функции авторизации, пока по умолчанию выводим все заказы</li>
            <li><b>company_id</b> &mdash; компания, заказы которой необходимо получить (необязательный)</li>
            <li><b>restaurant_id</b> &mdash; ресторан, заказы которого необходимо получить (необязательный)</li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getOrders?token=[code]
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{
  "code": 0,
  "data": [
    {
      "id": "38",
      "user_id": "1",
      "table": "2",
      "status_id": "1",
      "date_create": "1405688493",
      "payment_id": "1",
      "attention": "1",
      "separated": "1",
      "separated_info": "\u0442\u0435\u043a\u0441\u0442 ...",
      "date_execute": "0",
      "date_close": "0",
      "comment": "",
      "company_id": "1",
      "restaurant_id": "3",
      "garcon_id": "5",
      "count_items": "2",
      "total_price": "720",
      "items": [
        {
          "order_id": "38",
          "condition_id": "41",
          "status_id": "8",
          "price": "90",
          "count": "3"
        },
        {
          "order_id": "38",
          "condition_id": "16",
          "status_id": "8",
          "price": "150",
          "count": "3"
        }
      ]
    },
    ...
  ],
  "error": "\u0417\u0430\u043f\u0440\u043e\u0441 ..."
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>
<!-- getOrderStatuses -->
<div class="section">
    <h2 id="getOrderStatuses">getOrderStatuses</h2>
    <div class="method">
        <div class="title">Получение статусов заказа:</div>
        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getOrderStatuses
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{ "code" : 0,
  "data" : [ { "id" : "1",
        "name" : "Новый"
      },
      { "id" : "2",
        "name" : "Принят"
      },
      { "id" : "3",
        "name" : "Кухня"
      },
      { "id" : "4",
        "name" : "Отправлен"
      },
      { "id" : "5",
        "name" : "Закрыт"
      }
    ],
  "error" : "correct (успешный результат запроса)"
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>

<!-- getOrderPayments -->
<div class="section">
    <h2 id="getOrderPayments">getOrderPayments</h2>
    <div class="method">
        <div class="title">Получение способов оплаты:</div>
        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/getOrderPayments
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{ "code" : 0,
  "data" : [ { "id" : "1",
        "name" : "Наличными"
      },
      { "id" : "2",
        "name" : "Картой"
      }
    ],
  "error" : "correct (успешный результат запроса)"
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>
<!-- postOrder -->
<div class="section">
    <h2 id="postOrder">postOrder</h2>
    <div class="method">
        <div class="title">Создание заказа:</div>
        <div class="title_1">Параметры запроса:</div>
        <ul>
            <li><b>token</b> &mdash; появится при появлении функции авторизации, пока по умолчанию выводим все заказы</li>
        </ul>
        <br>

        <div class="title_1">Пример запроса:</div>
        <blockquote>
            http://bistroapp.ru/api/postOrder?token=[code]
        </blockquote>
        <br/>
        <div class="title_1">Пример тела запроса в формате JSON:</div>
        <blockquote>
            <pre>
{ "attention" : 1,
  "payment_id" : 1,
  "restaurant_id" : 3,
  "positions" : [{
        "condition_id" : 16,
        "count" : 3,
        "price" : 150,
      }],
  "separated" : 1,
  "separated_info" : "текст раздельного заказа",
  "table_id" : 12
}
            </pre>
        </blockquote>
        <br/>
        <div class="title_1">Пример ответа:</div>
        <blockquote>
            <pre>
{ "code" : 2,
  "data" : [  ],
  "error" : "Заказ должен содержать хотя бы одну позицию"
}
            </pre>
        </blockquote>
        <br>
    </div>
</div>

