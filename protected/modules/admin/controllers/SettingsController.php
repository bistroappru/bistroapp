<?php

class SettingsController extends Controller
{
    public $layout = 'admin_full_width';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'roles'=>array('inAdmin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $company_id = Yii::app()->user->company_id;
        if ($company_id){
            $this->redirect(array('company','id' => $company_id));
        }

        if (Yii::app()->request->isAjaxRequest){
            $this->renderPartial('index');
        }else{
            $this->render('index');
        }
    }

    public function actionCompany($id)
    {
        $model = CompanyTools::model()->findByPk($id);
        if($model===null){
            $model = new CompanyTools;
            $model->company_id = $id;
        }

        if(!Yii::app()->user->checkAccess('companyTools', array('company_id'=>$model->company_id))){
            throw new CHttpException(403, 'У вас нет прав для редактирования настроек данной компании');
        }

        if(isset($_POST['CompanyTools']))
        {
            $model->attributes=$_POST['CompanyTools'];
            if($model->save())
                $this->redirect(array('company','id'=>$model->company_id));
        }

        if (Yii::app()->request->isAjaxRequest){
            $this->renderPartial('company', array('model' => $model));
        }else{
            $this->render('company', array('model' => $model));
        }
    }
}