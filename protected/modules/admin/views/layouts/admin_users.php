<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_main'); ?>

<div id="page">
    <div class="column_left">
        <?php $this->widget('application.components.CompanyUser', array('company_id'=>$this->user_company_id, 'restaurant_id'=>$this->user_restaurant_id)); ?>
    </div>
    <div id="content_wrapper">
        <?php echo $content; ?>
    </div>
    <div class="clear"></div>
</div>
<?php $this->endContent(); ?>