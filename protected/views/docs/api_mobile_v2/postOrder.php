<h3>Создание заказа</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/postOrder<br/>
</p>
<hr/>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
            "uuid": "550e8400-e29b-41d4-a716-446655440000",
            "platform": "iOS",
            "name": "iPhone 5",
            "version": "7.1"
        },
        "order": {
            "attention": 1,
            "payment_id": 1,
            "restaurant_id": 47,
            "positions": [
                {
                    "position_id": 224,
                    "count": 1,
                    "price": 520,
                    "comment": "без лука"
                },
                {
                    "position_id": 225,
                    "count": 1,
                    "price": 240
                }
            ],
            "separated": 0,
            "comment": "без лука",
            "table": "1",
            "guests": 3
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
        {"status":"ok"}
    </code></pre>
<hr/>