<?php
/* @var $this SettingsController */
/* @var $model CompanyTools */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'company-tools-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->hiddenField($model, 'company_id'); ?>
    <?php echo $form->error($model, 'company_id'); ?>

    <table>
        <tr>
            <td width="45%" align="left" valign="top">
                <div class="input_row">
                    <label class="product_label">Категория содержащая бизнес-ланчи</label>
                    <div class="inp_wrap">
                        <?php echo $form->dropDownList($model, 'business_lunch_ids', (array('Не выбрано') + $model->company->getMenuCategories())); ?>
                        <?php echo $form->error($model, 'business_lunch_ids'); ?>
                    </div>
                </div>
            </td>
            <td width="10%">&nbsp;</td>
            <td align="left" valign="top">
                <div class="input_row">
                    <label class="product_label">Тестовое поле</label>
                    <div class="inp_wrap">
                        <input type="text" name="xxxxxx">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <ul class="leftmenu_buttons dark">
        <li><?php echo CHtml::link('<img src="/images/leftmenu-4.png" width="50" height="50" alt=""><span>Сохранить</span>', '#', array('id' => 'company_tools_save')); ?></li>
    </ul>
    <div class="clear"></div>
    <?php $this->endWidget(); ?>
</div>