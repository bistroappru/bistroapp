<div class="product_row condition_row">
    <?php echo $data->getErrorsForm();?>
    <div class="product_left">
        <input type="hidden" name="Position[conditions][<?php echo $key;?>][id]" value="<?php echo $data->id;?>">
        <input type="hidden" class="sort" name="Position[conditions][<?php echo $key;?>][sort]" value="<?php echo $data->sort;?>">
        <textarea  name="Position[conditions][<?php echo $key;?>][measure]" class="product_serving <?=$data->hasErrors('measure') ? 'error' : ''?>"><?php echo $data->measure;?></textarea>
        <a href="#" class="serving_del" data-id="<?=$data->id?>"></a>
    </div>
    <div class="product_right">
        <ul class="prices_list radio_wrap">
            <li class="cond_price_def">
                <div class="prices_left">
                    <div class="prices_cell">
                        <p>ЦЕНА:</p>
                    </div>
                </div>
                <div class="check_item">
                    <input type="radio" id="action_off_<?php echo $key;?>" <?php echo $data->action_on == 0 ? 'checked="checked"' : '';?> value="0" name="Position[conditions][<?php echo $key;?>][action_on]">
                    <label for="action_off_<?php echo $key;?>" class="label_radio"></label>
                    <input type="text" value="<?php echo $data->price;?>" name="Position[conditions][<?php echo $key;?>][price]" class="inp_price <?=$data->hasErrors('price') ? 'error' : ''?>">
                </div>
            </li>
            <li class="cond_price_act">
                <div class="prices_left"><div class="prices_cell"><p>ЦЕНА:</p><p class="action">Акция</p></div></div>
                <div class="check_item">
                    <input type="radio" id="action_on_<?php echo $key;?>" <?php echo $data->action_on == 1 ? 'checked="checked"' : '';?> value="1" name="Position[conditions][<?php echo $key;?>][action_on]">
                    <label for="action_on_<?php echo $key;?>" class="label_radio"></label>
                    <input type="text" value="<?php echo $data->action->sale_price;?>" name="Position[conditions][<?php echo $key;?>][action][sale_price]" class="inp_price <?=($data->action && $data->action->hasErrors('sale_price')) ? 'error' : ''?>">
                </div>
                <div class="action_period" <?php echo $data->action_on == 0 ? 'style="display: none;"' : '';?>>
                    <p class="action_time">срок действия акции:</p>
                    <input type="text" value="<?php echo $data->action->date_on ? date('d.m.Y', $data->action->date_on) : '';?>" name="Position[conditions][<?php echo $key;?>][action][date_on]" class="action_date datepicker <?=($data->action && $data->action->hasErrors('date_on')) ? 'error' : ''?>" readonly="readonly">
                    -
                    <input type="text" value="<?php echo $data->action->date_off ? date('d.m.Y', $data->action->date_off) : '';?>" name="Position[conditions][<?php echo $key;?>][action][date_off]" class="action_date datepicker <?=($data->action && $data->action->hasErrors('date_off')) ? 'error' : ''?>" readonly="readonly">
                </div>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
