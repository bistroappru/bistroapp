<tr class="manage_row">
    <td>
        <div class="td_inner">
            <div class="ord_search">
                <input type="text" value="<?=$data_params['id'];?>" class="inp_ord-srh" name="id">
                <input type="button" class="submit_ord-srh fliter_submit_but" value="">
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select" name="table_id">
                <option value="0">Все</option>
                <?
                foreach(RestaurantTable::getTables(Yii::app()->user->restaurant_id_full) as $v){
                        if($data_params['table_id'] == $v->id) {
                            $active = 'selected';
                        } else {
                            $active = '';
                        }
                    ?>
                    <option value="<?=$v->id;?>" <?=$active;?>><?=$v->name;?></option>
                <?php
                }
                ?>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select" name="status_id">
                <option value="0">Все</option>
                <?
                foreach(OrderStatus::getAll('order') as $k=>$v){
                    if(isset($data_params['status_id']) && $data_params['status_id'] != '') {
                        if($data_params['status_id'] == $k) {
                            $active = 'selected';
                        } else {
                            $active = '';
                        }

                    }?>
                    <option value="<?=$k;?>" <?=$active;?>><?=$v;?></option>
                <?php
                }
                ?>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a data-id="date_create asc" href="#" class="sort_up <?=$dataProvider->criteria->order == 'date_create asc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn"><a data-id="date_create desc" href="#" class="sort_down <?=$dataProvider->criteria->order == 'date_create desc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date <?=isset($data_params['date_create']) ? 'active' : ''?>"></a>
                    <div class="date_tooltip" id="date_create_tooltip">
                        <input type="hidden" name="field_name" value="date_create">
                        <h6>Период времени:</h6>
                        <div class="date_tooltip_row">
                            <label>от</label>
                            <input name="start_date" class="datepicker sort_date_input" value="<?php
                            if(isset($data_params['date_create']) && isset($data_params['date_create']['start_date'])) {
                                echo date('d.m.Y', $data_params['date_create']['start_date']);
                            }?>" />
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <input name="end_date" class="datepicker sort_date_input" value="<?php
                            if(isset($data_params['date_create']) && isset($data_params['date_create']['end_date'])) {
                                echo date('d.m.Y', $data_params['date_create']['end_date']);
                            }?>" />
                        </div>
                        <div class="filters_buttons_wrap">
                            <a href="javascript:void(0);" class="apply_sort_link">Применить</a>
                            <a href="#" class="cancel_sort_link">Сбросить</a>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select" name="payment_id">
                <option value="0">Все</option>
                <?
                foreach(OrderPayment::getAll() as $k=>$v){
                    if(isset($data_params['payment_id']) && $data_params['payment_id'] != '') {
                        if($data_params['payment_id'] == $k) {
                            $active = 'selected';
                        } else {
                            $active = '';
                        }

                    }?>
                    <option value="<?=$k;?>" <?= $active;?>><?=$v;?></option>
                <?php
                }
                ?>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner"></div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord_search">
                <input type="text" value="<?=$data_params['count_items'];?>" class="inp_ord-srh" name="count_items">
                <input type="button" class="submit_ord-srh fliter_submit_but" value="">
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner"></div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a data-id="date_execute asc" href="#" class="sort_up <?=$dataProvider->criteria->order == 'date_execute asc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn"><a data-id="date_execute desc" href="#" class="sort_down <?=$dataProvider->criteria->order == 'date_execute desc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date <?=isset($data_params['date_execute']) ? 'active' : ''?>"></a>
                    <div class="date_tooltip" id="date_execute_tooltip">
                        <input type="hidden" name="field_name" value="date_execute">
                        <h6>Период времени:</h6>
                        <div class="date_tooltip_row">
                            <label>от</label>
                            <input name="start_date" class="datepicker sort_date_input" value="<?php
                            if(isset($data_params['date_execute']) && isset($data_params['date_execute']['start_date'])) {
                                echo date('d.m.Y', $data_params['date_execute']['start_date']);
                            }?>" />
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <input name="end_date" class="datepicker sort_date_input" value="<?php
                            if(isset($data_params['date_execute']) && isset($data_params['date_execute']['end_date'])) {
                                echo date('d.m.Y', $data_params['date_execute']['end_date']);
                            }?>" />
                        </div>
                        <div class="filters_buttons_wrap">
                            <a href="#" class="apply_sort_link">Применить</a>
                            <a href="#" class="cancel_sort_link">Сбросить</a>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a data-id="date_close asc" href="#" class="sort_up <?=$dataProvider->criteria->order == 'date_close asc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn"><a data-id="date_close desc" href="#" class="sort_down <?=$dataProvider->criteria->order == 'date_close desc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn">
                    <a href="#" class="sort_date <?=isset($data_params['date_close']) ? 'active' : ''?>"></a>
                    <div class="date_tooltip" id="date_close_tooltip">
                        <input type="hidden" name="field_name" value="date_close">
                        <h6>Период времени:</h6>
                        <div class="date_tooltip_row">
                            <label>от</label>
                            <input name="start_date" class="datepicker sort_date_input" value="<?php
                            if(isset($data_params['date_close']) && isset($data_params['date_close']['start_date'])) {
                                echo date('d.m.Y', $data_params['date_close']['start_date']);
                            }?>" />
                        </div>
                        <div class="date_tooltip_row">
                            <label>до</label>
                            <input name="end_date" class="datepicker sort_date_input" value="<?php
                            if(isset($data_params['date_close']) && isset($data_params['date_close']['end_date'])) {
                                echo date('d.m.Y', $data_params['date_close']['end_date']);
                            }?>" />
                        </div>
                        <div class="filters_buttons_wrap">
                            <a href="#" class="apply_sort_link">Применить</a>
                            <a href="#" class="cancel_sort_link">Сбросить</a>
                        </div>
                        <span class="tooltip_corner"></span>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <select class="order_select" name="garcon_id">
                <option value="0">Все</option>
                <?
                foreach(User::getGarconByRestaurant(Yii::app()->user->restaurant_id_full) as $k=>$v){
                    if(isset($data_params['garcon_id']) && $data_params['garcon_id'] != '') {
                        if($data_params['garcon_id'] == $k) {
                            $active = 'selected';
                        } else {
                            $active = '';
                        }

                    }?>
                    <option value="<?=$k;?>" <?=$active;?>><?=$v;?></option>
                <?php
                }
                ?>
            </select>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <div class="ord-table_btns">
                <div class="ord-btn"><a data-id="total_price asc" href="#" class="sort_up <?=$dataProvider->criteria->order == 'total_price asc' ? 'active' : ''?>"></a></div>
                <div class="ord-btn"><a data-id="total_price desc" href="#" class="sort_down <?=$dataProvider->criteria->order == 'total_price desc' ? 'active' : ''?>"></a></div>
            </div>
        </div>
    </td>
</tr>