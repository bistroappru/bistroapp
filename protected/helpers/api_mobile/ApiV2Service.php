<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 02.02.15
 * Time: 10:26
 */
class ApiV2Service {

    private static $_api_errors = array(
        100 => 'Не удалось найти компанию с ID = {id}',
        101 => 'Не удалось обработать информацию об устройстве',
        102 => 'Ошибка сохранения информации об устройстве. ',
        103 => 'Ваше устройство заблокировано',
        104 => 'Не удалось найти ресторан с ID = {id}',
        105 => 'Не удалось обработать данные',
        106 => 'Ошибка сохранения фидбека',
        130 => 'Ошибка создания заказа. ',
        131 => 'Заказ не содержит ни одной позиции',
        132 => 'Не удалось найти заказ с ID = {id}',
        133 => 'Нет доступа. Заказ с ID = {id} был сделан с другого устройства',
        134 => 'К заданному столу не привязан оффициант',
    );

    /**
     * @param $code
     * @param bool $parse
     * @param array $params
     * @return mixed
     */
    public static function getError($code, $parse = false, $params = array()){
        if (isset($code) && array_key_exists($code, self::$_api_errors)){
            $msg = self::$_api_errors[$code];

            if ($parse && count($params)){
                $msg = str_replace(array_keys($params), array_values($params), self::$_api_errors[$code]);
            }

            return $msg;
        }
        return $code . ' (Неизвестная ошибка)';
    }

    private static function getFistValidationError(CActiveRecord $model){
        $errors = $model->getErrors();
        $error = reset($errors);
        return $error[0];
    }

    public static function parseData($json){
        $data = CJSON::decode($json);
        if (!$data){
            throw new Exception(self::getError(105), 105);
        }

        return $data;
    }

    public static function createDevice($attributes = array()){
        $model = new Device();
        $model->attributes = $attributes;

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new Exception(self::getError(102) . self::getFistValidationError($model), 102);
            }else{
                throw new Exception(self::getError(102) . 'Неизвестная ошибка сохранения в базу данных', 102);
            }
        }

        return $model;
    }

    public static function loadDevice($json){
        if (!is_array($json)){
            $device_data = CJSON::decode($json);
            if (!$device_data){
                throw new Exception(self::getError(101), 101);
            }
        }else{
            $device_data = $json;
        }

        // load device
        $device = Device::model()->findByAttributes(array('uuid' => $device_data['uuid']));

        if ($device === null)
            $device = self::createDevice($device_data);

        if ($device->blocked)
            throw new Exception(self::getError(103), 103);

        return $device;
    }

    public static function log(Device $device, $request = '', $body = ''){
        $attributes = array(
            'device_id' => $device->id,
            'command' => Yii::app()->controller->action->id,
            'body' => (is_array($body) ? json_encode($body, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 256) : $body),
            'request' => (is_array($request) ? json_encode($request, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 256) : $request),
        );

        $log = new DeviceLog();
        $log->setAttributes($attributes);
        $log->save();
    }

    public static function setPushToken(Device $model, $token){
        $model->setAttribute('push_token', $token);

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new Exception(self::getError(102) . self::getFistValidationError($model), 102);
            }else{
                throw new Exception(self::getError(102) . 'Неизвестная ошибка сохранения в базу данных', 102);
            }
        }
    }

    /**
     * @param $version
     * @return array
     */
    public static function getCompanies($version){
        $data = array(
            'data' =>array()
        );

        $list = Company::model()->findAll(array(
            'condition' => 'version > :version',
            'params' => array(':version' => $version),
        ));

        foreach ($list as $row){
            if (!isset($data['version']) || $data['version'] < $row->version)
                $data['version'] = $row->version;

            $data['data'][] = array(
                'id' => $row->id,
                'name' => $row->name,
                'hidden' => $row->hidden,
                'deleted' => $row->deleted,
                'description' => $row->description,
                'website' => $row->website,
                'email' => $row->email,
            );
        }

        if (empty($data['version'])){
            $data['version'] = Company::getLastVersion();
        }

        return $data;
    }

    /**
     * @param $version
     * @return array
     */
    public static function getCompaniesImages($version){
        $data = array(
            'data' =>array()
        );

        $list = Company::model()->findAll(array(
            'select' => 'id, logo, imageVersion',
            'condition' => 'imageVersion > :version',
            'params' => array(':version' => $version),
        ));

        foreach ($list as $row){
            if (!$row->logo || !file_exists($_SERVER['DOCUMENT_ROOT'].$row->logo)) continue;

            if (!isset($data['version']) || $data['version'] < $row->imageVersion)
                $data['version'] = $row->imageVersion;

            $image_info = getimagesize(Yii::app()->request->hostInfo.$row->logo);
            $path_info = pathinfo(Yii::app()->request->hostInfo.$row->logo);
            $data['data'][] = array(
                'id' => $row->id,
                'image' => base64_encode(file_get_contents(Yii::app()->request->hostInfo.$row->logo)),
                'extension' => $path_info['extension'],
                'size' => array(
                    'width' => $image_info[0],
                    'height' => $image_info[1]
                )
            );
        }

        if (empty($data['version'])){
            $data['version'] = Company::getLastVersion('imageVersion');
        }

        return $data;

    }

    /**
     * @param $company_id
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function getRestaurants($company_id, $version = -1){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании

        /*
        $company = Company::model()->findByPk($company_id);
        if ($company === null)
            throw new Exception(self::getError(100, true, array('{id}' => $company_id)), 100);
        */

        // выборка категорий
        $criteria = new CDbCriteria();
        $criteria->addCondition('version > ' . $version);
        if ($company_id)
            $criteria->addCondition('company_id = ' . $company_id);

        $list = Restaurant::model()->findAll($criteria);

        foreach ($list as $row){
            if (!isset($data['version']) || $data['version'] < $row->version)
                $data['version'] = $row->version;

            $data['data'][] = array(
                'id' => $row->id,
                'name' => $row->name,
                'company_id' => $row->company_id,
                'location' => $row->location,
                'description' => $row->description,
                'hidden' => $row->hidden,
                'deleted' => $row->deleted,
                'phone' => $row->phone,
                'working_hours' => $row->working_hours,
            );
        }

        if (empty($data['version'])){
            $data['version'] = Restaurant::getLastVersion('version');
        }

        return $data;
    }


    /**
     * @param $company_id
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function getRestaurantsPhotos($company_id, $version = -1){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании
        /*
        $company = Company::model()->findByPk($company_id);
        if ($company === null)
            throw new Exception(self::getError(100, true, array('{id}' => $company_id)), 100);
        */

        // выборка категорий
        $criteria = new CDbCriteria();
        $criteria->select = 'id, photos, imageVersion';
        $criteria->addCondition('imageVersion > ' . $version);
        if ($company_id)
            $criteria->addCondition('company_id = ' . $company_id);


        $list = Restaurant::model()->findAll($criteria);

        foreach ($list as $restaurant){
            $photos = CJSON::decode($restaurant->photos);
            $images = array();
            if (is_array($photos)){
                foreach ($photos as $photo){
                    if (!$photo || !file_exists($_SERVER['DOCUMENT_ROOT'].$photo)) continue;

                    if (!isset($data['version']) || $data['version'] < $restaurant->imageVersion)
                        $data['version'] = $restaurant->imageVersion;

                    $image_info = getimagesize(Yii::app()->request->hostInfo.$photo);
                    $path_info = pathinfo(Yii::app()->request->hostInfo.$photo);

                    $images[] = array(
                        'image' => base64_encode(file_get_contents(Yii::app()->request->hostInfo.$photo)),
                        'extension' => $path_info['extension'],
                        'size' => array(
                            'width' => $image_info[0],
                            'height' => $image_info[1]
                        )
                    );
                }
            }

            if (!count($images))
                continue;

            $data['data'][] = array(
                'id' => $restaurant->id,
                'photos' => $images
            );

            if (empty($data['version'])){
                $data['version'] = Restaurant::getLastVersion('imageVersion');
            }
        }

        return $data;
    }

    /**
     * @param $restaurant_id
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function getRestaurantTables($restaurant_id, $version){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании
        $restaurant = Restaurant::model()->findByPk($restaurant_id);
        if ($restaurant === null)
            throw new Exception(self::getError(104, true, array('{id}' => $restaurant_id)), 104);

        $list = RestaurantTable::model()->findAll(array(
            'condition' => 'restaurant_id = :restaurant_id AND version > :version',
            'params' => array(':version' => $version, ':restaurant_id' => $restaurant_id),
        ));

        foreach ($list as $row){
            if (!isset($data['version']) || $data['version'] < $row->version)
                $data['version'] = $row->version;

            $data['data'][] = array(
                'id' => $row->id,
                'name' => $row->name,
                'restaurant_id' => $row->restaurant_id,
                //'plan_id' => $row->plan_id,
                'hidden' => $row->hidden,
                'deleted' => $row->deleted
            );
        }

        if (empty($data['version'])){
            $data['version'] = RestaurantTable::getLastVersion($restaurant_id);
        }

        return $data;
    }

    /**
     * @param $company_id
     * @param int $version
     * @return array
     * @throws Exception
     */
    public static function getMenuCategories($company_id, $version = -1){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании
        $company = Company::model()->findByPk($company_id);
        if ($company === null)
            throw new Exception(self::getError(100, true, array('{id}' => $company_id)), 100);

        // выборка категорий
        $list = Menu::model()->findAll(array(
            'condition' => 'company_id = :company_id AND version > :version',
            'params' => array(':company_id' => $company_id, ':version' => $version),
        ));

        foreach ($list as $row){
            if (!isset($data['version']) || $data['version'] < $row->version)
                $data['version'] = $row->version;

            $data['data'][] = array(
                'id' => $row->id,
                'name' => $row->name,
                'company_id' => $row->company_id,
                'parent_id' => $row->parent_id,
                'sort' => $row->sort,
                'description' => $row->description,
                'hidden' => $row->hidden,
                'deleted' => $row->deleted,
            );
        }

        if (empty($data['version'])){
            $data['version'] = Menu::getLastVersion($company_id);
        }

        return $data;
    }

    /**
     * @param $company_id
     * @param int $version
     * @return array
     * @throws Exception
     */
    public static function getMenuCategoriesImages($company_id, $version = -1){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании
        $company = Company::model()->findByPk($company_id);
        if ($company === null)
            throw new Exception(self::getError(100, true, array('{id}' => $company_id)), 100);

        // выборка категорий
        $list = Menu::model()->findAll(array(
            'select' => 'id, image, imageVersion',
            'condition' => 'company_id = :company_id AND imageVersion > :version',
            'params' => array(':company_id' => $company_id, ':version' => $version),
        ));

        foreach ($list as $row){
            if (!$row->image || !file_exists($_SERVER['DOCUMENT_ROOT'].$row->image)) continue;

            if (!isset($data['version']) || $data['version'] < $row->imageVersion)
                $data['version'] = $row->imageVersion;

            $image_info = getimagesize(Yii::app()->request->hostInfo.$row->image);
            $path_info = pathinfo(Yii::app()->request->hostInfo.$row->image);

            $data['data'][] = array(
                'id' => $row->id,
                'image' => base64_encode(file_get_contents(Yii::app()->request->hostInfo.$row->image)),
                'extension' => $path_info['extension'],
                'size' => array(
                    'width' => $image_info[0],
                    'height' => $image_info[1]
                )
            );
        }

        if (empty($data['version'])){
            $data['version'] = Menu::getLastVersion($company_id, 'imageVersion');
        }

        return $data;
    }

    /**
     * @param $company_id
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function getDishes($company_id, $version = -1){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании
        $company = Company::model()->findByPk($company_id);
        if ($company === null)
            throw new Exception(self::getError(100, true, array('{id}' => $company_id)), 100);

        $menu_ids = Menu::getCategoriesIds($company_id);
        if (count($menu_ids)){
            // выборка блюд
            $criteria = new CDbCriteria();
            $criteria->addCondition('version > ' . $version);
            $criteria->addInCondition('menu_id', $menu_ids);
            $list = Position::model()->findAll($criteria);

            foreach ($list as $row){
                if (!isset($data['version']) || $data['version'] < $row->version)
                    $data['version'] = $row->version;

                $data['data'][] = array(
                    'id' => $row->id,
                    'is_group' => $row->is_group,
                    'name' => $row->name,
                    'description' => $row->description,
                    'menu_id' => $row->menu_id,
                    'sort' => $row->sort,
                    'depressed' => $row->depressed,
                    'label_id' => $row->label_id,
                    'hidden' => $row->hidden,
                    'deleted' => $row->deleted,
                    'calories' => $row->calories,
                    'in_stock' => $row->in_stock,
                    //'food_type' => $row->food_type,
                    //'workplace' => $row->workplace,

                    'price' => $row->price,
                    'price_mode' => $row->price_mode,
                    'measure' => $row->measure,
                    'portion_weight' => $row->portion_weight,
                    'count_accuracy' => $row->count_accuracy,
                    'one_change_weight' => $row->one_change_weight,

                    // TODO + цены + бизнес ланчи
                );
            }

        }

        if (empty($data['version'])){
            $data['version'] = Position::getLastVersion($company_id);
        }

        return $data;
    }

    /**
     * @param $company_id
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function getDishesImages($company_id, $version = -1){
        $data = array(
            'data' =>array()
        );

        // проверка существования компании
        $company = Company::model()->findByPk($company_id);
        if ($company === null)
            throw new Exception(self::getError(100, true, array('{id}' => $company_id)), 100);

        $menu_ids = Menu::getCategoriesIds($company_id);
        if (count($menu_ids)){
            // выборка блюд
            $criteria = new CDbCriteria();
            $criteria->select = 'id, image, imageVersion';
            $criteria->addCondition('version > ' . $version);
            $criteria->addInCondition('menu_id', $menu_ids);
            $list = Position::model()->findAll($criteria);

            foreach ($list as $row){
                if (!$row->image || !file_exists($_SERVER['DOCUMENT_ROOT'].$row->image)) continue;

                if (!isset($data['version']) || $data['version'] < $row->imageVersion)
                    $data['version'] = $row->imageVersion;

                $image_info = getimagesize(Yii::app()->request->hostInfo.$row->image);
                $path_info = pathinfo(Yii::app()->request->hostInfo.$row->image);
                $data['data'][] = array(
                    'id' => $row->id,
                    'image' => base64_encode(file_get_contents(Yii::app()->request->hostInfo.$row->image)),
                    'extension' => $path_info['extension'],
                    'size' => array(
                        'width' => $image_info[0],
                        'height' => $image_info[1]
                    )
                );
            }
        }

        if (empty($data['version'])){
            $data['version'] = Position::getLastVersion($company_id, 'imageVersion');
        }

        return $data;
    }

    /**
     * @return array
     */
    public static function getOrderPayments(){
        $list = OrderPayment::model()->findAll();
        $data = array();
        foreach($list as $v){
            $data['data'][] = array(
                'id' => $v->id,
                'name' => $v->name
            );
        }

        if (empty($data['version'])){
            $data['version'] = 0;
        }

        return $data;
    }

    /**
     * @return array
     */
    public static function getLabels(){
        $list = Label::model()->findAll();
        $data = array();
        foreach($list as $v){
            $data['data'][] = array(
                'id' => $v->id,
                'text' => $v->text,
                'style_id' => $v->style_id
            );
        }

        if (empty($data['version'])){
            $data['version'] = 0;
        }

        return $data;
    }

    /**
     * @return array
     */
    public static function getOrderStatuses(){
        $list = OrderStatus::model()->findAll();
        $data = array();
        foreach($list as $v){
            $data['data'][] = array(
                'id' => $v->id,
                'name' => $v->name,
                'type' => $v->type
            );
        }

        if (empty($data['version'])){
            $data['version'] = 0;
        }

        return $data;
    }

    public static function createOrder($data, $device_id){
        $model = new Order;
        $model->setAttribute('device_id', $device_id);
        $model->setAttribute('table_id', isset($data['table_id']) ? intval($data['table_id']) : intval($data['table']));
        //$model->setAttribute('table_text', $data['table']);
        $model->setAttribute('guests', (empty($data['guests']) ? 1 : $data['guests']));
        $model->setAttribute('restaurant_id', intval($data['restaurant_id']));
        $model->setAttribute('status_id', Order::STATUS_CREATE);
        $model->setAttribute('date_create', time());
        $model->setAttribute('date_execute', 0);
        $model->setAttribute('date_close', 0);
        $model->setAttribute('payment_id', isset($data['payment_id']) ? $data['payment_id'] : 1);
        $model->setAttribute('attention', isset($data['attention']) ? $data['attention'] : 0);
        $model->setAttribute('separated', isset($data['separated']) ? $data['separated'] : 0);
        $model->setAttribute('separated_info', isset($data['separated_info']) ? $data['separated_info'] : '');
        $model->setAttribute('comment', isset($data['comment']) ? $data['comment'] : '');
        $model->setAttribute('version', Order::getLastVersion($device_id)+1);


        $userTable = UserTable::model()->findByAttributes(array('table_id'=>$model->table_id));
        if ($userTable){
            $model->setAttribute('garcon_id', $userTable->user_id);
        }else{
            throw new Exception(self::getError(134), 134);
        }

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new Exception(self::getError(130) . self::getFistValidationError($model), 130);
            }else{
                throw new Exception(self::getError(130) . 'Неизвестная ошибка сохранения в базу данных', 130);
            }
        }

        if (is_array($data['positions']) && count($data['positions'])){
            foreach($data['positions'] as $v){
                $item = new OrderItem;
                $item->setAttributes($v, false);
                $item->setAttribute('order_id', $model->id);
                $item->setAttribute('status_id', OrderItem::STATUS_CREATE);
                $item->setAttribute('sub_order', 1);
                if (!$item->save()){
                    if ($item->hasErrors()){
                        throw new Exception(self::getError(130) . self::getFistValidationError($item), 130);
                    }else{
                        throw new Exception(self::getError(130) . 'Неизвестная ошибка сохранения в базу данных', 130);
                    }
                }
            }
        }else{
            throw new Exception(self::getError(131), 131);
        }

        if (!$model->calculateTotalPrice()){
            throw new Exception(self::getError(130) . 'Не удалось рассчитать сумму заказа', 130);
        }

        if (!$model->calculateTotalCount()){
            throw new Exception(self::getError(130) . 'Не удалось рассчитать количество позиций заказа', 130);
        }

        return $model;
    }

    public static function addOrderPosition(&$order, $positions){
        $items = array();

        if (is_array($positions) && count($positions)){
            $sub_order = Order::getNextSubOrder($order->id);
            foreach($positions as $v){
                $item = new OrderItem;
                $item->setAttributes($v, false);
                $item->setAttribute('order_id', $order->id);
                $item->setAttribute('status_id', OrderItem::STATUS_CREATE);
                $item->setAttribute('sub_order', $sub_order);
                if (!$item->save()){
                    if ($item->hasErrors()){
                        throw new Exception(self::getError(130) . self::getFistValidationError($item), 130);
                    }else{
                        throw new Exception(self::getError(130) . 'Неизвестная ошибка сохранения в базу данных', 130);
                    }
                }
                $items[$item->id] = $item;
            }
        }else{
            throw new Exception(self::getError(131), 131);
        }

        // обновление суммы заказа
        if (!$order->calculateTotalPrice()){
            throw new Exception(self::getError(130) . 'Не удалось рассчитать сумму заказа', 130);
        }

        // обновление кол-ва заказа
        if (!$order->calculateTotalCount()){
            throw new Exception(self::getError(130) . 'Не удалось рассчитать количество позиций заказа', 130);
        }

        return $items;
    }

    public static function loadOrder($order_id = 0, $device){
        $model = Order::model()->findByPk($order_id);

        if ($model ===  null){
            throw new Exception(self::getError(132, true, array('{id}' => $order_id)), 132);
        }

        if ($model->device_id !== $device->id){
            throw new Exception(self::getError(133, true, array('{id}' => $order_id)), 133);
        }

        return $model;
    }

    public static function getOrder($order_id = 0, $device){
        $model = self::loadOrder($order_id, $device);
        $data['data'] = self::prepareOrderData($model);
        return $data;
    }

    public static function getOrders($version, $device){
        $list = Order::model()->findAll('device_id = :device_id AND version > :version', array(
            ':device_id' => $device->id,
            ':version' => $version
        ));

        $data = array();

        foreach($list as $order){
            if (!isset($data['version']) || $data['version'] < $order->version)
                $data['version'] = $order->version;

            $data['data'][] = self::prepareOrderData($order);
        }

        if (empty($data['version'])){
            $data['version'] = Order::getLastVersion($device->id);
        }

        return $data;
    }

    public static function createFeedback($attributes){
        $model = new FeedbackContent();
        $model->attributes = $attributes;

        if (!$model->save()){
            if ($model->hasErrors()){
                throw new Exception(self::getError(106) . self::getFistValidationError($model), 106);
            }else{
                throw new Exception(self::getError(106) . 'Неизвестная ошибка сохранения в базу данных', 106);
            }
        }

        return $model;
    }

    // ====================================================================================
    //  PRIVATE FUNCTIONS
    // ====================================================================================
    public static function prepareOrderData(Order $order){
        $data = array(
            'id' => (string) $order->id,
            //'device_id' => $order->device_id,
            'table_id' => (string) $order->table_id,
            'table_name' => (string) $order->table->name,
            'external_id' => (string) $order->table_text,
            'guests' => (string) $order->guests,
            'status_id' => (string) $order->status_id,
            'date_create' => (string) $order->date_create,
            'payment_id' => (string) $order->payment_id,
            'attention' => (string) $order->attention,
            'separated' => (string) $order->separated,
            'separated_info' => (string) $order->separated_info,
            'date_execute' => (string) $order->date_execute,
            'date_close' => (string) $order->date_close,
            'comment' => (string) $order->comment,
            'restaurant_id' => (string) $order->restaurant_id,
            'company_id' => (string) $order->restaurant->company_id,
            'garcon_id' => (string) $order->garcon_id,
            'count_items' => (string) $order->count_items,
            'total_price' => (string) $order->total_price,
            'version' => (string) $order->version,
            'items' => self::prepareOrderItems($order)
        );

        return $data;
    }

    private static function prepareOrderItems(Order $order){
        $data = array();

        foreach($order->items as $item){
            $data[] = array(
                'id' => (string) $item->id,
                'order_id' => (string) $item->order_id,
                'position_id' => (string) $item->position_id,
                'status_id' => (string) $item->status_id,
                'price' => (string) $item->price,
                'count' => (string) $item->count,
                'comment' => (string) $item->comment,
                'deleted' => (string) $item->deleted,
                'sub_order' => (string) $item->sub_order,
            );
        }

        return $data;
    }




} 