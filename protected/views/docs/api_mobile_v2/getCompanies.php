<h3>Получение списка компаний</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getCompanies<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
</code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
    {
        "status": "ok",
        "data": [
            {
                "id": "1",
                "name": "Тануки",
                "hidden": "0",
                "deleted": "1",
                "description": "Рестораны Тануки заметно отличаются своим аутентичным интерьером. Частокол из бамбука, стены с иероглифами и статуэтками, светильники из рисовой бумаги, большие столы, сделанные не для экономии места, а для удобства Гостей, впрочем, как и всё в ресторанах Тануки",
                "website": "http:\/\/www.tanuki.ru\/",
                "email": "hotline@tanuki.ru"
            }
        ],
        "version": "0"
        }
</code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint">
    {
        "status": "error",
        "error": {
        "code": 101,
        "message": "Не удалось обработать информацию об устройстве"
        }
    }
    </code></pre>
<hr/>