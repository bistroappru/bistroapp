(function($){
    $(function() {
        var token = '51dfe5d42fb2b43e3300006e';
        var key = '86a2c2a06f1b2451a87d05512cc2c3edfdf41969';

        var map_json = JSON.parse($('#Restaurant_map').val() ? $('#Restaurant_map').val() : '{"zoom":10, "coords":{"x":55.76,"y":37.64}, "place":{"city":"7700000000000","street":{},"building":{}}}');//,"place":{"city":"","street":"","building":""}}');

        var city = $('#kladr_city');
        var street = $('#kladr_street');
        var building = $('#kladr_building');

        var map = null;
        var placemark = null;
        var map_created = false;

        // Формирует подписи в autocomplete
        var Label = function( obj, query ){
            var label = '';

            var name = obj.name.toLowerCase();
            query = query.toLowerCase();

            var start = name.indexOf(query);
            start = start > 0 ? start : 0;

            if(obj.typeShort){
                label += '<span class="ac-s2">' + obj.typeShort + '. ' + '</span>';
            }

            if(query.length < obj.name.length){
                label += '<span class="ac-s2">' + obj.name.substr(0, start) + '</span>';
                label += '<span class="ac-s">' + obj.name.substr(start, query.length) + '</span>';
                label += '<span class="ac-s2">' + obj.name.substr(start+query.length, obj.name.length-query.length-start) + '</span>';
            } else {
                label += '<span class="ac-s">' + obj.name + '</span>';
            }

            if(obj.parents){
                for(var k = obj.parents.length-1; k>-1; k--){
                    var parent = obj.parents[k];
                    if(parent.name){
                        if(label) label += '<span class="ac-st">, </span>';
                        label += '<span class="ac-st">' + parent.name + ' ' + parent.typeShort + '.</span>';
                    }
                }
            }

            return label;
        };

        street.kladr({
            token: token,
            key: key,
            type: $.ui.kladrObjectType.STREET,
            label: Label,
            verify: true,
            parentType: $.ui.kladrObjectType.CITY,
            parentId: map_json.place.city,
            select: function( event, ui ) {
                street.data("kladr-obj", ui.item.obj);
                map_json.place.street = ui.item.obj;
                building
                    .val('')
                    .data("kladr-obj", {})
                    .kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: ui.item.obj.id});
                //$('#kladr_street-error').remove();
                //street.removeClass('kladr-error');
                MapUpdate();
            }
        });

        // Подключение плагина для поля ввода номера дома
        building.kladr({
            token: token,
            key: key,
            type: $.ui.kladrObjectType.BUILDING,
            label: Label,
            verify: true,
            select: function(event, ui) {
                building.val('')
                    //.removeClass('kladr-error')
                    .data("kladr-obj", {});
                building.data("kladr-obj", ui.item.obj);
                map_json.place.building = ui.item.obj;
                //$('#kladr_building-error').remove();
                MapUpdate();
            }
        });

        if (map_json.place.street.id){
            street.val(map_json.place.street.name);
            street.data("kladr-obj", map_json.place.street);
            building.kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: map_json.place.street.id});
        }

        if (map_json.place.building.id){
            building.val(map_json.place.building.name);
            building.data("kladr-obj", map_json.place.building);
        }

        // Проверка корректности названия улицы (если пользователь ввёл сам, а не выбрал в списке)
        street.change(function(){
            building
                .val('')
                //.removeClass('kladr-error')
                .data("kladr-obj", {});
            //$('#kladr_building-error').remove();
            var query = {
                token: token,
                key: key,
                value: street.val(),
                type: $.ui.kladrObjectType.STREET
            };

            if (city.val()){
                query['parentType'] = $.ui.kladrObjectType.CITY;
                query['parentId'] = city.val();
            }

            $.kladrCheck(query, function(obj){
                if(obj){
                    street.val(obj.name);
                    street.data("kladr-obj", obj);
                    street.parent().find('label').text(obj.type);
                    building.kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: obj.id});
                    //street.removeClass('kladr-error');
                    //$('#kladr_street-error').remove();
                }else{
                    street.data("kladr-obj", {});
                    //street.addClass('kladr-error').after('<div id="kladr_street-error" class="label_error">Некорректные данные</div>');
                }
            });
        });

        // Проверка названия строения
        building.change(function(){
            var query = {
                token: token,
                key: key,
                value: building.val(),
                type: $.ui.kladrObjectType.BUILDING
            };

            var streetObj = street.data("kladr-obj");
            if(streetObj){
                query['parentType'] = $.ui.kladrObjectType.STREET;
                query['parentId'] = streetObj.id;
            }

            $.kladrCheck(query, function(obj){
                if(obj && (obj.name == building.val())){
                    building.val(obj.name);
                    building.data( "kladr-obj", obj );
                    //building.removeClass('kladr-error');
                    //$('#kladr_building-error').remove();
                    MapUpdate();
                } else {
                    building.data("kladr-obj", {});
                    //building.addClass('kladr-error').after('<div id="kladr_building-error" class="label_error">Некорректные данные</div>');
                }
            });
        });

        var setCoords = function(pos){
            map_json.coords.x = pos[0];
            map_json.coords.y = pos[1];
            //$('#kladr_x').val(pos[0]);
            //$('#kladr_y').val(pos[1]);
            $('#Restaurant_map').val(JSON.stringify(map_json));
        }

        // Обновляет карту
        var MapUpdate = function(){
            map_json.zoom = 10;
            var address = '';

            if(city.val()){
                if(address) address += ', ';
                address += 'г. ' + city.find('option:selected').html();
            }

            var streetObj = street.data("kladr-obj");
            if(streetObj){
                if(address) address += ', ';
                address += streetObj.typeShort + ' ' + streetObj.name;
            }

            var buildingObj = building.data("kladr-obj");
            if(buildingObj){
                if(address) address += ', ';
                address += buildingObj.typeShort + ' ' + buildingObj.name;
            }

            if(address && map_created){
                var geocode = ymaps.geocode(address);
                geocode.then(function(res){
                    map.geoObjects.each(function (geoObject) {
                        map.geoObjects.remove(geoObject);
                    });
                    var position = res.geoObjects.get(0).geometry.getCoordinates();
                    $('#Restaurant_location').val(address);
                    $('.adress_box .rightbox_text').html(address);
                    MapDraw(position[0], position[1]);
                });
            }
        }


        var MapDraw = function(x, y){
            map.geoObjects.each(function (geoObject) {
                map.geoObjects.remove(geoObject);
            });
            var position = [x,y];
            placemark = new ymaps.Placemark(position, {}, {draggable: true});
            placemark.events.add('drag', function (e) {
                setCoords(placemark.geometry.getCoordinates());
            });
            map.geoObjects.add(placemark);
            map.setCenter(position, map_json.zoom);
            setCoords(position);
        }

        //if(map_json.place.city) city.val(map_json.place.city);
        //if(map_json.place.street) street.val(map_json.place.street).change();
        //if(map_json.place.building) building.val(map_json.place.building);
        //MapUpdate();

        // Обновляет текстовое представление адреса
        var AddressUpdate = function(){
            console.log('AddressUpdate');
        }

        //запуск карты при загрузке
        ymaps.ready(function(){
            if(map_created) return;
            map_created = true;
            map = new ymaps.Map('map', {
                center: [map_json.coords.x, map_json.coords.y],
                zoom: map_json.zoom
            });
            map.controls.add('smallZoomControl', { top: 5, left: 5 });
            map.events.add('boundschange', function (e) {
                if (e.get('newZoom') != e.get('oldZoom')){
                    map_json.zoom = e.get('newZoom');
                    $('#Restaurant_map').val(JSON.stringify(map_json));
                }
            });
            MapDraw(map_json.coords.x, map_json.coords.y);
        });

        $('.map_choose input:radio').change(function(){
            $('.map_type').hide();
            $('.map_type[data-id='+$(this).val()+']').show();
        });

        $('.map_type .coords').keyup(function(k){
            var value = parseFloat($(this).val())
            if (!value) value = '';
            $(this).val(value);
            $('.map_type .coords').removeClass('error');
            $('.map_type .coords').each(function(){
                if (!$(this).val()) $(this).addClass('error');
            });
            if (!$('.map_type .coords.error').length){
                MapDraw($('#kladr_x').val(), $('#kladr_y').val());
            }
        });
    });
})(jQuery);