<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<div id="wrapper">
    <div id="home_header">
        <div class="center">
            <div class="header_device"></div>
            <a href="/" id="logo"><img src="/html/front/images/logo_home.png" width="158" height="161" alt=""></a>
        </div>
    </div>
    <script type="text/javascript">
        $('#home_header').backstretch("images/header.jpg");
        jQuery(document).ready(function(){jQuery(window).resize(function(){
            $('#home_header').backstretch("/html/front/images/header.jpg");
        });})
    </script>
    <section class="benefits_section">
        <div class="pattern_top"></div>
        <div class="pattern_bottom"></div>
        <div class="center">
            <ul class="benefits_list">
                <li>
                    <div class="thumb"><img src="/html/front/images/benefits1.png" width="121" height="121" alt=""></div>
                    <p>Приложение само определит ваши координаты и предложит ознакомиться с меню того ресторана, в котором вы находитесь.</p>
                </li>
                <li>
                    <div class="thumb"><img src="/html/front/images/benefits4.png" width="121" height="121" alt=""></div>
                    <p>Вы можете выбрать понравившиеся блюда и отправить заказ прямо со своего мобильного устройства.</p>
                </li>
                <li>
                    <div class="thumb"><img src="/html/front/images/benefits5.png" width="121" height="121" alt=""></div>
                    <p>Вы можете выбрать способ оплаты, и оплатить счет не дожидаясь официанта.</p>
                </li>
            </ul>
        </div>
    </section>
    <section class="home_gray" id="features">
        <div class="big_device">
            <img src="/html/front/images/device.png" width="1039" height="473" alt="">
        </div>
        <div class="pattern_bottom gray"></div>
    </section>
    <section class="simple-easy">
        <div class="simple_inner">
            <div class="simple_text">
                <h2>ПРОСТО и ЛЕГКО</h2>
                <p>Меню ресторанов и кафе в вашем телефоне - больше не нужно ждать официанта, меню и счета.</p>
                <p>Используя приложения можно удобно посмотреть меню, выбрать понравившиеся блюда и напитки, оплатить заказ и хранить историю ваших заказов. Вы также сможете получить индивидуальные специальные предложения и скидки в определенных ресторанах и кафе.</p>
            </div>
            <div class="simple_device"></div>
        </div>
    </section>

    <section class="home_gray" id="partners">
        <div class="pattern_top gray2"></div>
        <div class="big_device2">
            <img src="/html/front/images/device2.jpg" width="949" height="540" alt="">
        </div>
        <div class="pattern_bottom gray"></div>
    </section>
    <section class="gray_bg">
        <div class="simple_inner">
            <div class="simple_text">
                <h2>ПРОСТО и ЛЕГКО</h2>
                <p>Подключившись к сервису BISTRO вы сможете увеличить качество и скорость обслуживания посетителей ваших ресторанов.</p>
                <p>Используя приложение Bistro, посетители вашего заведения смогут удобно и быстро осуществить заказ. Вы сможет оперативно управлять меню, отслеживать выполнение заказов их оплату, а также предлагать постоянным посетителям специальные скидки и условия.</p>
            </div>
            <div class="simple_device2"></div>
        </div>
        <div class="pattern_bottom"></div>
    </section>
    <section class="simple-easy simple-easy2" id="contacts">
        <div class="feedback_form">
            <h2>Форма обратной связи</h2>
            <p class="notes">Поля, отмеченные <span>*</span> обязательны для заполнения.</p>
            <?php if(Yii::app()->user->hasFlash('contact')){ ?>
                <p class="notes" style="color: #ff8200; font-size: 16px;"><?php echo Yii::app()->user->getFlash('contact'); ?></p>
            <?php } ?>
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'contact-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>

            <div class="login_entry">
                <div class="login_row">
                    <label>Имя<span class="required">*</span>:</label>
                    <?php echo $form->textField($model2,'name'); ?>
                    <?php echo $form->error($model2,'name'); ?>
                    <div class="clear"></div>
                </div>
                <div class="login_row">
                    <label>e-mail<span class="required">*</span>:</label>
                    <?php echo $form->textField($model2,'email'); ?>
                    <?php echo $form->error($model2,'email'); ?>
                    <div class="clear"></div>
                </div>
                <div class="login_row">
                    <label>комментарий<span class="required">*</span>:</label>
                    <?php echo $form->textArea($model2,'body',array('rows'=>6, 'cols'=>50)); ?>
                    <?php echo $form->error($model2,'body'); ?>
                    <div class="clear"></div>
                </div>
                <?php if(CCaptcha::checkRequirements()): ?>
                    <div class="login_row">
                        <label>Проверочный код<span class="required">*</span>:</label>
                        <div class="captcha"><?php $this->widget('CCaptcha'); ?></div>
                        <div class="code_input">
                            <?php echo $form->textField($model2,'verifyCode'); ?>
                            <span class="reload">Обновить код</span>
                        </div>
                        <div class="clear"></div>
                        <?php echo $form->error($model2,'verifyCode'); ?>
                    </div>
                <?php endif; ?>

                <input type="submit" class="login_submit" value=" ">
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Submit',array('class'=>'login_submit')); ?>
            </div>

            <?php $this->endWidget(); ?>

            <style>
                .captcha a{display: none;}
            </style>
            <script>
                $(document).ready(function(){
                    $('.reload').on('click', function(){
                        $('.captcha a').trigger('click');
                    });
                });
            </script>

        </div><!-- form -->
    </section>
    <script type="text/javascript">
        $('.simple-easy').backstretch("images/simple-easy.jpg");
        jQuery(document).ready(function(){jQuery(window).resize(function(){
            $('.simple-easy').backstretch("images/simple-easy.jpg");
        });})
    </script>
    <section class="clients_section">
        <div class="pattern_top"></div>
        <div class="center">
            <h2>ГДЕ НАЙТИ</h2>
            <div class="clients_list_wrap">
                <ul class="clients_list">
                    <li><span><a href="#"><img src="/html/front/images/appstore.png" width="263" height="88"></a></span></li>
                    <li><span><a href="#"><img src="/html/front/images/google_play.png" width="272" height="88"></a></span></li>
                </ul>
            </div>
        </div>
    </section>
</div>

<!-- main menu -->
<nav id="nav_block">
    <div class="nav_wrap">
        <ul class="main_menu">
            <li><a href="#home_header">ГЛАВНАЯ</a></li>
            <li><a href="#features">ПОЛЬЗОВАТЕЛЯМ</a></li>
            <li><a href="#partners">ПАРТНЕРАМ</a></li>
            <li><a href="#contacts">КОНТАКТЫ</a></li>
            <? if(Yii::app()->user->isGuest) {
                echo '<li class="login-link"><span>ЛИЧНЫЙ КАБИНЕТ</span><b class="login_corner"></b></li>';
            } else {
                echo '<li class="link_admin"><a href="/admin">ЛИЧНЫЙ КАБИНЕТ ('.Yii::app()->user->name . ')</a></li>';            }
            ?>

        </ul>
        <div class="clear"></div>
    </div>
    <div class="login_form">
        <h2>Вход в личный кабинет</h2>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));?>
        <div class="login_entry">
            <div class="login_row">
                <label>ЛОГИН:<span class="required">*</span></label>
                <?php echo $form->textField($model, 'username'); ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>
            <div class="login_row">
                <label>ПАРОЛЬ:<span class="required">*</span></label>
                <?php echo $form->passwordField($model, 'password'); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
            <?php echo CHtml::submitButton('Войти', array('value'=>'', 'class'=>'login_submit'));?>
            <div class="login_check">
                <?php echo $form->checkBox($model, 'rememberMe', array('id'=>'remember'));?>
                <label class="check_label" for="remember">Запомнить меня</label>
                <?php echo $form->error($model, 'rememberMe'); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</nav>


