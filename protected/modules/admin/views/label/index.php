<?php
/* @var $this LabelController */
/* @var $model Label */
$this->pageTitle = Yii::app()->name . ' - Управление спецвыводами';
$this->menu=array(
	array('label'=>'Создать спецвывод', 'url'=>array('create')),
);
?>

<h1>Управление спецвыводами</h1>
<br/>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'label-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name'=>'id',
            'headerHtmlOptions'=>array('width'=>30),
        ),
		'text',
		'style_id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
