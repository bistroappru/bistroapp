<BISTRO_APP_RESPONSE>
    <BISTRO_APP status="OK">
        <?php
        if (count($requests)){
        ?>
        <REQUESTS>
            <?php
            foreach($requests as $request){
            ?>
                <REQUEST id="<?=$request['uuid']?>">
                    <![CDATA[
                    <?=$request['body']?>
                    ]]>
                </REQUEST>
            <?php
            }
            ?>
        </REQUESTS>
        <?php
        }
        ?>
    </BISTRO_APP>
</BISTRO_APP_RESPONSE>