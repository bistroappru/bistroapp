/**
 * Created by birlaver on 27.07.14.
 */

var params = {};
var intervalCheckOrders;
$(document).ready(function(){
    if ($('.ctrl_orders.act_index').length > 0){
        afterOrdersListRender();
        setIntervalNewOrders();
    }

    $(window).on('logobox_company', function(){
        if ($('.ctrl_orders.act_index').length > 0){
            params = {};
            updateOrdersList();
        }
    });

    $(window).on('logobox_restaurant', function(){
        if ($('.ctrl_orders.act_index').length > 0){
            params = {};
            updateOrdersList();
        }
    });

    function setIntervalNewOrders(){
        // установка интервала обновления
        clearInterval(intervalCheckOrders);
        intervalCheckOrders = setInterval(checkNewOrders, 1000*60);
    }

    function checkNewOrders(){
        ajaxRequest('/admin/orders/checkNewOrders', function(data){
            if (data.length){
                $('<audio id="msgAudio"><source src="/data/sound/5.mp3" type="audio/mpeg"></audio>').appendTo('body');
                $('#msgAudio')[0].play();
                jAlert('У вас есть необработанные заказы со статусом "Новый".<br/> Номера заказов: ' + data.join(','), 'Новые заказы', 'warning', function(answer){
                    if (answer){
                        params = {};
                        updateOrdersList();
                    }
                });
            }
        }, {id: $.cookie('restaurant_id')}, 'json');
    }

    function hiddenTrCookie(command, value){
        var cookie = $.cookie('hidden_tr') || '||';
        switch(command){
            case 'get':
                if (cookie.indexOf('||' + value + '||') > -1){
                    return 1;
                }else{
                    return 0;
                }
            break;
            case 'set':
                if (cookie.indexOf('||' + value + '||') == -1){
                    cookie += value + '||';
                    $.cookie('hidden_tr', cookie);
                }
                return;
            break;
            case 'del':
                cookie = cookie.replace('||' + value + '||', '||');
                $.cookie('hidden_tr', cookie);
                return;
            break;
        }
    }

    function updateOrdersList(){
        setIntervalNewOrders();
        var url = '/admin/orders/index' + (params.page ? '/page/'+params.page : '');
        $.ajax({
            url: url,
            beforeSend: showLoading,
            data: params,
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
            },
            success: function(data){
                history.pushState({},'',url);
                $('#content_wrapper').html(data);
                if ($('.order_table tr[data-id='+params.orderid+']').length == 0) {
                    $('.table_menu').hide();
                    $('.food_detail').hide();
                }else{
                    $('.table_menu').show();
                    $('.food_detail').show();
                }
                // ---------------
                afterOrdersListRender();
                // ---------------
                hideLoading();
            },
            type: 'post'
        });
    }


    function updateOrbHeight(){
        var height = $('.table_thead').outerHeight() + $('.resizable_block').outerHeight();
        $('.orb_container').height(height);
        $('.orb_wrapper').height(height + 25);
        $(window).trigger('resize');
    }

    function afterOrdersListRender(){
        $('select.order_select').selectBox();
        $("input.datepicker").datepicker({dateFormat: "dd.mm.yy"});
        $('.deploy_plus').tooltip({ position: { my: "top-65", at: "center"} });

        if (!$('#table_orders').hasClass('empty')){
            var table_tr_height = $('#table_orders tr').outerHeight();
            var otlr_rows = parseInt($('#table_orders tr').length);
            var otlr_size = parseInt($.cookie('otlr_size')) || otlr_rows;

            if (otlr_size < 2 && otlr_rows >= 2){
                otlr_size = 2;
                $.cookie('otlr_size', otlr_size);
            }

            if (otlr_size > otlr_rows){
                otlr_size = otlr_rows;
                $.cookie('otlr_size', otlr_size);
            }

            $('.resizable_block').height(otlr_size*table_tr_height);

            /*
            var scroll_start = $('.resizable_inner').scrollTop();
            $('.resizable_inner').unbind('scroll');
            $('.resizable_inner').bind('scroll', function(types,data){
                var scroll_end = $(this).scrollTop();
                if(scroll_end % table_tr_height){
                    var scroll = parseInt($(this).scrollTop()/table_tr_height) + (scroll_start < scroll_end ? 1 : 0);
                }else{
                    var scroll = parseInt($(this).scrollTop()/table_tr_height);
                }
                scroll_start = scroll*table_tr_height;
                $('.resizable_inner').scrollTop(scroll_start);

            });
            */

            var min_height = (otlr_rows < 2 ? otlr_rows : 2);
            var max_height = otlr_rows;

            $('.resizable_block').resizable({
                grid: table_tr_height,
                handles: 's',
                maxHeight: table_tr_height*max_height,
                minHeight: table_tr_height*min_height,
                create: function(event, ui){
                    updateOrbHeight();
                },
                resize: function(event, ui){
                    updateOrbHeight();
                    $.cookie('otlr_size', parseInt(ui.size.height/table_tr_height));
                }
            });

            $('.bol_scroller__bar-wrapper').show();
        }else{
            $('.bol_scroller__bar-wrapper').hide();
        }

        baron({
            root: '.bol_wrapper',
            scroller: '.bol_scroller',
            bar: '.bol_scroller__bar',
            barOnCls: 'bol_baron'
        }).autoUpdate();

        baron({
            root: '.orb_wrapper',
            scroller: '.orb_scroller',
            barOnCls: 'orb_baron',
            direction: 'h',
            bar: '.orb_scroller__bar'
        }).autoUpdate();

        updateTableWidth();
        updateHiddenTh();
        updateResizableWidth();
    }


    function afterOrderDetailRender(){
        var tr_height = $('.food_table tr').outerHeight();
        var dtl_rows = parseInt($('.food_table tr').length);
        var dtl_size = parseInt($.cookie('dtl_size')) || dtl_rows;

        if (dtl_size < 2 && dtl_rows >= 2){
            dtl_size = 2;
            $.cookie('dtl_size', dtl_size);
        }

        if (dtl_size > dtl_rows){
            dtl_size = dtl_rows;
            $.cookie('dtl_size', dtl_size);
        }

        $('.resizable_block_detail').height(dtl_size*tr_height);

        var min_height = (dtl_rows < 2 ? dtl_rows : 2);
        var max_height = dtl_rows;

        $('.resizable_block_detail').resizable({
            grid: tr_height,
            handles: 's',
            maxHeight: tr_height*max_height,
            minHeight: tr_height*min_height,
            create: function(event, ui){
                //checkSummRowSmall(table_tr_height*max_height, otlr_size);
            },
            resize: function(event, ui){
                $.cookie('dtl_size', parseInt(ui.size.height/tr_height));
                //checkSummRowSmall(table_tr_height*max_height, ui.size.height);
            }
        });

        /*
        var scroll_start = $('.resizable_inner_detail').scrollTop();
        $('.resizable_inner_detail').unbind('scroll');
        $('.resizable_inner_detail').bind('scroll', function(types,data){
            var scroll_end = $(this).scrollTop();
            if(scroll_end % tr_height){
                var scroll = parseInt($(this).scrollTop()/tr_height) + (scroll_start < scroll_end ? 1 : -1);
            }else{
                var scroll = parseInt($(this).scrollTop()/tr_height);
            }
            scroll_start = scroll_end;
            $('.resizable_inner_detail').scrollTop(scroll*tr_height);
        });
        */
    }

    function updateResizableWidth(){
        $('.resizable_block').width($('#table_orders_head').outerWidth());
        $(window).trigger('resize');
    }

    function updateTableWidth(){
        var trs = $('#table_orders, #table_orders_head').find('tr');
        for (var i=0; i < trs.length; i++){
            $(trs[i]).find('td, th').each(function(index){
                $(this).find('.th_inner, .td_inner').css('width', $('#table_orders_head tr:first th:eq('+index+')').attr('data-width')+'px');
                //$(this).find('.th_inner').css('width', (parseInt($('#table_orders_head tr:first th:eq('+index+')').attr('data-width'))-20)+'px');
            });
        }
    }

    function updateHiddenTh(){
        $('.deploy_hidden').each(function(){
            $(this).height($(this).closest('th').innerHeight());
        });

        $('.order_table th').each(function(){
            if (hiddenTrCookie('get', $(this).attr('data-alias'))) {
                $(this).addClass('th_hidden');
            }
            if ($(this).hasClass('th_hidden')) {
                var trs = $('#table_orders, #table_orders_head').find('tr');
                var ind = $(this).index();
                for (var i=0; i<trs.length; i++) {
                    $(trs[i]).find('td, th').eq(ind).addClass('td_hidden');
                }
            }
            $(this).find('.deploy_hidden').height($(this).closest('th').innerHeight());
        });
    }

    function setOrderStatus(status){
        $.ajax({
            url: '/admin/orders/changestatus',
            beforeSend: showLoading,
            data: {
                status: status,
                orderid: params.orderid
            },
            dataType: 'html',
            error: function(data){
                jAlert(data.responseText);
                hideLoading();
            },
            success: function(data){
                $('.order_table tr[data-id='+params.orderid+']').replaceWith(data);
                hideLoading();
            },
            type: 'post'
        });
    }

    function updateOrderTableRow(orderid){
        var id = orderid || params.orderid;
        if (!id) return;
        ajaxRequest('/admin/orders/getOrderRow', function(data){
            $('.order_table tr[data-id='+id+']').replaceWith(data);
        }, {id: id});
    }

    function setOrderItemStatus(status, item, full){
        $.ajax({
            url: '/admin/orders/changeitemstatus',
            beforeSend: showLoading,
            data: {
                status: status,
                itemid: item
            },
            dataType: 'html',
            error: function(data){
                jAlert(data.responseText);
                hideLoading();
            },
            success: function(data){
                if (full){
                    hideLoading();
                    openOrderView(params.orderid);
                }else{
                    $('.food_table tr[data-id='+item+']').replaceWith(data);
                    updateOrderTableRow();
                    hideLoading();
                }
            },
            type: 'post'
        });
    }

    function deleteOrder(item){
        jConfirm('Заказ будет удален без возможности восстановления. <br/> Вы действительно хотите удалить заказ?', 'Удаление заказа', 'question', function(r) {
            if(r){
                $.ajax({
                    url: '/admin/orders/delete/id/'+item+'?ajax=1',
                    beforeSend: showLoading,
                    dataType: 'html',
                    error: function(data){
                        jAlert(data.responseText);
                        hideLoading();
                    },
                    success: function(data){
                        hideLoading();
                        params.orderid = 0;
                        updateOrdersList();
                    },
                    type: 'post'
                });
            }
        });
    }

    function deleteOrderItem(item, full){
        jConfirm('Позиция будет удалена без возможности восстановления. <br/> Вы действительно хотите удалить позицию?', 'Удаление позиции заказа', 'question', function(r) {
            if(r){
                $.ajax({
                    url: '/admin/orders/deleteitem/id/'+item+'?ajax=1',
                    beforeSend: showLoading,
                    dataType: 'html',
                    error: function(data){
                        jAlert(data.responseText);
                        hideLoading();
                    },
                    success: function(data){
                        hideLoading();
                        if (full){
                            openOrderView(params.orderid);
                        }else{
                            updateOrdersList();
                        }
                    },
                    type: 'post'
                });
            }
        });
    }

    function setPaymentStatus(status){
        $.ajax({
            url: '/admin/orders/changepayment',
            beforeSend: showLoading,
            data: {
                status: status,
                orderid: params.orderid
            },
            dataType: 'html',
            error: function(data){
                jAlert(data.responseText);
                hideLoading();
            },
            success: function(data){
                $('.order_table tr[data-id='+params.orderid+']').replaceWith(data);
                hideLoading();
            },
            type: 'post'
        });
    }

    function openOrderView(id){
        if (!id) {
            jAlert('Неверный ID заказа');
            return false;
        }

        var url = '/admin/orders/view/id/'+id;
        $.ajax({
            url: url,
            beforeSend: showLoading,
            dataType: 'html',
            error: function(data){
                jAlert(data.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                $('.table_menu').hide();
                history.pushState({}, '', url);
                hideLoading();
            },
            type: 'get'
        });
    }

    function updateOrderDetail(orderid){
        ajaxRequest('/admin/orders/orderdetail',function(data){
            $('.food_detail_wrap').html(data);
            afterOrderDetailRender();
        },{id: orderid});
    }

    $('#wrapper').on('click', '.table_menu .set_status', function(){
        if ($(this).attr('data-id')){
            setOrderStatus($(this).attr('data-id'));
        }
    });

    $('#wrapper').on('click', '.table_menu .delete_order', function(){
        deleteOrder(params.orderid);
    });

    $('#wrapper').on('click','.table_menu .set_payment', function(){
        if ($(this).attr('data-id')){
            setPaymentStatus($(this).attr('data-id'));
        }
    });

    $('#root').on('click','.orders_content .set_order_item_status', function(){
        if ($(this).attr('data-id') && $(this).attr('item-id')){
            setOrderItemStatus($(this).attr('data-id'), $(this).attr('item-id'));
        }
    });

    $('#root').on('click','.orders_content .oif_set_status', function(){
        if ($(this).attr('data-id') && $(this).attr('item-id')){
            setOrderItemStatus($(this).attr('data-id'), $(this).attr('item-id'), true);
        }
    });

    $('#root').on('click','.orders_content .delete_order_item', function(){
        if ($(this).attr('item-id')){
            deleteOrderItem($(this).attr('item-id'));
        }
    });

    $('#root').on('click','.orders_content .oif_delete_item', function(){
        if ($(this).attr('item-id')){
            deleteOrderItem($(this).attr('item-id'), true);
        }
    });

    $('#root').on('click','.orders_content .detail_meta_link a', function(){
        openOrderView(params.orderid);
        return false;
    });

    $('#root').on('click','.orders_content .food_del', function(){
        deleteOrder(params.orderid);
    });



    $('#root').on('click','.ord-single_close', function(){
        updateOrdersList();
        return false;
    });

    $('#root').on('click','.ord-single_del', function(e){
        e.preventDefault();
        deleteOrder($(this).attr('data-id'));
    });

    $('#root').on('keyup', '.orders_content .pages_filter .pf_input', function(){
        var page = parseInt($(this).val());
        if (!page) page = 1;
        params.page = page;
        params.orderid = 0;
        updateOrdersList();
    });

    $('#root').on('click','.orders_content .pages_list a', function(){
        params.page = parseInt($(this).attr('href').split('/').slice(-1)) || 1;
        params.orderid = 0;
        updateOrdersList();
        return false;
    });

    $('#root').on('click', '.orders_content .deploy_minus', function(){
        $(this).closest('th').addClass('th_hidden');
        $(this).closest('th').find('.deploy_hidden').height($(this).closest('th').innerHeight());
        var trs = $('#table_orders').find('tr');
        var ind = $(this).closest('th').index();
        for (var i=0; i<trs.length; i++) {
            $(trs[i]).find('td').eq(ind).addClass('td_hidden');
        }

        $('#table_orders_head .manage_row').find('td').eq(ind).addClass('td_hidden');
        hiddenTrCookie('set', $(this).closest('th').attr('data-alias'));
        updateResizableWidth();
    });

    $('#root').on('click', '.orders_content .deploy_plus', function(){
        $(this).closest('th').removeClass('th_hidden');
        var trs = $('#table_orders').find('tr');
        var ind = $(this).closest('th').index();
        for (var i=0; i<trs.length; i++) {
            $(trs[i]).find('td').eq(ind).removeClass('td_hidden');
        }
        $('#table_orders_head .manage_row').find('td').eq(ind).removeClass('td_hidden');
        hiddenTrCookie('del', $(this).closest('th').attr('data-alias'));
        updateResizableWidth();
    });

    $('.deploy_plus').tooltip({ position: { my: "top-65", at: "center"} });


    $('#root').on('click', '.order_table tbody tr[data-id]', function(){
        if($(this).hasClass('current')){
            $(this).removeClass('current');
            $(this).find('input[type=checkbox]').prop('checked', false).removeClass('checked');
            $('.table_menu').hide();
            $('.food_detail').hide();
            params.orderid = 0;
        } else {
            $('.order_table tr').removeClass('current');
            $('.order_table tr input[type=checkbox]').prop('checked', false).removeClass('checked');
            $(this).addClass('current');
            $(this).find('input[type=checkbox]').prop('checked', true).addClass('checked');
            $('.table_menu').show();
            $('.food_detail').show();

            params.orderid = parseInt($('.order_table tr.current').attr('data-id')) || 0;
            if (params.orderid != $('.food_table_wrap div').attr('data-id')){
                updateOrderDetail(params.orderid);
            }
        }
    });

    $('#root').on('click', '.orders_content .food_detail .detail_arrow', function(){
        $(this).closest('.food_detail').toggleClass('opened');
        $(this).closest('.food_detail').find('.food_table_wrap').slideToggle();
        return false;
    });

    $('#root').on('click', '.orders_content .ord-btn .sort_date', function(){
        $(this).toggleClass('active');
        $(this).closest('.ord-btn').find('.date_tooltip').toggle();
       // if()
        return false;
    });

    $('body').on('click', function() {
        $('.position_list').hide();
    });

    $('#root').on('click', '.orders_content .position_box, .orders_content .date_tooltip', function(event){
        event.stopPropagation();
    });

    $('#root').on('click', '.orders_content a.position_text', function(){
        if ($(this).closest('.position_change').find('.position_list').is(':visible')) {
            $(this).closest('.position_change').find('.position_list').slideUp();
        } else {
            $('.position_list').hide();
            $(this).closest('.position_change').find('.position_list').slideDown();
        }
        return false;
    });

    $('#root').on('click', '.orders_content .sort_up, .orders_content .sort_down', function(){
        params.order = $(this).attr('data-id');
        updateOrdersList();
        return false;
    });

    $('#root').on('click', '.apply_sort_link', function() {
        var parent = $(this).parents('.date_tooltip');
        if(!parent) {
            return false;
        }
        var input_start = $(parent).find('input[name="start_date"]');
        var input_end = $(parent).find('input[name="end_date"]');

        var param_name = $(parent).find('input[name="field_name"]').val();
        var start_date = input_start.val();
        var end_date = input_end.val();

        if(start_date.length > 0) {
            start_date = input_start.datepicker("getDate").getTime() / 1000
        }

        if(end_date.length > 0) {
            end_date = input_end.datepicker("getDate").getTime() / 1000
        }

        if(start_date && end_date && end_date < start_date) {
            $('#start_date').val('');
            $('#end_date').val('');
            jAlert('Дата начала не может быть больше дата окончания');
        }

        if(start_date || end_date) {
            if(params.filters == '') {
                params.filters = {};
                params.filters.range = {};
                params.filters.range[param_name] = [start_date, end_date];
            } else {
                if(params.filters.range) {
                    params.filters.range[param_name] = [start_date, end_date];
                } else {
                    params.filters.range = {};
                    params.filters.range[param_name] = [start_date, end_date];
                }
            }
        }

        updateOrdersList();
        $('.date_tooltip').hide();
        return false;
    });

    $('#root').on('change', 'select.order_select', function() {
        var param_name = $(this).attr('name');
        var val = $(this).val();
        if(val) {
            if(params.filters == '') {
                params.filters = {};
                params.filters.compare = {};
                params.filters.compare[param_name] = val;
            } else {
                if(params.filters.compare) {
                    params.filters.compare[param_name] = val;
                } else {
                    params.filters.compare = {};
                    params.filters.compare[param_name] = val;
                }
            }
        }
        updateOrdersList();
    });

    $('#root').on('click', '.fliter_submit_but', function() {
        var param_name = $(this).prev().attr('name');
        var val = $(this).prev().val();
        if(val.length > 0) {
            if(params.filters == '') {
                params.filters = {};
                params.filters.like = {};
                params.filters.like[param_name] = val;
            } else {
                if(params.filters.like) {
                    params.filters.like[param_name] = val;
                } else {
                    params.filters.like = {};
                    params.filters.like[param_name] = val;
                }
            }

        }
        updateOrdersList();
    });
    $('#root').on('click', '.cancel_sort_link', function() {

        var parent = $(this).parents('.date_tooltip');
        if(!parent) {
            return false;
        }

        var param_name = $(parent).find('input[name="field_name"]').val();

        if(params.filters == '') {
            params.filters = {};
            params.filters.range = {};
            params.filters.range[param_name] = [];
        } else {
            if(params.filters.range) {
                params.filters.range[param_name] = [];
            } else {
                params.filters.range = {};
                params.filters.range[param_name] = [];
            }
        }
        updateOrdersList();
        return false;
    });
});