<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 23.05.14
 * Time: 3:17
 */
$dataProvider = Position::model()->with('conditions')->findAll(array('order'=>'t.sort, conditions.sort', 'condition'=>'menu_id = :menu_id'.($group==1 ? ' AND depressed = 1' :''), 'params' => array(':menu_id'=>$model->id)));
?>

<div class="dishes_head">
    <div class="dish_box">
        <span class="dish_entry">
            <span class="dish_title"><?php echo $model->name; ?></span>
            <span class="dish_desc"><?php echo $model->description; ?></span>
        </span>
        <span class="dish_icon"><img src="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>" width="80" height="80"></span>
    </div>
</div>

<?php
if ($group == 1){
    ?>
    <div class="dishes_section">
        <div class="scroll-pane">
            <ul class="dishes_list2">
                <?php foreach($dataProvider as $dish) { ?>
                    <li>
                        <input type="checkbox" id="dishes_<?php echo $dish->id;?>" data-id="<?php echo $dish->id;?>">
                        <label for="dishes_<?php echo $dish->id;?>">
                            <div class="dish_box">
                                <span class="dish_entry">
                                    <span class="dish_title"><?php echo $dish->name;?></span>
                                    <?php
                                    foreach ($dish->conditions as $condition) {
                                        ?>
                                        <a href="#" cond-id="<?php echo $condition->id;?>" class="dish_desc"><?php echo $condition->measure; ?> <span class="black">(<?php echo $condition->price;?> р.)</span></a>
                                    <?php
                                    }
                                    ?>
                                </span>
                                <span class="dish_icon"><img src="<?php echo $dish->image ? $dish->image : Yii::app()->params['emptyImage']; ?>" width="50" height="50"></span>
                            </div>
                        </label>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <br/>
    <?php
}else{
    ?>
    <ul class="dishes_list" id="dishes_sortable">
        <?php
        foreach ($dataProvider as $dish) {
            ?>
            <li data-id="<?php echo $dish->id;?>" class="<?php echo $dish->deleted ? 'style_del' : '';?> <?php echo $dish->hidden ? 'style_hid' : '';?> <?php echo $dish->depressed ? 'depressed' : '';?>">
                <?php if ($dish->label_id) {?>
                    <div class="spec_<?php echo $dish->label->style_id?>"></div>
                <?php }?>
                <div href="/admin/position/update/dish_id/<?php echo CHtml::encode($dish->id); ?>" class="dish_box <?php echo $dish->id == $dish_id? 'active': '';?>">
                        <span class="dish_entry">
                            <span class="dish_title"><?php echo $dish->name; ?></span>
                            <?php
                            foreach ($dish->conditions as $condition) {
                                ?>
                                <span class="dish_desc"><?php echo $condition->measure; ?> <span class="black">(<?php echo $condition->price;?> р.)</span></span>
                            <?php
                            }
                            ?>
                        </span>
                    <span class="dish_icon"><a href="<?php echo $dish->image ? $dish->image : Yii::app()->params['emptyImage']; ?>" class="fancybox_image" title="<?php echo $dish->name; ?>" rel="dish_gallery"><img src="<?php echo $dish->image ? $dish->image : Yii::app()->params['emptyImage']; ?>" width="80" height="80"></a></span>
                </div>
            </li>
        <?php
        }
        ?>
    </ul>
    <?php
}
?>

<ul class="leftmenu_buttons">
    <li>
        <?php echo CHtml::link('<img src="/images/leftmenu-3.png" width="50" height="50" alt=""><span>Добавить блюдо</span>',array('/admin/position/create/menu_id/'.$model->id)); ?>
    </li>
    <li>
        <?php echo CHtml::link('<img src="/images/leftmenu-3.png" width="50" height="50" alt=""><span>Добавить бизнес-ланч</span>',array('/admin/position/create/menu_id/'.$model->id.'?group=1')); ?>
    </li>
</ul>