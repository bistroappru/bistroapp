<h3>Установка push-токена для устройства</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/setPushToken<br/>
</p>
<hr/>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
            "uuid": "550e8400-e29b-41d4-a716-446655440000",
            "platform": "iOS",
            "name": "iPhone 5",
            "version": "7.1"
        },
        "push_token": "f5950b10 eb5e8888 ce549b49 0d24cbb1 a0202f49 929ca991 bd37a6c2 7070dbea"
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
        {"status":"ok"}
    </code></pre>
<hr/>