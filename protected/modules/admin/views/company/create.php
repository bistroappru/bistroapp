<?php
/* @var $this CompanyController */
/* @var $model Company */
$this->pageTitle = Yii::app()->name . ' - Создание компании';
?>

<div class="column_center2">
    <div class="opacity_block">
        <div class="warning">
            <h2>ВНИМАНИЕ</h2>
            <p>Список ресторанов будет доступен после создания компании</p>
        </div>
    </div>
</div>

<div class="column_right">
    <?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>