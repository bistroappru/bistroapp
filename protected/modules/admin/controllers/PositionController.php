<?php

class PositionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = 'admin_menu';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow',
                'roles'=>array('inAdmin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($menu_id)
	{
        $ajax = Yii::app()->request->isAjaxRequest;
        $model = new Position();

        $model->menu_id = $menu_id;
        //$model->setIsGroupByMenuId();

        if(!Yii::app()->user->checkAccess('menuDishCreate', array('company_id'=>$model->menu->company_id))){
            throw new CHttpException(403, 'Вы не можете создавать блюда в меню этой компании');
        }

        if(isset($_POST['Position']))
        {
            $position = CHtml::encodeArray($_POST['Position']);
            $model->setAttributes($position, false);
            if($model->save()){
                $this->redirect(array('/admin/position/update/dish_id/'.$model->id));
            }
        }

        if (!$ajax){
            $this->render('create',array(
                'model'=>$model
            ));
        }else{
            $this->renderPartial('create',array(
                'model'=>$model
            ));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($dish_id)
	{
        $ajax = Yii::app()->request->isAjaxRequest;
        $model = $this->loadModel($dish_id);

        if(!Yii::app()->user->checkAccess('menuDishView', array('company_id'=>$model->menu->company_id))){
            throw new CHttpException(403, 'Вы не можете просматривать блюда в меню этой компании');
        }

        if(isset($_POST['Position']))
        {
            if(!Yii::app()->user->checkAccess('menuDishUpdate', array('company_id'=>$model->menu->company_id))){
                throw new CHttpException(403, 'Вы не можете редактировать блюда в меню этой компании');
            }

            $position = CHtml::encodeArray($_POST['Position']);


            $model->setAttributes($position, false);


            if($model->save()){
                $this->redirect(array('/admin/position/update/dish_id/'.$model->id));
            }
        }

        if (!$ajax){
            $this->render('update',array(
                'model'=>$model
            ));
        }else{
            $this->renderPartial('update',array(
                'model'=>$model
            ));
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($dish_id)
	{
        $model = $this->loadModel($dish_id);
        $menu_id = $model->menu_id;

        if(!Yii::app()->user->checkAccess('menuDishDelete', array('company_id'=>$model->menu->company_id))){
            throw new CHttpException(403, 'Вы не можете удалять блюда в меню этой компании');
        }

        $model->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		$this->redirect(array('/admin/menu/update/id/'.$menu_id));
	}

    /**
     * resort positions
     */
    public function actionResort(){
        $list = Yii::app()->request->getParam('list');
        if (!is_array($list)) throw new CHttpException(500, 'Неправильный формат входящих данных.');

        // TODO переписать. сделать один запрос за всеми блюдами, и в цикле только сохранять.

        foreach($list as $k=>$v){
            $condition = Position::model()->findByPk($v);
            if (is_object($condition)){
                $condition->sort = $k;
                $condition->save();
            }
        }
    }


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Position the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Position::model()->with('label')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Position $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='position-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
