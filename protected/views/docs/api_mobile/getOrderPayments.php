<h3>Получение способов оплаты</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getOrderPayments <br/>
</p>

<hr/>


<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/getOrderPayments ');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "code": 0,
  "data": [
    {
      "id": "1",
      "name": "\u041d\u0430\u043b\u0438\u0447\u043d\u044b\u043c\u0438"
    },
    {
      "id": "2",
      "name": "\u041a\u0430\u0440\u0442\u043e\u0439"
    }
  ],
  "error": "\u0417\u0430\u043f\u0440\u043e\u0441 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d"
}');?>
    </code></pre>
<hr/>

