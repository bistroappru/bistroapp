<?php

/**
 * This is the model class for table "{{condition}}".
 *
 * The followings are the available columns in table '{{condition}}':
 * @property integer $id
 * @property string $out_id
 * @property integer $position_id
 * @property string $measure
 * @property integer $sort
 * @property integer $price
 * @property integer $action_on
 */
class Condition extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{condition}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('position_id, measure, price', 'required'),
			array('position_id, sort, price', 'numerical', 'integerOnly'=>true),
			array('out_id, measure', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, position_id, measure, sort, price, action_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'position' => array(self::BELONGS_TO, 'Position', 'position_id'),
            'action' => array(self::HAS_ONE, 'SalePrice', 'condition_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'position_id' => 'Позиция',
			'measure' => 'Размерность',
			'sort' => 'Сортировка',
			'price' => 'Цена',
            'action_on' => 'Акция',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('position_id',$this->position_id);
		$criteria->compare('measure',$this->measure,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('price',$this->price);
        $criteria->compare('action_on',$this->action_on);
        $criteria->order = 'sort ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getErrorsForm(){
        $errors_html = array();

        if ($this->hasErrors()){
            foreach($this->getErrors() as $v){
                $errors_html = array_merge($errors_html, $v);
            }
        }

        if ($this->action && $this->action->hasErrors()){
            foreach($this->action->getErrors() as $v){
                $errors_html = array_merge($errors_html, $v);
            }
        }

        return count($errors_html) ? CHtml::tag('div', array('class'=>'inp_wrap'), CHtml::tag('div', array('class'=>'errorMessage'), implode('<br/>', $errors_html))) : '';

    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Condition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave()
    {
        parent::afterSave();
        if ($this->action_on){
            if ($this->action instanceof SalePrice){
                $this->action->condition_id = $this->id;
                $this->action->save();
            }
        }else{
            if ($this->action instanceof SalePrice){
                $this->action->delete();
            }
        }
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        if ($this->action instanceof SalePrice){
            $this->action->delete();
        }
    }
}