<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 04.06.14
 * Time: 22:22
 */
class RestaurantChoose extends CWidget {
    public $company = 0;
    public $restaurant = 0;
    public $returnUrl = '/admin/orders';

    public function run(){
        if(Yii::app()->request->getParam('cr_choose',0) == 1){
            if (!Yii::app()->user->company_id){
                Yii::app()->request->cookies['company_id'] = new CHttpCookie('company_id', Yii::app()->request->getParam('company_id',0));
                unset(Yii::app()->request->cookies['restaurant_id']);
            }

            if (!Yii::app()->user->restaurant_id){
                Yii::app()->request->cookies['restaurant_id'] = new CHttpCookie('restaurant_id', Yii::app()->request->getParam('restaurant_id',0));
            }
            $this->owner->redirect($this->returnUrl);
        }

        if (!Yii::app()->user->company_id || !Yii::app()->user->restaurant_id) $this->render('_restaurant_choose', array(
            'company' => $this->company,
            'restaurant' => $this->restaurant,
        ));
    }
}