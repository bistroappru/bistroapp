<?php

/**
 * This is the model class for table "{{restaurant_table}}".
 *
 * The followings are the available columns in table '{{restaurant_table}}':
 * @property integer $id
 * @property string $out_id
 * @property string $out_code
 * @property string $name
 * @property integer $restaurant_id
 * @property integer $plan_id
 * @property integer $hidden
 * @property integer $deleted
 */
class RestaurantTable extends CActiveRecord
{
    public $action = 'update';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{restaurant_table}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, restaurant_id', 'required'),
			array('restaurant_id, plan_id, hidden, deleted', 'numerical', 'integerOnly'=>true),
			array('out_id, out_code, name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, out_code, name, restaurant_id, plan_id, hidden, deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'restaurant' => array(self::BELONGS_TO, 'Restaurant', 'restaurant_id'),
            'plan' => array(self::BELONGS_TO, 'RestaurantPlan', 'plan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'restaurant_id' => 'Ресторан',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('restaurant_id',$this->restaurant_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getTables($restaurant_id){
        $list = self::model()->findAllByAttributes(array('restaurant_id' => $restaurant_id));
        return $list;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RestaurantTable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public function hasUsers(){
        $list = UserTable::model()->findAllByAttributes(array('table_id'=>$this->id));
        if (count($list))
            return true;
        else
            return false;
    }


    // ---------    CUSTOM OBJECT METHODS   ----------

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function getIdByCode($code, $restaurant_id){
        $model = self::model()->findByAttributes(array('out_code'=>$code, 'restaurant_id'=>$restaurant_id));
        if ($model === null)
            return;
        return $model->id;
    }

    /**
     * получение индекса последней версии данных
     */
    public static function getLastVersion($restaurant_id, $field = 'version'){
        $row = self::model()->find(array(
            'select' => $field,
            'condition' => 'restaurant_id = :restaurant_id',
            'params' => array(':restaurant_id' => $restaurant_id),
            'order' => "{$field} DESC",
            'limit' => 1
        ));
        return is_object($row) ? $row->$field : 0;
    }

    public static function parseKeeperData(RKeeperQueue $queue){
        $response = @simplexml_load_string($queue->response);

        if (!$response){
            $queue->setFail('Не удалось обработать xml-ответ');
            return;
        }

        if ((string) $response['Status'] !== 'Ok'){
            $queue->setFail('Статус должен быть: Ok');
            return;
        }

        if ((string) $response['CMD'] !== $queue->getCMD()){
            $queue->setFail('Команда запроса и ответа не совпадает');
            return;
        }

        if ((string) $response->RK7Reference['Name'] !== strtoupper($queue->getRefName())){
            $queue->setFail('Справочник запроса и ответа не совпадает');
            return;
        }

        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AbcHelper::simple_xml_attr_to_array($row);
                if (!$item['Ident'] || !$item['MainParentIdent']) continue;

                $data = array();
                $data['out_id'] = $item['Ident'];
                $data['out_code'] = $item['Code'];
                $data['name'] = $item['Name'];
                $data['hidden'] = ($item['Status'] == 'rsDraft' ? 1 : 0);
                $data['deleted'] = ($item['Status'] == 'rsDeleted' ? 1 : 0);
                $plan_data = array(
                    'out_id' => $item['MainParentIdent'],
                    'name' => 'plan #' . $item['MainParentIdent']
                );

                $plan_id = RestaurantPlan::getIdByOutId($item['MainParentIdent'], true, $plan_data);
                $plan = RestaurantPlan::model()->findByPk($plan_id);

                $data['plan_id'] = $plan_id;
                $data['restaurant_id'] = (int) $plan->restaurant_id;

                $model = RestaurantTable::model()->findByAttributes(array('out_id' => $data['out_id']));
                if ($model === null){
                    $model = new RestaurantTable();
                }

                $model->setAttributes($data);

                if (!$model->save(false)){
                    $queue->setFail('Не удалось создать/обновить столик с out_id = ' . $model->out_id);
                    return;
                }

                $queue->setFinished();
            }
        }
    }
}
