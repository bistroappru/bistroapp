<h3>Получение заказов пользователя</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getOrders<br/>
</p>

<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>token</em></strong>: появится при появлении функции авторизации, пока по умолчанию выводим все заказы <b>(обязательный)</b><br/>
    <strong><em>company_id</em></strong>: компания, заказы которой необходимо получить <b>(необязательный)</b><br/>
    <strong><em>restaurant_id</em></strong>: ресторан, заказы которого необходимо получить <b>(необязательный)</b><br/>
</p>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/getOrders?token=d41d8cd98f00b204e9800998ecf8427e&company_id=1&restaurant_id=3');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "code": 0,
  "data": [
    {
      "id": "35",
      "out_id": "",
      "user_id": "1",
      "table_id": "12",
      "status_id": "2",
      "date_create": "1401193230",
      "payment_id": "1",
      "attention": "0",
      "separated": "0",
      "separated_info": "",
      "date_execute": "0",
      "date_close": "0",
      "comment": "",
      "restaurant_id": "3",
      "garcon_id": "0",
      "count_items": "1",
      "total_price": "180",
      "company_id": "1",
      "items": [
        {
          "id": "25",
          "order_id": "35",
          "condition_id": "41",
          "status_id": "11",
          "price": "90",
          "count": "2"
        }
      ]
    },
    {
      "id": "38",
      "out_id": "",
      "user_id": "1",
      "table_id": "12",
      "status_id": "2",
      "date_create": "1405688493",
      "payment_id": "1",
      "attention": "1",
      "separated": "1",
      "separated_info": "\u0442\u0435\u043a\u0441\u0442 \u0440\u0430\u0437\u0434\u0435\u043b\u044c\u043d\u043e\u0433\u043e \u0437\u0430\u043a\u0430\u0437\u0430",
      "date_execute": "0",
      "date_close": "0",
      "comment": "",
      "restaurant_id": "3",
      "garcon_id": "5",
      "count_items": "2",
      "total_price": "720",
      "company_id": "1",
      "items": [
        {
          "id": "29",
          "order_id": "38",
          "condition_id": "41",
          "status_id": "9",
          "price": "90",
          "count": "3"
        },
        {
          "id": "30",
          "order_id": "38",
          "condition_id": "16",
          "status_id": "9",
          "price": "150",
          "count": "3"
        }
      ]
    }
  ],
  "error": "\u0417\u0430\u043f\u0440\u043e\u0441 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d"
}');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "code": 5,
  "data": [],
  "error": "\u0420\u0435\u0441\u0442\u043e\u0440\u0430\u043d \u043d\u0435 \u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u0434\u043e\u0447\u0435\u0440\u043d\u0438\u043c \u0437\u0430\u0434\u0430\u043d\u043d\u043e\u0439 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438"
}');?>
    </code></pre>
<hr/>