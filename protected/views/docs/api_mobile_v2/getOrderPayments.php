<h3>Получение вариантов оплаты</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getOrderPayments<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    -
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
    {
        "status": "ok",
        "data": [
        {
        "id": "1",
        "name": "Наличными"
        },
        {
        "id": "2",
        "name": "Картой"
        }
        ]
    }
    </code></pre>
<hr/>