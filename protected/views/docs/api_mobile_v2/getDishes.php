<h3>Получение блюд меню</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getDishes<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>company_id</em></strong>: ID компании <b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
{
    "status": "ok",
    "data": [
        {
            "id": "142",
            "is_group": "0",
            "name": "Кола",
            "description": "",
            "menu_id": "44",
            "sort": "0",
            "depressed": "0",
            "label_id": "0",
            "hidden": "0",
            "deleted": "1",
            "calories": "",
            "in_stock": "43,44,45,46",
            "price": "0",
            "price_mode": "1",
            "measure": "",
            "portion_weight": "0",
            "count_accuracy": "0",
            "one_change_weight": "0"
        },
        {
            "id": "143",
            "is_group": "0",
            "name": "Фанта",
            "description": "",
            "menu_id": "44",
            "sort": "1",
            "depressed": "0",
            "label_id": "0",
            "hidden": "0",
            "deleted": "1",
            "calories": "",
            "in_stock": "43,44,45,46",
            "price": "0",
            "price_mode": "1",
            "measure": "",
            "portion_weight": "0",
            "count_accuracy": "0",
            "one_change_weight": "0"
        }
    ],
    "version": "4680"
}
    </code></pre>
<hr/>