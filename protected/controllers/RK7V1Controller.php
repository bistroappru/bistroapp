<?php

class RK7V1Controller extends Controller
{
    public $layout = 'clear';

    public function filters()
    {
        return array(
            'postOnly + registrationProxy',
            'postOnly + postNeedUpdate',
            'postOnly + getRequests',
            'postOnly + postChanges',
        );
    }

    protected function beforeRender($view){
        header('Content-type: text/xml; charset=utf-8;');
        return true;
    }

    protected function afterRender($view, &$output){
        $output = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . $output;
        return true;
    }

    /**
     * РЕГИСТРАЦИЯ НОВОГО ПРОКСИ-СЕРВЕРА
     */
    public function actionRegistrationProxy(){
        $input = file_get_contents('php://input');

        //$input = '<BA_PROXY_REQUEST><BA_PROXY id="{E9BE623D-511B-4BD9-B4E8-B151A0297C26}" type="RK6" name="BA_PROXY_REF"/><BISTRO_APP companyID="9" objectID="43"/></BA_PROXY_REQUEST>';

        try {
            $xml = RK7V1Service::XMLParseRequest($input);
            $xml_proxy = RK7V1Service::XMLGetPath($xml, 'BA_PROXY');
            $xml_bistro = RK7V1Service::XMLGetPath($xml, 'BISTRO_APP');

            RK7V1Service::createProxyServer(array(
                'proxy_uuid' => (string) $xml_proxy[0]['id'],
                'proxy_type' => strtoupper( (string) $xml_proxy[0]['type'] ),
                'proxy_name' => (string) $xml_proxy[0]['name'],
                'company_id' => (int) $xml_bistro[0]['companyID'],
                'restaurant_id' => (int) $xml_bistro[0]['objectID'],
                'create_time' => time(),
                'alive_interval' => (int) $xml_proxy[0]['TimeAlive'],
                'last_request_time' => time()
            ));
            $this->render('registrationProxy');
        } catch (Exception $e){
            $this->render('error', array('code' => $e->getCode(), 'message' => $e->getMessage()));
        }
    }

    /**
     * ЗАПИСЬ НУЖДАЮЩИХСЯ В ОБНОВЛЕНИИ СПРАВОЧНИКОВ
     */
    public function actionPostNeedUpdate(){
        $input = file_get_contents('php://input');
        //$input = '<BA_PROXY_REQUEST><BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF0}" ServerTimeStamp="2015-01-28T16:15:59"/><R_KEEPER><REFERENCES><REFERENCE RefName="RESTAURANTS"/><REFERENCE RefName="CASHGROUPS"/><REFERENCE RefName="CASHES"/><REFERENCE RefName="ROLES"/><REFERENCE RefName="EMPLOYEES"/><REFERENCE RefName="CATEGLIST"/><REFERENCE RefName="MENUITEMS"/><REFERENCE RefName="HALLPLANS"/><REFERENCE RefName="TABLES"/><REFERENCE RefName="PRICES"/><REFERENCE RefName="VISITS"/></REFERENCES></R_KEEPER></BA_PROXY_REQUEST>';

        try {
            $xml = RK7V1Service::XMLParseRequest($input);
            $xml_proxy = RK7V1Service::XMLGetPath($xml, 'BA_PROXY');
            $xml_refs = RK7V1Service::XMLGetPath($xml, 'R_KEEPER/REFERENCES/REFERENCE');
            $proxy = RK7V1Service::loadProxyServer($xml_proxy[0]['id']);

            $transaction = Yii::app()->db->beginTransaction();

            foreach($xml_refs as $ref){
                $ref_name = strtoupper( (string) $ref['RefName'] );
                switch ($ref_name){
                    case 'PRICES':
                    case 'VISITS':
                        $proxy_id = ($proxy->proxy_type == RKeeperProxy::TYPE_RK6) ? $proxy->id : RK7V1Service::getCompanyMID($proxy->company_id);
                    break;
                    default:
                        $proxy_id = $proxy->id;
                    break;
                }

                RK7V1Service::createTurn(array(
                    'proxy_id' => $proxy_id,
                    'reference' => $ref_name
                ));
            }
            $transaction->commit();
            $this->render('postNeedUpdate');

        } catch (Exception $e){
            if ($transaction)
                $transaction->rollback();

            $this->render('error', array('code' => $e->getCode(), 'message' => $e->getMessage()));
        }
    }

    /**
     * ПОЛУЧЕНИЕ ЗАПРОСОВ ДЛЯ КИПЕРА
     */
    public function actionGetRequests(){
        $input = file_get_contents('php://input');
        //$input = '<BA_PROXY_REQUEST><BA_PROXY id="{96896BD3-1D9F-45D5-B87C-9B2E6A924AF0}" ServerTimeStamp="2015-01-29T13:36:56"/></BA_PROXY_REQUEST>';

        try {
            $xml = RK7V1Service::XMLParseRequest($input);
            $xml_proxy = RK7V1Service::XMLGetPath($xml, 'BA_PROXY');
            $proxy = RK7V1Service::loadProxyServer($xml_proxy[0]['id']);

            RK7V1Service::prepareRequests($proxy);
            $this->render('getRequests', array('requests' => RK7V1Service::getRequests($proxy)));

        } catch (Exception $e){
            $this->render('error', array('code' => $e->getCode(), 'message' => $e->getMessage()));
        }
    }

    /**
     * ПОЛУЧЕНИЕ ОТВЕТОВ ОТ КИПЕРА
     */
    public function actionPostChanges(){
        $input = file_get_contents('php://input');
//        $input = '<BA_PROXY_REQUEST><BA_PROXY id="{E9BE623D-511B-4BD9-B4E8-B151A0297C26}" TimeAlive="5" ServerTimeStamp="UTC_DATETIME"/><BISTRO_APP requestID="{8690F280-4D77-24C0-AE91-861C06534EED}"/><R_KEEPER><RESPONSE><![CDATA[<RK7QueryResult ServerVersion="7.4.21.255" XmlVersion="53" Status="Ok" CMD="GetRefList" ErrorText="" DateTime="2014-11-13T16:21:15" WorkTime="0" Processed="1"><RK7RefList Count="119"><RK7Reference RefName="BONUSTYPES" Count="1" DataVersion="2"/><RK7Reference RefName="BRIGADES" Count="0" DataVersion="1"/></RK7RefList></RK7QueryResult>]]></RESPONSE></R_KEEPER></BA_PROXY_REQUEST>';

        try {
            $xml = RK7V1Service::XMLParseRequest($input);
            $xml_proxy = RK7V1Service::XMLGetPath($xml, 'BA_PROXY');
            $xml_bistro = RK7V1Service::XMLGetPath($xml, 'BISTRO_APP');
            $xml_response = RK7V1Service::XMLGetPath($xml, 'R_KEEPER/RESPONSE');

            $proxy = RK7V1Service::loadProxyServer($xml_proxy[0]['id']);

            $queue = RK7V1Service::setResponse((string) $xml_bistro[0]['requestID'], (string) $xml_response[0]);

            $proxy->updateLastRequestTime();

            /* втавка для обработки запроса */

            $transaction = Yii::app()->db->beginTransaction();
            try{
                $parser = new RKeeperQueueParser($queue);
                $parser->parse();
                $queue->finish();
                $transaction->commit();
            }catch(Exception $e){
                $transaction->rollback();
                RK7V1Service::failParseHandler($queue);
                $queue->fail($e->getMessage());
            }

            /* ---------------------------- */

            $this->render('postChanges');
        } catch (Exception $e){
            $this->render('error', array('code' => $e->getCode(), 'message' => $e->getMessage()));
        }
    }

    // попытка оплаты заказа
    public function actionPayOrder(){
        $input = file_get_contents('php://input');
        $input = '<?xml version="1.0" encoding="UTF-8"?><BA_PROXY_REQUEST><BA_PROXY id="{900617C2-246A-4EDE-0E81-02457F57B10E}" type="RK6" name="BA_PROXY_TEST_77" ServerTimeStamp="2016-03-12T20:26:18"/><R_KEEPER><REQUEST><![CDATA[<?xml version="1.0" encoding="UTF-8"?><RK6Query><RK6CMD CMD="PayOrder"/><Order guid="{FABC81A6-98F8-6719-2079-DC475CB7ECEA}"/><Station code="21"/><Payments><Payment code="40" amount="134000" name="Карта"/></Payments><Dishes><Dish id="4" code="   1" quantity="4000" sum="76000"/><Dish id="8" code="   5" quantity="2000" sum="11000"/><Dish id="11" code="   8" quantity="4000" sum="38000"/><Dish id="7" code="   4" quantity="2000" sum="9000"/></Dishes><Persons><Person id="9" code="1234" name="BACashier" role="W"/><Person id="7" code="2222" name="kassir" role="K"/></Persons></RK6Query>]]></REQUEST></R_KEEPER></BA_PROXY_REQUEST>';

        try {
            $xml = RK7V1Service::XMLParseRequest($input);
            $xml_proxy = RK7V1Service::XMLGetPath($xml, 'BA_PROXY');
            $xml_response = RK7V1Service::XMLGetPath($xml, 'R_KEEPER/REQUEST');
            $proxy = RK7V1Service::loadProxyServer($xml_proxy[0]['id']);
            $pay_data = RK7V1Service::XMLParsePayData($xml_response[0]);

            $transaction = Yii::app()->db->beginTransaction();
            $order = RK7V1Service::paymentOrder($pay_data, $proxy);
            $transaction->commit();

            $order->sendPush('pay_rk');
            $order->sendSMS('pay_rk');

            $this->render('payOrder');
        } catch (Exception $e){
            if ($transaction){
                $transaction->rollback();
            }
            $this->render('error', array('code' => $e->getCode(), 'message' => $e->getMessage()));
        }
    }

    // ОБРАБОТКА ПОДГОТОВЛЕННЫХ ЗАПРОСОВ
    public function actionParseChanges(){
        $queues = RK7V1Service::getRequestsForParse(1);
        foreach ($queues as $queue){
            $transaction = Yii::app()->db->beginTransaction();
            try{
                $parser = new RKeeperQueueParser($queue);
                $parser->parse();
                $queue->finish();
                $transaction->commit();
            }catch(Exception $e){
                $transaction->rollback();
                RK7V1Service::failParseHandler($queue);
                $queue->fail($e->getMessage());
            }
        }
    }

    // ОТМЕНА ПОДГОТОВЛЕННЫХ ЗАПРОСОВ
    public function actionResetQueue(){
        $queues = RK7V1Service::getWaitRequests();
        foreach ($queues as $queue){
            $transaction = Yii::app()->db->beginTransaction();
            try{
                $queue->fail('Запрос был просрочен');
                $data = unserialize($queue->data);
                $order = Order::model()->findByPk($data['order_id']);
                if ($order){
                    $order->status_id = Order::STATUS_CANCELED;
                    if ($order->save()){
                        $order->sendPush('cancel');
                    }
                }
                $transaction->commit();
            }catch(Exception $e){
                $transaction->rollback();
                $queue->fail($e->getMessage());
            }
        }
    }


    // =======================================================================================================================================================================================================
    // =======================================================================================================================================================================================================
    // =======================================================================================================================================================================================================
     // =======================================================================================================================================================================================================
    // =======================================================================================================================================================================================================
    // =======================================================================================================================================================================================================

    // СПИСОК ПРОКСИ
    public function actionListProxy(){
        header('Content-type: text/html;  charset=utf-8;');
        $list = RKeeperProxy::model()->findAll();
        $this->renderPartial('listProxy', array('list' => $list));
    }

    // СПИСОК ЗАДАНИЙ ПРОКСИ
    public function actionListQueue(){
        header('Content-type: text/html;  charset=utf-8;');
        $list = RKeeperQueue::model()->findAll();
        $this->renderPartial('listQueue', array('list' => $list));
    }

    // СПИСОК ПРОКСИ
    public function actionTest($id){
        $model = Order::model()->findByPk($id);

        if ($model === null)
            return 'Не найден заказ';

        RKeeperQueue::createRKeeperOrderItems($model);
    }


}