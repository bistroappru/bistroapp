<?php

/**
 * This is the model class for table "{{order_item}}".
 *
 * The followings are the available columns in table '{{order_item}}':
 * @property integer $id
 * @property integer $order_id
 * @property integer $position_id
 * @property integer $status_id
 * @property integer $price
 * @property integer $count
 * @property string $comment
 * @property integer $deleted
 * @property integer $sub_order
 */
class OrderItem extends CActiveRecord
{
	const STATUS_ERROR = 0; // ошибка создания позиции
	const STATUS_CREATE = 8; // создан из приложения
	const STATUS_ADOPTED = 9; // успешно передан в keeper || принят в обработку на сайте
	const STATUS_PREPARED = 10; // готовится
	const STATUS_COOKED = 11; // приготовлено
	const STATUS_DELIVERED = 12; // доставлено

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, position_id, status_id, price, count', 'required'),
			array('order_id, position_id, status_id, price, count, deleted, sub_order', 'numerical', 'integerOnly'=>true),
            array('position_id', 'validateOutId'),
            array('position_id', 'validateAvailability'),
            array('price', 'validatePrice'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order_id, position_id, status_id, price, count, comment, deleted, sub_order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'position' => array(self::BELONGS_TO, 'Position', 'position_id'),
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
            'status' => array(self::BELONGS_TO, 'OrderStatus', 'status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'ID заказа',
			'position_id' => 'ID блюда',
			'price' => 'Цена',
			'count' => 'Количество',
            'status_id' => 'Статус',
			'comment' => 'Комментарий',
			'deleted' => 'Удален',
			'sub_order' => 'Очередь'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('position_id',$this->position_id);
        $criteria->compare('status_id',$this->status_id);
		$criteria->compare('price',$this->price);
		$criteria->compare('count',$this->count);
		$criteria->compare('comment',$this->comment);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('sub_order',$this->sub_order);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function afterSave()
    {
        parent::afterSave();

        // TODO зависимость статуса блюда от статуса позиций calcOrderStatus
        // TODO сделать чтобы если отменили статус принят у блюда, отменялся принят и у заказа
    }

// ============================================================================================
// ВАЛИДАЦИОННЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================
    public function validateAvailability($attribute, $params){
        if ($this->isNewRecord && !$this->position->isAvailability($this->order->restaurant_id)) {
            $this->addError($attribute, 'Позиция "'.$this->position->name.'" недоступна для заказа в этом ресторане');
        }
    }

    public function validatePrice($attribute, $params){
        if ($this->isNewRecord && $this->position->price != $this->price) {
            $this->addError($attribute, 'Не соответствует цена для позиции "'.$this->position->name.'"');
        }
    }

    public function validateOutId($attribute, $params){
        if ($this->isNewRecord && !$this->position->out_id) {
            $this->addError($attribute, 'Позиция "'.$this->position->name.'" не может быть заказана. Отсутствует связь с внешним сервисом.');
        }
    }

// ============================================================================================
// ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================
    public function calcPrice(){
        return $this->price * $this->count;
    }

    public function calcCount(){
        return $this->count;
    }

// ============================================================================================
// СТАТИЧЕСКИЕ МЕТОДЫ МОДЕЛИ
// ============================================================================================
    public static function calOrderStatus($order_id){
        $items = OrderItem::model()->findAll('order_id = :order_id', array(':order_id' => $order_id));
        foreach($items as $v){
            if ($v->status_id != 9) return;
        }
        $order = Order::model()->findByPk($order_id);
        if ($order === null) return;
        $order->status_id = 2;
        $order->save();
    }



}
