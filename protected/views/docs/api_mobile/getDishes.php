<h3>Получение блюд компании</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getDishes<br/>
</p>

<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>company_id</em></strong>: ID компании (сеть ресторанов) <b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/getDishes?company_id=1&version=112');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "status": "ok",
  "version": "124",
  "data": [
    {
      "id": "18",
      "is_group": "0",
      "name": "\u0421\u044f\u043a\u044d \u0440\u043e\u043b\u043b",
      "description": "\u041a\u043b\u0430\u0441\u0441\u0438\u0447\u0435\u0441\u043a\u0438\u0439 \u0440\u043e\u043b\u043b \u0441 \u043b\u043e\u0441\u043e\u0441\u0435\u043c\r\n\r\n\u0421\u043e\u0441\u0442\u0430\u0432:(\u0440\u0438\u0441, \u0432\u043e\u0434\u043e\u0440\u043e\u0441\u043b\u0438)",
      "menu_id": "3",
      "sort": "0",
      "depressed": "0",
      "label_id": "0",
      "conditions": [
        {
          "id": "31",
          "measure": "8 \u0448\u0442.",
          "sort": "0",
          "price": "145",
          "dish_id": "18",
          "action": ""
        }
      ],
      "hidden": "0",
      "deleted": "0",
      "calories": "",
      "in_stock": "3,4,6"
    },
    {
      "id": "64",
      "is_group": "1",
      "name": "\u041b\u0430\u0439\u0442",
      "description": "",
      "menu_id": "13",
      "sort": "0",
      "depressed": "0",
      "label_id": "0",
      "conditions": [
        {
          "id": "91",
          "measure": "\u041e\u0431\u0449\u0430\u044f \u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c",
          "sort": "1",
          "price": "250",
          "dish_id": "64",
          "action": ""
        }
      ],
      "hidden": "0",
      "deleted": "0",
      "calories": "",
      "in_stock": ""
    }
  ]
}');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "status": "error",
  "error": {
    "code": 1,
    "message": "\u041d\u0435 \u0437\u0430\u0434\u0430\u043d \u043e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440 company_id"
  }
}');?>
    </code></pre>
<hr/>

