<?php

class DocsController extends Controller
{
    public $layout = 'docs_action';
    public $action_crumbs = '';
    public $action_method = '';
    public $action_menu = array();

	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionApi_mobile()
    {
        $this->action_method = Yii::app()->request->getParam('method');
        $this->action_menu = array(
            'getCompanies',
            'getDishesCategories',
            'getDishesCategoriesImages',
            'getDishes',
            'getDishesImages',
            'getOrders',
            'getOrderPayments',
            'getOrderStatuses',
            'postOrder'
        );
        $this->action_crumbs = 'API-Документация: Mobile Apps' . ($this->action_method ? ': '.$this->action_method : '');
        $this->render($this->action_method ? 'api_mobile/'.$this->action_method : 'api_mobile');
    }

    public function actionApi_mobile_v2()
    {
        $this->action_method = Yii::app()->request->getParam('method');
        $this->action_menu = array(
            'getCompanies',
            'getCompaniesImages',
            'getRestaurants',
            'getRestaurantPhotos',
            'getRestaurantTables',
            'getDishesCategories',
            'getDishesCategoriesImages',
            'getDishes',
            'getDishesImages',
            'getOrder',
            'getOrders',
            'getOrderPayments',
            'getOrderStatuses',
            'postOrder',
            'addOrderPosition',
            'getLabels',
            'setPushToken',
            'postFeedBack'
        );
        $this->action_crumbs = 'API-Документация: Mobile Apps (v2) ' . ($this->action_method ? ': '.$this->action_method : '');
        $this->render($this->action_method ? 'api_mobile_v2/'.$this->action_method : 'api_mobile_v2');
    }

    public function actionApi_rkeeper6()
    {
        $this->action_method = Yii::app()->request->getParam('method');
        $this->action_menu = array(
            'registrationProxy',
            'postNeedUpdate',
            'getRequests',
            'postChanges',
            'createOrder',
            'updateOrder',
            'payOrder',
            'other',
        );
        $this->action_crumbs = 'API-Документация: R-Keeper 6' . ($this->action_method ? ': '.$this->action_method : '');
        $this->render($this->action_method ? 'api_rkeeper6/'.$this->action_method : 'api_rkeeper6');
    }

    public function actionApi_rkeeper7()
    {
        $this->action_method = Yii::app()->request->getParam('method');
        $this->action_menu = array(
            'registrationProxy',
            'postNeedUpdate',
            'getRequests',
            'postChanges'
        );
        $this->action_crumbs = 'API-Документация: R-Keeper 7' . ($this->action_method ? ': '.$this->action_method : '');
        $this->render($this->action_method ? 'api_rkeeper7/'.$this->action_method : 'api_rkeeper7');
    }

}