/**
 * Created by TWT on 02.09.14.
 */

function updateCompanyUser(company_id, restaurant_id){
    $.ajax({
        url: '/admin/ajax/updateCompanyUser',
        beforeSend: showLoading,
        data: {
            company_id: company_id,
            restaurant_id: restaurant_id
        },
        dataType: 'html',
        error: function(jqXHR, textStatus){
            hideLoading();
            jAlert(jqXHR.responseText);
        },
        success: function(data){
            $('#page .column_left').html(data);
            hideLoading();
        },
        type: 'get'
    });
}

function updateRestaurantUserList(restaurant_id){
    var url = '/admin/user/getUserList';
    var active_user = $('#user_search_list li.active a').attr('data-id');
    $.ajax({
        url: url,
        beforeSend: showLoading,
        data: {restaurant_id: restaurant_id},
        dataType: 'html',
        error: function (jqXHR, textStatus){
            jAlert(jqXHR.responseText);
            hideLoading();
        },
        success: function (data) {
            hideLoading();
            $('#content_wrapper .column_center2').replaceWith(data);
            $('#user_search_list a[data-id='+active_user+']').closest('li').addClass('active');
        },
        type: 'get'
    });
}

function updateUserTables(){
    var tables = [];
    $('#user_table_list .table_inline').each(function(){
        if ($(this).attr('data-active') == 'yes')
            tables.push($(this).attr('data-id'));
    });
    $('#User_tables').val(JSON.stringify(tables));
}

function expandFirstUser(){
    var upci = 0;
    var upri = 0;
    if(cu_json['ids']){
        $.each(cu_json['ids'], function(k,v){
            if (upci) return;
            if (ru_json[v] && ru_json[v]['list']){
                $.each(ru_json[v]['list'], function(j,m){
                    if (upri) return;
                    if(m.users){
                        upci = v;
                        upri = j;
                    }
                });
            }
        });
    }
    $('#user_company_list a.ex_link_comp[data-id='+upci+']').trigger('click');
    $('#user_company_list a.ex_link_rest[data-id='+upri+']').trigger('click');
    setTimeout(function(){
        $('#user_search_list li:first a').trigger('click');
    },500);
}

$(document).ready(function(){
    $("#root").on('submit', '#changepassword-form', function(){
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                beforeSend: showLoading,
                data: form.serialize(),
                dataType: 'html',
                error: function(jqXHR, textStatus){
                    form.find('input[type=password]').val('');
                    jAlert(jqXHR.responseText);
                    hideLoading();
                },
                success: function(data){
                    form.find('input[type=password]').val('');
                    jAlert(data);
                    hideLoading();
                },
                type: 'POST'
            });
            return false;
    });

    $('#root').on('click', '#user_company_list a.ex_link_rest', function(e){
        e.preventDefault();
        var link = $(this);
        var id = parseInt(link.attr('data-id'));
        var url = '/admin/user/index';
        $.ajax({
            url: url,
            beforeSend: showLoading,
            data: {restaurant_id: id},
            dataType: 'html',
            error: function (jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function (data) {
                history.pushState({}, '', url);
                $('#user_company_list a.ex_link_rest').removeClass('active');
                link.addClass('active');
                ru_act = id;
                hideLoading();
                $('#content_wrapper').html(data);
            },
            type: 'get'
        });
    });

    $('#root').on('click', '#user_search_list a', function(){
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            data: {restaurant_id: ru_act},
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                user_id = link.attr('data-id');
                history.pushState({}, '', link.attr('href'));
                $('#content_wrapper').html(data);
                $('.phone_mask').inputmask("+7 (999) 999-99-99");
                hideLoading();
            },
            type: 'get'
        });
        return false;
    });

    $('#root').on('click', '#create_user', function(e){
        e.preventDefault();
        var link = $(this);
        $.ajax({
            url: link.attr('href'),
            beforeSend: showLoading,
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                history.pushState({}, '', link.attr('href'));
                $('#content_wrapper').html(data);
                $('.phone_mask').inputmask("+7 (999) 999-99-99");
                hideLoading();
            },
            type: 'get'
        });
    });


    $("#root").on('submit', '#user-form', function(e){
        e.preventDefault();
        var form = $(this);
        var type_before = form.attr('data-type');
        var type_after = '';
        $.ajax({
            url: form.attr('action'),
            beforeSend: showLoading,
            data: form.serialize(),
            dataType: 'html',
            error: function(jqXHR, textStatus){
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function(data){
                $('#content_wrapper').html(data);
                var newform = $('#user-form');
                type_after = newform.attr('data-type');
                if (type_before != type_after) history.pushState({},'',newform.attr('action'));
                if (newform.find('.errorMessage').length == 0){
                    updateCompanyUser(newform.find('#User_company_id').val(), newform.find('#User_restaurant_id').val());
                }
                $('.phone_mask').inputmask("+7 (999) 999-99-99");
                hideLoading();
            },
            type: 'POST'
        });
    });

    $('#root').on('click', '#user_set_deleted', function () {
        if ($('#User_deleted').val() == 1) {
            $('#User_deleted').val(0);
        } else {
            $('#User_deleted').val(1);
        }
        $('#user_save').trigger('click');
        return false;
    });

    $('#root').on('change', '#User_role', function(){
        $('#user-form .company_subtitle').html($('#User_role option[value='+$(this).val()+']').html());
        if ($(this).val() == 'garcon'){
            $('#user_tables_wrap').show();
        }else{
            $('#user_tables_wrap').hide();
        }
    });

    $('#root').on('change', '#User_restaurant_id', function(){
        var params = {
            'restaurant_id': $(this).val(),
            'role': $('#User_role').val(),
            'user_id': $(this).closest('form').attr('data-id')
        };
        ajaxRequest('/admin/user/tableList', function(data){
            $('#user_tables_wrap').replaceWith(data);
        }, params);
    });

    $('#root').on('keyup', '#User_fullname', function(){
        $('#user-form h1').html($(this).val());
    });

    $('#root').on('keyup', '#username', function(){
        $('#User_username').val($(this).attr('data-prefix') + '_' + $(this).val());
    });

    $('#root').on('click', '#user_table_list .table_inline .add', function(){
        var row = $(this).closest('.table_inline');
        row.attr('data-active', 'yes');
        updateUserTables();
    });

    $('#root').on('click', '#user_table_list .table_inline .del', function(){
        var row = $(this).closest('.table_inline');
        row.attr('data-active', 'no');
        updateUserTables();
    });

    $('.phone_mask').inputmask("+7 (999) 999-99-99");

});