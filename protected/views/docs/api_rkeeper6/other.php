<h3>Создание случайного заказа</h3>
<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/createRandomOrder?proxy_id=48<br/>
</p>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>proxy_id</em></strong>: ID прокси сервера <b>(обязательный)</b><br/>
    <strong><em>table</em></strong>: Номер стола <b>(не обязательный, по умолчанию 100)</b><br/>
</p>

<br>
<br>
<br>
<br>


<h3>Добавление случайных блюд в заказ</h3>
<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/updateOrder?order_id=123<br/>
    Просмотреть ID заказа, можно в списке <a target="_blank" href="http://bistroapp.ru/r_keeper_api/v1/listQueue">http://bistroapp.ru/r_keeper_api/v1/listQueue</a>
    в последнем столбце
</p>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>order_id</em></strong>: ID заказа <b>(обязательный)</b><br/>
</p>