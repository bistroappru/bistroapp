<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_main'); ?>
<table width="100%">
    <tr>
        <td width="80%" valign="top">
            <?php echo $content; ?>
        </td>
        <td valign="top">
            <div id="sidebar">
                <?php
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title'=>'Управление:',
                ));
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'operations'),
                ));
                $this->endWidget();
                ?>
            </div><!-- sidebar -->
        </td>
    </tr>
</table>
<?php $this->endContent(); ?>