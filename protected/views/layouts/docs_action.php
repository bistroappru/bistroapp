<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/docs'); ?>

<?php
    if (count($this->action_menu)){
        $this->beginClip('action_menu');
        foreach($this->action_menu as $row){
?>
            <a class="email-item pure-g <?=$this->action_method==$row ? 'active' : ''?>" href="<?=$this->createUrl('/docs/'.$this->action->id.'/method/'.$row);?>">
                <div class="pure-u">
                    <h5 class="email-name"><?=$row?></h5>
                </div>
            </a>
<?php
        }
        $this->endClip();
    }
?>

<div class="email-content-header pure-g">
    <div class="pure-u-11-12">
        <h1 class="email-content-title"><?=$this->action_crumbs;?></h1>
    </div>
</div>

<div class="email-content-body">
    <?=$content;?>
</div>
<!-- content -->
<?php $this->endContent(); ?>
