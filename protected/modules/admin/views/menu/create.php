<?php
/* @var $this MenuController */
/* @var $model Menu */
$this->pageTitle = Yii::app()->name . ' - Создание меню';
?>

<div class="column_center2">
    <div class="opacity_block">
        <div class="warning">
            <h2>ВНИМАНИЕ</h2>
            <p>Список блюд будет доступен после создания категории меню</p>
        </div>
    </div>
</div>

<div class="column_right">
    <div class="product_section">
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>