﻿$(document).ready(function() {

	$('input[type=checkbox]').each(function(){
		if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
		$(this).change(function(){
			if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
		});
	});

	jQuery('.order_table input[type=checkbox]').each(function(){
		if($(this).is(':checked')) {
			$(this).addClass('checked') ;
			$(this).closest('tr').addClass('current');
		} else {
			$(this).removeClass('checked');
			$(this).closest('tr').removeClass('current');
		};
	});

	$('.edit-link').click(function(){
		$(this).closest('.product_row').find('input[type=text], textarea').prop('readonly', false);
		return false;
	});

	$('.hit-btn').click(function(){
		$(this).closest('.product_row').find('.add_hit').slideToggle('fast');
		$(this).closest('.product_row').find('.hit_box').toggleClass('opened');
		return false;
	});


	jQuery('.modal_shower .show_arrow').click(function(){
		if ($(this).hasClass('m-up')) {
			$(this).closest('.modal_text').find('.hide_text').slideUp('fast');
			$(this).removeClass('m-up').addClass('m-down');
		} else if ($(this).hasClass('m-down')) {
			$(this).closest('.modal_text').find('.hide_text').slideDown('fast');
			$(this).removeClass('m-down').addClass('m-up');
		}
	});

    jQuery('.scroll-pane').jScrollPane({autoReinitialise:true});
	if (jQuery('.food_scroll .scroll-pane').hasClass('jspScrollable')) {
		jQuery('.food_total').css('marginRight', '18px');
	}

	$(window).resize(function(){
		jQuery('.food_scroll .scroll-pane, .food_scroll .scroll-pane .jspContainer, .food_scroll .scroll-pane .jspPane').css('width', '100%');
		jQuery('.food_scroll .scroll-pane .jspContainer').css('height', '397px');
		jQuery('.food_scroll .scroll-pane, .table-scroll .scroll-pane').jScrollPane({autoReinitialise:true});
		if (jQuery('.food_scroll .scroll-pane').hasClass('jspScrollable')) {
			jQuery('.food_total').css('marginRight', '18px');
		}
	});	



    $(document).on('click', '.msg-error, .msg-success', function(){
       $(this).fadeOut(500, function(){
           $(this).remove();
       });
    });
});