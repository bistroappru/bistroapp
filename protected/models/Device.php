<?php

/**
 * This is the model class for table "{{device}}".
 *
 * The followings are the available columns in table '{{device}}':
 * @property integer $id
 * @property string $uuid
 * @property string $platform
 * @property string $name
 * @property string $version
 * @property integer $blocked
 * @property string $push_token
 */
class Device extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{device}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uuid, platform, name, version', 'required'),
			array('blocked', 'numerical', 'integerOnly'=>true),
			array('uuid, platform, name, version', 'length', 'max'=>255),
            array('uuid', 'unique'),
            array('push_token', 'match', 'pattern' => '/^\{?[a-fA-F0-9]{8} [a-fA-F0-9]{8} [a-fA-F0-9]{8} [a-fA-F0-9]{8} [a-fA-F0-9]{8} [a-fA-F0-9]{8} [a-fA-F0-9]{8} [a-fA-F0-9]{8}\}?$/'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uuid, platform, name, version, blocked', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'logs' => array(self::HAS_MANY, 'DeviceLog', 'device_id'),
            'orders' => array(self::HAS_MANY, 'Order', 'device_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uuid' => 'UUID',
			'platform' => 'Платформа',
			'name' => 'Имя устройства',
			'version' => 'Версия ОС',
			'blocked' => 'Блокировка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uuid',$this->uuid,true);
		$criteria->compare('platform',$this->platform,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('blocked',$this->blocked);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Device the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
