<?php
/* @var $this PositionController */
/* @var $model Position */
$this->pageTitle = Yii::app()->name . ' - Редактирование позиции';
?>

<div class="column_center">
    <?php $this->widget('DishList', array('menu_id'=>$model->menu_id,'dish_id'=>$model->id,'group'=>$model->is_group)); ?>
    <?php $this->widget('PositionGroupList', array('menu'=>$model->menu,'dish_id'=>$model->id,'group'=>$model->is_group)); ?>
</div>

<div class="column_right">
    <div class="product_section">
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>

