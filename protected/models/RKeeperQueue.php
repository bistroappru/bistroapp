<?php

/**
 * This is the model class for table "{{rkeeper_queue}}".
 *
 * The followings are the available columns in table '{{rkeeper_queue}}':
 * @property integer $id
 * @property string $uuid
 * @property integer $proxy_id
 * @property integer $status
 * @property integer $type
 * @property string $body
 * @property string $response
 * @property integer $create_time
 * @property integer $request_time
 * @property integer $response_time
 * @property string $error_description
 * @property string $data
 */
class RKeeperQueue extends CActiveRecord
{
    private $_ref_name;
    private $_cmd;

    const STATUS_WAIT = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_FAIL = 2;
    const STATUS_RESPONSE = 3;
    const STATUS_PARSE = 4;
    const STATUS_FINISH = 5;
    const STATUS_CANCEL = 6;

    const TYPE_OUT = 0;
    const TYPE_REF = 1;
    const TYPE_MID = 2;
    const TYPE_ORDER = 3;
    const TYPE_ORDER_ITEMS = 4;
    const TYPE_RK6 = 5;
    const TYPE_ORDER_RK6 = 6;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rkeeper_queue}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uuid, proxy_id, status, body, type', 'required'),
			array('proxy_id, status, type, create_time, request_time, response_time', 'numerical', 'integerOnly'=>true),

            //array('uuid', 'length', 'max'=>255),
            array('uuid', 'ruleMaxLength', 'max'=>255),
            array('uuid', 'ruleUUID'),

            array('uuid', 'unique'),
            array('proxy_id', 'issetProxy'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uuid, proxy_id, type, body, response', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'proxy'=>array(self::BELONGS_TO, 'RKeeperProxy', 'proxy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'uuid' => 'UUID запроса',
			'proxy_id' => 'Прокси-сервер',
			'status' => 'Статус',
            'type' => 'Тип запроса',
			'body' => 'Запрос к R-Keeper',
			'response' => 'Ответ R-Keeper',
            'create_time' => 'Время создания',
            'request_time' => 'Время отправки запроса',
            'response_time' => 'Время получения ответа',
            'error_description' => 'Описание ошибки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('uuid',$this->uuid);
		$criteria->compare('proxy_id',$this->proxy_id);
        $criteria->compare('status',$this->status);
		$criteria->compare('type',$this->type);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('response',$this->response,true);
        $criteria->compare('create_time',$this->create_time);
        $criteria->compare('request_time',$this->request_time);
        $criteria->compare('response_time',$this->response_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RKeeperQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


// ============================================================================================
// ВАЛИДАЦИОННЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================

    public function issetProxy($attribute, $params){
        $model = RKeeperProxy::model()->findByPk($this->$attribute);
        if ($model === null){
            $this->addError($attribute, (string) $params['message']);
        }
    }

    public function ruleUUID($attribute, $params){
        $pattern = '/^\{?[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}\}?$/';
        if (!preg_match($pattern, $this->$attribute)) {
            $this->addError($attribute, (string) $params['message']);
        }
    }

    public function ruleMaxLength($attribute, $params){
        if (isset($params['max']) && strlen($this->$attribute) > $params['max']){
            $this->addError($attribute, (string) $params['message']);
        }
    }

// ============================================================================================
// ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ОБЪЕКТА
// ============================================================================================
    public function getRKNum(){
        return ($this->proxy->proxy_type == RKeeperProxy::TYPE_RK6) ? 6 : 7;
    }

    public function getCMD(){
        if (!$this->_cmd){
            $xml = @simplexml_load_string($this->body);

            if($this->proxy->proxy_type == RKeeperProxy::TYPE_RK6){
                $this->_cmd = (string) $xml->RK6CMD['CMD'];
            }else{
                $this->_cmd = (string) $xml->RK7CMD['CMD'];
            }
        }
        return $this->_cmd;
    }

    public function getRefName(){
        if (!$this->_ref_name){
            $xml = @simplexml_load_string($this->body);

            if($this->proxy->proxy_type == RKeeperProxy::TYPE_RK6){
                $this->_ref_name = (string) $xml->RK6CMD['RefName'];
            }else{
                $this->_ref_name = (string) $xml->RK7CMD['RefName'];
            }
        }
        return $this->_ref_name;
    }

    public function fail($msg = 'unknown error'){
        $this->status = self::STATUS_FAIL;
        $this->error_description = $msg;
        if (!$this->save()){
            throw new Exception('Не удалось завершить обработку (setFail) запроса');
        }
    }

    public function finish(){
        $this->status = self::STATUS_FINISH;
        if (!$this->save()){
            throw new Exception('Не удалось завершить обработку (setFinish) запроса');
        }
    }

// ============================================================================================
// СТАТИЧЕСКИЕ МЕТОДЫ МОДЕЛИ
// ============================================================================================
    /**
     * @param null $key
     * @return array
     */
    public static function getStatus($key = NULL){
        $items = array(
            self::STATUS_WAIT => 'В очереди',
            self::STATUS_ACTIVE => 'Ожидает ответа',
            self::STATUS_FAIL => 'Ошибка',
            self::STATUS_RESPONSE => 'Ожидает синхронизации',
            self::STATUS_FINISH => 'Завершен',
            self::STATUS_CANCEL => 'Отменен',
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }


    /**
     * @param null $key
     * @return array
     */
    public static function getType($key = NULL){
        $items = array(
            self::TYPE_OUT => 'Внешний запрос',
            self::TYPE_REF => 'Запрос к REF',
            self::TYPE_MID => 'Запрос к MID',
            self::TYPE_ORDER => 'Создание заказа',
            self::TYPE_ORDER_ITEMS => 'Обновление заказа',
            self::TYPE_RK6 => 'Запрос к RK6',
            self::TYPE_ORDER_RK6 => 'Создание заказа RK6'
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }


    /**
     * @param null $uuid
     * @return array|CActiveRecord|mixed|null
     */
    public static function loadModelByRequest($uuid = null){
        return self::model()->findByAttributes(array('uuid' => $uuid));
    }

    /**
     * @param $proxy_id
     * @return array|CActiveRecord|mixed|null
     */
    public static function getProxyRequests(RKeeperProxy $proxy, $limit = 1){
        return self::model()->findAllByAttributes(array(
            'proxy_id' => $proxy->id,
            'status' => self::STATUS_WAIT,
            //'type' => $proxy->proxy_type
        ), array('order' => 'type DESC, id ASC', 'limit' => $limit));
    }

    public static function getPreparedOrders($count = 1){
        return self::model()->findAllByAttributes(array(
            'status' => self::STATUS_RESPONSE,
            'type' => self::TYPE_ORDER
        ), array(
            'limit' => (int) $count
        ));
    }

    public static function getPreparedOrderItems($count = 1){
        return self::model()->findAllByAttributes(array(
            'status' => self::STATUS_RESPONSE,
            'type' => self::TYPE_ORDER_ITEMS
        ), array(
            'limit' => (int) $count
        ));
    }

    public static function getPreparedReferences($count = 1){
        return self::model()->findAllByAttributes(array(
            'status' => self::STATUS_RESPONSE,
            'type' => self::TYPE_REF
        ), array(
            'limit' => (int) $count
        ));
    }

    public static function checkRKeeper6Order(Order $order){
        $model = new RKeeperQueue();
        $model->uuid = AbcHelper::createUUID();
        $model->proxy_id = $order->getProxyId(RKeeperProxy::TYPE_RK6);
        $model->status = RKeeperQueue::STATUS_WAIT;
        $model->type = RKeeperQueue::TYPE_ORDER_RK6;
        $model->data = serialize(array('order_id' => $order->id));
        $model->body = Yii::app()->controller->renderPartial('application.views.rK7V1._request_CheckOrder_RK6', array('order' => $order), true);
        $model->create_time = time();

        if (!$model->save()){
            $order->status_id = Order::STATUS_ERROR;
            $order->save();
        }
    }

    public static function createRKeeper6Order(Order $order){
        $model = new RKeeperQueue();
        $model->uuid = AbcHelper::createUUID();
        $model->proxy_id = $order->getProxyId(RKeeperProxy::TYPE_RK6);
        $model->status = RKeeperQueue::STATUS_WAIT;
        $model->type = RKeeperQueue::TYPE_ORDER_RK6;

        // получение списка позиций
        $items = array();
        foreach($order->items as $one){
            $items[$one->id] = $one;
        }
        $model->data = serialize(array('order_id' => $order->id, 'order_items' => array_keys($items)));

        $data = array(
            'comment' => $order->comment,
            'order_guid' => AbcHelper::createUUID(),
            'table' => $order->table->name,
            'guests' => $order->guests
        );

        $model->body = Yii::app()->controller->renderPartial('application.views.rK7V1._request_CreateOrder_RK6', array('data' => $data, 'order' => $order, 'items' => $items), true);
        $model->create_time = time();

        if (!$model->save()){
            $order->status_id = Order::STATUS_ERROR;
            $order->save();
        }
    }

    public static function createRKeeper7Order(Order $order){
        $model = new RKeeperQueue();
        $model->uuid = AbcHelper::createUUID();
        $model->proxy_id = $order->getProxyId(RKeeperProxy::TYPE_MID);
        $model->status = RKeeperQueue::STATUS_WAIT;
        $model->type = RKeeperQueue::TYPE_ORDER;
        $model->data = serialize(array('order_id' => $order->id));

        $data = array(
            'comment' => $order->comment,
            'table_out_id' => $order->table->out_id,
            'garcon_out_id' => $order->garcon ? $order->garcon->out_id : 0,
            'station_out_id' => RestaurantTools::getCashOutId($order->restaurant_id),
        );

        $model->body = Yii::app()->controller->renderPartial('application.views.rK7V1._request_CreateOrder', array('data' => $data), true);
        $model->create_time = time();

        if (!$model->save()){
            $order->status_id = Order::STATUS_ERROR;
            $order->save();
        }
    }

    public static function addPositionRKeeper6Order(Order $order, $positions){
        $model = new RKeeperQueue();
        $model->uuid = AbcHelper::createUUID();
        $model->proxy_id = $order->getProxyId(RKeeperProxy::TYPE_RK6);
        $model->status = RKeeperQueue::STATUS_WAIT;
        $model->type = RKeeperQueue::TYPE_ORDER_RK6;
        $model->data = serialize(array('order_id' => $order->id, 'order_items' => array_keys($positions)));

        $data = array(
            'comment' => $order->comment,
            'order_guid' => AbcHelper::createUUID(),
            'table' => $order->table_text,
            'guests' => $order->guests
        );

        $model->body = Yii::app()->controller->renderPartial('application.views.rK7V1._request_UpdateOrder_RK6', array('data' => $data, 'order' => $order, 'items'=>$positions), true);
        $model->create_time = time();

        if (!$model->save()){
            $order->status_id = Order::STATUS_ERROR;
            $order->save();
        }
    }

    public static function addPositionRKeeper7Order(Order $order){

    }

    public static function createRKeeperOrderItems(Order $order){
        $model = new RKeeperQueue();
        $model->uuid = AbcHelper::createUUID();
        $model->proxy_id = $order->getProxyId(RKeeperProxy::TYPE_MID);
        $model->status = RKeeperQueue::STATUS_WAIT;
        $model->type = RKeeperQueue::TYPE_ORDER_ITEMS;
        $model->data = serialize(array('order_id' => $order->id));

        $model->body = Yii::app()->controller->renderPartial('application.views.rK7V1._request_SaveOrder', array('order' => $order), true);
        $model->create_time = time();

        if (!$model->save()){
            // TODO отправляем PUSH сообщение об ошибке
            // устанавливаем что заказ отменен
            $order->status_id = Order::STATUS_CANCELED;
            $order->save();
        }
    }

}