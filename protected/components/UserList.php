<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class UserList extends CWidget{
    public $restaurant_id = 0;
    public $filter_key;

    public function run(){
        if (!$this->restaurant_id) return;

        $rest = Restaurant::model()->findByPk($this->restaurant_id);

        $user_id = Yii::app()->request->getParam('id', 0);
        $criteria = new CDbCriteria;
        $criteria->order = 'restaurant_id,fullname ASC';
        $criteria->addCondition('id != '. Yii::app()->user->id);

        if(Yii::app()->user->role == 'admin'){
            $criteria->addCondition("company_id = {$rest->company_id} AND (restaurant_id = {$this->restaurant_id} OR restaurant_id = '')");
        }else{
            $criteria->compare('restaurant_id', $this->restaurant_id);
        }

        // ---- filter
        $filter_data = explode('||', (string) Yii::app()->request->cookies[$this->filter_key]->value);
        if (!in_array('blocked', $filter_data))
            $criteria->addCondition("blocked = 0");
        if (!in_array('deleted', $filter_data))
            $criteria->addCondition("deleted = 0");

        //if (Yii::app()->user->restaurant_id) $criteria->addCondition('restaurant_id = '.Yii::app()->user->restaurant_id);

        $user_list = User::model()->findAll($criteria);
        $list = AbcHelper::prepareModelsToArray($user_list);

        $this->render('_user_list', array(
            'user_list' => CJSON::encode($list),
            'user_id' => $user_id,
            'role_list' => CJSON::encode(AbcHelper::getRoleList()),
            'company_id' => $rest->company_id,
            'restaurant_id' => $this->restaurant_id,
            'filter_key' => $this->filter_key
        ));
    }
}