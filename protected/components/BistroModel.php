<?php
/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 15.12.14
 * Time: 15:30
 */

class BistroModel extends CActiveRecord {

    /*
    public static function createExternalObject($data = array()){
        $insert_id = null;
        $model_name = get_called_class();
        if (is_array($data) && count($data)){
            $bd = Yii::app()->db->createCommand();
            if ($bd->insert($model_name::model()->tableName(), $data)){
                $insert_id = Yii::app()->db->getLastInsertID();
            }
        }
        return $insert_id;
    }
    */

    /**
     * Создание пустых объектов по out_id при обработке данных синхронизации
     *
     * @param array $data
     * @return null|int
     */
    public static function createExternalObject($data = array()){
        $insert_id = null;
        $model_name = get_called_class();
        if (is_array($data) && count($data)){
            $restaurant = new $model_name;
            $restaurant->setAttributes($data);
            $restaurant->save(false);
            $insert_id = $restaurant->id;
        }
        return $insert_id;
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Restaurant the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}