<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class CompanyUser extends CWidget {
    public $company_id = 0;
    public $restaurant_id = 0;

    public function run(){
        $fields = 'id,name,logo,hidden,deleted';
        $criteria = new CDbCriteria();
        $criteria->select = $fields;
        $criteria->order = 'name ASC';

        if ($cid = Yii::app()->user->company_id)
            $criteria->compare('id', $cid);

        $rests = array();
        $list = array();
        $tmp = Company::model()->findAll($criteria);
        foreach($tmp as $k=>$v){
            $list['ids'][] = $v->id;
            $list['list'][$v->id] = $v->getAttributes($fields);
            $list['list'][$v->id]['restaurants'][] = count($v->restaurants);
            $list['list'][$v->id]['users'] = count($v->users);

            // выборка ресторанов
            $fr = 'id,name,location,hidden,deleted';
            $cr = new CDbCriteria();
            $cr->select = $fr;
            $cr->order = 'name ASC';
            if ($rid = Yii::app()->user->restaurant_id)
                $cr->compare('id', $rid);
            $cr->compare('company_id', $v->id);

            $rests_tmp = Restaurant::model()->findAll($cr);
            foreach($rests_tmp as $rest){
                $rests[$v->id]['ids'][] = $rest->id;
                $rests[$v->id]['list'][$rest->id] = $rest->getAttributes($fr);
                $rests[$v->id]['list'][$rest->id]['users'] = count($rest->users);
            }
        }

        $this->render('_company_user',array(
            'company_list' => $list,
            'restaurant_list' => $rests,
            'company_id' => $this->company_id,
            'restaurant_id' => $this->restaurant_id,
        ));
    }
}