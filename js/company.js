/**
 * Created by birlaver on 30.08.14.
 */
function updateCompanyList(company_id) {
    var search = $('#company_search input[name=company_search]').val();
    $.ajax({
        url: '/admin/ajax/updateCompanyList',
        beforeSend: showLoading,
        data: {company_id: company_id},
        dataType: 'html',
        error: function (jqXHR, textStatus) {
            hideLoading();
            jAlert(jqXHR.responseText);
        },
        success: function (data) {
            $('#page .column_left').html(data);
            $('#company_search input[name=company_search]').val(search);
            searchCompanies();
            hideLoading();
        },
        type: 'get'
    });
}

$(document).ready(function () {
    $('#root').on('click', '#company_list a', function () {
        var link = $(this);
        checkFormModified(function(){
            $.ajax({
                url: link.attr('href'),
                beforeSend: showLoading,
                dataType: 'html',
                error: function (jqXHR, textStatus) {
                    hideLoading();
                    jAlert(jqXHR.responseText);
                },
                success: function (data) {
                    company_id = parseInt(link.closest('li').attr('data-id'));
                    $('#company_list li').removeClass('active');
                    link.closest('li').addClass('active');
                    $('#content_wrapper').html(data);
                    history.pushState({}, '', link.attr('href'));
                    inputChecked($('input[type=checkbox]'));
                    hideLoading();
                },
                type: 'post'
            });
        });
        return false;
    });

    $('#root').on('click', '#company_create', function (e) {
        e.preventDefault();
        var link = $(this);
        checkFormModified(function(){
            $.ajax({
                url: link.attr('href'),
                beforeSend: showLoading,
                dataType: 'html',
                error: function (jqXHR, textStatus) {
                    hideLoading();
                    jAlert(jqXHR.responseText);
                },
                success: function (data) {
                    company_id = 0;
                    $('#company_list li').removeClass('active');
                    $('#content_wrapper').html(data);
                    history.pushState({}, '', link.attr('href'));
                    hideLoading();
                },
                type: 'post'
            });
        });
    });

    $('#root').on('change', '#company-form .workplaces input[type=checkbox]', function(){
        var string = '';
        $('#company-form .workplaces input[type=checkbox]:checked').each(function(){
            string += ','+$(this).val();
        });
        $('#Company_workplaces').val(string.substr(1));
    });

    $('#root').on('submit', '#company-form', function (e) {
        e.preventDefault();
        var form = $(this);
        var type_before = form.attr('data-type');
        var type_after = '';
        $.ajax({
            url: form.attr('action'),
            beforeSend: showLoading,
            data: form.serialize(),
            dataType: 'html',
            error: function (jqXHR, textStatus) {
                jAlert(jqXHR.responseText);
                hideLoading();
            },
            success: function (data) {
                $('#content_wrapper').html(data);
                var newform = $('#company-form');
                type_after = newform.attr('data-type');
                if (type_before != type_after) history.pushState({}, '', newform.attr('action'));
                if (newform.find('.errorMessage').length == 0) {
                    updateCompanyList(newform.attr('data-id'));
                }
                inputChecked($('input[type=checkbox]'));
                hideLoading();
            },
            type: 'POST'
        });
    });

    $('#root').on('click', '#company_save', function () {
        $('#company-form').submit();
        return false;
    });

    $('#root').on('keyup', '#Company_name', function () {
        $('#company-form h1').html($(this).val());
    });

    $('#root').on('click', '#company_set_deleted', function () {
        if ($('#Company_deleted').val() == 1) {
            $('#Company_deleted').val(0);
        } else {
            $('#Company_deleted').val(1);
        }
        $('#company_save').trigger('click');
        return false;
    });

    $('#root').on('click', '#company_set_hidden', function () {
        if ($('#Company_hidden').val() == 1) {
            $('#Company_hidden').val(0);
        } else {
            $('#Company_hidden').val(1);
        }
        $('#company_save').trigger('click');
        return false;
    });

    // изменение полей формы
    $('#root').on('change', '#company-form input, #company-form textarea', function () {
        setDirty($(this).closest('form'));
    });

});