<h3>Получение изображений блюд</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getDishesImages<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>company_id</em></strong>: ID компании <b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
        {
        "status": "ok",
        "data": [
            {
                "id": "8",
                "image": "\/9j\/4AAQSkZJRgABAQAAAQABA...",
                "extension": "jpeg",
                "size": {
                    "width": 640,
                    "height": 640
                }
            },
            {
                "id": "18",
                "image": "\/9j\/4AAQSkZJRgABAQEASABIAAD...",
                "extension": "jpeg",
                "size": {
                    "width": 640,
                    "height": 640
                }
            }
        ],
        "version": "4"
        }
    </code></pre>
<hr/>