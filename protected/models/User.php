<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $out_id
 * @property string $out_code
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $fullname
 * @property string $phone
 * @property string $role
 * @property integer $blocked
 * @property integer $deleted
 * @property string $company_id
 * @property string $restaurant_id
 * @property string $image
 */
class User extends CActiveRecord
{
    const ROLE_GUEST = 'guest';
    const ROLE_MOBILE_GUEST = 'mobile_guest';
    const ROLE_MOBILE = 'mobile';
    const ROLE_COOK = 'cook';
    const ROLE_BARMAN = 'barman';
    const ROLE_GARCON = 'garcon';
    const ROLE_CASHIER = 'cashier';
    const ROLE_MANAGER = 'manager';
    const ROLE_OWNER = 'owner';
    const ROLE_ADMIN = 'admin';

    public static function getRoles($key = NULL){
        $items = array(
            self::ROLE_GUEST => 'Гость',
            self::ROLE_MOBILE_GUEST => 'Девайс (гость)',
            self::ROLE_MOBILE => 'Девайс',
            self::ROLE_COOK => 'Повар',
            self::ROLE_BARMAN => 'Бармен',
            self::ROLE_GARCON => 'Оффициант',
            self::ROLE_CASHIER => 'Кассир',
            self::ROLE_MANAGER => 'Администратор ресторана',
            self::ROLE_OWNER => 'Владелец сети',
            self::ROLE_ADMIN => 'СуперАдмин',
        );

        if($key !== NULL && isset($items[$key]))
            return $items[$key];
        return $items;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email, fullname, role, company_id', 'required'),
            array('username', 'unique'),
            array('email', 'ruleCompanyMail'),
            array('email', 'email'),
            array('restaurant_id', 'ruleRestaurant'),
            array('phone', 'rulePhone'),
			array('blocked, deleted, restaurant_id, company_id', 'numerical', 'integerOnly'=>true),
			array('out_id, out_code, username, password, email, fullname', 'length', 'max'=>255),
			array('role', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, out_code, username, password, email, fullname, phone, role, blocked, deleted, restaurant_id, company_id, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'company'=>array(self::BELONGS_TO, 'Company', 'company_id'),
            'restaurant'=>array(self::BELONGS_TO, 'Restaurant', 'restaurant_id'),
            'tables' => array(self::HAS_MANY, 'UserTable', 'user_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'out_id' => 'Ident',
            'out_code' => 'Code',
			'username' => 'Логин',
			'password' => 'Пароль',
			'email' => 'Email',
			'fullname' => 'ФИО',
            'phone' => 'Телефон',
			'role' => 'Роль',
			'blocked' => 'Заблокирован',
            'deleted' => 'Удален',
			'restaurant_id' => 'Ресторан',
            'company_id' => 'Компания',
            'image' => 'Аватар'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('blocked',$this->blocked);
        $criteria->compare('deleted',$this->deleted);
        $criteria->compare('phone',$this->phone);
		$criteria->compare('restaurant_id',$this->restaurant_id,true);
        $criteria->compare('company_id',$this->company_id,true);
        $criteria->compare('image',$this->image);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function prepareWithPostData(){
        $this->setAttributes($_POST['User'], false);

        $tables_list = array();
        $tmp = CJSON::decode($_POST['User']['tables']);
        if (is_array($tmp) && $this->role = User::ROLE_GARCON){
            foreach($tmp as $v){
                $table = UserTable::model()->findByAttributes(array('table_id' => $v, 'user_id' => $this->id));
                if ($table == null){
                    $table = new UserTable();
                }
                $table->table_id = $v;
                $tables_list[] = $table;
            }
        }
        $this->tables = $tables_list;
    }

    protected function beforeSave(){
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->password = md5($this->password);
            }
            return true;
        }
        else
            return false;
    }

    protected function afterSave(){
        parent::afterSave();

        // удаление неактуальных столиков
        $criteria = new CDbCriteria();
        $criteria->compare('user_id', $this->id);
        $criteria->addNotInCondition('table_id', $this->getTableList());
        UserTable::model()->deleteAll($criteria);

        // привязка столиков к юзеру
        foreach($this->tables as $one) {
            $one->user_id = $this->id;
            $one->save();
        }
    }

    public function ruleRestaurant($attribute, $params){
        if ($this->role != 'owner' && !$this->restaurant_id) $this->addError('restaurant_id', 'Необходимо заполнить поле «Ресторан».');
        if ($this->role == 'owner' && $this->restaurant_id) $this->addError('restaurant_id', 'Для роли "'.AbcHelper::getRoleName('owner').'" поле «Ресторан» необходимо оставить пустым.');
    }

    public function rulePhone(){
        if ($this->phone && !preg_match("/^\+7 \([0-9]{3}\) [0-9]{3}-[0-9]{2}\-[0-9]{2}/", $this->phone)) {
            $this->addError('phone', 'Необходимо указать телефон в формате +7 (ххх) ххх-хх-хх');
        }
    }

    public function ruleCompanyMail($attribute, $params){
        $criteria = new CDbCriteria();
        $criteria->addCondition('company_id = '. $this->company_id);
        $criteria->addCondition('email = \''. $this->email.'\'');
        $criteria->addCondition('id != '. $this->id);
        $user = self::model()->find($criteria);
        if ($user) {
            $this->addError($attribute, 'Этот E-Mail уже используется у пользователя ' . $user->fullname);
        }
    }

    public static function getGarconByRestaurant($id){
        $out = array();
        foreach(self::model()->findAll('role = :garcon AND restaurant_id = :restaurant_id', array(':restaurant_id' => $id, ':garcon' => 'garcon')) as $one){
            $out[$one->id] = $one->fullname;
        }
        return $out;
    }

    public function getTableList(){
        $list = array();
        if (is_array($this->tables)){
            foreach($this->tables as $v){
                $list[] = $v->table_id;
            }
        }
        return $list;
    }

    public function getRole_name(){
        return self::getRoles($this->role);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    // ---------    CUSTOM OBJECT METHODS   ----------

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------
    public static function parseKeeperData(RKeeperQueue $queue){
        $response = @simplexml_load_string($queue->response);

        if (!$response){
            $queue->setFail('Не удалось обработать xml-ответ');
            return;
        }

        if ((string) $response['Status'] !== 'Ok'){
            $queue->setFail('Статус должен быть: Ok');
            return;
        }

        if ((string) $response['CMD'] !== $queue->getCMD()){
            $queue->setFail('Команда запроса и ответа не совпадает');
            return;
        }

        if ((string) $response->RK7Reference['Name'] !== strtoupper($queue->getRefName())){
            $queue->setFail('Справочник запроса и ответа не совпадает');
            return;
        }

        if ($response->RK7Reference->Items->count()){
            foreach ($response->RK7Reference->Items->children() as $row){
                $item = AbcHelper::simple_xml_attr_to_array($row);
                if (!$item['Ident']) continue;

                $data = array();
                $data['out_id'] = $item['Ident'];
                $data['out_code'] = $item['Code'];
                $data['company_id'] = $queue->proxy->company_id;

                $model = User::model()->findByAttributes(array('out_id' => $data['out_id']));
                if ($model === null){
                    $model = new User();

                    // задаем только для нового, чтобы не затереть изменения
                    $data['username'] = $queue->proxy->company->prefix . '_user' . $item['Ident'];
                    $data['fullname'] = $item['Name'];
                    $data['email'] = $data['username'] . '@bistroapp.ru';
                    $data['blocked'] = 1;
                    $data['role'] = User::ROLE_GARCON;
                    $data['password'] = '123';
                }

                $model->setAttributes($data);
                if (!$model->save(false)){
                    $queue->setFail('Не удалось создать/обновить пользователя с out_id = ' . $model->out_id);
                    return;
                }

                $queue->setFinished();
            }
        }
    }

}
