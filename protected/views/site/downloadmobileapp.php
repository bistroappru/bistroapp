<?php
/* @var $this SiteController */
$this->pageTitle = 'Скачать мобильное приложение - ' . Yii::app()->name;
?>

<div id="wrapper">
    <div id="home_header">
        <div class="center">
            <div class="header_device"></div>
            <a href="/" id="logo"><img src="/html/front/images/logo_home.png" width="158" height="161" alt=""></a>
        </div>
    </div>
    <script type="text/javascript">
        $('#home_header').backstretch("images/header.jpg");
        jQuery(document).ready(function(){jQuery(window).resize(function(){
            $('#home_header').backstretch("/html/front/images/header.jpg");
        });})
    </script>
    <section class="benefits_section">
        <div class="pattern_top"></div>
        <div class="pattern_bottom"></div>
        <div class="center" style="padding-top: 100px;">
            <center>
                <a href="itms-services://?action=download-manifest&url=http://bistroapp.ru/apps/bistroApp_v1.2.2.plist"><img width="263" height="88" src="/html/front/images/download_app.png"></a>
            </center>
        </div>
    </section>
</div>


