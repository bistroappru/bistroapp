<div class="rest_search" id="user_search">
    <form>
        <input class="inp_rest_srh" type="text" name="user_search" value="Поиск" onfocus="this.value=(this.value=='Поиск')? '' : this.value ;" onblur="this.value=(this.value=='')? 'Поиск' : this.value ;">
        <input class="submit_rest_srh" type="submit" value=" ">
    </form>

    <?php
    Yii::app()->controller->renderPartial('/partials/_user_list_filter', array('key' => $filter_key, 'company_id' => $company_id, 'restaurant_id' => $restaurant_id));
    ?>

</div>

<ul class="rests_menu" id="user_search_list"></ul>
<ul class="leftmenu_buttons">
    <li><a id="create_user" href="<? echo CHtml::normalizeUrl('/admin/user/create/restaurant_id/'.$restaurant_id);?>"><img src="/images/leftmenu-5.png" width="50" height="50" alt=""><span>Добавить пользователя</span></a></li>
</ul>

<script>
    var user_json = <?=$user_list?>;
    var role_list = <?=$role_list?>;
    var user_id =  <?=$user_id?>;

    function searchUsers(){
        var search = $('#user_search input[name=user_search]').val();
        if (!search || search == 'Поиск') search = '';
        var ul_html = '';
        $.each(user_json, function(k, v){
            pos = v.fullname.toLowerCase().indexOf(search.toLowerCase());
            if (pos != -1){
                ul_html += '<li class="'+(user_id == v.id ? 'active' : '')+' '
                    +(v.deleted == 1 ? 'style_del' : '')+' '
                    +(v.blocked == 1 ? 'style_hid' : '')+'"> \
                        <a href="/admin/user/update/id/'+ v.id+'" data-id="'+v.id+'">\
                            <span class="user_ico"><img src="'+(v.image ? v.image : '<?=Yii::app()->params['emptyImage']?>')+'" width="40" height="41"></span> \
                            <span class="rest_title">'+ v.fullname+'</span>\
                            <span>'+ role_list[v.role]+'</span>\
                        </a>\
                    </li>';
            }
        });
        $('#user_search_list').html(ul_html);
    }

    searchUsers();
    $('#user_search input[name=user_search]').on('keyup', function(){
        searchUsers();
    });

    $('#user_search form').on('submit', function(){
        searchUsers();
        return false;
    });
</script>

