<?php

/**
 * This is the model class for table "{{cash_groups}}".
 *
 * The followings are the available columns in table '{{cash_groups}}':
 * @property integer $id
 * @property string $out_id
 * @property integer $restaurant_id
 * @property string $name
 * @property integer $status
 */
class CashGroups extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cash_groups}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('out_id, restaurant_id, name, status', 'required'),
			array('restaurant_id, status', 'numerical', 'integerOnly'=>true),
			array('out_id, name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, out_id, restaurant_id, name, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'out_id' => 'Out',
			'restaurant_id' => 'Restaurant',
			'name' => 'Name',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('out_id',$this->out_id,true);
		$criteria->compare('restaurant_id',$this->restaurant_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CashGroups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    // ---------    CUSTOM OBJECT METHODS   ----------

    // ---------    CUSTOM STATIC OBJECT METHODS   ----------

    public static function getIdByOutId($out_id, $create = false){
        $model = CashGroups::model()->findByAttributes(array('out_id' => $out_id));
        if ($model === null && $create){
            $model = new CashGroups();
            $model->setAttributes(array(
                'out_id' => $out_id,
                'name' => '#'.$out_id,
                'status' => RKeeperQueueParser::STATUS_DRAFT
            ));
            if ($model->save(false)){
                return $model->id;
            }
        }elseif(is_object($model)){
            return $model->id;
        }
        return null;
    }

}
