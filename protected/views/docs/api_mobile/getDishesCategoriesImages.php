<h3>Получение изображений категорий меню</h3>

<p>
    <b><em>Метод</em></b>: GET<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/getDishesCategoriesImages<br/>
</p>

<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>company_id</em></strong>: ID компании(сеть ресторанов), меню которой нужно получить <b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример запроса:</h4>
<pre><code class="prettyprint"><?=CHtml::encode('http://bistroapp.ru/api/getDishesCategoriesImages?company_id=1&version=3 ');?>
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "status": "ok",
  "version": "5",
  "data": [
    {
      "id": "1",
      "image": "iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6\/NlyAAAG2ElEQVRogeWbbWiWVRjH\/2eMMcaQJTpChgwxE1KJMSxkiIWEiZTFEtE+iMg...",
      "extension": "png",
      "size": {
        "width": 60,
        "height": 60
      }
    }
  ]
}');?>
    </code></pre>
<hr/>

<h4>Пример ответа (ERROR):</h4>
<pre><code class="prettyprint"><?=CHtml::encode('{
  "status": "error",
  "error": {
    "code": 1,
    "message": "\u041d\u0435 \u0437\u0430\u0434\u0430\u043d \u043e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440 company_id"
  }
}');?>
    </code></pre>
<hr/>

