<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 22.05.14
 * Time: 9:51
 */
class CompanyLogo extends CWidget {
    public function run(){
            if (Yii::app()->user->company_id_full){
                $company = Company::model()->findByPk(Yii::app()->user->company_id_full);
            }

            if (Yii::app()->user->restaurant_id_full){
                $restaurant = Restaurant::model()->findByPk(Yii::app()->user->restaurant_id_full);
            }
            $this->render('_company_logo', array(
                'company' => $company,
                'restaurant' => $restaurant,
            ));
    }
}