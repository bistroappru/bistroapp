<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function actionDownloadmobileapp(){
        $this->layout = 'new_home';
        $this->render('downloadmobileapp');
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $this->layout = 'new_home';
        //if(Yii::app()->user->checkAccess('sa')){echo "hello, I'm ssa";}

        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                //$this->redirect(Yii::app()->user->returnUrl);
                $this->redirect('/admin');
        }


        $model2=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model2->attributes=$_POST['ContactForm'];
            if($model2->validate())
            {

                $name='=?UTF-8?B?'.base64_encode($model2->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode('Сообщение с сайта').'?=';
                $headers="From: $name <{$model2->email}>\r\n".
                    "Reply-To: {$model2->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'],$subject,$model2->body,$headers);
                Yii::app()->user->setFlash('contact','Ваше сообщение отправлено');
                $this->refresh();
            }
        }

        // display the login form
        $this->render('index',array('model'=>$model,'model2'=>$model2));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function actionLoadImg()
    {
        error_reporting(E_ALL | E_STRICT);
        $options = array(
            'script_url' => '/',
            'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/data/images/temp/',
            'upload_url' => '/data/images/temp/',

        );
        $upload_handler = new UploadHandler($options);
    }

    public function actionCropImg()
    {
        $crop_x = Yii::app()->request->getParam('x1', 0);
        $crop_y = Yii::app()->request->getParam('y1', 0);
        $crop_w = Yii::app()->request->getParam('w', 0);
        $crop_h = Yii::app()->request->getParam('h', 0);
        $crop_img = Yii::app()->request->getParam('url', '');
        $crop_folder = Yii::app()->request->getParam('folder', '');
        $crop_fw = Yii::app()->request->getParam('fw', 0);
        $crop_fh = Yii::app()->request->getParam('fh', 0);

        if (!$crop_w || !$crop_h || !$crop_img || !$crop_folder || !$crop_fw || !$crop_fh) throw new CHttpException(500,'Неверные данные');

        $filename = $_SERVER['DOCUMENT_ROOT'].$crop_img;
        $info = pathinfo($filename);
        $size = getimagesize($filename);
        $func = 'imagejpeg';
        switch(strtolower($info['extension'])){
            case 'png':
                $func = 'imagepng';
                $image = imagecreatefrompng($filename);
                break;
            case 'gif':
                $func = 'imagegif';
                $image = imagecreatefromgif($filename);
                break;
            case 'jpg':
            case 'jpeg':
                $func = 'imagejpeg';
                $image = imagecreatefromjpeg($filename);
                break;
        }

        $aspect = $size[0]/$crop_fw;
        $aspectRatio = $size[0]/$size[1];

        $new_x = intval($crop_x * $aspect);
        $new_y = intval($crop_y * $aspect);
        $new_w = intval($crop_w * $aspect);
        $new_h = intval($crop_h * $aspect);

        $dst_image = imagecreatetruecolor($new_w, $new_h);

        $transparent = imagecolorallocatealpha($dst_image, 0, 0, 0, 127);
        imagefill($dst_image, 0, 0, $transparent);
        imagesavealpha($dst_image, true);


        imagecopy($dst_image, $image, 0, 0, $new_x, $new_y, $new_w, $new_h);
        //imagecopyresized($dst_image_mini, $dst_image, 0, 0, 0, 0, 305, intval(305/$aspectRatio), $new_w, $new_h);

        $folder_img = str_replace('/data/images/temp', $crop_folder, $crop_img);

        switch($func){
            case 'imagepng':
                imagepng($dst_image, $_SERVER['DOCUMENT_ROOT'].$folder_img);
            break;
            case 'imagegif':
                imagegif($dst_image, $_SERVER['DOCUMENT_ROOT'].$folder_img);
                break;
            case 'imagejpeg':
                imagejpeg($dst_image, $_SERVER['DOCUMENT_ROOT'].$folder_img, 100);
                break;
        }
        //imagejpeg($dst_image_mini, $folder.$name_mini, 100);

        imagedestroy($image);
        imagedestroy($dst_image);
        //imagedestroy($dst_image_mini);

        echo $folder_img;
    }

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				//$this->redirect(Yii::app()->user->returnUrl);
                $this->redirect('/admin');
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
        Yii::app()->user->logout();
        unset(Yii::app()->request->cookies['company_id']);
        unset(Yii::app()->request->cookies['restaurant_id']);
		$this->redirect('/');
	}


    /**
     * TEST
     */
    public function actionSendSMS(){
        $number = '79851663398';
        $number = '375292832942';
        $from = 'BistroApp';
        $msg = 'Тестовая SMS от '.date('d.m.Y H:i:s');
        $a = Yii::app()->sms->sms_send($number, $msg, $from, null, false);
        var_dump($a);
    }

    public function actionSendPush(){
        $apns = Yii::app()->apns;
        $msg = 'Тест push '.date('d-m-Y H:i:s');
        $a = $apns->send(str_replace(" ", "", 'a6bc179e b8a4ca46 bb291514 d9a7f347 d0b754a4 56b081e2 dc308338 a0d55d40'), $msg, array(), array('sound' => 'default'));
        var_dump($a);
    }


}