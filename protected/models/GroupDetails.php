<?php

/**
 * This is the model class for table "{{group_details}}".
 *
 * The followings are the available columns in table '{{group_details}}':
 * @property integer $id
 * @property integer $group_id
 * @property integer $sort
 * @property integer $condition_id
 * @property integer $pg_item_id
 */
class GroupDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{group_details}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, condition_id, pg_item_id', 'required'),
			array('group_id, sort, condition_id, pg_item_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, group_id, sort, condition_id, pg_item_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'condition' => array(self::BELONGS_TO, 'Condition', 'condition_id'),
            'groups' => array(self::BELONGS_TO, 'PositionGroup', 'pg_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
			'group_id' => 'ID группы',
			'sort' => 'Сортировка',
			'condition_id' => 'Условие',
            'pg_item_id' => 'Группа'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('condition_id',$this->condition_id);
        $criteria->compare('pg_item_id',$this->pg_item_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GroupDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getGroupPosition($id){
        if (!$id) return array();
        $group = GroupDetails::model()->findAllByAttributes(array(), 'group_id = :group_id', array('group_id' => $id));
        return $group;
    }

    public function getGroupPositionList($id){
        $json = array();
        if ($id){
            $group = GroupDetails::model()->findAll(array('condition' => 'group_id = :group_id', 'order' => 'sort ASC', 'params' => array(':group_id' => $id)));
            foreach ($group as $one){
                $json[] = array(
                    'condition_id' => $one->condition_id,
                    'position_id' => $one->condition->position_id,
                    'groups_id' => $one->pg_item_id,
                    'sort' => $one->sort
                );
            }
        }
        return CJSON::encode($json);
    }
}