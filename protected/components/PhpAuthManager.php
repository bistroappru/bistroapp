<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 24.04.14
 * Time: 16:12
 */

class PhpAuthManager extends CDbAuthManager{
    public function init(){
        //if($this->authFile===null)
            //$this->authFile=Yii::getPathOfAlias('application.config.auth').'.php';

        parent::init();

        if(!Yii::app()->user->isGuest){
            // стираем старые назначения текущего пользователя
            $this->db->createCommand()->delete($this->assignmentTable, 'userid=:userid', array(':userid'=>Yii::app()->user->id));

            // записываем новое назначение
            $this->assign(Yii::app()->user->role, Yii::app()->user->id);
        }
    }
}