<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/api.css"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div class="container" id="page">
    <div id="header">
        <center><div id="logo">API Документация</div></center>
    </div>
    <!-- header -->
	<div id="sidebar" style="border-left: 1px solid #ccc;">
        <ul>
            <?php echo $this->clips['sidebar'];?>
        </ul>
    </div>
    <div id="content">
        <?php echo $content;?>
    </div>
    <div class="clear"></div>
</div>
</body>
</html>