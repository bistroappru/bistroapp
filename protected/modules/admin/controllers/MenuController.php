<?php

class MenuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='admin_menu';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',
                'roles'=>array('inAdmin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($parent_id)
	{
        $ajax = Yii::app()->request->isAjaxRequest;
		$model=new Menu;
        $model->company_id = Yii::app()->user->company_id_full;

        if(!Yii::app()->user->checkAccess('menuCategoryCreate', array('company_id'=>$model->company_id))){
            throw new CHttpException(403, 'Вы не можете создавать меню в этой компании');
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Menu']))
		{
			$model->attributes=$_POST['Menu'];
            $model->setAttribute('description', $_POST['Menu']['description']); // TODO почемуто не подхватывается в предыдущей строке

			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

        if ($ajax){
            $this->renderPartial('create',array(
                'model'=>$model,
            ));
        }else{
            $this->render('create',array(
                'model'=>$model,
            ));
        }
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $ajax = Yii::app()->request->isAjaxRequest || Yii::app()->request->getParam('ajax', 0);
		$model=$this->loadModel($id);

        if(!Yii::app()->user->checkAccess('menuCategoryView', array('company_id'=>$model->company_id))){
            throw new CHttpException(403, 'Вы не можете просматривать меню этой компании');
        }

        $attr_old = $model->getAttributes();
        $attr_new = array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        if(isset($_POST['Menu']))
		{
            if(!Yii::app()->user->checkAccess('menuCategoryUpdate', array('company_id'=>$model->company_id))){
                throw new CHttpException(403, 'Вы не можете редактировать меню этой компании');
            }

			$model->attributes=$_POST['Menu'];
            $model->setAttribute('description', $_POST['Menu']['description']); // TODO почемуто не подхватывается в предыдущей строке

            // - проверка версионирования
            $attr_new = $model->getAttributes();
            $attr_diff = array_diff_assoc($attr_old, $attr_new);

            if(count($attr_diff)){

                // перенес логику в beforeSave

//                if (array_key_exists('image', $attr_diff)){
//                    $version_image = Menu::model()->getLastVersion($model->company_id, 'imageVersion');
//                    $model->imageVersion = ++$version_image;
//                    unset($attr_diff['image']);
//                }
//
//                if (count($attr_diff)){
//                    $version_data = Menu::model()->getLastVersion($model->company_id);
//                    $model->version = ++$version_data;
//                }

            }
            // --------------------------

			if($model->save())
            	$this->redirect(array('update','id'=>$model->id));
		}

        if ($ajax){
            $this->renderPartial('update',array(
                'model'=>$model,
            ));
        }else{
            $this->render('update',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

        if(!Yii::app()->user->checkAccess('menuCategoryDelete', array('company_id'=>$model->company_id))){
            throw new CHttpException(403, 'Вы не можете удалять меню этой компании');
        }

        $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('/admin/menu'));
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        if(!Yii::app()->user->checkAccess('menuCategoryView', array('company_id'=>Yii::app()->user->company_id))){
            throw new CHttpException(403, 'Вы не можете удалять меню этой компании');
        }
        $this->render('index');
	}


    /**
     * resort menu
     */
    public function actionResort(){
        $list = Yii::app()->request->getParam('list');
        if (!is_array($list)) throw new CHttpException(500, 'Неправильный формат входящих данных.');

        // TODO переписать. сделать один запрос за всеми категориями, и в цикле только сохранять.

        foreach($list as $k=>$v){
            $menu = Menu::model()->findByPk($v);
            if (is_object($menu)){
                $menu->sort = $k;
                $menu->save();
            }
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Menu the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Menu::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Menu $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='menu-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * проверка прав доступа owner
     */
    //protected function beforeAction() {}
}
