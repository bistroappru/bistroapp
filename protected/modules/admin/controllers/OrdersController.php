<?php

class OrdersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='admin_orders';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
            'postOnly + deleteitem',
            'ajaxOnly + orderdetail',
            'ajaxOnly + changeitemstatus',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('inAdmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $referer = Yii::app()->request->getParam('referer', '/admin/orders/index');
        $ajax = Yii::app()->request->isAjaxRequest;

        // TODO сделать нормальную проверку прав доступа

        if (!$ajax){
            $this->render('view',array(
                'model'=>$this->loadModel($id),
                'referer'=>$referer,
            ));
        }else{
            $this->renderPartial('view',array(
                'model'=>$this->loadModel($id),
                'referer'=>$referer,
            ));
        }
	}

    public function actionOrderdetail($id)
    {
        $this->renderPartial('orderdetail',array(
            'model'=>$this->loadModel($id),
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $this->loadModel($id)->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    public function actionDeleteitem($id)
    {
        $model = OrderItem::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404, 'Позиция не найдена');
        $id = $model->order_id;
        if ($model->delete()){
            Order::updateTotalPrice($id);
            Order::updateCountItems($id);
        }

        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $condition = array();
        $params = array();
        $data_params = array();
        $params_json = array();

        $orderid = Yii::app()->request->getParam('orderid', 0);
        $page = Yii::app()->request->getParam('page', 1);
        $filters = Yii::app()->request->getParam('filters', array());
        $orderBy =  Yii::app()->request->getParam('order', 'date_create desc');

        $params_json['order'] = $orderBy;
        $params_json['page'] = $page;
        $params_json['orderid'] = $orderid;
        $params_json['filters'] = $filters;

        // установка ресторана
        $condition[] = 'restaurant_id = :restaurant_id';
        $params[':restaurant_id'] = Yii::app()->user->restaurant_id_full;

        foreach($filters as $k => $v) {
            switch($k) {
                case 'like': {
                    if(count($v)) {
                        foreach($v as $key => $filter_val) {
                            if ($filter_val)  $condition[] = '`'.$key . '` LIKE "%' . $filter_val . '%"';
                            $data_params[$key] = $filter_val;
                        }
                    }
                }
                    break;
                case 'compare': {
                    if(count($v)) {
                        foreach($v as $key => $filter_val) {
                            if ($filter_val) $condition[] = $key . ' = ' . $filter_val;
                            $data_params[$key] = $filter_val;
                        }
                    }
                }
                    break;
                case 'range': {
                    if(count($v)) {
                        foreach($v as $key => $filter_val) {
                            if($filter_val[0] != '') {
                                $condition[] = $key . ' > ' . $filter_val[0];
                                $data_params[$key]['start_date'] = $filter_val[0];
                            }

                            if($filter_val[1] != '') {
                                $condition[] = $key . ' < ' . $filter_val[1];
                                $data_params[$key]['end_date'] = $filter_val[1];
                            }

                        }
                    }
                }
                    break;
            }
        }

        $dataProvider = new CActiveDataProvider('Order', array(
            'criteria'=>array(
                'condition' => implode(' AND ', $condition),
                'params' => $params,
                'order' => $orderBy
            ),
            'pagination'=>array(
                'pageSize'=>10,
                'pageVar'=>'page',
            ),
        ));

        if(Yii::app()->request->isAjaxRequest){
            $this->renderPartial('index',array(
                'dataProvider'=>$dataProvider,
                'orderid'=>$orderid,
                'filters'=>$filters,
                'data_params'=>$data_params,
                'params_json' => json_encode($params_json)
            ));
        }else{
            $this->render('index',array(
                'dataProvider'=>$dataProvider,
                'orderid'=>$orderid,
                'filters'=>$filters,
                'data_params'=>$data_params,
                'params_json' => json_encode($params_json)
            ));
        }
	}


    public function actionChangestatus(){
        $status = Yii::app()->request->getParam('status', 0);
        $orderid = Yii::app()->request->getParam('orderid', 0);
        if (!$status || !$orderid) throw new CHttpException('500', 'Неверные данные');

        $status_model = OrderStatus::model()->findByPk($status);
        if ($status_model === null) throw new CHttpException('500', 'Статус не найден');

        $roles = explode(',', $status_model->roles);
        if (!in_array(Yii::app()->user->role, $roles)) throw new CHttpException('403', 'Действие недоступно для вашей учетной записи');

        $model = $this->loadModel($orderid);

        if (Yii::app()->user->role === User::ROLE_GARCON && $model->garcon_id !== Yii::app()->user->id) throw new CHttpException('403', 'Этот заказ относится к другому оффицианту');


        switch($status){
            case 2:
                $model->date_execute = time();
            break;
            case 4:
                if ($model->status_id != Order::STATUS_ADOPTED) throw new CHttpException(403, 'Нельзя выставить оплату в данном статусе заказа');
                $model->sendPush('pay_rk');
                if ($model->isOutOrder()) $model->sendSMS('pay_rk');
            break;
            case 5:
                $model->sendPush('cancel');
            break;
            case 6:
                $model->date_close = time();
            break;
        }

        $model->status_id = $status;
        $model->save();
        $this->renderPartial('_view', array('data'=>$model,'orderid'=>$orderid));
    }

    public function actionGetOrderRow($id){
        // TODO проверка на получение своего заказа
        $model = $this->loadModel($id);
        $this->renderPartial('_view', array('data'=>$model,'orderid'=>$id));
    }

    public function actionCheckNewOrders($id){
        header('Content-type: application/json');

        // TODO проверка на получение своего заказа
        $list = array();
        if ($id){
            $orders = Order::model()->findAll(array('select'=>'id', 'condition'=>'restaurant_id = :restaurant_id AND status_id = 1', 'params'=>array(':restaurant_id' => $id)));
            foreach($orders as $v){
                $list[] = $v->id;
            }
        }
        echo CJSON::encode($list);
        Yii::app()->end();
    }

    public function actionChangeitemstatus(){
        $status = Yii::app()->request->getParam('status', 0);
        $itemid = Yii::app()->request->getParam('itemid', 0);
        if (!$status || !$itemid) throw new CHttpException('500', 'Неверные данные');

        $status_model = OrderStatus::model()->findByPk($status);
        if ($status_model === null) throw new CHttpException('500', 'Статус не найден');

        $roles = explode(',', $status_model->roles);
        if (!in_array(Yii::app()->user->role, $roles)) throw new CHttpException('403', 'Действие недоступно для вашей учетной записи');

        $model = OrderItem::model()->findByPk($itemid);
        if ($model === null) throw new CHttpException('500', 'Позиция не найдена!');

        if ((Yii::app()->user->role == 'barman' && $model->position->food_type != Position::TYPE_BAR) ||
            (Yii::app()->user->role == 'cook' && $model->position->food_type != Position::TYPE_KITCHEN)
        ){
            throw new CHttpException('403', 'Место приготовления не соответствует вашей учетной записи.');
        }

        // TODO проверить тип блюда, бар или кухня

        $model->status_id = $status;
        $model->save();
        $this->renderPartial('_orderitem', array('one'=>$model));
    }

    public function actionChangepayment(){
        // TODO проверить права доступа admin,owner,manager,garcon

        $status = Yii::app()->request->getParam('status', 0);
        $orderid = Yii::app()->request->getParam('orderid', 0);
        if (!$status || !$orderid) throw new CHttpException('500', 'Неверные данные');
        $model = $this->loadModel($orderid);
        $model->payment_id = $status;
        $model->save();
        $this->renderPartial('_view', array('data'=>$model,'orderid'=>$orderid));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Order the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Order::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404, 'Заказ не найден');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Order $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}