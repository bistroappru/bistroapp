﻿$(document).ready(function() {
	function radioChecked(input) {
		input.each(function(){
			if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
			$(this).change(function(){
				input.each(function(){
					if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
				});
			});
		});
	}
	radioChecked($('input[type=radio]'));
	$('input[type=checkbox]').each(function(){
		if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
		$(this).change(function(){
			if($(this).is(':checked')) { $(this).addClass('checked') } else {$(this).removeClass('checked')};
		});
	});
	
	jQuery('.order_table input[type=checkbox]').each(function(){
		if($(this).is(':checked')) { 
			$(this).addClass('checked') ;
			$(this).closest('tr').addClass('current');
		} else {
			$(this).removeClass('checked');
			$(this).closest('tr').removeClass('current');
		};
		
	});
	jQuery('.order_table tr+tr').click(function(){
		if($(this).hasClass('current')) { 
			$(this).removeClass('current') ;
			$(this).find('input[type=checkbox]').attr('checked', false).removeClass('checked');
		} else {
			$(this).addClass('current');
			$(this).find('input[type=checkbox]').attr('checked', true).addClass('checked');
		};
	});
	$('.deploy_hidden').each(function(){
		$(this).height($(this).closest('th').innerHeight());
	});
	$('.order_table th').each(function(){
		if ($(this).hasClass('th_hidden')) {
			var trs = $(this).closest('table').find('tr');
			var ind = $(this).index();
			for (var i=0; i<trs.length; i++) {
				$(trs[i]).find('td').eq(ind).addClass('td_hidden');
			}
		}
		$(this).find('.deploy_hidden').height($(this).closest('th').innerHeight());
	});
	$('.deploy_plus').click(function(){
		$(this).closest('th').addClass('th_hidden');
		$(this).closest('th').find('.deploy_hidden').height($(this).closest('th').innerHeight());
		var trs = $(this).closest('table').find('tr');
		var ind = $(this).closest('th').index();
		for (var i=0; i<trs.length; i++) {
			$(trs[i]).find('td').eq(ind).addClass('td_hidden');
		}
	});
	$('.deploy_minus').click(function(){
		$(this).closest('th').removeClass('th_hidden');
		var trs = $(this).closest('table').find('tr');
		var ind = $(this).closest('th').index();
		for (var i=0; i<trs.length; i++) {
			$(trs[i]).find('td').eq(ind).removeClass('td_hidden');
		}
	});
	
	/*
	jQuery('.order_table input[type=checkbox]').each(function(){
		if($(this).is(':checked')) { 
			$(this).addClass('checked') ;
			$(this).closest('tr').addClass('current');
		} else {
			$(this).removeClass('checked');
			$(this).closest('tr').removeClass('current');
		};
		$(this).change(function(){
			if($(this).is(':checked')) { 
				$(this).addClass('checked') ;
				$(this).closest('tr').addClass('current');
			} else {
				$(this).removeClass('checked');
				$(this).closest('tr').removeClass('current');
			};
		});
	});
	*/
	
	$('.edit-link').click(function(){
		$(this).closest('.product_row').find('input[type=text], textarea').prop('readonly', false);
		return false;
	});
	
	$('.serving_del').click(function(){
		$(this).closest('.product_row').remove();
		return false;
	});
	
	$('.serving_add').click(function(){
		var serv_ind = Math.floor( Math.random( ) * 100);
		$(this).closest('.product_row').before('<div class="product_row"><div class="product_left"><textarea class="product_serving"></textarea><a href="#" class="serving_del"></a></div><div class="product_right"><ul class="prices_list radio_wrap"><li><div class="prices_left"><div class="prices_cell"><p>ЦЕНА:</p></div></div><div class="check_item"><input type="radio" id="serving'+ serv_ind +'_1" checked name="serving'+ serv_ind +'"><label for="serving'+ serv_ind +'_1" class="label_radio"></label><input type="text" value="250 р." class="inp_price"></div></li><li><div class="prices_left"><div class="prices_cell"><p>ЦЕНА:</p><p class="action">Акция</p></div></div><div class="check_item"><input type="radio" id="serving'+ serv_ind +'_2" name="serving'+ serv_ind +'"><label for="serving'+ serv_ind +'_2" class="label_radio"></label><input type="text" value="150 р." class="inp_price"></div></li></ul></div></div>');
		radioChecked($('input[type=radio]'));
		$('.serving_del').click(function(){
			$(this).closest('.product_row').remove();
			return false;
		});
		return false;
	});
	
	$('.hit-btn').click(function(){
		$(this).closest('.product_row').find('.add_hit').slideToggle('fast');
		$(this).closest('.product_row').find('.hit_box').toggleClass('opened');
		return false;
	});
	
	jQuery('.right_box .box_link').click(function(){
		$(this).closest('.right_box').find('.box_addit').slideToggle();
		$(this).toggleClass('opened');
		return false;
	});
	jQuery('.food_detail .detail_arrow').click(function(){
		$(this).closest('.food_detail').find('.food_table_wrap').slideToggle();
		$(this).closest('.food_detail').toggleClass('opened');
		return false;
	});
	$('.photo_list li a').click(function(){
		$(this).parent('li').toggleClass('selected');
		return false;
	});
	
	$('.position_change .position_text').click(function(){
		if ($(this).closest('.position_change').find('.position_list').is(':visible')) {
			$(this).closest('.position_change').find('.position_list').slideUp();
		} else {
			$('.position_list').hide();
			$(this).closest('.position_change').find('.position_list').slideDown();
		}
		return false;
	});
	$('.position_list li').click(function(){
		$(this).toggleClass('selected');
		$(this).closest('.position_box').find('.position_count').html($(this).closest('.position_box').find('.position_list li.selected').length);
		return false;
	});
	
	$('.ord-btn .sort_date').click(function(){
		$(this).closest('.ord-btn').find('.date_tooltip').toggle();
		return false;
	});
	
	jQuery('body').click(function() {
		$('.position_list, .date_tooltip').hide();
	});
	jQuery('.position_box, .date_tooltip').click(function(event){
		event.stopPropagation();
	});
	
	jQuery('.modal_shower .show_arrow').click(function(){
		if ($(this).hasClass('m-up')) {
			$(this).closest('.modal_text').find('.hide_text').slideUp('fast');
			$(this).removeClass('m-up').addClass('m-down');
		} else if ($(this).hasClass('m-down')) {
			$(this).closest('.modal_text').find('.hide_text').slideDown('fast');
			$(this).removeClass('m-down').addClass('m-up');
		}
	});
	
	jQuery('.scroll-pane').jScrollPane({autoReinitialise:true});
	if (jQuery('.food_scroll .scroll-pane').hasClass('jspScrollable')) {
		jQuery('.food_total').css('marginRight', '18px');
	}
	$(window).resize(function(){
		jQuery('.food_scroll .scroll-pane, .food_scroll .scroll-pane .jspContainer, .food_scroll .scroll-pane .jspPane').css('width', '100%');
		jQuery('.food_scroll .scroll-pane .jspContainer').css('height', '397px');
		jQuery('.food_scroll .scroll-pane').jScrollPane({autoReinitialise:true});
		if (jQuery('.food_scroll .scroll-pane').hasClass('jspScrollable')) {
			jQuery('.food_total').css('marginRight', '18px');
		}
	});	
	
	$('input[placeholder], textarea[placeholder]').placeholder();
	
	$('select.order_select').selectBox();
	
	//fixed menu
	var top = $('#nav_block').offset().top - parseFloat($('#nav_block').css('marginTop').replace(/auto/, 0));
	$(window).scroll(function (event) {
		var y = $(this).scrollTop();
		if (y >= top) {
			$('#nav_block').addClass('fixed');
			onResize();
		} else {
			$('#nav_block').removeClass('fixed');
			onResize();
		}
	});
	
	global = {
			window: $(window)
	};
	
	global.window.resize(onResize);
    onResize();
	
	$(".main_menu li a").click(function() {
		$("html, body").animate({
				scrollTop: $($(this).attr("href")).offset().top - onResize()
		}, {
				duration: 500
		});
		return false;
	});
	
	$(".main_menu li.login-link").click(function() {
		$(this).find('span').toggleClass('active');
		$('.login_corner').toggleClass('corner_visible');
		$('.login_form').slideToggle('fast');
	});
	
});

function onResize(){
	return $('.nav_wrap').height();
}
