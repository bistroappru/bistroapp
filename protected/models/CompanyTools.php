<?php

/**
 * This is the model class for table "{{company_tools}}".
 *
 * The followings are the available columns in table '{{company_tools}}':
 * @property integer $company_id
 * @property string $business_lunch_ids
 */
class CompanyTools extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company_tools}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, business_lunch_ids', 'required'),
			array('company_id', 'numerical', 'integerOnly'=>true),
			array('business_lunch_ids', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('company_id, business_lunch_ids', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'company_id' => 'Company',
			'business_lunch_ids' => 'Business Lunch Ids',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyTools the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getBusinessLunchID($id){
        $model = self::model()->findByPk($id);
        if ($model === null)
            return false;
        return $model->business_lunch_ids;
    }
}
