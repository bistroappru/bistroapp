<?php
/* @var $this UserController */
/* @var $model User */
$this->pageTitle = Yii::app()->name . ' - Пользователи';
if ($restaurant_id){
?>
    <div class="column_center2">
        <?php $this->widget('application.components.UserList', array('restaurant_id' => $restaurant_id, 'filter_key' => 'lc_user_list'));?>
    </div>
    <div class="column_right"></div>
<?php
}else{
?>
    <div class="wrapper">
        <div class="opacity_block">
            <div class="warning">
                <h2>ВНИМАНИЕ</h2>
                <p>Выберите компанию и ресторан в списке слева для просмотра пользователей</p>
            </div>
        </div>
    </div>

    <script>
        $(window).load(function(){
            expandFirstUser();
        });
    </script>
<?php
}
?>