<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 18.07.14
 * Time: 13:08
 */

?>

<li <?= $company_id == $data->id ? 'class="active"' : ''?>>
    <a href="<?=CHtml::normalizeUrl('/admin/company/update/id/'.$data->id);?>">
        <span class="company_ico"><img src="<?=$data->logo ? $data->logo : Yii::app()->params['emptyImage']?>" width="40" height="41"></span>
        <span class="company_name"><?=$data->name?></span>
        <span class="company_rest">ресторанов (<?=count($data->restaurants)?>)</span>
    </a>
</li>