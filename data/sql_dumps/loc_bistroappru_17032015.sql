/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50619
Source Host           : localhost:3306
Source Database       : bistroappru

Target Server Type    : MYSQL
Target Server Version : 50619
File Encoding         : 65001

Date: 2015-03-17 11:50:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `app_auth_assignment`;
CREATE TABLE `app_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `app_auth_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_auth_assignment
-- ----------------------------
INSERT INTO `app_auth_assignment` VALUES ('admin', '1', null, 'N;');
INSERT INTO `app_auth_assignment` VALUES ('owner', '2', null, 'N;');

-- ----------------------------
-- Table structure for `app_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `app_auth_item`;
CREATE TABLE `app_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_auth_item
-- ----------------------------
INSERT INTO `app_auth_item` VALUES ('admin', '2', 'СуперАдмин', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('barman', '2', 'Бармен', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyCreate', '0', 'Создание компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyDelete', '0', 'Удаление компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyOwnTools', '1', 'Настройки своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('companyOwnUpdate', '1', 'Обновление своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('companyOwnView', '1', 'Просмотр своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('companyTools', '0', 'Настройки компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyUpdate', '0', 'Обновление компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('companyView', '0', 'Просмотр компании', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('cook', '2', 'Повар', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('garcon', '2', 'Оффициант', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('guest', '2', 'Гость', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('inAdmin', '0', 'Доступ к админке', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('manager', '2', 'Администратор ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryCreate', '0', 'Создание категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryDelete', '0', 'Удаление категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryUpdate', '0', 'Обновление категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuCategoryView', '0', 'Просмотр категории меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishCreate', '0', 'Создание блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishDelete', '0', 'Удаление блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishUpdate', '0', 'Обновление блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuDishView', '0', 'Просмотр блюда меню', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryCreate', '1', 'Создание категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryDelete', '1', 'Удаление категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryUpdate', '1', 'Обновление категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnCategoryView', '1', 'Просмотр категории меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishCreate', '1', 'Создание блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishDelete', '1', 'Удаление блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishUpdate', '1', 'Обновление блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('menuOwnDishView', '1', 'Просмотр блюда меню своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('mobile', '2', 'Девайс', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('mobile_guest', '2', 'Девайс (гость)', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('owner', '2', 'Владелец сети', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyCreate', '1', 'Создание ресторанов в своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyDelete', '1', 'Удаление ресторанов в своей компании', 'return Yii::app()->user->company_id==$params[\"restaurant\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyUpdate', '1', 'Обновление ресторанов своей компании', 'return Yii::app()->user->company_id==$params[\"restaurant\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCompanyView', '1', 'Просмотр ресторанов своей компании', 'return Yii::app()->user->company_id==$params[\"company_id\"];', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantCreate', '0', 'Создание ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantDelete', '0', 'Удаление ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantOwnUpdate', '1', 'Обновление своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"restaurant\"]->id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantUpdate', '0', 'Обновление ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('restaurantView', '0', 'Просмотр ресторана', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userCreate', '0', 'Создание пользователя', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userDelete', '0', 'Удаление пользователя', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyCreate', '1', 'Создание пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyDelete', '1', 'Удаление пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyUpdate', '1', 'Редактирование пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnCompanyView', '1', 'Просмотр пользователя своей компании', 'return Yii::app()->user->company_id==$params[\"user\"]->company_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantCreate', '1', 'Создание пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantDelete', '1', 'Удаление пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantUpdate', '1', 'Редактирование пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userOwnRestaurantView', '1', 'Просмотр пользователя своего ресторана', 'return Yii::app()->user->restaurant_id==$params[\"user\"]->restaurant_id;', 'N;');
INSERT INTO `app_auth_item` VALUES ('userUpdate', '0', 'Обновление пользователя', null, 'N;');
INSERT INTO `app_auth_item` VALUES ('userView', '0', 'Просмотр пользователя', null, 'N;');

-- ----------------------------
-- Table structure for `app_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `app_auth_item_child`;
CREATE TABLE `app_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `app_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `app_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_auth_item_child
-- ----------------------------
INSERT INTO `app_auth_item_child` VALUES ('manager', 'barman');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'companyOwnTools');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'companyOwnUpdate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'companyOwnView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyTools');
INSERT INTO `app_auth_item_child` VALUES ('companyOwnTools', 'companyTools');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('companyOwnUpdate', 'companyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'companyView');
INSERT INTO `app_auth_item_child` VALUES ('companyOwnView', 'companyView');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'cook');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'garcon');
INSERT INTO `app_auth_item_child` VALUES ('barman', 'inAdmin');
INSERT INTO `app_auth_item_child` VALUES ('cook', 'inAdmin');
INSERT INTO `app_auth_item_child` VALUES ('garcon', 'inAdmin');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'manager');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryCreate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryCreate', 'menuCategoryCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryDelete');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryDelete', 'menuCategoryDelete');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryUpdate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryUpdate', 'menuCategoryUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuCategoryView');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnCategoryView', 'menuCategoryView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishCreate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishCreate', 'menuDishCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishDelete');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishDelete', 'menuDishDelete');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishUpdate');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishUpdate', 'menuDishUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'menuDishView');
INSERT INTO `app_auth_item_child` VALUES ('menuOwnDishView', 'menuDishView');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryUpdate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnCategoryView');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishUpdate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'menuOwnDishView');
INSERT INTO `app_auth_item_child` VALUES ('mobile', 'mobile_guest');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'owner');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'restaurantCompanyCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'restaurantCompanyDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'restaurantCompanyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'restaurantCompanyView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantCreate');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyCreate', 'restaurantCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantDelete');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyDelete', 'restaurantDelete');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'restaurantOwnUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyUpdate', 'restaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('restaurantOwnUpdate', 'restaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'restaurantView');
INSERT INTO `app_auth_item_child` VALUES ('restaurantCompanyView', 'restaurantView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userCreate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyCreate', 'userCreate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantCreate', 'userCreate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userDelete');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyDelete', 'userDelete');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantDelete', 'userDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyCreate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyDelete');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyUpdate');
INSERT INTO `app_auth_item_child` VALUES ('owner', 'userOwnCompanyView');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantCreate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantDelete');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantUpdate');
INSERT INTO `app_auth_item_child` VALUES ('manager', 'userOwnRestaurantView');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userUpdate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyUpdate', 'userUpdate');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantUpdate', 'userUpdate');
INSERT INTO `app_auth_item_child` VALUES ('admin', 'userView');
INSERT INTO `app_auth_item_child` VALUES ('userOwnCompanyView', 'userView');
INSERT INTO `app_auth_item_child` VALUES ('userOwnRestaurantView', 'userView');

-- ----------------------------
-- Table structure for `app_cash`
-- ----------------------------
DROP TABLE IF EXISTS `app_cash`;
CREATE TABLE `app_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `cashgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_cash
-- ----------------------------
INSERT INTO `app_cash` VALUES ('1', '15002', '1', 'F_ST', '1', '1');
INSERT INTO `app_cash` VALUES ('2', '15003', '3', 'rasp_rasp_STO1', '3', '1');
INSERT INTO `app_cash` VALUES ('3', '15004', '1', 'rasp_rasp_ST01', '3', '1');
INSERT INTO `app_cash` VALUES ('4', '15006', '4', 'ST02', '3', '2');
INSERT INTO `app_cash` VALUES ('5', '15008', '3', 'ST01', '3', '3');
INSERT INTO `app_cash` VALUES ('6', '15010', '5', 'ST04', '3', '4');
INSERT INTO `app_cash` VALUES ('7', '15012', '6', 'ST05', '3', '5');
INSERT INTO `app_cash` VALUES ('8', '15014', '7', 'ST06', '3', '6');
INSERT INTO `app_cash` VALUES ('9', '15016', '8', 'ST07', '3', '7');
INSERT INTO `app_cash` VALUES ('10', '15018', '9', 'ST08', '3', '8');
INSERT INTO `app_cash` VALUES ('11', '15020', '10', 'ST09', '3', '9');
INSERT INTO `app_cash` VALUES ('12', '15022', '11', 'ST10', '3', '15');
INSERT INTO `app_cash` VALUES ('13', '15025', '2', 'F_ST', '3', '11');
INSERT INTO `app_cash` VALUES ('14', '15028', '1', 'F_ST', '3', '12');
INSERT INTO `app_cash` VALUES ('15', '15030', '1', 'F_ST', '3', '13');
INSERT INTO `app_cash` VALUES ('16', '15033', '1', 'F_ST', '3', '14');
INSERT INTO `app_cash` VALUES ('17', '15036', '1', 'Новая станция', '1', '16');

-- ----------------------------
-- Table structure for `app_cash_groups`
-- ----------------------------
DROP TABLE IF EXISTS `app_cash_groups`;
CREATE TABLE `app_cash_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_cash_groups
-- ----------------------------
INSERT INTO `app_cash_groups` VALUES ('1', '15001', '45', 'F_MIDSERV', '1');
INSERT INTO `app_cash_groups` VALUES ('2', '15005', '45', 'MIDSERV2', '3');
INSERT INTO `app_cash_groups` VALUES ('3', '15007', '45', 'MIDSERV3', '3');
INSERT INTO `app_cash_groups` VALUES ('4', '15009', '45', 'MIDSERV4', '3');
INSERT INTO `app_cash_groups` VALUES ('5', '15011', '45', 'MIDSERV5', '3');
INSERT INTO `app_cash_groups` VALUES ('6', '15013', '45', 'MIDSERV6', '3');
INSERT INTO `app_cash_groups` VALUES ('7', '15015', '45', 'MIDSERV7', '3');
INSERT INTO `app_cash_groups` VALUES ('8', '15017', '45', 'MIDSERV8', '3');
INSERT INTO `app_cash_groups` VALUES ('9', '15019', '45', 'MIDSERV9', '3');
INSERT INTO `app_cash_groups` VALUES ('11', '15024', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('12', '15027', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('13', '15029', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('14', '15031', '43', 'F_MIDSERV', '3');
INSERT INTO `app_cash_groups` VALUES ('15', '15021', '45', 'MIDSERV10', '3');
INSERT INTO `app_cash_groups` VALUES ('16', '15034', '47', 'NEW_CASH_SERVER', '1');

-- ----------------------------
-- Table structure for `app_company`
-- ----------------------------
DROP TABLE IF EXISTS `app_company`;
CREATE TABLE `app_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(500) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `kitchen` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `workplaces` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `imageVersion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_company
-- ----------------------------
INSERT INTO `app_company` VALUES ('1', '', 'Тануки', '/data/images/logos/1401926957_c8b85841d570f75773a922451541676d.jpeg', '0', '1', 'Рестораны Тануки заметно отличаются своим аутентичным интерьером. Частокол из бамбука, стены с иероглифами и статуэтками, светильники из рисовой бумаги, большие столы, сделанные не для экономии места, а для удобства Гостей, впрочем, как и всё в ресторанах Тануки', '', 'http://www.tanuki.ru/', 'hotline@tanuki.ru', 'tanuki', '1,2', '0', '0');
INSERT INTO `app_company` VALUES ('2', '', 'Япоша', '/data/images/logos/yaposha.jpg', '0', '1', '«Япоша» — сеть популярных демократичных японских ресторанов с яркой, жизнеутверждающей концепцией, с оригинальным двойным меню – суши и антисуши. Мы подарим Вам счастливые минуты беззаботного отдыха в кругу семьи и друзей.', '', '', '', 'yapo', '1', '0', '0');
INSERT INTO `app_company` VALUES ('3', '', 'Якитория', '/data/images/logos/yakitoriya.jpg', '0', '1', 'Сеть культовых кафе авторской японской кухни «Якитория» – самый масштабный проект ассоциации ресторанов \"Веста-центр интернешнл\". Городские кафе, удобно расположенные вблизи станций метро в Москве, предлагают лучшую японскую кухню в городе.', '', '', '', 'yaki', '1', '0', '0');
INSERT INTO `app_company` VALUES ('4', '', 'IL PATIO', '/data/images/logos/il_patio.jpg', '0', '1', '', '', '', '', 'patio', '1', '0', '0');
INSERT INTO `app_company` VALUES ('6', '', 'Кофе Хауз', '/data/images/logos/1403777097_b54435daaf447e4d08027640ee117ff2.jpeg', '0', '1', '', '', '', '', 'house', '1', '0', '0');
INSERT INTO `app_company` VALUES ('7', '', 'шоколадница', '/data/images/logos/1403786730_a08d079201683af1763fae7e6f6e6270.jpeg', '1', '1', '', '', '', '', 'choko', '', '0', '0');
INSERT INTO `app_company` VALUES ('8', '', 'тестовая', '/data/images/logos/1404304731_6c88a64d5f598c51d6a8e9e9aa69fc3b.png', '0', '1', '', '', '', '', 'test', '', '0', '0');
INSERT INTO `app_company` VALUES ('9', '', 'Тануки (RKeeper7)', '/data/images/logos/1422972523_60318ae052edef881beb015dfad26228.jpeg', '0', '0', 'Сеть «Тануки» включает 63 ресторанов, расположенных по всей Москве, Украине и регионам России. ', '', 'http://www.tanuki.ru/', 'hotline@tanuki.ru', 'rkeeper', '1,2', '0', '0');

-- ----------------------------
-- Table structure for `app_company_tools`
-- ----------------------------
DROP TABLE IF EXISTS `app_company_tools`;
CREATE TABLE `app_company_tools` (
  `company_id` int(11) NOT NULL,
  `business_lunch_ids` int(11) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_company_tools
-- ----------------------------
INSERT INTO `app_company_tools` VALUES ('1', '13');

-- ----------------------------
-- Table structure for `app_condition`
-- ----------------------------
DROP TABLE IF EXISTS `app_condition`;
CREATE TABLE `app_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `position_id` int(11) NOT NULL,
  `measure` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `action_on` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_condition
-- ----------------------------
INSERT INTO `app_condition` VALUES ('16', '', '9', '1000 мл.', '0', '150', '0');
INSERT INTO `app_condition` VALUES ('25', '', '8', '200 гр.', '0', '250', '1');
INSERT INTO `app_condition` VALUES ('30', '', '17', '8 шт.', '0', '95', '0');
INSERT INTO `app_condition` VALUES ('31', '', '18', '8 шт.', '0', '145', '0');
INSERT INTO `app_condition` VALUES ('32', '', '19', '1 шт.', '0', '355', '0');
INSERT INTO `app_condition` VALUES ('33', '', '20', '1 шт.', '0', '325', '0');
INSERT INTO `app_condition` VALUES ('34', '', '21', '6 шт.', '0', '95', '0');
INSERT INTO `app_condition` VALUES ('35', '', '22', '1 шт.', '0', '235', '0');
INSERT INTO `app_condition` VALUES ('38', '', '25', '1 шт.', '0', '290', '0');
INSERT INTO `app_condition` VALUES ('39', '', '26', '1000 мл.', '0', '110', '0');
INSERT INTO `app_condition` VALUES ('40', '', '27', '330 мл.', '0', '145', '0');
INSERT INTO `app_condition` VALUES ('41', '', '28', '250 мл.', '0', '90', '0');
INSERT INTO `app_condition` VALUES ('42', '', '29', '1000 мл.', '0', '120', '0');
INSERT INTO `app_condition` VALUES ('43', '', '30', '1 шт.', '0', '310', '0');
INSERT INTO `app_condition` VALUES ('44', '', '31', '1 шт.', '0', '245', '0');
INSERT INTO `app_condition` VALUES ('45', '', '32', '1 шт.', '0', '360', '0');
INSERT INTO `app_condition` VALUES ('46', '', '33', '1 шт.', '0', '235', '0');
INSERT INTO `app_condition` VALUES ('47', '', '34', '1 шт.', '0', '290', '0');
INSERT INTO `app_condition` VALUES ('48', '', '35', '1 шт.', '0', '1230', '0');
INSERT INTO `app_condition` VALUES ('49', '', '36', '1 шт.', '0', '2790', '0');
INSERT INTO `app_condition` VALUES ('50', '', '37', '1 шт.', '0', '1110', '0');
INSERT INTO `app_condition` VALUES ('51', '', '38', '1 шт.', '0', '310', '0');
INSERT INTO `app_condition` VALUES ('52', '', '39', '1 шт.', '0', '440', '0');
INSERT INTO `app_condition` VALUES ('53', '', '40', '1 шт.', '0', '35', '0');
INSERT INTO `app_condition` VALUES ('54', '', '41', '1 шт.', '0', '75', '0');
INSERT INTO `app_condition` VALUES ('55', '', '42', '1 шт.', '0', '70', '0');
INSERT INTO `app_condition` VALUES ('56', '', '43', '1 шт.', '0', '90', '0');
INSERT INTO `app_condition` VALUES ('57', '', '44', '1 шт.', '0', '80', '0');
INSERT INTO `app_condition` VALUES ('69', '', '48', '500 мл.', '0', '150', '0');
INSERT INTO `app_condition` VALUES ('70', '', '49', '150 мл.', '0', '50', '0');
INSERT INTO `app_condition` VALUES ('71', '', '49', '250 мл.', '0', '100', '0');
INSERT INTO `app_condition` VALUES ('72', '', '49', '500 мл.', '0', '200', '0');
INSERT INTO `app_condition` VALUES ('74', '', '51', 'Основная', '0', '115', '0');
INSERT INTO `app_condition` VALUES ('75', '', '52', 'Основная', '0', '150', '0');
INSERT INTO `app_condition` VALUES ('76', '', '53', 'Основная', '0', '220', '0');
INSERT INTO `app_condition` VALUES ('77', '', '54', 'Основная', '0', '245', '0');
INSERT INTO `app_condition` VALUES ('78', '', '55', 'Основная', '0', '247', '0');
INSERT INTO `app_condition` VALUES ('79', '', '56', 'Основная', '0', '299', '0');
INSERT INTO `app_condition` VALUES ('80', '', '57', 'Основная', '0', '287', '0');
INSERT INTO `app_condition` VALUES ('81', '', '58', 'Основная', '0', '237', '0');
INSERT INTO `app_condition` VALUES ('82', '', '59', 'Основная', '0', '100', '0');
INSERT INTO `app_condition` VALUES ('83', '', '60', 'комбо', '0', '250', '0');
INSERT INTO `app_condition` VALUES ('88', '', '24', '1 шт.', '0', '260', '0');
INSERT INTO `app_condition` VALUES ('90', '', '63', '1 шт.', '1', '100', '0');
INSERT INTO `app_condition` VALUES ('91', '', '64', 'Общая стоимость', '1', '250', '0');
INSERT INTO `app_condition` VALUES ('92', '', '65', '1 шт.', '1', '20', '0');
INSERT INTO `app_condition` VALUES ('93', '', '66', '1 шт.', '1', '345', '0');
INSERT INTO `app_condition` VALUES ('101', '', '23', '1 шт.', '1410877406', '1', '0');
INSERT INTO `app_condition` VALUES ('102', '', '67', 'Общая стоимость', '1415782139', '123', '0');
INSERT INTO `app_condition` VALUES ('103', '', '142', '1 шт.', '1419233612', '150', '0');
INSERT INTO `app_condition` VALUES ('104', '', '143', '1 шт.', '1419233944', '98', '0');
INSERT INTO `app_condition` VALUES ('105', '', '213', '1 шт.', '1421217907', '55', '0');
INSERT INTO `app_condition` VALUES ('106', '', '212', '1 шт.', '1421218166', '100', '0');
INSERT INTO `app_condition` VALUES ('107', '', '220', '1 шт.', '1422967719', '299', '0');
INSERT INTO `app_condition` VALUES ('108', '', '221', '1 шт.', '1422969146', '285', '0');
INSERT INTO `app_condition` VALUES ('109', '', '222', '1 шт.', '1422969307', '240', '0');
INSERT INTO `app_condition` VALUES ('110', '', '223', '1 шт.', '1422969629', '190', '0');
INSERT INTO `app_condition` VALUES ('111', '', '224', '1 шт.', '1422970052', '520', '0');
INSERT INTO `app_condition` VALUES ('112', '', '214', '1 шт.', '1422970469', '1', '0');
INSERT INTO `app_condition` VALUES ('113', '', '215', '1 шт.', '1422970489', '1', '0');
INSERT INTO `app_condition` VALUES ('114', '', '217', '1 шт.', '1422970504', '1', '0');
INSERT INTO `app_condition` VALUES ('115', '', '216', '1 шт.', '1422970510', '1', '0');
INSERT INTO `app_condition` VALUES ('116', '', '225', '1 шт.', '1422970747', '240', '0');
INSERT INTO `app_condition` VALUES ('117', '', '226', '1 шт.', '1422974556', '440', '0');
INSERT INTO `app_condition` VALUES ('118', '', '227', '1 шт.', '1422975189', '145', '0');
INSERT INTO `app_condition` VALUES ('119', '', '228', '1 шт.', '1422975225', '90', '0');
INSERT INTO `app_condition` VALUES ('120', '', '229', '1 шт.', '1422975235', '150', '0');
INSERT INTO `app_condition` VALUES ('121', '', '230', '1 шт.', '1422975248', '430', '0');
INSERT INTO `app_condition` VALUES ('122', '', '231', '1 шт.', '1422976655', '190', '0');
INSERT INTO `app_condition` VALUES ('123', '', '232', '1 шт.', '1422976935', '75', '0');
INSERT INTO `app_condition` VALUES ('124', '', '233', '1 шт.', '1422977064', '470', '0');
INSERT INTO `app_condition` VALUES ('125', '', '234', '1 шт.', '1422977175', '510', '0');
INSERT INTO `app_condition` VALUES ('126', '', '235', '1 шт.', '1422977307', '510', '0');
INSERT INTO `app_condition` VALUES ('127', '', '236', '1 шт.', '1422978503', '245', '0');
INSERT INTO `app_condition` VALUES ('128', '', '237', '1 шт.', '1422978635', '360', '0');
INSERT INTO `app_condition` VALUES ('129', '', '238', '1 шт.', '1422979066', '930', '0');
INSERT INTO `app_condition` VALUES ('130', '', '239', '1 шт.', '1422979304', '1290', '0');
INSERT INTO `app_condition` VALUES ('131', '', '240', '1 шт.', '1422979434', '920', '0');
INSERT INTO `app_condition` VALUES ('132', '', '241', '1 шт.', '1422979618', '3500', '0');
INSERT INTO `app_condition` VALUES ('133', '', '245', '1 шт.', '1422979968', '290', '0');
INSERT INTO `app_condition` VALUES ('134', '', '242', '1 шт.', '1422980009', '95', '0');
INSERT INTO `app_condition` VALUES ('135', '', '243', '1 шт.', '1422980047', '210', '0');
INSERT INTO `app_condition` VALUES ('136', '', '244', '1 шт.', '1422980083', '125', '0');
INSERT INTO `app_condition` VALUES ('137', '', '246', '1 шт.', '1422980411', '70', '0');
INSERT INTO `app_condition` VALUES ('138', '', '247', '1 шт.', '1422980451', '85', '0');
INSERT INTO `app_condition` VALUES ('139', '', '248', '1 шт.', '1422980480', '80', '0');
INSERT INTO `app_condition` VALUES ('140', '', '249', '1 шт.', '1422980510', '100', '0');
INSERT INTO `app_condition` VALUES ('141', '', '250', '1 шт.', '1422980567', '145', '0');
INSERT INTO `app_condition` VALUES ('142', '', '255', '1 шт.', '1422981798', '290', '0');
INSERT INTO `app_condition` VALUES ('143', '', '251', '1 шт.', '1422981967', '360', '0');
INSERT INTO `app_condition` VALUES ('144', '', '252', '1 шт.', '1422982000', '385', '0');
INSERT INTO `app_condition` VALUES ('145', '', '253', '1 шт.', '1422982076', '310', '0');
INSERT INTO `app_condition` VALUES ('146', '', '254', '1 шт.', '1422982098', '295', '0');
INSERT INTO `app_condition` VALUES ('147', '', '256', '1 шт.', '1422982450', '920', '0');
INSERT INTO `app_condition` VALUES ('148', '', '257', '1 шт.', '1422982486', '230', '0');
INSERT INTO `app_condition` VALUES ('149', '', '258', '1 шт.', '1422982526', '210', '0');
INSERT INTO `app_condition` VALUES ('150', '', '259', '1 шт.', '1422982558', '185', '0');
INSERT INTO `app_condition` VALUES ('151', '', '260', '1 шт.', '1422982589', '295', '0');

-- ----------------------------
-- Table structure for `app_device`
-- ----------------------------
DROP TABLE IF EXISTS `app_device`;
CREATE TABLE `app_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `blocked` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_device
-- ----------------------------
INSERT INTO `app_device` VALUES ('2', '550e8400-e29b-41d4-a716-446655440000', 'iOS', 'iPhone 5', '7.1', '0');

-- ----------------------------
-- Table structure for `app_device_log`
-- ----------------------------
DROP TABLE IF EXISTS `app_device_log`;
CREATE TABLE `app_device_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `command` varchar(255) DEFAULT NULL,
  `body` text,
  `request` text,
  `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `device` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_device_log
-- ----------------------------
INSERT INTO `app_device_log` VALUES ('5', '2', 'getCompanies', '', '{\"version\":-1}', '2015-02-25 12:14:06');
INSERT INTO `app_device_log` VALUES ('6', '2', 'getCompaniesImages', '', '{\"version\":-1}', '2015-02-25 12:28:01');
INSERT INTO `app_device_log` VALUES ('7', '2', 'getRestaurants', '', '{\"company_id\":\"1\",\"version\":-1}', '2015-02-25 12:28:26');
INSERT INTO `app_device_log` VALUES ('8', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:30:31');
INSERT INTO `app_device_log` VALUES ('9', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:30:33');
INSERT INTO `app_device_log` VALUES ('10', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:33:04');
INSERT INTO `app_device_log` VALUES ('11', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:33:21');
INSERT INTO `app_device_log` VALUES ('12', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:33:48');
INSERT INTO `app_device_log` VALUES ('13', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:47:17');
INSERT INTO `app_device_log` VALUES ('14', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:48:16');
INSERT INTO `app_device_log` VALUES ('15', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:48:18');
INSERT INTO `app_device_log` VALUES ('16', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 12:58:26');
INSERT INTO `app_device_log` VALUES ('17', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:06:21');
INSERT INTO `app_device_log` VALUES ('18', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:06:23');
INSERT INTO `app_device_log` VALUES ('19', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"9\"}', '2015-02-25 14:06:31');
INSERT INTO `app_device_log` VALUES ('20', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"9\"}', '2015-02-25 14:06:33');
INSERT INTO `app_device_log` VALUES ('21', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"89\"}', '2015-02-25 14:06:39');
INSERT INTO `app_device_log` VALUES ('22', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"89\"}', '2015-02-25 14:06:41');
INSERT INTO `app_device_log` VALUES ('23', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"189\"}', '2015-02-25 14:06:50');
INSERT INTO `app_device_log` VALUES ('24', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"189\"}', '2015-02-25 14:06:51');
INSERT INTO `app_device_log` VALUES ('25', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"8888\"}', '2015-02-25 14:07:08');
INSERT INTO `app_device_log` VALUES ('26', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":\"8888\"}', '2015-02-25 14:12:00');
INSERT INTO `app_device_log` VALUES ('27', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:12:04');
INSERT INTO `app_device_log` VALUES ('28', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:12:38');
INSERT INTO `app_device_log` VALUES ('29', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:12:39');
INSERT INTO `app_device_log` VALUES ('30', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:12:57');
INSERT INTO `app_device_log` VALUES ('31', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:16:52');
INSERT INTO `app_device_log` VALUES ('32', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:16:53');
INSERT INTO `app_device_log` VALUES ('33', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:17:12');
INSERT INTO `app_device_log` VALUES ('34', '2', 'getDishes', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:21:08');
INSERT INTO `app_device_log` VALUES ('35', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:21:43');
INSERT INTO `app_device_log` VALUES ('36', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:16');
INSERT INTO `app_device_log` VALUES ('37', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:18');
INSERT INTO `app_device_log` VALUES ('38', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:20');
INSERT INTO `app_device_log` VALUES ('39', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:20');
INSERT INTO `app_device_log` VALUES ('40', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:22');
INSERT INTO `app_device_log` VALUES ('41', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:23');
INSERT INTO `app_device_log` VALUES ('42', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:23');
INSERT INTO `app_device_log` VALUES ('43', '2', 'getDishesImages', '', '{\"company_id\":\"9\",\"version\":-1}', '2015-02-25 14:22:23');
INSERT INTO `app_device_log` VALUES ('44', '2', 'getDishesImages', '', '{\"company_id\":\"1\",\"version\":-1}', '2015-02-25 14:22:27');
INSERT INTO `app_device_log` VALUES ('45', '2', 'getCompanies', '', '{\"version\":-1}', '2015-02-25 14:47:27');
INSERT INTO `app_device_log` VALUES ('46', '2', 'getCompanies', '', '{\"version\":-1}', '2015-02-27 15:59:21');
INSERT INTO `app_device_log` VALUES ('47', '2', 'postOrder', '', '', '2015-03-10 14:02:29');
INSERT INTO `app_device_log` VALUES ('48', '2', 'postOrder', '', '', '2015-03-10 14:02:48');
INSERT INTO `app_device_log` VALUES ('49', '2', 'postOrder', '', '', '2015-03-10 14:23:45');
INSERT INTO `app_device_log` VALUES ('50', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":250}],\"separated\":0,\"table\":36}', '', '2015-03-10 15:46:34');
INSERT INTO `app_device_log` VALUES ('51', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":250}],\"separated\":0,\"table\":36}', '', '2015-03-10 15:57:16');
INSERT INTO `app_device_log` VALUES ('52', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":250}],\"separated\":0,\"table\":36}', '', '2015-03-10 15:57:28');
INSERT INTO `app_device_log` VALUES ('53', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":250}],\"separated\":0,\"table\":36}', '', '2015-03-11 10:41:29');
INSERT INTO `app_device_log` VALUES ('54', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table\":36}', '', '2015-03-11 11:27:05');
INSERT INTO `app_device_log` VALUES ('55', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table\":36}', '', '2015-03-11 12:44:46');
INSERT INTO `app_device_log` VALUES ('56', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":36}', '', '2015-03-11 12:45:29');
INSERT INTO `app_device_log` VALUES ('57', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:45:42');
INSERT INTO `app_device_log` VALUES ('58', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:45:56');
INSERT INTO `app_device_log` VALUES ('59', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:46:11');
INSERT INTO `app_device_log` VALUES ('60', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:46:11');
INSERT INTO `app_device_log` VALUES ('61', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:46:12');
INSERT INTO `app_device_log` VALUES ('62', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:46:13');
INSERT INTO `app_device_log` VALUES ('63', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:46:13');
INSERT INTO `app_device_log` VALUES ('64', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 12:46:33');
INSERT INTO `app_device_log` VALUES ('65', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 13:28:16');
INSERT INTO `app_device_log` VALUES ('66', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 13:28:37');
INSERT INTO `app_device_log` VALUES ('67', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 13:28:43');
INSERT INTO `app_device_log` VALUES ('68', '2', 'postOrder', '{\"attention\":1,\"payment_id\":1,\"restaurant_id\":47,\"positions\":[{\"position_id\":224,\"count\":1,\"price\":520},{\"position_id\":225,\"count\":1,\"price\":240}],\"separated\":0,\"table_id\":34}', '', '2015-03-11 13:29:16');

-- ----------------------------
-- Table structure for `app_group_details`
-- ----------------------------
DROP TABLE IF EXISTS `app_group_details`;
CREATE TABLE `app_group_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `condition_id` int(11) NOT NULL,
  `pg_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_group_details
-- ----------------------------
INSERT INTO `app_group_details` VALUES ('26', '67', '0', '25', '2');
INSERT INTO `app_group_details` VALUES ('27', '67', '1', '35', '2');
INSERT INTO `app_group_details` VALUES ('63', '64', '0', '88', '1');
INSERT INTO `app_group_details` VALUES ('64', '64', '1', '38', '1');
INSERT INTO `app_group_details` VALUES ('65', '64', '0', '35', '2');
INSERT INTO `app_group_details` VALUES ('66', '64', '0', '25', '3');
INSERT INTO `app_group_details` VALUES ('67', '64', '1', '101', '3');

-- ----------------------------
-- Table structure for `app_label`
-- ----------------------------
DROP TABLE IF EXISTS `app_label`;
CREATE TABLE `app_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `style_id` varchar(125) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_label
-- ----------------------------
INSERT INTO `app_label` VALUES ('1', 'ХИТ', 'hit');
INSERT INTO `app_label` VALUES ('2', 'НОВИНКА', 'new');

-- ----------------------------
-- Table structure for `app_menu`
-- ----------------------------
DROP TABLE IF EXISTS `app_menu`;
CREATE TABLE `app_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_menu
-- ----------------------------
INSERT INTO `app_menu` VALUES ('1', '', 'Закуски', '1', '0', '0', 'Описание \"Закуски\"\r\nЗдесь могла бы быть  ваша реклама.', '/data/images/menu/1404301480_40b5de0c3abc86700b15cd62a6aa0793.png', '0', '0', '15', '5');
INSERT INTO `app_menu` VALUES ('2', '', 'Напитки', '1', '0', '1', 'Описание \"Напитки\"', '/data/images/menu/74f4f3fba4992673c6568afaa6e8748f.png', '0', '0', '21', '0');
INSERT INTO `app_menu` VALUES ('3', '', 'Роллы', '1', '0', '2', 'Описание \"Роллы\"', '/data/images/menu/ee927ac9dd447449d100a297c55a7030.png', '0', '0', '17', '0');
INSERT INTO `app_menu` VALUES ('4', '', 'Салаты', '1', '0', '5', 'Описание \"Салаты\"', '/data/images/menu/a0bb357344944233001a7aa70df48faf.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('5', '', 'Сашими', '1', '0', '4', 'Описание \"Сашими\"', '/data/images/menu/5acfabbff1963fe83ec0a5b671625f44.png', '0', '1', '0', '0');
INSERT INTO `app_menu` VALUES ('6', '', 'Сеты', '1', '0', '6', 'Описание \"Сети\"', '/data/images/menu/c094dfc66865c2bca0e109286ee7ca1c.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('7', '', 'Суши', '1', '0', '7', 'Описание \"Суши\"', '/data/images/menu/a00e1192e3e7cd2c07a28c9974952063.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('9', '', 'Антисуши', '2', '0', '0', '', '/data/images/menu/1403777336_952582388a081cda41d8fb3ab6a7f448.png', '0', '0', '16', '0');
INSERT INTO `app_menu` VALUES ('10', '', 'Супы', '3', '0', '0', 'Супы японской кухни', '/data/images/menu/1403778236_f89f3b28186a4fc7b2a495315f14535d.png', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('11', '', 'Шоколад', '7', '0', '0', 'вкусно и питательно', '/data/images/menu/1403786888_9502a5ed48137020cfe8ff8f50b52730.jpeg', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('12', '', 'Супы', '7', '0', '0', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('13', '', 'Бизнес-ланчи', '1', '0', '3', 'djkfhkjd', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('14', '', 'Супы', '6', '0', '0', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('15', '', 'закуски', '6', '0', '0', '', '', '0', '0', '0', '0');
INSERT INTO `app_menu` VALUES ('18', '', 'первая', '8', '0', '0', '', '', '0', '0', '1', '1');
INSERT INTO `app_menu` VALUES ('19', '', 'вторые блюда', '3', '0', '1', '', '', '0', '0', '1', '1');
INSERT INTO `app_menu` VALUES ('44', '1000018', 'Напитки', '9', '65', '45', '', '', '0', '1', '1941', '0');
INSERT INTO `app_menu` VALUES ('45', '1000063', 'Групповой выбор', '9', '0', '47', '', '', '0', '1', '1943', '0');
INSERT INTO `app_menu` VALUES ('46', '1000073', 'Бизнес ланч', '9', '65', '48', '', '', '0', '1', '1944', '0');
INSERT INTO `app_menu` VALUES ('47', '1000078', 'Салаты', '9', '46', '49', '', '', '0', '1', '1945', '0');
INSERT INTO `app_menu` VALUES ('48', '1000082', 'Супы', '9', '46', '50', '', '', '0', '1', '1946', '0');
INSERT INTO `app_menu` VALUES ('49', '1000086', 'Напитки', '9', '46', '51', '', '', '0', '1', '1947', '0');
INSERT INTO `app_menu` VALUES ('50', '1000104', 'Десерты2', '9', '65', '52', '', '', '0', '1', '1948', '0');
INSERT INTO `app_menu` VALUES ('51', '1000112', 'Салаты', '9', '65', '53', '', '', '0', '1', '1949', '0');
INSERT INTO `app_menu` VALUES ('52', '1000191', 'Вторые блюда', '9', '46', '54', '', '', '0', '1', '1950', '0');
INSERT INTO `app_menu` VALUES ('53', '1000207', 'Вторые блюда', '9', '65', '55', '', '', '0', '1', '1951', '0');
INSERT INTO `app_menu` VALUES ('54', '1000221', 'Супы', '9', '65', '56', '', '', '0', '1', '1952', '0');
INSERT INTO `app_menu` VALUES ('55', '1000227', 'Горячие закуски', '9', '65', '57', '', '', '0', '1', '1953', '0');
INSERT INTO `app_menu` VALUES ('56', '1000233', 'Коньяк', '9', '66', '58', '', '', '0', '1', '1954', '0');
INSERT INTO `app_menu` VALUES ('57', '1000474', 'фисв5', '9', '73', '72', '', '', '0', '1', '1968', '0');
INSERT INTO `app_menu` VALUES ('58', '1000239', 'Пиво бутылочное', '9', '65', '59', '', '', '0', '1', '1955', '0');
INSERT INTO `app_menu` VALUES ('59', '1000346', 'Новое комбо', '9', '65', '60', '', '', '0', '1', '1956', '0');
INSERT INTO `app_menu` VALUES ('60', '1000468', 'тест 300500', '9', '69', '67', '', '', '0', '1', '1963', '0');
INSERT INTO `app_menu` VALUES ('61', '1000490', 'комбо', '9', '0', '74', '', '', '0', '1', '1970', '0');
INSERT INTO `app_menu` VALUES ('62', '1000504', 'тестовая группа', '9', '54', '75', '', '', '0', '1', '1971', '0');
INSERT INTO `app_menu` VALUES ('63', '1000062', 'Групповой выбор', '9', '44', '46', '', '', '0', '1', '1942', '0');
INSERT INTO `app_menu` VALUES ('64', '1000459', 'НЕ БУДЕТ ТАКОГО 2', '9', '65', '61', '', '', '0', '1', '1957', '0');
INSERT INTO `app_menu` VALUES ('65', '1000463', 'Корень', '9', '0', '62', '', '', '0', '1', '1958', '0');
INSERT INTO `app_menu` VALUES ('66', '1000464', 'Еще 1 корень', '9', '65', '63', '', '', '0', '1', '1959', '0');
INSERT INTO `app_menu` VALUES ('67', '1000465', 'Не будет такого 3', '9', '64', '64', '', '', '0', '1', '1960', '0');
INSERT INTO `app_menu` VALUES ('68', '1000466', 'тест 100500', '9', '65', '65', '', '', '0', '1', '1961', '0');
INSERT INTO `app_menu` VALUES ('69', '1000467', 'тест 200500', '9', '68', '66', '', '', '0', '1', '1962', '0');
INSERT INTO `app_menu` VALUES ('70', '1000470', 'фисв', '9', '68', '68', '', '', '0', '1', '1964', '0');
INSERT INTO `app_menu` VALUES ('71', '1000471', 'фисв2', '9', '70', '69', '', '', '0', '1', '1965', '0');
INSERT INTO `app_menu` VALUES ('72', '1000472', 'фисв3', '9', '71', '70', '', '', '0', '1', '1966', '0');
INSERT INTO `app_menu` VALUES ('73', '1000473', 'фисв4', '9', '65', '71', '', '', '0', '1', '1967', '0');
INSERT INTO `app_menu` VALUES ('74', '1000489', 'тест', '9', '0', '73', '', '', '0', '1', '1969', '0');
INSERT INTO `app_menu` VALUES ('75', '1000505', 'хаха папка', '9', '73', '76', '', '', '0', '1', '1972', '0');
INSERT INTO `app_menu` VALUES ('76', '1000549', 'Напитки', '9', '0', '77', '', '', '0', '1', '1973', '0');
INSERT INTO `app_menu` VALUES ('77', '1000550', 'Безалкогольные', '9', '76', '78', '', '', '0', '1', '1974', '0');
INSERT INTO `app_menu` VALUES ('78', '1000551', 'Алкогольные', '9', '76', '79', '', '', '0', '1', '1975', '0');
INSERT INTO `app_menu` VALUES ('79', '', 'Кофе', '9', '0', '0', 'Лучшее кофе', '/data/images/menu/1422962043_44c5c7cb9d709555d90425bfa5274021.png', '0', '1', '29', '1');
INSERT INTO `app_menu` VALUES ('80', '1000563', 'Напитки (безалкгольные)', '9', '0', '0', 'Газированные напитки, минеральная вода, соки', '/data/images/menu/1422970296_fc3a6645fa4ff1fc014cdd9558f403f3.png', '1', '0', '1986', '4');
INSERT INTO `app_menu` VALUES ('81', '1000568', 'Блины, сырники, выпечка и каши', '9', '0', '81', '', '', '0', '1', '1977', '0');
INSERT INTO `app_menu` VALUES ('82', '1000571', 'Лапша', '9', '0', '82', '', '', '0', '1', '1978', '0');
INSERT INTO `app_menu` VALUES ('83', '1000573', 'Салаты', '9', '0', '1', '', '/data/images/menu/1422969408_f61f277547fb9049d7dee24f7f66e584.png', '0', '0', '1979', '2');
INSERT INTO `app_menu` VALUES ('84', '1000577', 'Напитки (алкогольные)', '9', '0', '2', 'Пиво, вино, игристые вина, крепкий алкоголь', '/data/images/menu/1422969943_a6d05f2857cf8a8e49876a93d2bdc0da.png', '0', '0', '1980', '3');
INSERT INTO `app_menu` VALUES ('85', '1000579', 'Супы', '9', '0', '3', '', '/data/images/menu/1422970938_b477fa31f2e89dd391ef98dacac47803.png', '0', '0', '1981', '5');
INSERT INTO `app_menu` VALUES ('86', '1000586', 'Горячее', '9', '0', '4', 'Из мяса, из рыбы, из птицы, из лапши, из риса', '/data/images/menu/1422976743_4b4ebba90d183bfeefa21a3c9e4f638e.png', '0', '0', '1982', '6');
INSERT INTO `app_menu` VALUES ('87', '1000592', 'Десерты', '9', '0', '5', '', '/data/images/menu/1422978373_51c04de95c303f63e7be4d84a8c83181.png', '0', '0', '1983', '9');
INSERT INTO `app_menu` VALUES ('88', '1000593', 'Суши', '9', '0', '6', 'Нигири, гункан', '/data/images/menu/1422978280_8f4364d399467195595ae660d91ba6ad.png', '0', '0', '1984', '7');
INSERT INTO `app_menu` VALUES ('89', '1000594', 'Роллы', '9', '0', '7', 'Традиционные, теплые, спринг', '/data/images/menu/1422978329_d4346c2422c7c920872298c254f10537.png', '0', '0', '1985', '8');

-- ----------------------------
-- Table structure for `app_order`
-- ----------------------------
DROP TABLE IF EXISTS `app_order`;
CREATE TABLE `app_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_visit` varchar(255) NOT NULL,
  `device_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `date_create` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `attention` tinyint(1) NOT NULL DEFAULT '0',
  `separated` tinyint(1) NOT NULL DEFAULT '0',
  `separated_info` text NOT NULL,
  `date_execute` int(11) NOT NULL,
  `date_close` int(11) NOT NULL,
  `comment` text NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `garcon_id` int(11) NOT NULL,
  `count_items` int(11) NOT NULL,
  `total_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order
-- ----------------------------
INSERT INTO `app_order` VALUES ('110', '256', '655361045', '1', '34', '2', '1424194500', '2', '0', '0', '', '0', '0', '', '47', '36', '2', '760');
INSERT INTO `app_order` VALUES ('179', '', '', '2', '34', '0', '1426069716', '1', '1', '0', '', '0', '0', '', '47', '36', '2', '760');
INSERT INTO `app_order` VALUES ('180', '', '', '2', '34', '0', '1426069723', '1', '1', '0', '', '0', '0', '', '47', '36', '2', '760');
INSERT INTO `app_order` VALUES ('181', '', '', '2', '34', '0', '1426069756', '1', '1', '0', '', '0', '0', '', '47', '36', '2', '760');

-- ----------------------------
-- Table structure for `app_order_item`
-- ----------------------------
DROP TABLE IF EXISTS `app_order_item`;
CREATE TABLE `app_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order_item
-- ----------------------------
INSERT INTO `app_order_item` VALUES ('160', '110', '225', '9', '240', '1');
INSERT INTO `app_order_item` VALUES ('161', '110', '224', '9', '520', '1');
INSERT INTO `app_order_item` VALUES ('198', '179', '224', '1', '520', '1');
INSERT INTO `app_order_item` VALUES ('199', '179', '225', '1', '240', '1');
INSERT INTO `app_order_item` VALUES ('200', '180', '224', '1', '520', '1');
INSERT INTO `app_order_item` VALUES ('201', '180', '225', '1', '240', '1');
INSERT INTO `app_order_item` VALUES ('202', '181', '224', '1', '520', '1');
INSERT INTO `app_order_item` VALUES ('203', '181', '225', '1', '240', '1');

-- ----------------------------
-- Table structure for `app_order_payment`
-- ----------------------------
DROP TABLE IF EXISTS `app_order_payment`;
CREATE TABLE `app_order_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order_payment
-- ----------------------------
INSERT INTO `app_order_payment` VALUES ('1', 'Наличными');
INSERT INTO `app_order_payment` VALUES ('2', 'Картой');

-- ----------------------------
-- Table structure for `app_order_status`
-- ----------------------------
DROP TABLE IF EXISTS `app_order_status`;
CREATE TABLE `app_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'order',
  `prev_id` tinyint(1) NOT NULL,
  `next_id` tinyint(1) NOT NULL,
  `roles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order_status
-- ----------------------------
INSERT INTO `app_order_status` VALUES ('1', 'Новый', 'order', '0', '0', 'admin,owner,manager');
INSERT INTO `app_order_status` VALUES ('2', 'Принят', 'order', '0', '0', 'admin,owner,manager');
INSERT INTO `app_order_status` VALUES ('3', 'Оплачен', 'order', '0', '0', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('6', 'Закрыт', 'order', '0', '0', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('8', 'Новый', 'item', '0', '9', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('9', 'Принято', 'item', '8', '10', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('10', 'Готовится', 'item', '9', '11', 'admin,owner,manager,barman,cook');
INSERT INTO `app_order_status` VALUES ('11', 'Готово', 'item', '10', '12', 'admin,owner,manager,barman,cook');
INSERT INTO `app_order_status` VALUES ('12', 'Доставлено', 'item', '11', '0', 'admin,owner,manager,garcon');
INSERT INTO `app_order_status` VALUES ('13', 'Отменен', 'order', '0', '0', 'admin,owner,manager');
INSERT INTO `app_order_status` VALUES ('14', 'В обработке', 'order', '0', '0', '');

-- ----------------------------
-- Table structure for `app_position`
-- ----------------------------
DROP TABLE IF EXISTS `app_position`;
CREATE TABLE `app_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `is_group` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `depressed` tinyint(1) NOT NULL DEFAULT '0',
  `label_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `calories` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `in_stock` varchar(500) NOT NULL,
  `food_type` tinyint(1) NOT NULL DEFAULT '1',
  `workplace` tinyint(1) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `imageVersion` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  `price_mode` int(11) NOT NULL,
  `measure` varchar(255) NOT NULL,
  `portion_weight` float NOT NULL,
  `count_accuracy` int(11) NOT NULL,
  `one_change_weight` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_position
-- ----------------------------
INSERT INTO `app_position` VALUES ('8', '', '0', 'Калифорния рору', 'Мясо краба, авокадо, руккола', '1', '1', '0', '2', '/data/images/positions/1401927643_297cc5cc806b05ef6c055377170e81f8.jpeg', '0', '', '0', '3,4', '1', '1', '77', '3', '250', '0', '200 гр.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('9', '', '0', 'Сок апельсиновый', 'Пакетированный апельсиновый сок\r\n\r\n21313234', '2', '1', '0', '0', '/data/images/positions/e5c0c07963cc3d3d0b75bb704091eda2.jpeg', '0', '', '0', '', '2', '1', '0', '0', '150', '0', '1000 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('17', '', '0', 'Абокадо ролл', 'Классический ролл с авокадо', '3', '1', '0', '0', '/data/images/positions/f0837c2e091cd50ce59b12b03a585bcd.jpeg', '0', '', '0', '', '1', '1', '0', '0', '95', '0', '8 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('18', '', '0', 'Сякэ ролл', 'Классический ролл с лососем\r\n\r\nСостав:(рис, водоросли)', '3', '0', '0', '0', '/data/images/positions/1b63e7273a45181773e764dd46468ae7.jpeg', '0', '', '0', '3,4,6', '1', '1', '124', '0', '145', '0', '8 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('19', '', '0', 'Калифорния', 'Ролл с мясом краба, авокадо, огурцом, тобико и майонезом', '3', '2', '0', '0', '/data/images/positions/89a1127cd2d987cc4918949480db00dc.jpeg', '0', '', '0', '', '1', '1', '0', '0', '355', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('20', '', '0', 'Филадельфия', 'Ролл с лососем, мягким сыром, зеленым луком, кунжутом, огурцом и авокадо', '3', '3', '0', '0', '/data/images/positions/d3b5ac1126fd045fb69c591d789cd343.jpeg', '0', '', '0', '', '1', '1', '0', '0', '325', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('21', '', '0', 'Каппа ролл', 'Классический ролл с огурцом', '3', '4', '0', '0', '/data/images/positions/83d648b0a2638c769651c334e5c5fad9.jpeg', '0', '', '0', '', '1', '1', '0', '0', '95', '0', '6 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('22', '', '0', 'Гюнику татаки', 'Маринованная говядина с перцем и чесноком, дайконом, салатом, лимоном и луком, украшается петрушкой \r\n\r\n+ соус Каниши', '1', '2', '0', '2', '/data/images/positions/709d251f56858dff4da693b1a46337a2.jpeg', '0', '', '0', '', '1', '1', '0', '0', '235', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('23', '', '0', 'Кимучи', 'Острый салат из капусты, моркови, яблока, лука-порей и дайкона', '1', '0', '0', '1', '/data/images/positions/1404307274_2fb3eec1550ba464fbd732c607568722.jpeg', '0', '', '0', '', '1', '1', '136', '4', '1', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('24', '', '0', 'Сякэ усугири', 'Ломтики лосося с перцем чили и соусом \"Юзу\"', '1', '3', '0', '0', '/data/images/positions/cb43416d18773ec2bc624ae03e839a0c.jpeg', '0', '', '0', '', '1', '1', '98', '0', '260', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('25', '', '0', 'Торинику соба', 'Гречневая лапша с курицей, морковью, кунжутом и соусом \"Юзу-понзу\"', '1', '4', '0', '0', '/data/images/positions/dacc7b13857bc54a87ce851b3734ede7.jpeg', '0', '', '0', '', '1', '1', '0', '0', '290', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('26', '', '0', 'Спрайт', 'Газированный напиток Спрайт', '2', '0', '0', '0', '/data/images/positions/bfd78bb7f13c5cae729e72ea1ab008f9.jpeg', '0', '', '0', '', '2', '1', '120', '0', '110', '0', '1000 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('27', '', '0', ' Перье', 'Минеральная вода Перье', '2', '2', '0', '0', '/data/images/positions/2bc6f91d3cef70ab4805a42e43829ebc.jpeg', '0', '', '0', '', '2', '1', '104', '0', '145', '0', '330 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('28', '', '0', 'Кока-кола', 'Газированный напиток Кока-кола', '2', '3', '0', '0', '/data/images/positions/31ab3caf52db4a6f0cae3b9cebbb7407.jpeg', '0', '', '0', '', '2', '1', '105', '0', '90', '0', '250 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('29', '', '0', 'Нести Лимон', 'Холодный чай Нести Лимон', '2', '4', '0', '0', '/data/images/positions/2461b677fe0d18d2f665937a687e23b2.jpeg', '0', '', '0', '', '2', '1', '106', '0', '120', '0', '1000 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('30', '', '0', ' Рифу сарада', 'Кальмар, мидии, креветки с листьями салата корн, базиликом, кинзой и острым соусом', '4', '0', '0', '2', '/data/images/positions/c7d2f2cc2c8558421a038ca84e7e4e92.jpeg', '0', '', '0', '', '1', '1', '7', '0', '310', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('31', '', '0', '«Цезарь-сан» с курицей', 'Салат с курицей, салатом айсберг, чесночными гренками, отварным яйцом, черри и сыром грана падано', '4', '0', '0', '0', '/data/images/positions/db6fda642133122a0a9390263e465106.jpeg', '0', '', '0', '', '1', '1', '0', '0', '245', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('32', '', '0', 'Дайкани сарада', 'Салат с мясом краба, апельсином и тобико', '4', '0', '0', '0', '/data/images/positions/3033a1c3b159c0d0e7a3369353187302.jpeg', '0', '', '0', '', '1', '1', '0', '0', '360', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('33', '', '0', 'Осэй сарада', 'Салат из пикантной курицы с салатом айсберг, сельдереем, авокадо, огурцами и сладким перцем', '4', '0', '0', '0', '/data/images/positions/7f2c9de3808410e00f15c0040849c4f1.jpeg', '0', '', '0', '', '1', '1', '0', '0', '235', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('34', '', '0', 'Гюнику сарада', 'Говядина с томатами, огурцом и зелёным луком', '4', '0', '0', '0', '/data/images/positions/dc407fad2b77d7b9ca81fcdca7b576d8.jpeg', '0', '', '0', '', '1', '1', '0', '0', '290', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('35', '', '0', 'Тануки сет', 'Сяке темпура - 1\r\nФиладельфия -1\r\nЭби сирогома -1\r\nСяке кадо - 1\r\nХотате караи - 1\r\nПорция васаби XL (30г) - 1\r\nПорция имбиря XL (60г) - 1', '6', '0', '0', '0', '/data/images/positions/75322a8622bee9eaba6e554ded571ec7.jpeg', '0', '', '0', '', '1', '1', '0', '0', '1230', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('36', '', '0', 'Харакири сет', 'Дракон ролл - 1\r\nОвара ролл - 1\r\nЙонака ролл - 1\r\nОкинава ролл - 1\r\nКалифорния - 2\r\nФиладельфия - 1\r\nХигата ролл - 1\r\nМексиканский ролл - 1\r\nХияши унаги - 1\r\nУнаги урамаки - 1\r\nПорция васаби XL (30 г) - 2\r\nПорция имбиря XL (60 г) - 2\r\n', '6', '0', '0', '0', '/data/images/positions/04279f8326e750e85a75a50571878969.jpeg', '0', '300', '0', '', '1', '1', '0', '0', '2790', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('37', '', '0', 'Суши сет', 'Суши с угрем - 5\r\nСуши с лососем - 5\r\nСуши с тунцом - 5\r\nСуши с лакедрой - 2\r\nПорция васаби (10г) - 2\r\nПорция имбиря (20г) - 2\r\n', '6', '0', '0', '0', '/data/images/positions/251fdbc662899a16ed50ca94cc548864.jpeg', '0', '', '0', '', '1', '1', '0', '0', '1110', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('38', '', '0', 'Якинику мориавасэ', 'Ассорти мясное: шашлычки из куриного фарша, говядины, свинины и кукурузы', '6', '0', '0', '0', '/data/images/positions/7981d6b7a38d03d12b639fc0d9fa7c1e.jpeg', '0', '', '0', '', '1', '1', '0', '0', '310', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('39', '', '0', 'Сифудо мориавасэ', 'Ассорти из морепродуктов: шашлычки из креветок, морского гребешка, лосося и осьминожек', '6', '0', '0', '0', '/data/images/positions/a04ea60470ca58ddfc36804ab98ce963.jpeg', '0', '', '0', '', '1', '1', '0', '0', '440', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('40', '', '0', 'Сякэ', 'Суши нигири с лососем', '7', '1', '0', '0', '/data/images/positions/76a954a2f113df9109946ce5c9d10250.jpeg', '0', '', '0', '', '1', '1', '0', '0', '35', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('41', '', '0', 'Хотатэ', 'Суши нигири с морским гребешком', '7', '2', '0', '0', '/data/images/positions/cdf8a124550b9cf2c472c5c5dcbd6b2d.jpeg', '0', '', '0', '', '1', '1', '0', '0', '75', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('42', '', '0', 'Эби', 'Суши нигири с вареной сладкой креветкой, маринованной в имбирном соусе', '7', '0', '0', '0', '/data/images/positions/b4e6e929f2c157942f4c90bede5e707b.jpeg', '0', '', '0', '', '1', '1', '0', '0', '70', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('43', '', '0', 'Икура', 'Суши гункан с икрой лосося', '7', '3', '0', '0', '/data/images/positions/d4b9f8240ae9d09674b50e5276aeece5.jpeg', '0', '', '0', '', '1', '1', '0', '0', '90', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('44', '', '0', 'Спайси сякэ', 'Острый суши гункан с лососем', '7', '4', '0', '0', '/data/images/positions/4780ead67ef309276efd0327436c4cef.jpeg', '0', '', '0', '', '1', '1', '0', '0', '80', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('48', '', '0', 'Напиток для ланча 1', '', '2', '5', '1', '0', '', '0', '', '0', '4', '2', '1', '107', '0', '150', '0', '500 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('49', '', '0', 'Напиток для ланча 2', '', '2', '6', '1', '0', '', '0', '', '0', '3,4', '2', '1', '108', '0', '50', '0', '150 мл.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('51', '', '0', 'Вареники с картошкой', 'Домашние вареники ручной лепки с картофелем. Подаются со сметаной.', '9', '1', '0', '0', '/data/images/positions/1403777530_9f2df9e3815e570d992c3d3f9066ecd3.jpeg', '0', '', '0', '7', '1', '1', '1', '0', '115', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('52', '', '0', 'Сибирские пельмени', 'Домашние пельмени, ручной лепки, с нежным мясом. Подаются со сметаной.', '9', '0', '0', '0', '/data/images/positions/1403777673_7f785013ba9c808fd9d1569cea05a442.jpeg', '0', '', '0', '7', '1', '1', '0', '0', '150', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('53', '', '0', 'Жареная картошечка с грибами', 'Вкус знакомый с детства, настоящая картошка с грибами по домашнему.', '9', '2', '0', '0', '/data/images/positions/1403777736_14e2e8157345fb67f3c3abbca2563b00.jpeg', '0', '', '0', '7', '1', '1', '0', '0', '220', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('54', '', '0', 'Домашние котлеты с курицей', 'Домашние котлеты подаются с картофелем и соусом.', '9', '3', '0', '0', '/data/images/positions/1403777806_fbe1fa3d940d51939a8037f9a9ee4641.jpeg', '0', '', '0', '7', '1', '1', '0', '0', '245', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('55', '', '0', 'Ао чиз', 'крем-суп из голубого сыра, с креветками', '10', '0', '0', '0', '/data/images/positions/1403778356_4eb753196945f76f362eb00e77b04036.jpeg', '0', '', '0', '8', '1', '1', '0', '0', '247', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('56', '', '0', 'Янагава набэ', 'набэ из копченого угря', '10', '0', '0', '0', '/data/images/positions/1403778489_21cb40c0718c77d529d29825a946a7fb.jpeg', '0', '', '0', '8', '1', '1', '0', '0', '299', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('57', '', '0', 'Том Ям набэ', 'традиционный кокосовый суп с креветками и грибами. Сервируется рисом \"Гохан\"', '10', '0', '0', '0', '/data/images/positions/1403778545_dfa04c8886ee9729a5378a8063be92d8.jpeg', '0', '', '0', '', '1', '1', '0', '0', '287', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('58', '', '0', 'Нику рамэн', 'суп-лапша со свининой и яйцом', '10', '0', '0', '0', '/data/images/positions/1403778650_c0fdcdd3a8f8f4ff71504a7e34659070.jpeg', '0', '', '0', '8', '1', '1', '0', '0', '237', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('59', '', '0', 'Борщ', '', '12', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0', '100', '0', 'Основная', '0', '0', '0');
INSERT INTO `app_position` VALUES ('60', '', '1', 'Бизнес ланч 1', '', '12', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0', '250', '0', 'комбо', '0', '0', '0');
INSERT INTO `app_position` VALUES ('63', '', '0', 'Борщ1', '', '14', '0', '0', '0', '', '0', '', '0', '', '1', '1', '1', '0', '100', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('64', '', '1', 'Лайт', '', '13', '0', '0', '0', '', '0', '', '0', '', '1', '1', '134', '0', '250', '0', 'Общая стоимость', '0', '0', '0');
INSERT INTO `app_position` VALUES ('65', '', '0', 'рпг', '', '13', '0', '0', '0', '', '0', '', '0', '', '1', '1', '0', '0', '20', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('66', '', '0', '345345', '', '18', '0', '0', '0', '', '0', '', '0', '', '1', '1', '1', '1', '345', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('67', '', '1', '123', '', '13', '0', '0', '0', '', '0', '', '0', '', '1', '0', '123', '5', '123', '0', 'Общая стоимость', '0', '0', '0');
INSERT INTO `app_position` VALUES ('142', '1000019', '0', 'Кола', '', '44', '0', '0', '0', '', '0', '', '1', '43,44,45,46', '1', '1', '4561', '0', '150', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('143', '1000020', '0', 'Фанта', '', '44', '1', '0', '0', '', '0', '', '1', '43,44,45,46', '1', '1', '4562', '0', '98', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('144', '1000064', '0', 'Бизнес-ланч', '', '45', '2', '0', '0', '', '0', '', '1', '', '1', '0', '4563', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('145', '1000074', '0', 'Ланч 3 позиции', '', '46', '3', '0', '0', '', '0', '', '1', '', '1', '0', '4564', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('146', '1000079', '0', 'Овощной', '', '47', '4', '0', '0', '', '0', '', '1', '', '1', '0', '4565', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('147', '1000080', '0', 'Мясной', '', '47', '5', '0', '0', '', '0', '', '1', '', '1', '0', '4566', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('148', '1000081', '0', 'Рыбный', '', '47', '6', '0', '0', '', '0', '', '1', '', '1', '0', '4567', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('149', '1000083', '0', 'Борщ', '', '48', '7', '0', '0', '', '0', '', '1', '', '1', '0', '4568', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('150', '1000084', '0', 'Уха', '', '48', '8', '0', '0', '', '0', '', '1', '', '1', '0', '4569', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('151', '1000085', '0', 'Харчо', '', '48', '9', '0', '0', '', '0', '', '1', '', '1', '0', '4570', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('152', '1000087', '0', 'Молочный коктель', '', '49', '10', '0', '0', '', '0', '', '1', '', '1', '0', '4571', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('153', '1000088', '0', 'Морс', '', '49', '11', '0', '0', '', '0', '', '1', '', '1', '0', '4572', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('154', '1000089', '0', 'Чай', '', '49', '12', '0', '0', '', '0', '', '1', '', '1', '0', '4573', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('155', '1000105', '0', 'Мороженое', '', '50', '13', '0', '0', '', '0', '', '1', '', '1', '0', '4574', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('156', '1000110', '0', 'Тирамису', '', '50', '14', '0', '0', '', '0', '', '1', '', '1', '0', '4575', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('157', '1000111', '0', 'Медовик', '', '50', '15', '0', '0', '', '0', '', '1', '', '1', '0', '4576', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('158', '1000113', '0', 'Греческий', '', '51', '16', '0', '0', '', '0', '', '1', '', '1', '0', '4577', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('159', '1000114', '0', 'Цезарь', '', '51', '17', '0', '0', '', '0', '', '1', '', '1', '0', '4578', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('160', '1000175', '0', 'Пиво', '', '44', '18', '0', '0', '', '0', '', '1', '', '1', '0', '4579', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('161', '1000190', '0', 'Ланч 4 позиции', '', '46', '19', '0', '0', '', '0', '', '1', '', '1', '0', '4580', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('162', '1000192', '0', 'Котлеты по киевски', '', '52', '20', '0', '0', '', '0', '', '1', '', '1', '0', '4581', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('163', '1000193', '0', 'Рагу из овощей', '', '52', '21', '0', '0', '', '0', '', '1', '', '1', '0', '4582', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('164', '1000208', '0', '\"Медальон\"', '', '53', '22', '0', '0', '', '0', '', '1', '', '1', '0', '4583', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('165', '1000209', '0', 'Мясо \"По- мексикански\"', '', '53', '23', '0', '0', '', '0', '', '1', '', '1', '0', '4584', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('166', '1000210', '0', 'Шашлык из свинины', '', '53', '24', '0', '0', '', '0', '', '1', '', '1', '0', '4585', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('167', '1000211', '0', 'Говядина «Фламбэ» с белыми грибами', '', '53', '25', '0', '0', '', '0', '', '1', '', '1', '0', '4586', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('168', '1000212', '0', 'Стейк говяжий на решетке', '', '53', '26', '0', '0', '', '0', '', '1', '', '1', '0', '4587', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('169', '1000213', '0', '\"Капрезе\"', '', '51', '27', '0', '0', '', '0', '', '1', '', '1', '0', '4588', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('170', '1000214', '0', '\"Экзотика\"', '', '51', '28', '0', '0', '', '0', '', '1', '', '1', '0', '4589', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('171', '1000215', '0', '\"Парма ди Руккола\"', '', '51', '29', '0', '0', '', '0', '', '1', '', '1', '0', '4590', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('172', '1000216', '0', 'Штрудель яблочный', '', '50', '30', '0', '0', '', '0', '', '1', '', '1', '0', '4591', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('173', '1000217', '0', 'Салат \"Фруктовый\"', '', '50', '31', '0', '0', '', '0', '', '1', '', '1', '0', '4592', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('174', '1000218', '0', 'Соки \"RICH\" в ассортименте	200 мл.', '', '44', '32', '0', '0', '', '0', '', '1', '', '1', '0', '4593', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('175', '1000219', '0', 'Брусничный морс', '', '44', '33', '0', '0', '', '0', '', '1', '', '1', '0', '4594', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('176', '1000220', '0', 'Акваминерале', '', '44', '34', '0', '0', '', '0', '', '1', '', '1', '0', '4595', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('177', '1000222', '0', 'Борщ \"Московский\"', '', '54', '35', '0', '0', '', '0', '', '1', '', '1', '0', '4596', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('178', '1000223', '0', 'Солянка по-домашнему', '', '54', '36', '0', '0', '', '0', '', '1', '', '1', '0', '4597', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('179', '1000224', '0', 'Уха ростовская', '', '54', '37', '0', '0', '', '0', '', '1', '', '1', '0', '4598', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('180', '1000225', '0', 'Крем-суп с белыми грибами', '', '54', '38', '0', '0', '', '0', '', '1', '', '1', '0', '4599', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('181', '1000226', '0', '\"Сицилия\"', '', '54', '39', '0', '0', '', '0', '', '1', '', '1', '0', '4600', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('182', '1000228', '0', 'Сыр в кляре', '', '55', '40', '0', '0', '', '0', '', '1', '', '1', '0', '4601', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('183', '1000229', '0', 'Крылышки куриные на углях', '', '55', '41', '0', '0', '', '0', '', '1', '', '1', '0', '4602', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('184', '1000230', '0', 'Креветки тигровые в кляре', '', '55', '42', '0', '0', '', '0', '', '1', '', '1', '0', '4603', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('185', '1000231', '0', 'Копченый сыр к пиву', '', '55', '43', '0', '0', '', '0', '', '1', '', '1', '0', '4604', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('186', '1000232', '0', 'Гренки острые с сыром', '', '55', '44', '0', '0', '', '0', '', '1', '', '1', '0', '4605', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('187', '1000234', '0', '\"Ахтамар\" 0,050', '', '56', '45', '0', '0', '', '0', '', '1', '', '1', '0', '4606', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('188', '1000235', '0', '\"Арарат\" 0,050', '', '56', '46', '0', '0', '', '0', '', '1', '', '1', '0', '4607', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('189', '1000236', '0', '\"Remy Martin\" V.S. 0,050', '', '56', '47', '0', '0', '', '0', '', '1', '', '1', '0', '4608', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('190', '1000237', '0', '\"Hennessy\" V.S. 0,050', '', '57', '48', '0', '0', '', '0', '', '1', '', '1', '0', '4609', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('191', '1000238', '0', '\"Праздничный\" 0,050', '', '56', '49', '0', '0', '', '0', '', '1', '', '1', '0', '4610', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('192', '1000240', '0', '\"Miller\" 0,33', '', '58', '50', '0', '0', '', '0', '', '1', '', '1', '0', '4611', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('193', '1000241', '0', '\"Corona Extra\" 0,33', '', '58', '51', '0', '0', '', '0', '', '1', '', '1', '0', '4612', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('194', '1000242', '0', '\"Beck\'s\" безалкогольное 0,33', '', '58', '52', '0', '0', '', '0', '', '1', '', '1', '0', '4613', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('195', '1000243', '0', '\"Hoegaarden\" 0,5', '', '58', '53', '0', '0', '', '0', '', '1', '', '1', '0', '4614', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('196', '1000244', '0', 'Сибирская Корона 0,5', '', '58', '54', '0', '0', '', '0', '', '1', '', '1', '0', '4615', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('197', '1000305', '0', 'Новый напиток', '', '44', '55', '0', '0', '', '0', '', '1', '', '1', '0', '4616', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('198', '1000306', '0', 'Новый напиток', '', '44', '56', '0', '0', '', '0', '', '1', '', '1', '0', '4617', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('199', '1000347', '0', 'модикомбо', '', '59', '57', '0', '0', '', '0', '', '1', '', '1', '0', '4618', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('200', '1000391', '0', '111', '', '59', '58', '0', '0', '', '0', '', '1', '', '1', '0', '4619', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('201', '1000404', '0', 'вкусняшка', '', '54', '59', '0', '0', '', '0', '', '1', '', '1', '0', '4620', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('202', '1000469', '0', '111', '', '60', '60', '0', '0', '', '0', '', '1', '', '1', '0', '4621', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('203', '1000475', '0', 'приветик', '', '56', '61', '0', '0', '', '0', '', '1', '', '1', '0', '4622', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('204', '1000482', '0', 'хахаха', '', '56', '62', '0', '0', '', '0', '', '1', '', '1', '0', '4623', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('205', '1000483', '0', 'xxx', '', '56', '63', '0', '0', '', '0', '', '1', '', '1', '0', '4624', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('206', '1000484', '0', 'куку черновик', '', '56', '64', '0', '0', '', '0', '', '1', '', '1', '0', '4625', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('207', '1000493', '0', 'комбо', '', '61', '65', '0', '0', '', '0', '', '1', '', '1', '0', '4626', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('208', '1000503', '0', 'Фокус тест', '', '62', '66', '0', '0', '', '0', '', '1', '', '1', '0', '4627', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('209', '1000520', '0', 'Новый десерт', '', '50', '67', '0', '0', '', '0', '', '1', '', '1', '0', '4628', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('210', '1000521', '0', 'Блюдо1', '', '61', '68', '0', '0', '', '0', '', '1', '', '1', '0', '4629', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('211', '1000529', '0', 'Блюдо2', '', '61', '69', '0', '0', '', '0', '', '1', '', '1', '0', '4630', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('212', '1000552', '0', 'Водка', '', '78', '70', '0', '0', '', '0', '', '1', '43,44,45,46,47', '2', '0', '4631', '0', '100', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('213', '1000553', '0', 'Пепси', '', '77', '71', '0', '0', '', '0', '', '1', '43,44,45,46,47', '2', '0', '4632', '0', '55', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('214', '1000564', '0', 'Эспрессо классический', '', '80', '72', '0', '0', '', '0', '', '1', '', '2', '0', '4633', '0', '1', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('215', '1000565', '0', 'Ристретто', '', '80', '73', '0', '0', '', '0', '', '1', '', '2', '0', '4634', '0', '1', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('216', '1000566', '0', 'Эспрессо Романо', '', '80', '74', '0', '0', '', '0', '', '1', '', '2', '0', '4635', '0', '1', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('217', '1000567', '0', 'Дабл Капучино сливочный «Амереттини-кара', '', '80', '75', '0', '0', '', '0', '', '1', '', '2', '0', '4636', '0', '1', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('218', '1000569', '0', 'Блинчики 2 штуки, без начинки', '', '81', '76', '0', '0', '', '0', '', '1', '', '1', '0', '4637', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('219', '1000570', '0', 'Сырники со сметаной и ягодами', '', '81', '77', '0', '0', '', '0', '', '1', '', '1', '0', '4638', '0', '0', '0', '', '0', '0', '0');
INSERT INTO `app_position` VALUES ('220', '1000572', '0', 'Стеклянная лапша в соусе «Тай-вок»', 'Калории: 660 kcal\r\nВес: 360 г', '82', '78', '0', '0', '/data/images/positions/1422968051_d22e24b2e499d3349d2ca524c6df58d6.jpeg', '0', '', '1', '', '1', '1', '4639', '1', '299', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('221', '1000574', '0', 'Тако беби сарада', 'Осьминоги с салатом «Чука»', '83', '79', '0', '0', '/data/images/positions/1422969177_ada6cf5e96f758bf1eb4a326ea34188b.jpeg', '0', '', '0', '', '1', '1', '4640', '2', '285', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('222', '1000575', '0', 'Кагаяки сарада', 'Краб сурими с кукурузой, рисом и тобико с пикантным соусом', '83', '80', '0', '0', '/data/images/positions/1422969325_46f8a1ae3c1a8a9a164e61528577e3da.jpeg', '0', '', '0', '', '1', '1', '4641', '3', '240', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('223', '1000576', '0', 'Тори но маринадо', 'Курица, обжаренная в сливках и соевом соусе, с фасолью, сладким перцем и зелёным луком', '83', '81', '0', '0', '/data/images/positions/1422969694_49b35646513e413edb52c9ad1755a7d3.jpeg', '0', '', '0', '', '1', '2', '4642', '4', '190', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('224', '1000578', '0', 'Кроненбург Блан', 'Пиво нефильтрованное 1000мл', '84', '82', '0', '0', '/data/images/positions/1422970150_47c3028f01611f60e8152fcf314aa24b.jpeg', '0', '', '0', '47', '2', '0', '4643', '5', '520', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('225', '1000580', '0', 'Канраку сиру', 'Сырный суп с копчёной курицей, беконом, томатами черри и шампиньонами', '85', '83', '0', '0', '/data/images/positions/1422970772_5259e08ce22df10f341c87e9da1b4219.jpeg', '0', '', '0', '45,47', '1', '1', '4644', '6', '240', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('226', '1000581', '0', 'Фреш (Яблоко-сельдерей-морковь)', 'Свежевыжатый сок яблоко-сельдерей-морковь', '80', '84', '0', '0', '/data/images/positions/1422974935_ccb6e12ea6389dde52783bd16b8aa02c.jpeg', '0', '', '0', '', '2', '0', '4645', '7', '440', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('227', '1000582', '0', 'Перье (с газом)', 'Производство Франция. Минеральная вода с газом Перье', '80', '85', '0', '0', '/data/images/positions/1422975540_beebe5914eb4ef644e66a97cc814a6bd.jpeg', '0', '', '0', '', '2', '0', '4646', '9', '145', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('228', '1000583', '0', 'Кока-кола Лайт', 'Производство Россия. Газированный напиток Кока-кола Лайт', '80', '86', '0', '0', '/data/images/positions/1422975404_dc0614c8be3692180f4beb198f55b147.jpeg', '0', '', '0', '', '2', '0', '4647', '8', '90', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('229', '1000584', '0', 'Сок апельсиновый', 'Производство Россия. Пакетированный апельсиновый сок', '80', '87', '0', '0', '/data/images/positions/1422975594_566d29c93685f6b47cc2a9e2b50eeab0.jpeg', '0', '', '0', '', '2', '0', '4648', '10', '150', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('230', '1000585', '0', 'Морс клюквенный', 'Домашний клюквенный морс', '80', '88', '0', '0', '/data/images/positions/1422975645_f7930524591cd98786545c8de3a2ace4.jpeg', '0', '', '0', '', '2', '0', '4649', '11', '430', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('231', '1000587', '0', 'Тамагояки тяхан', 'Жареный рис с беконом, яйцом и чесноком', '86', '89', '0', '0', '/data/images/positions/1422976824_367d262ad584481734fa4e4ad799b8b6.jpeg', '0', '', '0', '', '1', '1', '4650', '12', '190', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('232', '1000588', '0', 'Гохан', 'Отварной рис по-японски', '86', '90', '0', '0', '/data/images/positions/1422976996_01f43d5ea8673c535e5a27b8bbb0e1af.jpeg', '0', '', '0', '', '1', '1', '4651', '13', '75', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('233', '1000589', '0', 'Унайдзю', 'Копчёный угорь на рисе', '86', '91', '0', '0', '/data/images/positions/1422977117_5b443ea6c56884c65b8642e80ac8099c.jpeg', '0', '', '0', '', '1', '1', '4652', '14', '470', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('234', '1000590', '0', 'Дорадо', 'Дорадо с соусом «Тар-тар»', '86', '92', '0', '0', '/data/images/positions/1422977227_f4b1b887bd5169c8dd9a7b274707ee76.jpeg', '0', '', '0', '', '1', '1', '4653', '15', '510', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('235', '1000591', '0', 'Киниро сякэ', 'Стейк из сёмги под остро-сладким соусом, с овощами и мини-картофелем', '86', '93', '0', '0', '/data/images/positions/1422977315_4d6fcf5191245ea8f2e5305bc56b06a9.jpeg', '0', '', '0', '', '1', '1', '4654', '16', '510', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('236', '1000595', '0', 'Хияши вакамэ', 'Салат «Чука» с ореховым соусом', '83', '94', '0', '0', '/data/images/positions/1422978539_f429f1dd59a0f1d3c5d31d1ce6ce4f3e.jpeg', '0', '', '0', '', '1', '2', '4655', '17', '245', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('237', '1000596', '0', 'Дайкани сарада', 'Мясо краба, сурими с апельсином и тобико', '83', '95', '0', '0', '/data/images/positions/1422978641_48ffabf63fdaaffb4394a841eb2ad825.jpeg', '0', '', '0', '', '1', '2', '4656', '18', '360', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('238', '1000597', '0', 'Саке нефильтрованное', 'Саке производится на основе местной родниковой воды. Ферментация протекает при низких температурах. После окончания ферментации саке проходит грубый отжим, в результате которого часть осадка попадает в саке. Выдерживается в стальных эмалированных чанах в течение 6 месяцев.\r\nНефильтрованное саке с фруктовыми нотами.\r\nПрекрасно сочетается с японской кухней. Также с азиатской и средиземноморской. Саке прекрасно сочетается с любыми рыбными блюдами.\r\n12°С-14°С\r\n14.90%', '84', '96', '0', '0', '/data/images/positions/1422979087_86d0f04a233a0cdb8682c7190b6cb397.jpeg', '0', '', '0', '', '2', '0', '4657', '19', '930', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('239', '1000598', '0', 'Сушивайн белое', 'Производство Франция. Вино Сушивайн белое, обладает лёгким цветочным ароматом с нотами яблока и грейпфрута, специально созданное для того, чтобы подчеркнуть тонкий, нежный и свежий вкус традиционных японских блюд', '84', '97', '0', '0', '/data/images/positions/1422979310_4ee1613f44f427cf4441b314c605201b.jpeg', '0', '', '0', '', '2', '0', '4658', '20', '1290', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('240', '1000599', '0', 'Фрескелло Бьянко', 'Производство Италия. Тонкий фруктовый аромат, легкий сбалансированный вкус с освежающим послевкусием.', '84', '98', '0', '0', '/data/images/positions/1422979453_e45d8cb491b4ec40c1fc7e2f2dda2812.jpeg', '0', '', '0', '', '2', '0', '4659', '21', '920', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('241', '1000600', '0', 'Сантори Олд', 'Производство Япония. Виски Сантори Олд', '84', '99', '0', '0', '/data/images/positions/1422979625_9747b7461931c7a765b8b79a885e09e5.jpeg', '0', '', '0', '', '2', '0', '4660', '22', '3500', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('242', '1000601', '0', 'Мисо сиру', 'Суп из соевой пасты мисо с тофу и водорослями', '85', '100', '0', '0', '/data/images/positions/1422980027_328127bd011d45947767cb6198e4cc49.jpeg', '0', '', '0', '', '1', '1', '4661', '24', '95', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('243', '1000602', '0', 'Сякэ тядзукэ', 'Рисовый суп с лососем, водорослями нори и кунжутом', '85', '101', '0', '0', '/data/images/positions/1422980073_a598b2878b04dee6f1a363542fe6b580.jpeg', '0', '', '0', '', '1', '1', '4662', '25', '210', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('244', '1000603', '0', 'Эби чили мисо', 'Острый суп из соевой пасты мисо с креветкой', '85', '102', '0', '0', '/data/images/positions/1422980102_6b3c5be348587f0e3868ff7ff5c0cc76.jpeg', '0', '', '0', '', '1', '1', '4663', '26', '125', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('245', '1000604', '0', 'Унаги сиру', 'Суп с угрём, стеклянной лапшой, шампиньонами и яйцом', '85', '103', '0', '0', '/data/images/positions/1422979994_ad8351a0f296052663abeb64bf925680.jpeg', '0', '', '0', '', '1', '1', '4664', '23', '290', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('246', '1000605', '0', 'Сякэ', 'Лосось', '88', '104', '0', '0', '/data/images/positions/1422980432_ec0282b20a01cda17fa01bccc10e280a.jpeg', '0', '', '0', '', '1', '2', '4665', '27', '70', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('247', '1000606', '0', 'Хотатэ', 'Морской гребешок', '88', '105', '0', '0', '/data/images/positions/1422980472_1472bc356f5e6b1f708f57189c1273d2.jpeg', '0', '', '0', '', '1', '2', '4666', '28', '85', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('248', '1000607', '0', 'Эби', 'Креветка', '88', '106', '0', '0', '/data/images/positions/1422980500_33c4b825558eed45504ef46859d3e30d.jpeg', '0', '', '0', '', '1', '2', '4667', '29', '80', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('249', '1000608', '0', 'Икура', 'Красная икра', '88', '107', '0', '0', '/data/images/positions/1422980546_c63b5a6c82e6bb63a6ef0a702fe09fd1.jpeg', '0', '', '0', '', '1', '2', '4668', '30', '100', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('250', '1000609', '0', 'Инари Унаги', 'Копчёный угорь, тофу, острый соус', '88', '108', '0', '0', '/data/images/positions/1422980586_066216e22c2d01b84cc648cda6b93d36.jpeg', '0', '', '0', '', '1', '2', '4669', '31', '145', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('251', '1000610', '0', 'Филадельфия', 'С лососем, огурцом, авокадо и мягким сыром', '89', '0', '0', '0', '/data/images/positions/1422981985_c19815b118b4db855490cc663ca61751.jpeg', '0', '', '0', '', '1', '2', '4670', '33', '360', '1', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('252', '1000611', '0', 'Калифорния', 'С мясом краба, авокадо, огурцом и тобико', '89', '1', '0', '0', '/data/images/positions/1422982023_0481ebfdd5ef1262ebbdb66a704170da.jpeg', '0', '', '0', '', '1', '2', '4671', '34', '385', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('253', '1000612', '0', 'Сякэ кадо', 'С лососем, угрём, красной икрой, мягким сыром и огурцом', '89', '2', '0', '0', '/data/images/positions/1422982083_ade200dc0c2fab614dd5b18b2aced842.jpeg', '0', '', '0', '', '1', '2', '4672', '35', '310', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('254', '1000613', '0', 'Дракон ролл', 'С угрём, авокадо, мягким сыром и тобико', '89', '3', '0', '0', '/data/images/positions/1422982117_04c75f1905a8645358cf82a97c02ba4e.jpeg', '0', '', '0', '', '1', '2', '4673', '36', '295', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('255', '1000614', '0', 'Изуми спринг ролл', 'Морской окунь, шпинат, сыр, зеленый лук и шампиньоны в хрустящем тесте, с соусом «Каниши»', '89', '4', '0', '0', '/data/images/positions/1422981941_a6ba8b9ccf294dd19a99addb63c4c12f.jpeg', '0', '', '0', '', '1', '2', '4674', '32', '290', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('256', '1000615', '0', 'Моджи \"Манго\"', 'Традиционное японское мороженое со вкусом манго в тонком рисовом тесте.\r\n8 шт', '87', '114', '0', '0', '/data/images/positions/1422982472_6eddaa6a8b579bb4759a1cedf0ecad77.jpeg', '0', '', '0', '', '1', '2', '4675', '37', '920', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('257', '1000616', '0', 'Тирамису', 'Десерт из сыра маскарпоне и бисквитного печеньем савоярди, пропитанного кофе и коньяком', '87', '115', '0', '0', '/data/images/positions/1422982504_bf7dab300173beb83589baa4706097ce.jpeg', '0', '', '0', '', '1', '2', '4676', '38', '230', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('258', '1000617', '0', 'Куро ёру', 'Ролл с киви, ананасом, клубникой, бананом, миндалём и сырным кремом', '87', '116', '0', '0', '/data/images/positions/1422982547_f67f6a5b69851015c6483b458e2a6c80.jpeg', '0', '', '0', '', '1', '2', '4677', '39', '210', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('259', '1000618', '0', 'Итиго панкэки', 'Блинный торт с заварным кремом и клубничным соусом с ликёром', '87', '117', '0', '0', '/data/images/positions/1422982576_f474d79f8168896b9f19292ecd6067cd.jpeg', '0', '', '0', '', '1', '2', '4678', '40', '185', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('260', '1000619', '0', 'Пайнаппуру', 'Кусочки свежего ананаса', '87', '118', '0', '0', '/data/images/positions/1422982608_f7e42536eac45cbc114a5c5d3cb1ecfc.jpeg', '0', '', '0', '', '1', '2', '4679', '41', '295', '0', '1 шт.', '0', '0', '0');
INSERT INTO `app_position` VALUES ('261', '1000620', '0', 'Цезарь', '', '83', '119', '0', '0', '', '0', '', '0', '', '1', '0', '4680', '0', '0', '0', '', '0', '0', '0');

-- ----------------------------
-- Table structure for `app_position_groups`
-- ----------------------------
DROP TABLE IF EXISTS `app_position_groups`;
CREATE TABLE `app_position_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_position_groups
-- ----------------------------
INSERT INTO `app_position_groups` VALUES ('1', '', '1', 'Первое');
INSERT INTO `app_position_groups` VALUES ('2', '', '1', 'Второе');
INSERT INTO `app_position_groups` VALUES ('3', '', '1', 'Десерт');

-- ----------------------------
-- Table structure for `app_restaurant`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant`;
CREATE TABLE `app_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `working_hours` varchar(255) NOT NULL,
  `map` varchar(500) NOT NULL,
  `photos` varchar(500) NOT NULL,
  `version` int(11) NOT NULL,
  `imageVersion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant
-- ----------------------------
INSERT INTO `app_restaurant` VALUES ('3', '', '0', 'Борисовские пруды', 'г. Москва, ул Борисовские Пруды, д 10', '0', '1', 'Лучший ресторан', '+7 (555) 100-02-30', 'Пн-Пт 10:00-23:00', '{\"zoom\":11,\"coords\":{\"x\":55.635869,\"y\":37.740974},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000047100\",\"name\":\"Борисовские Пруды\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000004710001\",\"name\":\"10\",\"zip\":115211,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45296557000\",\"contentType\":\"building\"}}}', '[\"\\/data\\/images\\/restaurants\\/1406195747_0cf8570408c51a95107b73e5d6844e8b.jpeg\"]', '0', '0');
INSERT INTO `app_restaurant` VALUES ('4', '', '0', 'Боровское шоссе, д.31', 'г. Москва, ш. Боровское, д. 31', '0', '1', '', '+7 (111) 222-33-44', '', '{\"zoom\":13,\"coords\":{\"x\":55.642453,\"y\":37.361517},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000089000\",\"name\":\"Боровское\",\"zip\":null,\"type\":\"Шоссе\",\"typeShort\":\"ш\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000008900030\",\"name\":\"31\",\"zip\":119633,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45268577000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('6', '', '0', 'ул. Большая Академическая, д.65', 'г. Москва, ул. Академическая Б., д. 65', '0', '1', 'Посадочных мест: 198, Парковка: Есть, Wi-fi: Free, Кальянная карта: Есть', '+7 (499) 153-81-44', 'Пн-Чт (11:30 – 00:00), Пт-Сб (11:30 – 06:00), Вс.11:30 – 00:00', '{\"zoom\":11,\"coords\":{\"x\":55.840371,\"y\":37.547477},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000072300\",\"name\":\"Академическая Б.\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000007230016\",\"name\":\"65\",\"zip\":125183,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45277580000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('7', '', '0', 'шоссе Энтузиастов, 20', 'шоссе Энтузиастов, 20', '0', '2', 'Wi-Fi', '+7(495) 362-27-07', '11:00-00:00 ежедневно (до 6:00 только по праздникам), 11:00-16:00 ланч', '', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('8', '', '0', 'Авиамоторная ул., 41', 'Авиамоторная ул., 41', '0', '3', 'Одно из лучших кафе нашей сети,расположенное в 1-ой минуте ходьбы от метро, с курящим и отдельным некурящим залами. На летний период — веранда на крыше.', '+7 (495) 514-02-70', 'с 10:00 до 06:00 .бронирование: с 10:00 до 12:00  (только на текущий день, в Пт, Сб, Вс, а так же праздничные дни резервы не принимаются )', '', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('9', '', '0', 'Лениградка', 'Лениградка', '0', '6', '', '', '', '', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('10', '', '0', 'какой-то адрес', 'какой-то адрес', '0', '7', '', '', '', '', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('11', '', '0', 'маяквока', 'маяквока', '0', '6', '', '', '', '', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('12', '', '0', '6666', 'г. Москва, ул. 8 Марта 4-я, д. 5', '0', '4', '', '+7 (666) 666-66-66', '', '{\"zoom\":11,\"coords\":{\"x\":55.804218,\"y\":37.548887},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000103000\",\"name\":\"8 Марта 4-я\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45277553000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000010300002\",\"name\":\"5\",\"zip\":125319,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45277553000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('13', '', '0', '44', 'г. Москва, ул. 8 Марта 4-я, д. 4к1', '0', '4', '', '+7 (444) 444-44-44', '', '{\"zoom\":11,\"coords\":{\"x\":55.803398,\"y\":37.548896},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000103000\",\"name\":\"8 Марта 4-я\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45277553000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000010300001\",\"name\":\"4к1\",\"zip\":125167,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45277553000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('43', '1', '1', 'Наименование Ресторана', 'г. Москва, ул. 26-ти Бакинских Комиссаров, д. 10', '0', '9', '', '+7 (586) 854-52-65', '', '{\"zoom\":11,\"coords\":{\"x\":55.660054,\"y\":37.490326},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000116600\",\"name\":\"26-ти Бакинских Комиссаров\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45268592000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000011660003\",\"name\":\"10\",\"zip\":119526,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45268592000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('44', '2', '1', 'Центральный Офис', 'г. Москва, ул. Тверская, д. 22', '0', '9', '', '+7 (777) 888-99-11', '', '{\"zoom\":11,\"coords\":{\"x\":55.767418,\"y\":37.601169},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000287700\",\"name\":\"Тверская\",\"zip\":null,\"type\":\"Улица\",\"typeShort\":\"ул\",\"okato\":\"45286585000\",\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000028770019\",\"name\":\"22\",\"zip\":125009,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45286585000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('45', '1000010', '0', 'Кантемировская, Каширская', 'г. Москва, ш. Каширское, д. 46к1', '0', '9', '198 посадочных мест, парковка, Wi-Fi, кальянная карта', '+7 (499) 324-71-91', 'Пн-Чт,Вс(11:30-00:00), Пт-Сб(11:30-02:00)', '{\"zoom\":11,\"coords\":{\"x\":55.647894,\"y\":37.664788},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000049600\",\"name\":\"Каширское\",\"zip\":null,\"type\":\"Шоссе\",\"typeShort\":\"ш\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000004960082\",\"name\":\"46к1\",\"zip\":115409,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45296569000\",\"contentType\":\"building\"}}}', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('46', '1000506', '1', 'Центральный офис', '', '0', '9', '', '', '', '', '', '0', '0');
INSERT INTO `app_restaurant` VALUES ('47', '1000546', '0', 'Волгоградский пр-т, Пролетарская', 'г. Москва, пр-кт. Волгоградский, д. 17', '0', '9', '150 посадочных мест, парковка, Wi-Fi, кальянная карта', '+7 (495) 676-99-22', 'Пн-Чт,Вс(11:30-00:00), Пт-Сб(11:30-02:00)', '{\"zoom\":11,\"coords\":{\"x\":55.72856,\"y\":37.679116},\"place\":{\"city\":\"7700000000000\",\"street\":{\"id\":\"77000000000001300\",\"name\":\"Волгоградский\",\"zip\":null,\"type\":\"Проспект\",\"typeShort\":\"пр-кт\",\"okato\":null,\"contentType\":\"street\"},\"building\":{\"id\":\"7700000000000130026\",\"name\":\"17\",\"zip\":109316,\"type\":\"дом\",\"typeShort\":\"д\",\"okato\":\"45286580000\",\"contentType\":\"building\"}}}', '', '0', '0');

-- ----------------------------
-- Table structure for `app_restaurant_plan`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant_plan`;
CREATE TABLE `app_restaurant_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant_plan
-- ----------------------------
INSERT INTO `app_restaurant_plan` VALUES ('3', '1000015', '1', '45', '0', '0');
INSERT INTO `app_restaurant_plan` VALUES ('4', '1000561', 'Новый план', '47', '0', '0');

-- ----------------------------
-- Table structure for `app_restaurant_table`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant_table`;
CREATE TABLE `app_restaurant_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant_table
-- ----------------------------
INSERT INTO `app_restaurant_table` VALUES ('8', '', '', '44', '0', '13', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('9', '', '', '44', '0', '13', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('10', '', '', '66', '0', '13', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('11', '', '', '1', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('12', '', '', '2', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('13', '', '', '3', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('14', '', '', '4', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('15', '', '', '5', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('16', '', '', '6', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('17', '', '', '7', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('18', '', '', '8', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('19', '', '', '9', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('20', '', '', '10', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('21', '', '', '11', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('22', '', '', '12', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('23', '', '', '13', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('24', '', '', '14', '0', '3', '0', '0', '1');
INSERT INTO `app_restaurant_table` VALUES ('25', '', '', '15', '0', '3', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('26', '', '', '1', '0', '4', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('27', '', '', '2', '0', '4', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('28', '1000017', '2', '2', '3', '45', '0', '0', '1');
INSERT INTO `app_restaurant_table` VALUES ('29', '1000027', '3', '3', '3', '45', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('30', '1000028', '1', '1', '3', '45', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('31', '1000132', '6', 'Pool1', '3', '45', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('32', '1000206', '4', 'Игра', '3', '45', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('33', '1000328', '111', 'Д-ка', '3', '45', '0', '0', '0');
INSERT INTO `app_restaurant_table` VALUES ('34', '1000562', '5', 'Стол', '4', '47', '0', '0', '5');

-- ----------------------------
-- Table structure for `app_restaurant_tools`
-- ----------------------------
DROP TABLE IF EXISTS `app_restaurant_tools`;
CREATE TABLE `app_restaurant_tools` (
  `restaurant_id` int(11) NOT NULL,
  `cash_id` int(11) NOT NULL,
  PRIMARY KEY (`restaurant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_restaurant_tools
-- ----------------------------
INSERT INTO `app_restaurant_tools` VALUES ('45', '1');
INSERT INTO `app_restaurant_tools` VALUES ('47', '17');

-- ----------------------------
-- Table structure for `app_rkeeper_proxy`
-- ----------------------------
DROP TABLE IF EXISTS `app_rkeeper_proxy`;
CREATE TABLE `app_rkeeper_proxy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `proxy_uuid` varchar(255) NOT NULL,
  `proxy_type` varchar(255) NOT NULL,
  `proxy_name` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `alive_interval` int(11) NOT NULL,
  `last_request_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_rkeeper_proxy
-- ----------------------------
INSERT INTO `app_rkeeper_proxy` VALUES ('30', '9', '{2B039492-0768-4315-B3B5-A7A7FB58B811}', 'REF', 'BA_PROXY_REF', '1422951998', '0', '1423208772');
INSERT INTO `app_rkeeper_proxy` VALUES ('31', '9', '{96896BD3-1D9F-45D5-B87C-9B2E6A924AF6}', 'MID', 'BA_PROXY_MID', '1422952157', '0', '1423208773');
INSERT INTO `app_rkeeper_proxy` VALUES ('32', '9', '{FC949F7D-9FF3-4F2F-BD06-1A63314BD9D3}', 'MID', 'BA_PROXY_MID2', '1422973242', '0', '1423208772');

-- ----------------------------
-- Table structure for `app_rkeeper_queue`
-- ----------------------------
DROP TABLE IF EXISTS `app_rkeeper_queue`;
CREATE TABLE `app_rkeeper_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `proxy_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `body` longtext NOT NULL,
  `response` longtext NOT NULL,
  `create_time` int(11) NOT NULL,
  `request_time` int(11) NOT NULL,
  `response_time` int(11) NOT NULL,
  `error_description` varchar(255) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_rkeeper_queue
-- ----------------------------

-- ----------------------------
-- Table structure for `app_rkeeper_turn`
-- ----------------------------
DROP TABLE IF EXISTS `app_rkeeper_turn`;
CREATE TABLE `app_rkeeper_turn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proxy_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_rkeeper_turn
-- ----------------------------

-- ----------------------------
-- Table structure for `app_sale_price`
-- ----------------------------
DROP TABLE IF EXISTS `app_sale_price`;
CREATE TABLE `app_sale_price` (
  `condition_id` int(11) NOT NULL,
  `date_on` int(11) NOT NULL,
  `date_off` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_sale_price
-- ----------------------------

-- ----------------------------
-- Table structure for `app_user`
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_id` varchar(255) NOT NULL,
  `out_code` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `restaurant_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('1', '', '', 'ssa', 'e10adc3949ba59abbe56e057f20f883e', 'birlaver@gmail.com', 'Eugene Lepeshko', 'admin', '0', '0', '0');
INSERT INTO `app_user` VALUES ('2', '', '', 'tanuki_owner', 'e10adc3949ba59abbe56e057f20f883e', 'ssa@bistroapp.ru', 'Владелец Тануки', 'owner', '0', '0', '1');
INSERT INTO `app_user` VALUES ('3', '', '', 'tanuki_manager', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_manager@bistroapp.ru', 'Администратор Тануки', 'manager', '0', '3', '1');
INSERT INTO `app_user` VALUES ('4', '', '', 'tanuki_cook', 'e10adc3949ba59abbe56e057f20f883e', 'tanuki_cook@bistroapp.ru', 'Повар Тануки', 'cook', '0', '3', '1');
INSERT INTO `app_user` VALUES ('5', '', '', 'tanuki_garcon', 'e10adc3949ba59abbe56e057f20f883e', 'kalin@abcwww.ru', 'Оффициант Тануки', 'garcon', '0', '3', '1');
INSERT INTO `app_user` VALUES ('6', '', '', 'tanuki_barman', 'e10adc3949ba59abbe56e057f20f883e', 'lunatik_37@mail.ru', 'Бармен', 'barman', '0', '3', '1');
INSERT INTO `app_user` VALUES ('7', '', '', 'tanuki_wer', 'e10adc3949ba59abbe56e057f20f883e', 'wer@bistroapp.ru', 'wer', 'barman', '0', '3', '1');
INSERT INTO `app_user` VALUES ('8', '', '', 'tanuki_ert', 'e10adc3949ba59abbe56e057f20f883e', 'ert@bistroapp.ru', 'ert', 'manager', '0', '7', '2');
INSERT INTO `app_user` VALUES ('9', '', '', 'tanuki_1234323', 'e10adc3949ba59abbe56e057f20f883e', 'abc-coder@mail.ru', '6324234', 'garcon', '0', '3', '1');
INSERT INTO `app_user` VALUES ('10', '', '', 'tanuki_234243', 'e10adc3949ba59abbe56e057f20f883e', 'ssa23423@ewrwer.ui', '234234234', 'garcon', '0', '3', '1');
INSERT INTO `app_user` VALUES ('11', '', '', 'rkeeper_owner', '202cb962ac59075b964b07152d234b70', 'rkeeper_owner@bistroapp.ru', 'Владелец компании', 'owner', '0', '0', '9');
INSERT INTO `app_user` VALUES ('12', '1', '7', 'rkeeper_user1', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1@bistroapp.ru', 'Администратор', 'garcon', '1', '45', '9');
INSERT INTO `app_user` VALUES ('13', '9001', '9001', 'rkeeper_user9001', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9001@bistroapp.ru', 'Система', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('14', '9002', '9002', 'rkeeper_user9002', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9002@bistroapp.ru', '777', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('15', '9003', '9003', 'rkeeper_user9003', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9003@bistroapp.ru', 'Контроль Кодов', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('16', '9004', '9004', 'rkeeper_user9004', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9004@bistroapp.ru', 'RK6 Импорт', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('17', '9005', '9005', 'rkeeper_user9005', '202cb962ac59075b964b07152d234b70', 'rkeeper_user9005@bistroapp.ru', 'Веб-Резервирование', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('18', '1000002', '2', 'rkeeper_user1000002', '202cb962ac59075b964b07152d234b70', 'a.evtyushin@gmail.com1', 'Tester', 'garcon', '1', '47', '9');
INSERT INTO `app_user` VALUES ('19', '1000003', '3', 'rkeeper_user1000003', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000003@bistroapp.ru', 'Крылов', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('20', '1000005', '4', 'rkeeper_user1000005', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000005@bistroapp.ru', 'Официант №2', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('21', '1000006', '5', 'rkeeper_user1000006', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000006@bistroapp.ru', 'Официант №1', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('22', '1000007', '6', 'rkeeper_user1000007', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000007@bistroapp.ru', 'Кассир №1', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('23', '1000008', '8', 'rkeeper_user1000008', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000008@bistroapp.ru', 'Кассир №2', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('24', '1000009', '12', 'rkeeper_user1000009', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000009@bistroapp.ru', 'Спец работник', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('25', '401862', '3', 'rkeeper_user401862', '202cb962ac59075b964b07152d234b70', 'rkeeper_user401862@bistroapp.ru', 'Ахметзянова Алсу', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('26', '401923', '1', 'rkeeper_user401923', '202cb962ac59075b964b07152d234b70', 'rkeeper_user401923@bistroapp.ru', 'Распопов Андрей', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('27', '402314', '10', 'rkeeper_user402314', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402314@bistroapp.ru', 'Кравцов Евгений', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('28', '402341', '13', 'rkeeper_user402341', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402341@bistroapp.ru', 'Беляев Владимир', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('29', '402356', '9', 'rkeeper_user402356', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402356@bistroapp.ru', 'Щербакова Ирина', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('30', '402444', '11', 'rkeeper_user402444', '202cb962ac59075b964b07152d234b70', 'rkeeper_user402444@bistroapp.ru', 'Tester', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('31', '1000004', '15', 'rkeeper_user1000004', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000004@bistroapp.ru', 'Алсу', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('32', '1000011', '1', 'rkeeper_user1000011', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000011@bistroapp.ru', 'Кассир1', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('33', '1000012', '3', 'rkeeper_user1000012', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000012@bistroapp.ru', 'Админ', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('34', '1000013', '9', 'rkeeper_user1000013', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000013@bistroapp.ru', '45345', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('35', '1000015', '13', 'rkeeper_user1000015', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000015@bistroapp.ru', 'Кассир', 'garcon', '1', '0', '9');
INSERT INTO `app_user` VALUES ('36', '1000016', '14', 'rkeeper_user1000016', 'c4ca4238a0b923820dcc509a6f75849b', 'evtyushin@corp.mail.ru', 'Tester', 'garcon', '1', '47', '9');
INSERT INTO `app_user` VALUES ('37', '1000017', '1', 'rkeeper_user1000017', '202cb962ac59075b964b07152d234b70', 'rkeeper_user1000017@bistroapp.ru', 'test1', 'garcon', '1', '0', '9');

-- ----------------------------
-- Table structure for `app_user_table`
-- ----------------------------
DROP TABLE IF EXISTS `app_user_table`;
CREATE TABLE `app_user_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_user_table
-- ----------------------------
INSERT INTO `app_user_table` VALUES ('6', '12', '9');
INSERT INTO `app_user_table` VALUES ('7', '14', '9');
INSERT INTO `app_user_table` VALUES ('8', '20', '9');
INSERT INTO `app_user_table` VALUES ('9', '24', '9');
INSERT INTO `app_user_table` VALUES ('10', '34', '36');
