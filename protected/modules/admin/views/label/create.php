<?php
/* @var $this LabelController */
/* @var $model Label */
$this->pageTitle = Yii::app()->name . ' - Создание спецвывода';
$this->menu=array(
	array('label'=>'Список спецвыводов', 'url'=>array('index')),
);
?>

<h1>Создание спецвывода</h1>
<br/>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>