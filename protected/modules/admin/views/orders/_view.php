<?php
/* @var $this OrdersController */
/* @var $data Order */
?>
<tr data-id="<?php echo CHtml::encode($data->id); ?>" class="<?php echo $orderid==$data->id ? 'current' : '';?>">
    <td>
        <div class="td_inner">
            <input <?php echo $orderid==$data->id ? 'checked="checked" class="checked"' : '';?> type="checkbox" id="row<?php echo CHtml::encode($data->id); ?>">
            <span class="big"><?php echo CHtml::encode($data->id);?></span>
        </div>
    </td>
    <td>
        <div class="td_inner"><span class="big"><?php echo CHtml::encode($data->table->name); echo $data->table_text ? ' <small>('.$data->table_text.')</small>' : ''; ?></span></div>
    </td>
    <td class="<?php echo 1==$data->status_id ? 'new' : '';?>">
        <div class="td_inner"><?php echo CHtml::encode($data->status->name); ?></div>
    </td>
    <td>
        <div class="td_inner">
            <?php
            if($data->date_create){
                ?>
                <?php echo date('H:i', $data->date_create);?><br/><span class="small"><?php echo date('d.m.Y', $data->date_create);?></span>
            <?php
            }else{
                ?>
                <b>&mdash;</b>
            <?php
            }
            ?>
        </div>
    </td>
    <td>
        <div class="td_inner"><?php echo CHtml::encode($data->payment->name); ?></div>
    </td>
    <td>
        <div
            class="td_inner"><?php echo CHtml::encode($data->attention) == 1 ? '<img src="/images/exclam_white.png" width="22" height="22" alt="">' : ''; ?></div>
    </td>
    <td>
        <div class="td_inner">
            <div class="position_box">
                <div class="position_count"><?php echo $data->count_items; ?></div>
                <div class="position_change">
                    <a href="#" class="position_text"><span>Позиции</span></a>
                    <ul class="position_list">
                        <?php
                        foreach($data->items as $one){
                            if (is_object($one->position)){
                                ?>
                                <li><a href="javascript:void(0);"><?php echo $one->position->name?> <?php echo $one->position->measure?></a></li>
                            <?php
                            }else{
                                ?>
                                <li><a href="javascript:void(0);">Позиция уже удалена</a></li>
                            <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </td>
    <td><div class="td_inner"><span class="small"><?php echo CHtml::encode($data->separated) == 1 ? 'раздельный' : 'общий'; ?></span></div></td>
    <td>
        <div class="td_inner">
            <?php
            if($data->date_execute){
            ?>
                <?php echo date('H:i', $data->date_execute);?><br/><span class="small"><?php echo date('d.m.Y', $data->date_execute);?></span>
            <?php
            }else{
                ?>
                <b>&mdash;</b>
            <?php
            }
            ?>
        </div>
    </td>
    <td>
        <div class="td_inner">
            <?php
            if($data->date_close){
                ?>
                <?php echo date('H:i', $data->date_close);?><br/><span class="small"><?php echo date('d.m.Y', $data->date_close);?></span>
            <?php
            }else{
                ?>
                <b>&mdash;</b>
            <?php
            }
            ?>
        </div>
    </td>
    <td><div class="td_inner"><span class="small"><?php echo $data->garcon->fullname;?></span></div></td>
    <td><div class="td_inner"><?php echo $data->total_price; ?><br><span class="small">руб.</span></div></td>
</tr>