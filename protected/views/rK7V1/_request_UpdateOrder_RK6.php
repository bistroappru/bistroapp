<RK6Query>
    <RK6CMD CMD="UpdateOrder">
        <Order Guid="<?=$data['order_guid']?>" Comment="<?= CHtml::encode($data['comment'])?>">
            <Table ID="<?=$data['table']?>"/>
            <Guests Count="<?=$data['guests']?>"/>
            <MenuItems>
                <? foreach($items as $item):?>
                    <MenuItem Sifr="<?= $item->position->out_id ?>" Quantity="<?= $item->count ?>" Price = "<?= $item->price ?>" Comment="<?= CHtml::encode($item->comment)?>"/>
                <? endforeach; ?>
            </MenuItems>
        </Order>
    </RK6CMD>
</RK6Query>