<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>
    <div class="form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'menu-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array(
                'data-id' => $model->id,
                'data-type' => $model->isNewRecord ? 'create' : 'update'
            )
        ));
        ?>

        <?php echo $form->hiddenField($model, 'parent_id', array('value' => Yii::app()->request->getParam('parent_id', 0)));?>
        <?php echo $form->hiddenField($model, 'company_id');?>
        <?php echo $form->hiddenField($model, 'sort');?>
        <?php echo $form->hiddenField($model, 'hidden');?>
        <?php echo $form->hiddenField($model, 'deleted');?>
        <?php echo $form->error($model, 'company_id');?>

        <div class="product_row">
            <label class="product_label">Наименование</label>

            <div class="product_title inp_wrap">
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
                <a href="#" class="edit-link"></a>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <p class="orig_text"><?=$model->name_orig?></p>
        </div>

        <div class="product_row">
            <label class="product_label">Описание</label>

            <div class="product_desc">
                <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
                <a href="#" class="edit-link"></a>
            </div>
            <div class="product_thumb"><img
                    src="<?php echo $model->image ? $model->image : Yii::app()->params['emptyImage']; ?>" width="130"
                    height="130"><a href="javascript:void(0);" class="refresh"></a>
            </div>
            <p class="orig_text"><?=$model->description_orig?></p>
            <?php echo $form->hiddenField($model, 'image'); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>

        <ul class="leftmenu_buttons dark">
            <li><?php echo CHtml::link('<img src="/images/leftmenu-4.png" width="50" height="50" alt=""><span>' . ($model->isNewRecord ? 'Создать' : 'Сохранить') . '</span>', '#', array('id' => 'menu_save')); ?></li>
            <?php
            if (!$model->isNewRecord) {
                ?>
                <li><?php echo CHtml::link('<img src="/images/leftmenu-2' . ($model->deleted ? '_act' : '') . '.png" width="50" height="50" alt=""><span>' . ($model->deleted ? 'Восстановить' : 'Удалить') . '</span>', '#', array('id' => 'menu_set_deleted', 'class' => $model->deleted ? 'active' : '')); ?></li>
                <li><?php echo CHtml::link('<img src="/images/leftmenu-6' . ($model->hidden ? '_act' : '') . '.png" width="50" height="50" alt=""><span>' . ($model->hidden ? 'Включить' : 'Отключить') . '</span>', '#', array('id' => 'menu_set_hidden', 'class' => $model->hidden ? 'active' : '')); ?></li>
            <?php
            }
            ?>
        </ul>
        <div class="clear"></div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
<?php $this->renderPartial('/partials/_load_img', array('folder' => '/data/images/menu', 'selector' => 'Menu_image')); ?>