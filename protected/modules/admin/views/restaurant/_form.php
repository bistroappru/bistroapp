<?php
/* @var $this RestaurantController */
/* @var $model Restaurant */
/* @var $form CActiveForm */
?>
<div class="product_section" id="restaurant_form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'restaurant-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'data-id' => $model->id,
            'data-type' => $model->isNewRecord ? 'create' : 'update'
        )
    )); ?>
        <?php
        if ($model->hasErrors()){
            print_r($model->getErrors());
        }
        ?>
        <?php echo $form->hiddenField($model,'hidden');?>
        <?php echo $form->hiddenField($model,'deleted');?>
        <?php echo $form->textArea($model,'map',array('style' => 'display: none;'));?>
        <div class="company_section">
            <h1 id="restaurant_title"><?php echo $model->name;?></h1>
            <div class="product_row input_row">
                <label class="product_label">Компания: <b><?=$model->company->name;?></b></label>
                <?php echo $form->hiddenField($model,'company_id'); ?>
                <?php echo $form->error($model,'company_id'); ?>
            </div>
            <br/>
            <div class="product_row input_row">
                <label class="product_label">Название</label>
                <div class="inp_wrap">
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>
            </div>

            <?php echo $form->hiddenField($model,'location',array('size'=>60, 'maxlength'=>255));?>

            <div class="adress_box right_box">
                <div class="rightbox_title">
                    <div class="rightbox_text"><?=$model->location ? $model->location : 'г. Москва';?></div>
                    <span class="adress_arrow box_link <?=$model->isNewRecord ? 'opened' : ''?>"></span>
                </div>
                <div class="box_addit input_row" <?=$model->isNewRecord ? 'style="display: block;"' : ''?>>
                    <div class="map_type" data-id="1">
                        <select id="kladr_city" name="kladr_city" required="required">
                            <option value="7700000000000" selected="selected">Москва</option>
                        </select>
                        <div class="inp_wrap">
                            <input id="kladr_street" name="kladr_street" type="text" class="kladr_valid" placeholder="Улица">
                            <?php echo $form->error($model,'kladr_street'); ?>
                        </div>
                        <div class="inp_wrap">
                            <input id="kladr_building" name="kladr_building" type="text" class="kladr_valid" placeholder="Номер дома">
                            <?php echo $form->error($model,'kladr_building'); ?>
                        </div>
                    </div>
                </div>
                <div id="map"></div>
            </div>
            <div class="product_gallery">
                <label class="product_label">Фотографии</label>
                <ul class="photo_list">
                    <?php
                        foreach($model->photos as $k=>$v){
                            ?>
                                <li>
                                    <a>
                                        <img id="Restaurant_photos_<?=$k;?>_img" src="<?=$v;?>" width="88" height="88">
                                    </a>
                                    <input type="hidden" id="Restaurant_photos_<?=$k;?>" name="Restaurant[photos][<?=$k;?>]" value="<?=$v;?>"/>
                                </li>
                            <?php
                        }
                    ?>
                </ul>
                <div class="photo_buttons">
                    <a style="display: none;" class="ph_refresh"></a>
                    <a style="display: none;" class="ph_del"></a>
                    <a class="ph_add"></a>
                </div>
            </div>

            <div class="product_row input_row inp_wrap">
                <label class="product_label">Описание ресторана</label>
                <div class="inp_wrap">
                    <?php echo $form->textArea($model,'description'); ?>
                    <?php echo $form->error($model,'description'); ?>
                </div>
            </div>

            <div class="product_row input_row">
                <label class="product_label">Режим работы</label>
                <div class="inp_wrap">
                    <?php echo $form->textField($model, 'working_hours'); ?>
                    <?php echo $form->error($model,'working_hours'); ?>
                </div>
            </div>

            <div class="product_row input_row phone_list">
                <label class="product_label">Телефон</label>
                <?php
                foreach ($model->phone as $k => $v) {
                    ?>
                    <div class="inp_wrap">
                        <input type="text" value="<?= $v; ?>" name="Restaurant[phone][<?=$k?>]" placeholder="+7 (___) ___-__-__"/>
                        <?= $k > 0 ? '<a class="phone_del" href="#"></a>' : '' ?>
                        <?php echo $form->error($model,'phone_'.$k); ?>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="product_row add_phone">
                <div class="new_phone">Добавить телефон<a class="phone_add" href="#"></a></div>
            </div>

            <div id="table_list" class="product_row inp_wrap">
                <label class="product_label_big">Столики</label>
                <div id="table_list_add" class="table_list_action">
                    <label>Добавить столик: </label>
                    <input type="text"/>
                    <span class="add"><i class="fa fa-plus"></i></span>
                </div>
                <div id="table_list_edit" class="table_list_action" style="display: none;">
                    <label>Редактировать столик: </label>
                    <input type="text"/>
                    <span class="save"><i class="fa fa-check"></i></span>
                    <span class="cancel"><i class="fa fa-reply"></i></span>
                </div>
                <div class="table_list">
                    <?php
                    foreach ($model->tables as $v){
                    ?>
                        <div class="table_inline" data-action="<?= $v->action; ?>" data-id="<?= $v->id; ?>" data-name="<?= $v->name; ?>">
                            <span class="name"><?= $v->name; ?> (id=<?= $v->id;?>)</span>
                            <span class="edit"><i class="fa fa-pencil"></i></span>
                            <span class="del"><i class="fa fa-close"></i></span>
                            <span class="recovery"><i class="fa fa-undo"></i></span>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <textarea id="Restaurant_tables" name="Restaurant[tables]" style="display: none;"></textarea>
                <?php echo $form->error($model, 'tables'); ?>
            </div>

            <label class="product_label_big">Взаимодействие со сторонними программами</label>

            <div class="product_row inp_wrap">
                <div class="spec_checkbox">
                    <?php echo $form->hiddenField($model,'lock_order_transfer'); ?>
                    <input <?php if ($model->lock_order_transfer) echo 'checked="checked"';?> id="lock_order_transfer" type="checkbox">
                    <label for="lock_order_transfer">Блокировать передачу заказа</label>
                    <div class="checkbox_bg"></div>
                </div>
            </div>

            <ul class="leftmenu_buttons dark">
                <li><?php echo CHtml::link('<img src="/images/leftmenu-4.png" width="50" height="50" alt=""><span>'.($model->isNewRecord ? 'Создать' : 'Сохранить').'</span>', 'javascript:void(0);', array('id'=>'restaurant_save'));?></li>
                <?php
                if (!$model->isNewRecord){
                    ?>
                    <li><?php echo CHtml::link('<img src="/images/leftmenu-2'.($model->deleted ? '_act' : '').'.png" width="50" height="50" alt=""><span>'.($model->deleted ? 'Восстановить' : 'Удалить').'</span>', 'javascript:void(0);', array('id' => 'restaurant_set_deleted', 'class' => $model->deleted ? 'active' : ''));?></li>
                    <li><?php echo CHtml::link('<img src="/images/leftmenu-6'.($model->hidden ? '_act' : '').'.png" width="50" height="50" alt=""><span>'.($model->hidden ? 'Включить' : 'Отключить').'</span>', 'javascript:void(0);', array('id' => 'restaurant_set_hidden', 'class' => $model->hidden ? 'active' : ''));?></li>
                <?php
                }
                ?>
            </ul>
        </div>
    <?php $this->endWidget(); ?>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/restaurant_ready.js"></script>
<?php $this->renderPartial('/partials/_load_img', array('folder' => '/data/images/restaurants', 'selector' => 'Restaurant_photos')); ?>