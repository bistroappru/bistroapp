<?php

class CompanyController extends Controller
{
    public $layout='admin_company';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('inAdmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        if(!Yii::app()->user->checkAccess('companyCreate')){
            throw new CHttpException(403, 'Действие недоступно для вашей учетной записи');
        }

        $ajax = Yii::app()->request->isAjaxRequest;
		$model=new Company;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Company']))
		{
			$model->attributes=$_POST['Company'];
            if($model->save()){

                $user = array(
                    'username' => $model->prefix .'_owner',
                    'password' => '123',
                    'email' =>  $model->prefix .'_owner@bistroapp.ru',
                    'fullname' => 'Владелец компании',
                    'role' => 'owner',
                    'blocked' => 0,
                    'company_id' => $model->id
                );

                Company::createUser($user);

                $this->redirect(array('update','id'=>$model->id));
            }
		}

        if ($ajax){
            $this->renderPartial('create',array(
                'model'=>$model,
            ));
        }else{
            $this->render('create',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        if(!Yii::app()->user->checkAccess('companyView', array('company_id'=>$id))){
            throw new CHttpException(403, 'Вы не можете просматривать эту компанию');
        }

        $ajax = Yii::app()->request->isAjaxRequest;
        $model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 //$this->performAjaxValidation($model);

		if(isset($_POST['Company']))
		{
            if(!Yii::app()->user->checkAccess('companyUpdate', array('company_id'=>$model->id))){
                throw new CHttpException(403, 'Вы не можете редактировать эту компанию');
            }
			$model->attributes=$_POST['Company'];
			$model->save();
		}

        if ($ajax){
            $this->renderPartial('update',array(
                'model'=>$model
            ));
        }else{
            $this->render('update',array(
                'model'=>$model
            ));
        }
	}

    public function actionGetrestaurants(){
        $restaurants = AbcHelper::allCompanyRestaurants(Yii::app()->request->getParam('company_id',0));
        $empty = AbcHelper::allCompanyRestaurants(Yii::app()->request->getParam('empty',0));
        $this->renderPartial('_getrestaurants',array('restaurants'=>$restaurants,'empty'=>$empty));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        if (!Yii::app()->user->checkAccess('companyDelete')){
            throw new CHttpException(403, 'Действие недоступно для вашей учетной записи');
        }

		//$this->loadModel($id)->delete();
        $company = $this->loadModel($id);
        $company->setAttribute('deleted', 1);
        $company->save();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->render('index',array());
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Company the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
        $model=Company::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Company $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='company-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
