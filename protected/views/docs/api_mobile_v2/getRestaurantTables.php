<h3>Получение столиков ресторана</h3>

<p>
    <b><em>Метод</em></b>: POST<br/>
    <b><em>URL запроса</em></b>: http://bistroapp.ru/api/v2/getRestaurantTables<br/>
</p>
<hr/>

<h4>Параметры запроса:</h4>
<p>
    <strong><em>restaurant_id</em></strong>: ID ресторана<b>(обязательный)</b><br/>
    <strong><em>version</em></strong>: номер последней версии полученной приложением <b>(необязательный, по умолчанию вернет всё)</b><br/>
</p>

<h4>Пример тела запроса:</h4>
<pre><code class="prettyprint">
    {
        "device": {
        "uuid": "550e8400-e29b-41d4-a716-446655440000",
        "platform": "iOS",
        "name": "iPhone 5",
        "version": "7.1"
        }
    }
    </code></pre>
<hr/>

<h4>Пример ответа (OK):</h4>
<pre><code class="prettyprint">
    {
        "status": "ok",
        "data": [
        {
            "id": "28",
            "name": "2",
            "restaurant_id": "45",
            "hidden": "0",
            "deleted": "0"
        },
        {
            "id": "29",
            "name": "3",
            "restaurant_id": "45",
            "hidden": "0",
            "deleted": "0"
        }
        ],
        "version": "1"
    }
    </code></pre>
<hr/>