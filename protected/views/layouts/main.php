<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.jscrollpane.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.backstretch.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.placeholder.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.selectBox.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body class="homepage">
<div id="wrapper">
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery(window).resize(function(){
                $('#home_header').backstretch("<?php echo Yii::app()->request->baseUrl; ?>/images/header.jpg");
            });
            $('#home_header').backstretch("<?php echo Yii::app()->request->baseUrl; ?>/images/header.jpg");
        });
    </script>
    <?php echo $content; ?>
</div>
</body>
</html>
