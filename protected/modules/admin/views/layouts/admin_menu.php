<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_main'); ?>

<div id="page">
    <div class="column_left">
        <?php $this->widget('application.components.CompanyMenu', array('company_id' => Yii::app()->user->company_id_full, 'menu_id'=>Yii::app()->request->getParam('id', 0), 'filter_key' => 'lc_menu')); ?>
    </div>
    <div id="content_wrapper">
        <?php echo $content; ?>
    </div>
    <div class="clear"></div>
</div>

<?php $this->endContent(); ?>