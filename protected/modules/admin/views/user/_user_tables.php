<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 29.10.14
 * Time: 17:43
 */
?>

<?php
if (is_array($model->restaurant->tables)){
?>
    <div id="user_tables_wrap" class="product_row input_row" style="display: <?= ($model->role == User::ROLE_GARCON ? 'block' : 'none')?>;">
        <label class="product_label_big">Столики</label>
        <div class="inp_wrap">
            <div id="user_table_list">
                <?php
                foreach ($model->restaurant->tables as $v){
                ?>

                    <div class="table_inline" data-id="<?= $v->id;?>" data-active="<?= (in_array($v->id, $model->getTableList()) ? 'yes' : 'no')?>">
                        <span class="name"><?= $v->name;?> <small>(id=<?= $v->id;?>)</small></span>
                        <span class="act"><i class="fa fa-check"></i></span>
                        <span class="del"><i class="fa fa-remove"></i></span>
                        <span class="add"><i class="fa fa-plus"></i></span>
                    </div>

                <?php
                }
                ?>
            </div>
            <textarea id="User_tables" name="User[tables]" style="display: none;"></textarea>
        </div>
    </div>
    <script>
        updateUserTables();
    </script>
<?php
}else{
?>
    <div id="user_tables_wrap" class="product_row input_row"></div>
<?php
}
?>