/**
 * Created by TWT on 01.09.14.
 */
$(document).ready(function(){
    var kladr_token = '51dfe5d42fb2b43e3300006e';
    var kladr_key = '86a2c2a06f1b2451a87d05512cc2c3edfdf41969';

    var map_json = JSON.parse($('#Restaurant_map').val() ? $('#Restaurant_map').val() : '{"zoom":10, "coords":{"x":55.76,"y":37.64}, "place":{"city":"7700000000000","street":null,"building":null}}');

    var kladr_city = $('#kladr_city');
    var kladr_street = $('#kladr_street');
    var kladr_building = $('#kladr_building');

    var map = null;
    var placemark = null;
    var map_created = false;

    // Формирует подписи в autocomplete
    var Label = function(obj, query ){
        var label = '';

        var name = obj.name.toLowerCase();
        query = query.toLowerCase();

        var start = name.indexOf(query);
        start = start > 0 ? start : 0;

        if(obj.typeShort){
            label += '<span class="ac-s2">' + obj.typeShort + '. ' + '</span>';
        }

        if(query.length < obj.name.length){
            label += '<span class="ac-s2">' + obj.name.substr(0, start) + '</span>';
            label += '<span class="ac-s">' + obj.name.substr(start, query.length) + '</span>';
            label += '<span class="ac-s2">' + obj.name.substr(start+query.length, obj.name.length-query.length-start) + '</span>';
        } else {
            label += '<span class="ac-s">' + obj.name + '</span>';
        }

        if(obj.parents){
            for(var k = obj.parents.length-1; k>-1; k--){
                var parent = obj.parents[k];
                if(parent.name){
                    if(label) label += '<span class="ac-st">, </span>';
                    label += '<span class="ac-st">' + parent.name + ' ' + parent.typeShort + '.</span>';
                }
            }
        }
        return label;
    };

    kladr_street.kladr({
        token: kladr_token,
        key: kladr_key,
        type: $.ui.kladrObjectType.STREET,
        label: Label,
        verify: true,
        parentType: $.ui.kladrObjectType.CITY,
        parentId: map_json.place.city,
        select: function( event, ui ) {
            kladr_street.val(ui.item.obj.name).data("kladr-obj", ui.item.obj);
            kladr_street.removeClass('error').closest('.inp_wrap').find('.errorMessage').remove();
            map_json.place.street = ui.item.obj;

            map_json.place.building = null;
            kladr_building.val('').data("kladr-obj", null);
            kladr_building.kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: ui.item.obj.id});
            kladr_building.removeClass('error').closest('.inp_wrap').find('.errorMessage').remove();

            MapUpdate();
        }
    });

    // Подключение плагина для поля ввода номера дома
    kladr_building.kladr({
        token: kladr_token,
        key: kladr_key,
        type: $.ui.kladrObjectType.BUILDING,
        label: Label,
        verify: true,
        select: function(event, ui) {
            map_json.place.building = ui.item.obj;

            kladr_building.data("kladr-obj", ui.item.obj);
            kladr_building.removeClass('error').closest('.inp_wrap').find('.errorMessage').remove();

            MapUpdate();
        }
    });

    if (map_json.place.street){
        kladr_street.val(map_json.place.street.name);
        kladr_street.data("kladr-obj", map_json.place.street);
        kladr_building.kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: map_json.place.street.id});
    }

    if (map_json.place.building){
        kladr_building.val(map_json.place.building.name);
        kladr_building.data("kladr-obj", map_json.place.building);
    }

    // Проверка корректности названия улицы (если пользователь ввёл сам, а не выбрал в списке)
    kladr_street.change(function(){
        kladr_building.val('').data("kladr-obj", null);
        kladr_building.removeClass('error').closest('.inp_wrap').find('.errorMessage').remove();
        kladr_building.kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: null});
        map_json.place.building = null;

        var query = {
            token: kladr_token,
            key: kladr_key,
            value: kladr_street.val(),
            type: $.ui.kladrObjectType.STREET
        };

        if (kladr_city.val()){
            query['parentType'] = $.ui.kladrObjectType.CITY;
            query['parentId'] = kladr_city.val();
        }

        $.kladrCheck(query, function(obj){
            if(obj && (obj.name == kladr_street.val())){
                kladr_street.val(obj.name).data("kladr-obj", obj);
                kladr_street.removeClass('error').closest('.inp_wrap').find('.errorMessage').remove();
                map_json.place.street = obj;
                kladr_building.kladr('option', {parentType: $.ui.kladrObjectType.STREET, parentId: obj.id});
            }else{
                map_json.place.street = null;
                kladr_street.data("kladr-obj", null);
                kladr_street.addClass('error').after('<div class="errorMessage">Поле содержит некорректные данные</div>');
            }
            MapUpdate();
        });
    });

    // Проверка названия строения
    kladr_building.change(function(){
        var query = {
            token: kladr_token,
            key: kladr_key,
            value: kladr_building.val(),
            type: $.ui.kladrObjectType.BUILDING
        };

        var streetObj = kladr_street.data("kladr-obj");
        if(streetObj){
            query['parentType'] = $.ui.kladrObjectType.STREET;
            query['parentId'] = streetObj.id;
        }

        $.kladrCheck(query, function(obj){
            if(obj && (obj.name == kladr_building.val())){
                kladr_building.val(obj.name);
                kladr_building.data( "kladr-obj", obj);
                kladr_building.removeClass('error').closest('.inp_wrap').find('.errorMessage').remove();
                map_json.place.building = obj;
            }else{
                kladr_building.data("kladr-obj", null);
                kladr_building.addClass('error').after('<div class="errorMessage">Поле содержит некорректные данные</div>');
                map_json.place.building = null;
            }
            MapUpdate();
        });
    });


    var setCoords = function(pos){
        console.log('setCoords');
        map_json.coords.x = pos[0];
        map_json.coords.y = pos[1];
        $('#Restaurant_map').val(JSON.stringify(map_json));
    }

    var getAddress = function(){
        var address = '';

        if(kladr_city.val()){
            if(address) address += ', ';
            address += 'г. ' + kladr_city.find('option:selected').html();
        }

        var streetObj = kladr_street.data("kladr-obj");
        if(streetObj && streetObj.id){
            if(address) address += ', ';
            address += streetObj.typeShort + '. ' + streetObj.name;
        }

        var buildingObj = kladr_building.data("kladr-obj");
        if(buildingObj && buildingObj.id){
            if (address) address += ', ';
            address += buildingObj.typeShort + '. ' + buildingObj.name;
        }

        return address;
    }

    // Обновляет карту
    var MapUpdate = function(){
        console.log('MapUpdate');
        map_json.zoom = 11;
        var address = getAddress();

        if(address && map_created){
            var geocode = ymaps.geocode(address);
            geocode.then(function(res){
                map.geoObjects.each(function (geoObject) {
                    map.geoObjects.remove(geoObject);
                });
                var position = res.geoObjects.get(0).geometry.getCoordinates();
                $('#Restaurant_location').val(address);
                $('.adress_box .rightbox_text').html(address);
                MapDraw(position[0], position[1]);
            });
        }
    }

    var MapDraw = function(x, y){
        console.log('MapDraw');
        map.geoObjects.each(function (geoObject) {
            map.geoObjects.remove(geoObject);
        });
        var position = [x,y];
        placemark = new ymaps.Placemark(position, {}, {});
        map.geoObjects.add(placemark);
        map.setCenter(position, map_json.zoom);
        setCoords(position);
    }

    //запуск карты при загрузке
    ymaps.ready(function(){
        if(map_created) return;
        map_created = true;
        map = new ymaps.Map('map', {
            center: [map_json.coords.x, map_json.coords.y],
            zoom: map_json.zoom
        });

        map.controls.add('smallZoomControl', { top: 5, left: 5 });
        map.events.add('boundschange', function (e) {
            if (e.get('newZoom') != e.get('oldZoom')){
                map_json.zoom = e.get('newZoom');
                $('#Restaurant_map').val(JSON.stringify(map_json));
            }
        });

        MapDraw(map_json.coords.x, map_json.coords.y);
    });

    $(".phone_list input").inputmask("+7 (999) 999-99-99");

    updateRestaurantTables();

    if ($('.adress_box .errorMessage').length){
        $('.adress_box .box_addit').show();
        $('.adress_box .box_link').addClass('opened');
    }
});


