<div class="lc_filter" data-key="<?=$key?>">
    <span class="lc_filter_icon"><i class="fa fa-bars"></i> фильтр</span>
    <div class="lc_filter_popup">
        <p>Показывать:</p>
        <span data-type="blocked">заблокированные</span>
        <span data-type="deleted">удаленные</span>
    </div>
</div>

<script>
    var filter_val,
        filter_data = '<?=Yii::app()->request->cookies[$key]->value?>';

    filter_data = filter_data.split('||');

    $.each(filter_data, function(i, val){
        if ($('.lc_filter[data-key=<?=$key?>] .lc_filter_popup span[data-type=' + val + ']').length){
            $('.lc_filter[data-key=<?=$key?>] .lc_filter_popup span[data-type=' + val + ']').addClass('active');
        }
    });

    $('.lc_filter[data-key=<?=$key?>]').on('click', '.lc_filter_icon', function(){
        $(this).next().toggle();
    });

    $('.lc_filter[data-key=<?=$key?>]').on('click', '.lc_filter_popup span', function(){
        $(this).toggleClass('active');

        filter_val = '';
        $('.lc_filter[data-key=<?=$key?>] .lc_filter_popup span.active').each(function(){
            filter_val += '||' + $(this).attr('data-type');
        });

        $.cookie('<?=$key?>', filter_val.substr(2), {expires: 7, path: '/'});

        updateRestaurantUserList(<?=$restaurant_id?>);
    });
</script>