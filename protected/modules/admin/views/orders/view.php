<?php
/* @var $this OrdersController */
/* @var $model Order */
$this->pageTitle = Yii::app()->name . ' - Информация по заказу';
?>
<script>
    params.orderid = <?=$model->id?>;
</script>

<table class="order_single_table">
    <tr>
        <th style="width:38%;">Информация по заказу <b>№<?php echo $model->id;?></b></th>
        <th>
            <div class="pull_left">Состав заказа</div>
            <div class="pull_right">
                <a href="#" class="ord-single_close"></a>
                <a href="#" data-id="<?=$model->id;?>" class="ord-single_del"></a>
                <a href="#" onclick="window.print(); return false;" class="ord-single_print"></a>
            </div>
        </th>
    </tr>
    <tr>
        <td style="vertical-align:top; width:38%;">
            <div>
                <div class="order_def">
                    <div class="dt"><div class="cell">ID</div></div>
                    <div class="dd"><div class="cell"><b><?php echo CHtml::encode($model->id);?></b></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell"><b>СТОЛИК</b></div></div>
                    <div class="dd"><div class="cell"><b><?php echo CHtml::encode($model->table->name);?></b></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell"><b>СТАТУС</b></div></div>
                    <div class="dd"><div class="cell"><b><?php echo CHtml::encode($model->status->name);?></b></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell"><b>ОПЛАТА</b></div></div>
                    <div class="dd"><div class="cell"><b><?php echo CHtml::encode($model->payment->name);?></b></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell">Дата создания</div></div>
                    <div class="dd"><div class="cell"><b><?php echo date('H:i:s', $model->date_create);?></b> &nbsp;&nbsp;<span class="small"><?php echo date('d.m.Y', $model->date_create);?></span></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell">Дата обработки</div></div>
                    <div class="dd">
                        <div class="cell">
                            <?php
                            if($model->date_execute){
                            ?>
                                <b><?php echo date('H:i:s', $model->date_execute);?></b> &nbsp;&nbsp;<span class="small"><?php echo date('d.m.Y', $model->date_execute);?></span>
                            <?php
                            }else{
                            ?>
                                <b>&mdash;</b>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell">Дата закрытия</div></div>
                    <div class="dd">
                        <div class="cell">
                            <?php
                            if($model->date_close){
                                ?>
                                <b><?php echo date('H:i:s', $model->date_close);?></b> &nbsp;&nbsp;<span class="small"><?php echo date('d.m.Y', $model->date_close);?></span>
                            <?php
                            }else{
                                ?>
                                <b>&mdash;</b>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell">Внимание официанта</div></div>
                    <div class="dd"><div class="cell"><?php echo CHtml::encode($model->attention ? 'Да' : 'Нет');?></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell">Раздельный заказ</div></div>
                    <div class="dd"><div class="cell"><?php echo CHtml::encode($model->separated ? 'Да' : 'Нет');?></div></div>
                    <span class="def_bg"></span>
                </div>
                <div class="order_def">
                    <div class="dt"><div class="cell">Информация о раздельном заказе</div></div>
                    <div class="dd"><div class="cell"><?php echo CHtml::encode($model->separated_info);?></div></div>
                    <span class="def_bg"></span>
                </div>
            </div>
        </td>
        <td style="vertical-align:top;">
            <div class="food_scroll">
                <div class="scroll-pane">
                    <?php
                    foreach($model->items as $one){
                    ?>
                        <ul class="food_item">
                            <li class="food_name">
                                <div class="pad">
                                    <div class="food_thumb"><img src="<?php echo $one->position->image;?>" width="50" height="50"></div>
                                    <div class="food_entry">
                                        <p><?php echo $one->position->name;?></p>
                                        <p><?php echo $one->position->measure;?></p>
                                    </div>
                                </div>
                            </li>
                            <li class="food_price"><div class="pad"><?php echo $one->price;?> руб.</div></li>
                            <li class="food_count"><div class="pad">х<?php echo $one->count;?></div></li>
                            <li class="food_action">
                                <div class="pad">
                                    <?php if($one->status->prev_id){ ?>
                                        <a item-id="<?=$one->id?>" data-id="<?=$one->status->prev_id?>" class="oif_set_status food_cancel-white" href="javascript:void(0);">
                                            <span class="icon"></span>
                                            Отменить
                                        </a>
                                    <?php } ?>

                                    <?php if($one->status->next_id){ ?>
                                        <a item-id="<?=$one->id?>" data-id="<?=$one->status->next_id?>" class="oif_set_status status_<?=$one->status->next_id?>" href="javascript:void(0);">
                                            <span class="icon"></span>
                                            <?=OrderStatus::getParamById($one->status->next_id, 'name')?>
                                        </a>
                                    <?php } ?>
                                    <a item-id="<?=$one->id?>" class="oif_delete_item food_delete-white" href="javascript:void(0);"><span class="icon"></span>Удалить</a>
                                </div>
                            </li>
                        </ul>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </td>
    </tr>
    <tr class="total_row">
        <td>
            <div class="order_def">
                <div class="dt"><div class="cell">Комментарий к заказу</div></div>
                <div class="dd"><div class="cell"><?php echo CHtml::encode($model->comment);?></div></div>
                <span class="def_bg"></span>
            </div>
        </td>
        <td>
            <ul class="food_total">
                <li class="total_text">ИТОГО</li>
                <li class="total_price"><?php echo $model->getTotalPrice(); ?> <span class="small">руб.</span></li>
            </ul>
        </td>
    </tr>
</table>