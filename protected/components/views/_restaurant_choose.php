<?php
/**
 * Created by PhpStorm.
 * User: TWT
 * Date: 04.06.14
 * Time: 22:24
 */
?>

<div class="choose_restaurant">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'restaurant-choose-form',
        'enableAjaxValidation'=>false,
    )); ?>
    <?php if($company){ ?>
    <label for="cr_company">Компания:</label>
    <?php echo CHtml::hiddenField('cr_choose', 1); ?>
        <?php echo CHtml::dropDownList('company_id', Yii::app()->user->company_id_full, AbcHelper::allCompanies(Yii::app()->user->company_id),
            array(
                'id'=>'cr_company',
                'ajax' => array(
                    'type'=>'POST',
                    'url'=>CHtml::normalizeUrl('/admin/company/getrestaurants'),
                    'update'=>'#cr_restaurant',
                )));
        ?>
    <?php }?>

    <?php if($restaurant){ ?>
    <label for="cr_restaurant">Ресторан:</label>
    <?php echo CHtml::dropDownList('restaurant_id', Yii::app()->user->restaurant_id_full, AbcHelper::allCompanyRestaurants(Yii::app()->user->company_id_full), array('id'=>'cr_restaurant')); ?>
    <?php }?>
    <?php echo CHtml::submitButton('Выбрать'); ?>
    <?php $this->endWidget(); ?>
</div>